/* 
 * @(#) TeamBean.java    0.1 2008/03/22
 *
 * A Bean to provide interaction with the Teams table.
 * Copyright (C) 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Bean to allow interaction with the Teams table.
 *
 * @version       0.1    22 March 2008
 * @author        Steven Lilley
 */
public class TeamBean extends GenericBean {
    
    private String teamCode = "";
    private String teamName = "";
    private int listOrder = 0;

    public TeamBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("TeamBean constructor", nex);
        }
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    public String getEditForm() {
        return "";
    }
    
    public String getItem() {
        return "";
    }
    
    public String getResponse() {
        return "<p>Not written.</p>";
    }
    
    public String getSearchForm() {
        return "";
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getStandard() {
        return "";
    }
    
    protected boolean validateData() {
        return true;
    }
    
    protected boolean writeToDatabase() {
        return false;
    }
}

