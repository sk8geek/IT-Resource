<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Messages</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>
<div class="content">
<h2>Messages</h2>
<jsp:useBean id="msgBean" scope="request" class="uk.co.channele.itres.MessageBean" />
<jsp:setProperty name="msgBean" property="*" />
<jsp:getProperty name="msgBean" property="msgList" />

<h3>New Message</h3>
<form method="post" name="newMessage" action="checkout">
<p>Recipient: <jsp:getProperty name="userIdentity" property="registeredUserSelectControl" /></p>

<p>Message:<br>
<textarea name="msgText" cols="40" rows="4" wrap="virtual" onMouseOver="this.focus();">
</textarea></p>

<p><input type="checkbox" name="secretMsg" value="secret"> User must log in to retrieve message.</p>
<p><input type="submit" value="Send" class="button">
</form>  

</div>
</body>
</html>

