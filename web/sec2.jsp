<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2010 Calderdale MBC -->
<html>
<head>
<title>LS-DB</title>
<%@ include file="head.inc" %>
<link rel="stylesheet" type="text/css" href="secform.css" />
</head>
<body>
<jsp:useBean id="formBean" scope="request" class="uk.co.channele.itres.FormBean" />
<p class="secFormHeader">SEC2</p>

<h1 class="secForm">CMBC - AUTHORISATION FOR ACCESS TO INTERNET</h1>

<p class="secForm">A.  Completed form to be forwarded to Mulcture House -  Principal Software Officer.<br />
B.  Officers name and telephone number for further information: <span class="userData">
<jsp:getProperty name="formBean" property="contact" /></span><br />
C.  Date required by: <span class="userData">
<jsp:getProperty name="formBean" property="targetDate" /></span></p>

<table class="secForm"><thead>
<tr><th>DIRECTORATE/SERVICE</th><th class="userData">Safer &amp; Stronger Communities</th></tr>
<tr><th>SECTION(S)</th><th class="userData">
<jsp:getProperty name="formBean" property="section" /></th></tr>
<tr class="secFormSubTitle"><th>Create/Delete</th><th>Name and LID</th></tr></thead><tbody>
<jsp:getProperty name="formBean" property="transactionRows" />
</tbody></table>

<h2 class="secForm">SECTION 2  - CERTIFICATION OF AUTHORISING OFFICER FOR SERVICES</h2>
<p class="secForm">I authorise the Head of IT to implement the above.</p>
<br /><br />
<p class="secForm">Signed............................................................................  Date ....................</p>
<h2 class="secForm">FOR SYSTEM ADMINISTRATION USE ONLY (I.T.SECTION)</h2>
<br /><br />
<p class="secForm">Actioned by.......................................................................  Date ....................</p>
</body>
</html>
