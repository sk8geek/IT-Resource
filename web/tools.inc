<div id="tools">

<div class="function">
<p style="margin-left:0"><b>Links</b></p>
<ul style="list-style:none; margin-left:0.5ex">
<li><a href="http://connect/" title="Intranet">Intranet</a></li>
<li><a href="http://www.calderdale.gov.uk/" title="Calderdale Website">Website</a> | 
<a href="http://192.168.100.106" title="Website, internal, may be out of date!">[internal]</a></li>
<li><a href="https://cmbchb1.cmbc.internal/owa" title="Outlook Web Access">Email</a></li>
<li><a href="http://stansfield/forms/frmservlet?config=FN" title="Corp Fins">Financials</a> | 
<a href="http://stansfield/forms/frmservlet?config=FNjre" title="Corp Fins, Java Runtime version">[JRE]</a></li>
<li><a href="http://gorple:9992/Rhythmyx/sys_cx/mainpage.html" title="Content Management System">CMS</a></li>
<li><a href="http://ls-db/cmsnotes/" title="CMS User Notes">CMS Notes</a></li>
<li><a href="http://intranet/docs/main.html" title="FOI Awareness Learning"><abbr title="Freedom of Information">FOI</abbr> Awareness</a></li> 
<li><a href="http://ls-db/whosmysuper.jsp" title="Find your Corp Fins supervisor">Who's My Supervisor</a></li>
</ul>
</div>

<div class="function">
<jsp:useBean id="faqs" scope="request" class="uk.co.channele.itres.FAQBean" />
<p style="margin-left:0"><b><acronym title="Frequently Asked Questions">FAQ</acronym>s</b> &nbsp; 
<a class="button" href="faqs.jsp">Show all questions</a></p>
<jsp:getProperty name="faqs" property="categoryList" />
</div>

</div>

