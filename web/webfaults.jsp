<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Web Fault</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="viewingPage" value="webfaults.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<jsp:useBean id="wwwFaultList" scope="page" class="uk.co.channele.itres.DropDownBean" />
<jsp:setProperty name="wwwFaultList" property="tableName" value="webfaults" />
<jsp:setProperty name="wwwFaultList" property="controlName" value="webFault" />
<h2>Report a Web Fault</h2>

<p>Please select the fault type or choose other and enter a description.</p>
<form method="post" action="recordwebfault.jsp">
<p>Fault Type: <jsp:getProperty name="wwwFaultList" property="selectControl" /></p>
<p>Other Fault:<br>
<textarea name="other" cols="40" rows="4" wrap="virtual">
</textarea></p>
<p>
<input type="hidden" name="remoteHost" value="<%= request.getRemoteHost() %>">
<input type="submit" value="Save" class="button"></p>
</form>
</div>

<%@ include file="tools.inc" %>
</body>
</html>

