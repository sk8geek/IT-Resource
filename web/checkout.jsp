<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Check Out</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>
<div class="content">
<h2>Check Out</h2>

<form method="post" name="checkout" action="checkout">
<p>When will you be back? 
<jsp:useBean id="returning" scope="request" class="uk.co.channele.itres.DropDownBean" />
<jsp:getProperty name="returning" property="returningSelectControl" /></p>

<p>Where are you going? 
<input type="text" name="destination" maxlength="40" size="30" onMouseOver="this.focus();")></p>

<p><input type="submit" value="Check Out" class="button">
</form>  

</div>
</body>
</html>

