<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Administration</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="admin.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Administration</h2>

<p>
<a href="teams">Teams</a><br>
<a href="sites">Sites</a>
</p>

<jsp:useBean id="admin" scope="session" class="uk.co.channele.itres.AdminBean" />
<jsp:setProperty name="admin" property="*" />
<jsp:getProperty name="admin" property="response" />

</div>

<%@ include file="tools.inc" %>
</body>
</html>

