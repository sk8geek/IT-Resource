<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2008 Steven Lilley -->
<html>
<head>
<title>LS-DB: Sites</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>

<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<div class="menu">
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Sites</h2>
<jsp:useBean id="siteBean" scope="session" class="uk.co.channele.itres.SiteBean" />
<jsp:setProperty name="siteBean" property="*" />
<jsp:getProperty name="siteBean" property="response" />
</div>

<jsp:getProperty name="siteBean" property="debug"/>
<%@ include file="tools.inc" %>
</body>
</html>

