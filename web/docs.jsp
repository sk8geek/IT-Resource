<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Documents</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="docs" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<jsp:useBean id="docs" scope="application" class="uk.co.channele.itres.DocumentBean" />
<jsp:setProperty name="docs" property="staticURL" value="/static/" />
<jsp:setProperty name="docs" property="staticPath" value="/home/tomcat/sscdb/static/" />
<jsp:setProperty name="docs" property="documentPath" value="docs/" />
<jsp:setProperty name="docs" property="itDocumentPath" value="itdocs/" />
<jsp:setProperty name="docs" property="itMinutesPath" value="itminutes/" />

<div class="content">
<h2>Documents</h2>
<h3><abbr title="Community Services IT Sterring Group">CITSTEG</abbr> Minutes</h3>
<p>Our IT Steering Group meets every six weeks to coordinate IT issues.</p>
<form method="get" name="itminutes" onSubmit="return getMinutes();">
<jsp:getProperty name="docs" property="itMinutesDropDown" />
</form>

<h3>Library</h3>
<p><form name="adddoc" action="docs" method="post" enctype="multipart/form-data">
File: <input type="file" name="addfile" size="30">
<input type="submit" name="submit" value="submit">
</form>

<h3>General IT</h3>
<jsp:getProperty name="docs" property="documentList" />

<h3>Technical</h3>
<jsp:getProperty name="docs" property="technicalDocumentList" />
</div>

<%@ include file="tools.inc" %>
<jsp:setProperty name="userIdentity" property="currentPage" value="docs.jsp" />
</body>
</html>

