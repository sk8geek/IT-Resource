<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Web Fault - Thank you</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Web Fault - Thank you</h2>
<jsp:useBean id="webFault" scope="page" class="uk.co.channele.itres.WebFaultBean" />
<jsp:setProperty name="webFault" property="*" />
<jsp:getProperty name="webFault" property="recordWebFault" />

<p style="font-size: smaller;"><jsp:getProperty name="userIdentity" property="currentPageLink" /></p>
</div>

</body>
</html>

