<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="search.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>
<div class="fullwidth">

<h2>Search</h2>
<jsp:useBean id="search" scope="request" class="uk.co.channele.itres.SearchBean" />
<jsp:setProperty name="search" property="*" />
<jsp:getProperty name="search" property="response" />
</div>

</body>
</html>

