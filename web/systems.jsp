<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Systems</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="systems.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="systems" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<jsp:useBean id="systems" scope="request" class="uk.co.channele.itres.SystemsStatusBean" />
<h2>Systems Monitor</h2>
<jsp:setProperty name="systems" property="*" />
<jsp:getProperty name="systems" property="response" />

<h2>Internet Access Faults</h2>
<p>Record problems accessing the internet here.<br />
<a class="button" href="webfaults.jsp">Record a fault</a></p>
<p><jsp:getProperty name="systemMonitor" property="webFaultSummary" /></p>


</div>

<%@ include file="tools.inc" %>
</body>
</html>

