<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Inventory</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="inventory.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="inventory" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Inventory</h2>
<jsp:useBean id="inventoryBean" scope="session" class="uk.co.channele.itres.InventoryBean" />
<jsp:setProperty name="inventoryBean" property="*" />
<jsp:getProperty name="inventoryBean" property="response" />
</div>

<%@ include file="tools.inc" %>
</body>
</html>
`
