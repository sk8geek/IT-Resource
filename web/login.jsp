<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Log In</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<%@ include file="title.inc" %>

<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<div class="menu">
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<h2>Log In</h2>
<jsp:setProperty name="userIdentity" property="*" />
<jsp:getProperty name="userIdentity" property="loginForm" />
</div>

<%@ include file="tools.inc" %>
</body>
</html>

