<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Feedback</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Feedback</h2>

<p>Please enter your comments.</p>

<form method="post" action="recordfeedback.jsp" name="feedbackForm">
<p>Please rate your comment: 
<select name="rating">
<option value="4">Very positive</option>
<option value="3">Positive</option>
<option value="2" selected>Neutral</option>
<option value="1">Negative</option>
<option value="0">Very negative</option>
</select></p>

<p>Comment/Feedback:<br>
<textarea name="comment" cols="40" rows="4" wrap="virtual" onMouseOver="this.focus();">
</textarea></p>

<p>If you require a response please leave your name: 
<input type="text" name="respondant" size="16" maxlength="40" onMouseOver="this.focus();"></p>

<p><jsp:getProperty name="userIdentity" property="viewingPageControl" />
<input type="hidden" name="remoteHost" value="<%= request.getRemoteHost() %>">
<input type="submit" value="Save" class="button"></p>
</form>
</div>

<%@ include file="tools.inc" %>
</body>
</html>

