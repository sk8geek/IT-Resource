<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Notice Editor</title>
<%@ include file="head.inc" %>
<script type="text/javascript">
<!-- 
function dropDownChanged(control, value) {
    alert("control=" + control + " and value=" + value);
    if (document.notice == null) {
        return;
    }
    if (control == "team") {
        if (value != "") {
            if (document.notice.audience != null) {
                document.notice.audience[2].click();
            }
            if (document.notice.office != null) {
                document.notice.office.value = "";
            }
        } else {
            if (document.notice.audience != null) {
                document.notice.audience[0].click();
            }
        }
    }
    if (control == "office") {
        if (value != "") {
            if (document.notice.audience != null) {
                document.notice.audience[1].click();
            }
            if (document.notice.team != null) {
                document.notice.team.value = "";
            }
        } else {
            if (document.notice.audience != null) {
                document.notice.audience[0].click();
            }
        }
    }
}
-->
</script>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="notices" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="notices" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Notice Editor</h2>
<jsp:useBean id="newsBean" scope="request" class="uk.co.channele.itres.NewsBean" />
<jsp:setProperty name="newsBean" property="*" />
<jsp:getProperty name="newsBean" property="response" />
</div>

<%@ include file="tools.inc" %>
</body>
</html>

