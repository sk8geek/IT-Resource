<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Tasks</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="tasks.jsp" />
<%@ include file="title.inc" %>
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="tasks" />
<div class="menu">
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Tasks</h2>
<p>Content for this page is not yet available.</p>
<p>It is currently being used to test the document library.</p>
<jsp:useBean id="library" scope="request" class="uk.co.channele.itres.LibraryBean" />
<jsp:getProperty name="library" property="response" />

</div>

<%@ include file="controls.inc" %>
</body>
</html>

