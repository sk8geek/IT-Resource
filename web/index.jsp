<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2008 Steven Lilley -->
<html>
<head>
<title>LS-DB</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="index.jsp" />
<%@ include file="title.inc" %>

<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="index" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<jsp:getProperty name="userIdentity" property="welcome" />
<h2>Notices</h2>
<jsp:useBean id="noticeBean" scope="request" class="uk.co.channele.itres.NoticeBean" />
<jsp:getProperty name="noticeBean" property="response" />

<div class="function">
<h2>e-Register</h2>
<jsp:getProperty name="userIdentity" property="userPresenceSummary" />
<p class="button"><a class="button" href="presence.jsp" title="User Presence List">User Presence List</a></p>
</div>

<jsp:getProperty name="userIdentity" property="debug" />

</div>
<%@ include file="tools.inc" %>
</body>
</html>

