<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: Users</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="tasks.jsp" />
<%@ include file="title.inc" %>
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="tasks" />
<div class="menu">
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>Users</h2>
<jsp:useBean id="userBean" scope="request" class="uk.co.channele.itres.RegisterBean" />
<jsp:setProperty name="userBean" property="*" />
<jsp:getProperty name="userBean" property="response" />
</div>

<%@ include file="controls.inc" %>
</body>
</html>

