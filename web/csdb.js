// Copyright (c) 2005 Steven J Lilley
// www.channel-e.co.uk

function loginFieldFocus() {
    if (document.login.userName != null) {
        var uname = new String(document.login.userName.value);
        if (uname.length > 0) {
            document.login.currentPassword.focus();
        } else {
            document.login.userName.focus();
        }
	}
}

function validateNews() {
	var valid = true;
	// check item lifespan is not zero
	var liveDays = document.storyeditor.livedays.selectedIndex;
	var liveHours = document.storyeditor.livehours.selectedIndex;
	if (liveDays == 0 && liveHours == 0) {
		valid = false;
		alert("The display period can not be zero.");
	}
	return valid;
}

function getMinutes() {
    window.location.href = document.itminutes.filename.value;
    return false;
}

function setAudience(aud) {
    if (aud == "all") {
        document.notice.team.value = "";
        document.notice.office.value = "";
    } else 
    if (aud == "team") {
        document.notice.office.value = "";
    } else
    if (aud == "office") {
        document.notice.team.value = "";
    }
}

function setEntryBoxes() {
    var allWeek = document.getElementById("weekAll").checked;
    if (allWeek) {
        document.getElementById("monPM").disabled = true;
        document.getElementById("tuesAM").disabled = true;
        document.getElementById("tuesPM").disabled = true;
        document.getElementById("wednesAM").disabled = true;
        document.getElementById("wednesPM").disabled = true;
        document.getElementById("thursAM").disabled = true;
        document.getElementById("thursPM").disabled = true;
        document.getElementById("friAM").disabled = true;
        document.getElementById("friPM").disabled = true;
    } else {
        document.getElementById("monPM").disabled = false;
        document.getElementById("tuesAM").disabled = false;
        document.getElementById("tuesPM").disabled = false;
        document.getElementById("wednesAM").disabled = false;
        document.getElementById("wednesPM").disabled = false;
        document.getElementById("thursAM").disabled = false;
        document.getElementById("thursPM").disabled = false;
        document.getElementById("friAM").disabled = false;
        document.getElementById("friPM").disabled = false;
    }
    if (document.getElementById("monAll").checked || allWeek) {
        document.getElementById("monPM").disabled = true;
    } else {
        document.getElementById("monPM").disabled = false;
    }
    if (document.getElementById("tuesAll").checked || allWeek) {
        document.getElementById("tuesPM").disabled = true;
    } else {
        document.getElementById("tuesPM").disabled = false;
    }
    if (document.getElementById("wednesAll").checked || allWeek) {
        document.getElementById("wednesPM").disabled = true;
    } else {
        document.getElementById("wednesPM").disabled = false;
    }
    if (document.getElementById("thursAll").checked || allWeek) {
        document.getElementById("thursPM").disabled = true;
    } else {
        document.getElementById("thursPM").disabled = false;
    }
    if (document.getElementById("friAll").checked || allWeek) {
        document.getElementById("friPM").disabled = true;
    } else {
        document.getElementById("friPM").disabled = false;
    }
}

