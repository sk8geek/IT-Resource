/* version 3 style sheet for csdb */

/* global styles */
body {
	font-family: Helvetica, Arial, sans-serif;
    margin: 0
}

div {
	margin: 0;
    padding: 0;
}
div.content {
    margin: 0.5ex 20em 0.5ex 1ex;
    padding: 0.5ex 1ex;
}
div.fullwidth {
    margin: 0.5ex 0 0.5ex 1ex;
    padding: 0.5ex 1ex;
}
div#tools {
    position: absolute;
    top: 5em;
    right: 0;
    display: block;
    border: 1px #900 solid;
    margin: 1ex;
    padding: 0.5ex;
    width: 14em;
    max-width: 14em;
    color: inherit;
    background-color: #fff;
}
div.function {
    font-size: smaller;
    padding: 0.5ex 0.2ex;
    margin: 0.5ex 0;
    color: inherit;
    background-color: #fff;
}
div#footer {
    clear: both;
    border: #aaa solid;
    border-width: 1px 0 0 0;
    padding: 1ex;
    color: inherit;
    background-color: #ddd;
    margin: 4px 0 0 0;
}
div#footer p, form {
    display: inline;
}
div.footerFunction {
    display: inline;
    font-size: smaller;
    padding: 1ex 0.5ex;
    margin: 0.5ex 1ex;
}

div.titleBar p {
    text-align: right;
    color: #666;
    background-color: #fff;
    margin: 0.5ex;
}

div.titleBar a {
    font-size: x-small;
}

div.titleBar a.button:hover {
    color: #000;
    background-color: #fd6;
}

div.titleBar input {
    border: 1px #933 solid;
    font-size: x-small;
    color: #333;
    background-color: #fff;
    margin: 0;
    padding: 1px 2px;
}

div.titleBar input.button {
    border: 1px #933 solid;
    margin: 0 0.5ex 0 0.5ex;
    padding: 0 2px;
    color: #000;
    background-color: #ea6;
}

div.titleBar input:hover {
    color: #000;
    background-color: #fd6;
}

div.menu {
    margin-top: 10pt;
}
    
/* headings */
h1 {
    position: absolute;
    font-size: 14pt;
    font-weight: bold;
    color: #900;
    background-color: #fff;
    margin: 0 8ex 0 1ex;
    letter-spacing: 0.8ex;
}
h2 { 
	font-weight: bold; 
    font-size: 14pt;
	color: #c33;
	background-color: inherit;
    padding: 0.5ex 0.5ex 0 0.5ex;
    margin: 0 0.2ex 0.4ex 0.2ex;
}
h3 { 
	font-weight: bold; 
    font-size: 12pt;
	margin: 1ex 0.5ex;
	padding: 2px 1ex 2px 0.5ex;
}
/* paragraphs */
p {
    margin: 0.5ex 0.5ex 1ex 0.5ex;
    padding: 0 0.5ex;
}
p.button {
    margin: 1ex 0 0.5ex 0;
    font-size: small;
}
p.buttons { 
	text-align: right;
    margin-top: 2ex;
}
p.menu {
    margin: 0 0 2ex 0;
    padding: 8px 0 1px 8px;
    font-size: small;
    color: #000;
    background: #900;
    border: solid #c66;
    border-width: 2px 0 0 0;
}
p.menu a {
    margin: 0;
    padding: 2px 1ex;
    border: solid #c66;
    border-width: 1px 1px 0 1px;
}
p.menu a:hover { 
	color: inherit;
	background-color: #fd6;
}
p.greeting {
    font-weight: bold;
    font-size: medium;
    color: #900;
    background-color: #fe9;
    border: 1px solid #fc9;
}
p.success {
    font-weight: bold;
    font-size: medium;
    color: #000;
    background-color: #dec;
    border: 1px solid #dca;
}
p.failure {
    font-weight: bold;
    font-size: medium;
    color: #000;
    background-color: #f88;
    border: 1px solid #d77;
}
/* tables */
table {
	margin: 1ex;
	padding: 1px 1ex;
    empty-cells: show;
}
th {
	border: 1px solid #666;
    color: black;
    background-color: #acd;
    background-color: #dde;
    padding: 0 4px;
}
tr, th, td {
	text-align: left;
}
tr.itsMe {
    color: inherit;
    background-color: #dda;
}
td {
    white-space: nowrap;
    padding: 0 0.6ex 0 0.4ex;
}
table.boxed td {
	border: 1px solid #ccc;
    white-space: nowrap;
    padding: 0 4px;
}
th.controlHeading {
    color: inherit;
    background-color: #ccc;
}
table.boxed td.systemstatus {
    text-align: left;
    border: 0;
}
td.inv_live {
    color: inherit; background-color: #9c9;
}
td.inv_mute {
    color: inherit; background-color: #9cc;
}
td.inv_dead {
    color: inherit; background-color: #f60;
}
td.inv_away {
    color: inherit; background-color: #c0f;
}
td.grey {
    color: inherit;
    background-color: #dde;
}
th.right,td.right {text-align: right}
/* lists */
li {
    list-style-type: square;
    margin-left: 0.5ex;
}
dl.msgs {
    margin: 0.5ex;
}
ul.msgs {
    margin: 0.5ex;
}
dt.headline {
    color: inherit;
    background-color: #9bd;
    background-color: #dde;
    padding: 0.5ex 1ex;
}
dl.msgs dd {
    padding: 0.5ex 1ex;
    margin: 0;
}    
ul.msgs li {
    list-style-type: none;
    padding: 0.5ex 1ex;
    margin: 0 0 1ex 0;
}    
dd.latest {
    color: inherit;
    background-color: #ffc;
}
.latest {
	color: inherit;
	background-color: #ffc;
}
ul {
    margin: 0.5ex 2ex;
    padding-left: 0.5ex;
}
ul.faqs {
    padding-left: 0;
    margin: 0.5ex;
}
ul.faqs li {
    list-style-type: none;
    margin-left: 0.5ex;
    padding: 0;
}
/* anchors */
a:link { 
	color: #00f;
	background-color: transparent;
}
a:visited {
	color: #909;
	background-color: transparent;
}
a:hover { 
	color: inherit;
	background-color: #fd6;
}
a.button {
    text-decoration: none;
    border: 1px solid #c66;
    padding: 1px 4px;
}
a.button:link, a.button:visited {
    color: #000;
    background-color: #ea6;
    text-decoration: none;
}
a.button:hover { 
	color: inherit;
    background-color: #fd6;
}
a.menuTab:link, a.menuTab:visited {
    color: #000;
    background-color: #ea6;
    text-decoration: none;
}
a.selected {
    font-weight: bold;
}
a.selected:link, a.selected:visited {
    color: #000;
    background-color: #fff;
    text-decoration: none;
}
a.selected {
    color: #000;
    background-color: #fff;
    text-decoration: none;
}
a.invisible {
    color: #000;
    background-color: transparent;
    text-decoration: none;
}
/* forms and related */
form {
    display: inline;
}
input, select, button, textarea {
	border: 1px #666 solid;
	margin: 0 0.5ex 0 0.5ex;
    padding-left: 2px;
    padding-right: 2px;
}
input.button {
    color: #000;
    background-color: #ea6;
}
input:hover {
    color: inherit;
    background-color: #fd6;
}
input.button:hover {
    color: inherit;
    background-color: #fd6;
}
input#logout {
    color:inherit; 
    background-color:#fc6;
}
/* images */
img {
    border: 0;
    margin: 0;
	float: left;
}
img.button {
	float: right;
	margin: 2px 1ex 0 0;
	padding: 0;
}
img.systemstatus {
    float: none;
    text-align: center;
    height: 15px;
    width: 15px;
    margin: 2px 4px 0 4px;
}
/* spans and miscellaneous */
span.spacer {
    color: #900;
    background-color: #900;
}
code { 
	background-color: inherit; 
	color: #060;
}
.date {
    color: #666;
    background-color: inherit;
    font-style: italic;
    font-size: smaller;
}
.alert { 
	color: #ff3;
	background-color: #c00;
	font-weight: bold;
    padding: 1px 2px;
}
.centre { 
	text-align: center;
}
.healthySystem {
    color: inherit;
    background-color: #dec;
    padding: 1px 2px;
}
.sicklySystem {
    color: inherit;
    background-color: #ff0;
    padding: 1px 2px;
}
.knownDeadSystem {
    color: inherit;
    background-color: #f88;
    padding: 1px 2px;
}
.deadSystem {
    color: inherit;
    background-color: #f66;
    padding: 1px 2px;
}
.grey {
	color: inherit;
	background-color: #ccc;
}
.greytext {
    color: #666;
    background-color: inherit;
}
.notImportant {
 	color: #666;
	background-color: inherit;
}
.reqd {
    color: inherit;
    background-color: #f66;
}








/* *************************** OLD STUFF *********************** */

/* ******** Paragraphs ******** */
/* ******** Images ******** */
/* ******** Miscellaneous ******** */
/* **  Forms  ** */
form.boxed {
	border: 1px solid #999;
	color: inherit;
	background-color: white;
}
.resultinfo {
    color: #666;
    background-color: inherit;
    font-style: italic;
    font-size: smaller;
}
.larger {
	font-size: larger;
}
#smaller {
	font-size: smaller;
}
.right {
	text-align: right;
}
/* ************
 * **  Menu  **
 * ************/
div.usermenu {
	color: inherit;
	background-color: #eee;
	margin: 4px 0 0 0;
	padding: 0;
}
p.menuform {
	margin: 0;
	padding: 0 0 0 1px;
}
a.menuitem {
	display: block;
	text-decoration: none;
	color: black;
	background-color: inherit;
	border: 1px solid #eee;
	border-width: 1px 0 1px 0;
	margin: 0;
	padding: 2px 8px 2px 8px;
}
a.menuitem:link {
	color: black;
	background-color: inherit;
}
a.menuitem:visited {
	color: black;
	background-color: inherit;
}
a.menuitem:hover {
	color: black;
	background-color: #fc9;
	border-color: #666;
}
form.menuitem {
    display: block;
	font-size: small;
    padding: 1px;
    margin: 1px;
}
form.menuitem:hover {
	color: black;
	background-color: #fc9;
}

input#logout {
    color:inherit; background-color:#fc6;
}


/* *****************
 * **  Inventory  **
 * *****************/

/* *****************
 * **  Documents  **
 * *****************/
.htmlfile {
	color: #00f; background-color: inherit;
	font-size: small;
}
.pdffile {
	color: #f00; background-color: inherit;
	font-size: small;
}

/* ***************
 * **  Message  **
 * ***************/
p.okay {
    color: inherit; background-color: #cadcca;
}
p.warn {
    font-weight: bold;
    color: inherit; background-color: #f60;
    border: #000 2px dotted;
}
 
 /* *************
 * **  Admin  **
 * *************/
table.admin {
	font-size: x-small;
}

table.admin td {
	border-width: 0 1px 1px 1px;
	border-style: solid;
	border-color: #ccc;
}
/* **  Chores  ** */
tr.running{
	color: inherit; background-color: #9c9;
}
tr.running>td {	border-width: 0 0 1px 0;
	border-color: #333;
	border-style: solid;
}
tr.stopped{
	color: inherit; background-color: #f66;
}
tr.stopped>td {
	border-width: 0 0 1px 0;
	border-color: #333;
	border-style: solid;
}

