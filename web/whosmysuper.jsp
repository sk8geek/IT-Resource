<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2008 Steven Lilley -->
<!-- Copyright (c) 2010 Calderdale MBC -->
<html>
<head>
<title>LS-DB</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="whosmysuper.jsp" />
<%@ include file="title.inc" %>

<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="index" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<jsp:getProperty name="userIdentity" property="welcome" />
<h2>Who's My Supervisor</h2>
<jsp:useBean id="corpFinBean" scope="request" class="uk.co.channele.itres.CorpFinBean" />
<jsp:setProperty name="corpFinBean" property="*" />
<jsp:getProperty name="corpFinBean" property="response" />

</div>
<%@ include file="tools.inc" %>
</body>
</html>

