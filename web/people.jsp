<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<!-- Copyright (c) 2003-2006 Steven Lilley -->
<html>
<head>
<title>LS-DB: People</title>
<%@ include file="head.inc" %>
</head>
<body>
<jsp:include page="identity" flush="true" />
<jsp:useBean id="userIdentity" scope="session" class="uk.co.channele.itres.IdentityBean" />
<jsp:setProperty name="userIdentity" property="currentPage" value="people.jsp" />
<%@ include file="title.inc" %>
<div class="menu">
<jsp:useBean id="menu" scope="session" class="uk.co.channele.itres.ItresMenuBean" />
<jsp:setProperty name="menu" property="pageName" value="people" />
<jsp:getProperty name="menu" property="menuTabs" />
</div>

<div class="content">
<h2>People</h2>
<jsp:useBean id="peopleBean" scope="session" class="uk.co.channele.itres.PeopleBean" />
<jsp:setProperty name="peopleBean" property="*" />
<jsp:getProperty name="peopleBean" property="response" />
</div>

<%@ include file="tools.inc" %>
</body>
</html>

