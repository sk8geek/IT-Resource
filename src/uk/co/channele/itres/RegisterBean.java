/* 
 * @(#) RegisterBean.java    0.1 2010/02/26
 *
 * A Bean to provide interaction with people, posts and employment.
 * Copyright (C) 2006 - 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Provides an interface for notices.
 *
 * @version       0.1   5 March 2010
 * @author        Steven Lilley
 */
public class RegisterBean extends GenericBean {
    
    private final String sqlGetPeople = "SELECT id,lid,givenName,surname FROM estPeople ORDER BY lid";
    private final String sqlAddPerson = "INSERT INTO estPeople (lid, givenName, surname) VALUES (?, ?, ?)";
    private final String sqlAddPost = "INSERT INTO estPosts (lid, jobTitle, costCentre) VALUES (?, ?, ?)";
    private final String sqlGetListBySurname = "SELECT id,estPeople.lid,givenName,surname,jobTitle FROM estPeople LEFT JOIN estPosts USING (lid) ORDER BY surname";
    private final String sqlGetListByLid = "SELECT id,estPeople.lid,givenName,surname,jobTitle FROM estPeople LEFT JOIN estPosts USING (lid) ORDER BY estPeople.lid";
    private int personId = 0;
    private String lid = "";
    private String givenName = "";
    private String surname = "";
    private String jobTitle = "";
    private boolean orderByLid = true;
    
    public RegisterBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("RegisterBean constructor", nex);
        }
    }
    
    /**
     * Method to clear values
     */
    protected void clearValues() {
        personId = 0;
        lid = "";
        givenName = "";
        surname = "";
        jobTitle = "";
        orderByLid = true;
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    protected String getEditForm() {
        return "[getEditForm]";
    }
    
    protected String getItem() {
        return "[getItem]";
    }
    
    protected String getList() {
        Connection con;
        PreparedStatement getList;
        ResultSet results;
        StringBuilder h = new StringBuilder();
        boolean startedList = false;
        String lidGroup = "";
        int tenCounter = 0;
        int unitCounter = 0;
        String id = "";
        String lid = "";
        String given = "";
        String surname = "";
        String jobTitle = "";
        String initials = "";
        try {
            con = dataSrc.getConnection();
            if (orderByLid) {
                getList = con.prepareStatement(sqlGetListByLid);
                h.append("<h2>LID Grid</h2>");
            } else {
                getList = con.prepareStatement(sqlGetListBySurname);
                h.append("<h2>User List</h2>");
            }
            results = getList.executeQuery();
            while (results.next()) {
                if (!startedList) {
                    if (orderByLid) {
                        h.append("<table>");
                    } else {
                        h.append("<table><tr><th>Name</th><th>LID</th><th>Job Title</th></tr>");
                    }
                    startedList = true;
                }
                id = "";
                lid = "";
                given = "";
                surname = "";
                jobTitle = "";
                id = results.getString("id");
                if (results.getString("lid") != null) {
                    lid = results.getString("lid");
                }
                if (results.getString("givenName") != null) {
                    given = results.getString("givenName");
                }
                if (results.getString("surname") != null) {
                    surname = results.getString("surname");
                }
                if (results.getString("jobTitle") != null) {
                    jobTitle = results.getString("jobTitle");
                }
                initials = "";
                if (given != null) {
                    initials = given.substring(0, 1);
                }
                if (surname.length() > 0) {
                    initials += surname.substring(0, 1);
                }
                if (orderByLid) {
                    if (id.substring(0, 1) != lidGroup) {
                        lidGroup = id.substring(0, 1);
                        h.append("<tr><th>" + lidGroup + "xx</th><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th>");
                        h.append("<th>5</th><th>6</th><th>7</th><th>8</th><th>9</th></tr>");
                    } else {
                        // write some clever code here to show empty LIDs or the users initials where they exist
                        if (tenCounter != Integer.getInteger(lid.substring(2, 3))) {
                            tenCounter = Integer.getInteger(lid.substring(2, 3));
                            h.append("</tr>");
                        } else {
                            if (unitCounter < 9) {
                                if (unitCounter == Integer.getInteger(lid.substring(3, 4))) {
                                    h.append("<td><a href=register?id=" + id + ">" + initials + "</a></td>");
                                } else
                                if (unitCounter < Integer.getInteger(lid.substring(3, 4))) {
                                    h.append("<td>[]</td>");
                                } else {
                                    h.append("<td>!!</td>");
                                }
                            } else {
                                unitCounter = 0;
                                tenCounter++;
                                if (tenCounter > 9) {
                                    tenCounter = 0;
                                    h.append("</tr>");
                                }
                            }
                        }
                    }
                } else {
                    // order by surname
                    h.append("<tr><td><a href=register?id=" + id + ">" + given + " " + surname + "</a></td><td>");
                    h.append(jobTitle + "</td></tr>");
                }
            }
            results.close();
            getList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("RegisterBean, getList", sqlex);
            h.append(sqlex);
        }
        results = null;
        getList = null;
        con = null;
        return h.toString();
    }
    
    public String getSearchForm() {
        return "[searchForm]";
    }
    
    protected String getSearchResults() {
        return "[searchResults]";
    }
    
    public String getStandard() {
        StringBuilder h = new StringBuilder();
        h.append(getList());
        return h.toString();
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    protected boolean validateData() {
        return false;
    }
    
    protected boolean writeToDatabase() {
        return false;
    }
}
