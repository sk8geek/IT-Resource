/* 
 * @(#) MessageBean.java    0.1 2006/10/29
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * Handles messages.
 *
 * @version       0.1    29 October 2006
 * @author        Steven Lilley
 */
public class MessageBean implements Serializable { 

    final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    public static final int MESSAGE_STATUS_UNOPENED = 0;
    public static final int MESSAGE_STATUS_READ_LATER = 1;
    public static final int MESSAGE_STATUS_OPENED = 2;
    private final String sqlCheckForMessages = 
        "SELECT * FROM message_view WHERE recipient=? AND status=? ORDER BY sent DESC";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String userName;
    private String action = "";
    private int msgId = 0;
    private String messageText = "";
    private boolean secretMessage = false;
    private int messageTypeToView = 0;
    private int userPresence = 0;
    private Logger log;
    
    public MessageBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("MessageBean constructor", nex);
        }
    }
    
    public void setUserName(String user) {
        userName = user;
    }
    
    public void setMessageTypeToView(String msgTypeLimit) {
        try {
            messageTypeToView = Integer.parseInt(msgTypeLimit);
            if (messageTypeToView < 0) {
                messageTypeToView = 0;
            }
            if (messageTypeToView > 2) {
                messageTypeToView = 2;
            }
        } catch (NumberFormatException nfex) {
            messageTypeToView = 0;
        }
    }
    
    public String getMessageList() {
        if (userName == null || userPresence < 0) {
            return "";
        }
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement checkForMessages;
        ResultSet results;
        boolean headerDone = false;
        try {
            con = dataSrc.getConnection();
            checkForMessages = con.prepareStatement(sqlCheckForMessages);
            checkForMessages.clearParameters();
            checkForMessages.setString(1, userName);
            checkForMessages.setInt(2, messageTypeToView);
            results = checkForMessages.executeQuery();
            while (results.next()) {
                if (!headerDone) {
                    h.append("<ul class=\"msgs\">");
                    headerDone = true;
                }
                if (results.getInt("status") == 0) {
                    h.append("<li class=\"latest\">");
                } else {
                    h.append("<li class=\"msgs\">");
                }
                h.append("<a href=\"message.jsp?msgId=" + results.getInt("msgid"));
                h.append("&action=delete\" class=\"button\" ");
                h.append("style=\"font-size: smaller;float:right;margin-left:0.5ex;\">Delete</a>");
                h.append("<a href=\"message.jsp?msgId=" + results.getInt("msgid"));
                h.append("&action=reply\" class=\"button\" ");
                h.append("style=\"font-size: smaller;float:right;margin-left:2ex;\">Reply</a>");
                h.append(results.getString("msg"));
                h.append("<br><span class=\"date\">");
                h.append(results.getString("sender_name") + " &nbsp; ");
                h.append(itemDateTime.format(results.getTimestamp("sent")));
                h.append("</span>");
                h.append("</li>");
            }
            if (headerDone) {
                h.append("</ul>");
            }
            results.close();
            checkForMessages.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("MessageBean, getMessageList", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    void setUserPresence(int presence) {
        userPresence = presence;
    }
}

