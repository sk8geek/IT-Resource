/* 
 * @(#) Documents.java	0.1 2007/03/18
 * 
 * Copyright (C) 2006-2007 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
/**
 * Servlet to initialise/set editable a NewsBean.
 *
 * @version		0.2     17 March 2007
 * @author		Steven Lilley
 */
public class Documents extends HttpServlet { 

	private ServletContext context;
    private Logger log;

	public void init() {
        log = Logger.getLogger("itres");
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        NewsBean newsBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                if (userId.isRegistered()) {
                    if (session.getAttribute("newsBean") == null) {
                        newsBean = new NewsBean();
                        session.setAttribute("newsBean", newsBean);
                    } else {
                        newsBean = (NewsBean)session.getAttribute("newsBean");
                    }
                    newsBean.setUserId(userId.getUserName());
                    newsBean.setRemoteHost(rqst.getRemoteHost());
                    newsBean.setEditable(true);
                    handler = context.getRequestDispatcher("/notices.jsp");
                } else {
                    handler = context.getRequestDispatcher("/register.jsp");
                }
            } else {
                handler = context.getRequestDispatcher("/index.jsp");
            }
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        ServletFileUpload fileUpload = new ServletFileUpload(fileFactory);
        if (ServletFileUpload.isMultipartContent(rqst)) {
            log.debug("Documents, doPost: got multipart content");
            try {
                List fileList = fileUpload.parseRequest(rqst);
                Iterator i = fileList.iterator();
                while (i.hasNext()) {
                    log.debug("Documents, doPost: iterator has items");
                    FileItem item = (FileItem)i.next();
                    if (!item.isFormField()) {
                        log.debug("Documents, doPost: filename=" + item.getName());
                        File uploadedFile = new File("/home/tomcat/itres_static/docs/" + item.getName());
                        item.write(uploadedFile);
                    } else {
                        log.debug("Documents, doPost: form field=" + item.getFieldName());
                    }
                }
            } catch (FileUploadException fuex) {
                log.error("Documents, doPost:", fuex);
            } catch (Exception ex) {
                log.error("Documents, doPost:", ex);
            }
        } else {
            log.debug("Documents, doPost: NOT multipart content");
        }
        handler = context.getRequestDispatcher("/docs.jsp");
        handler.include(rqst, resp);
    }
}

