/* 
 * @(#) FeedbackBean.java    0.1 2006/10/29
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
/**
 * Stores feedback.
 *
 * @version       0.1    29 October 2006
 * @author        Steven Lilley
 */
public class FeedbackBean implements Serializable { 

    private final String sysAdmin = "steven.lilley@calderdale.gov.uk";
    final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    public final String[] feedbackRatings = {"It sucks, badly!", "It sucks", 
        "Don't care", "S'okay", "Pretty good"};
    private final String sqlRecordFeedback = 
        "INSERT INTO feedback (sitepage, rating, respondant, remotehost, comment) VALUES (?, ?, ?, ?, ?)";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String sitePage = "";
    private int rating = 0;
    private String respondant = "";
    private String remoteHost = "";
    private String comment = "";
    private Logger log;
    
    public FeedbackBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("FeedbackBean, constuctor", nex);
        }
    }
    
    public void setViewingPage(String viewingPage) {
        sitePage = viewingPage;
    }
    
    public void setRating(String rate) {
        try {
            rating = Integer.parseInt(rate);
            if (rating < 0) {
                rating = 0;
            }
            if (rating > 4) {
                rating = 4;
            }
        } catch (NumberFormatException nfex) {
            rating = 0;
        }
    }
    
    public void setRespondant(String visitor) {
        respondant = visitor;
    }
    
    public void setRemoteHost(String visitingHost) {
        remoteHost = visitingHost;
    }
    
    public void setComment(String wordsOfWisdom) {
        comment = wordsOfWisdom;
    }
    
    public String getRecordFeedback() {
        StringBuilder h = new StringBuilder();
        h.append("<p class=\"greeting\">Thank you for your feedback.</p>");
        if (respondant.length() > 0) {
            h.append("<p>I will endeavour to respond to your comment as soon as possible.</p>");
        }
        Connection con;
        PreparedStatement recordFeedback;
        try {
            con = dataSrc.getConnection();
            recordFeedback = con.prepareStatement(sqlRecordFeedback);
            recordFeedback.clearParameters();
            recordFeedback.setString(1, sitePage);
            recordFeedback.setInt(2, rating);
            recordFeedback.setString(3, respondant);
            recordFeedback.setString(4, remoteHost);
            recordFeedback.setString(5, comment);
            if (recordFeedback.executeUpdate() != 1) {
                log.warn("FeedbackBean, getRecordFeedback failed to save feedback.");
                h.append("<p>Sorry, but a problem with the database has ");
                h.append("prevented your feedback from being recorded.</p>");
            }
            recordFeedback.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FeedbackBean, getRecordFeedback", sqlex);
            h.append("<p>An database error occurred:<br>" + sqlex + "</p>");
        }
        con = null;
        notifySysAdmin();
        return h.toString();
    }
    
    private void notifySysAdmin() {
        StringBuilder h = new StringBuilder();
        h.append("New feedback has been received regarding your wonderful site.\n");
        h.append("\nSite page: " + sitePage);
        h.append("\nRespondant: ");
        if (respondant.length() == 0) {
            h.append("anonymous coward!");
        } else {
            h.append(respondant);
        }
        h.append("\nRating: " + rating + " " + feedbackRatings[rating]);
        h.append("\nComment:\n" + comment);
        h.append("\n[" + remoteHost + "]");
        h.append("\n" + itemDateTime.format(new Date()) + "\n");
        sendMail(sysAdmin, "Feedback on IT Resource", h.toString());
    }
    
    /**
     * Send an email via the application context.
     * @param recipient the email recipient
     * @param subject the subject line of the email
     * @param body the body text of the email
     * @return true if the email was sent successfully
     */
    private boolean sendMail(String recipient, String subject, String body) {
        Session mailSession;    
        MimeMessage message;
        boolean success = false;
        try {
            mailSession = (Session)nameCnxt.lookup("mail/smtp");
            message = new MimeMessage(mailSession);
            message.setSender(new InternetAddress("ls-db@calderdale.gov.uk"));
            message.addRecipients(MimeMessage.RecipientType.TO, recipient);
            message.setSubject(subject);
            message.setText(body, "UTF-8");
            Transport.send(message);
            success = true;
        } catch (NamingException nex) {
            log.error("IdentityBean, sendMail: ", nex);
        } catch (AddressException aex) {
            log.error("IdentityBean, sendMail: ", aex);
        } catch (MessagingException mex) {
            log.error("IdentityBean, sendMail: ", mex);
        }
        return success;
    }
}

