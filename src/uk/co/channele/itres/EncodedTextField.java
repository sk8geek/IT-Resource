/* 
 * @(#) EncodedTextField.java	0.1 2003/11/7
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.lang.*;
/**
 * Part of my IT Resource suite of servlets.
 *
 * @version		0.1 7 November 2003
 * @author		Steven Lilley
 */
public class EncodedTextField extends Object {

	public String text;
	
	EncodedTextField (String textField) {
		StringBuffer b = new StringBuffer();
		int p = 0;
		while (textField.indexOf("\n", p) > -1) {
			b.append(textField.substring(p,textField.indexOf("\n", p)));
			b.append("<br />");
			p = textField.indexOf("\n", p) + 1;
		}
		b.append(textField.substring(p));
		text = b.toString();
	}
	
	public static String getEncodedText(String inputText) {
		EncodedTextField t = new EncodedTextField(inputText);
		return t.text;
	}
 
}
