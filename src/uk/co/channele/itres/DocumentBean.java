/* 
 * @(#) DocumentBean.java    0.1 2006/10/13
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
/**
 * A Bean to provide a list of static content.
 *
 * @version       0.1 13 October 2006
 * @author        Steven Lilley
 */
public class DocumentBean implements Serializable { 

    private final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private String staticURL = "";
    private String staticPath = "";
    private String documentPath = "";
    private String itDocumentPath = "";
    private String itMinutesPath = "";
    
    public DocumentBean() {
    }
    
    public void setStaticURL(String static_url) {
        staticURL = static_url;
    }
    
    public void setStaticPath(String static_path) {
        staticPath = static_path;
    }
    
    public void setDocumentPath(String doc_path) {
        documentPath = doc_path;
    }
    
    public void setItDocumentPath(String it_doc_path) {
        itDocumentPath = it_doc_path;
    }
    
    public void setItMinutesPath(String it_minutes_path) {
        itMinutesPath = it_minutes_path;
    }
    
    public String getDocumentList() {
        StringBuilder h = new StringBuilder();
        int c = 0;
        File documentDir = new File(staticPath + documentPath);
        String[] fileList = documentDir.list(new docFileFilter());
        DocObject[] documentList = new DocObject[fileList.length];
        for (c = 0; c < documentList.length; c++) {
            documentList[c] = new DocObject(fileList[c]);
        }
        h.append("<p>");
        for (c = (documentList.length-1); c >= 0 ; c--) {
            h.append("<a href=\"" + staticURL + documentPath);
            h.append(documentList[c].fileName + "\">");
            h.append(documentList[c].docTitle + "</a>");
            // identify file type 
//                if ((documentList[c].fileName).endsWith(".html")) {
//                    h.append(" <span class=\"htmlfile\">html</span>");
//                }
            if ((documentList[c].fileName).endsWith(".pdf")) {
                h.append(" <span class=\"pdffile\">pdf</span>");
            }
            if (c > 0) {
                h.append("<br>\n");
            }
        }
        h.append("</p>");
        return h.toString();
    }
    
    public String getTechnicalDocumentList() {
        StringBuilder h = new StringBuilder();
        int c = 0;
        File itDocumentDir = new File(staticPath + itDocumentPath);
        String[] itFileList = itDocumentDir.list(new docFileFilter());
        DocObject[] itDocumentList = new DocObject[itFileList.length];
        for (c = 0; c < itDocumentList.length; c++) {
            itDocumentList[c] = new DocObject(itFileList[c]);
        }
        Arrays.sort(itDocumentList);
        h.append("<p>");
        for (c = (itDocumentList.length-1); c >= 0 ; c--) {
            h.append("<a href=\"" + staticURL + itDocumentPath);
            h.append(itDocumentList[c].fileName + "\">");
            h.append(itDocumentList[c].docTitle + "</a> ");
            h.append("<span class=\"date\"> ");
            h.append(HouseKeeper.itemDate.format(
                itDocumentList[c].docModified));
            h.append("</span>");
            if (c > 0) {
                h.append("<br>\n");
            }
        }
        h.append("</p>");
        return h.toString();
    }
    
    public String getItMinutesDropDown() {
        StringBuilder h = new StringBuilder();
        int c = 0;
        File itMinutesDir = new File(staticPath + itMinutesPath);
        String[] itMinutesFileList = itMinutesDir.list(new docFileFilter());
        DocObject[] itMinutesDocList = new DocObject[itMinutesFileList.length];
        for (c = 0; c < itMinutesDocList.length; c++) {
            itMinutesDocList[c] = new DocObject(itMinutesFileList[c]);
        }
        Arrays.sort(itMinutesDocList);
        h.append("<p>Minutes: <select name=\"filename\">");
        for (c = (itMinutesDocList.length-1); c >= 0 ; c--) {
            h.append("<option value=\"");
            h.append(staticURL + itMinutesPath);
            h.append(itMinutesDocList[c].fileName);
            h.append("\">");
            h.append(itemDate.format(itMinutesDocList[c].docModified));
            h.append("</option>\n");
        }
        h.append("</select> \n");
        h.append("<input type=\"submit\" value=\"Go\" /></p>");
        return h.toString();
    }
}

class docFileFilter implements FilenameFilter {

    public boolean accept(File dir, String name) {
        if (name.endsWith(".html") || name.endsWith(".pdf")) {
            return true;
        }
        return false;
    }
}

