/* 
 * @(#) Search.java    0.1 2006/11/07
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Instantiates an SearchBean, possibly with admin rights, for this request.
 * <p>This servlet is called rather than calling search.jsp directly.
 * It instantiates the SearchBean and checks for an administrator being 
 * logged in.  If this is so then the SearchBean is marked as adminEnabled.
 * Subsequent Beans created by the SearchBean will have adminEnabled set.
 *
 * @version     0.1    7 November 2006
 * @author      Steven J Lilley
 */
public class Search extends HttpServlet { 
    
	private ServletContext context;
    
	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        SearchBean searchBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            searchBean = new SearchBean();
            if (session.getAttribute("admin") != null) {
                // this is an administrator
                searchBean.setAdminEnabled(true);
            }
            rqst.setAttribute("search", searchBean);
            handler = context.getRequestDispatcher("/search.jsp");
            handler.include(rqst, resp);
        } // end sync
    }
}

