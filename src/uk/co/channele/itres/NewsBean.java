/* 
 * @(#) NewsBean.java    0.2 2006/11/24
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.LiteGenericBean;
/**
 * Returns results from the itnews table.
 *
 * @version       0.2    24 November 2006
 * @author        Steven Lilley
 */
public class NewsBean extends LiteGenericBean { 

    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private final String sqlGetGlobalNews = "SELECT * FROM notices WHERE site IS NULL ORDER BY issued DESC";
    private final String sqlGetSiteNews = "SELECT * FROM notices WHERE site IS NULL OR site=? ORDER BY issued DESC";
    private final String sqlGetOwnedNews = "SELECT * FROM notices WHERE poster=? ORDER BY issued DESC";
    private final String sqlUpdateNotice = "UPDATE notices SET headline=?, story=?, site=?, team=?, expires=? WHERE storynum=? LIMIT 1";
    private final String sqlAddNotice = "INSERT INTO notices (headline, story, site, team, expires, poster, src) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private int storyId = 0;
    private String headline = "";
    private String story = "";
    private String audience = "all";
    private String site = "";
    private String team = "";
    private String userName = "";
    private String src = "";
    private Calendar expiryDate = Calendar.getInstance();
    
    public NewsBean() {
        log = Logger.getLogger("itres");
        expiryDate.add(Calendar.MINUTE, 5);
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("NewsBean constructor", nex);
        }
    }
    
    public void setStoryId(String story_id) {
        try {
            setStoryId(Integer.parseInt(story_id));
        } catch (NumberFormatException nfex) {
            log.debug("NewsBean, setStoryId", nfex);
            storyId = 0;
        }
    }
    
    public void setStoryId(int story_id) {
        storyId = story_id;
    }
    
    public void setHeadline(String new_headline) {
        headline = new_headline;
    }
    
    public void setStory(String new_story) {
        story = new_story;
    }
    
    public void setAudience(String targetAudience) {
        audience = targetAudience;
    }
    
    public void setSite(String coverage) {
        site = coverage;
    }
    
    public void setSource(String hostname) {
        src = hostname;
    }
    
    public void setSqlExpiryDate(String expiryDate) {
        // TODO: set these to 1am on the given date
    }
    
    public void setSqlExpiryTime(String expiryDate) {
        // TODO: set these to date is specified, otherwise today, at the given hour
    }
    
    public void setLifetimeHours(String life) {
        int lifetime = 1;
        try {
            lifetime = Integer.parseInt(life);
        } catch (NumberFormatException nfex) {
            log.debug("NewsBean, setLifetimeHours", nfex);
        }
        expiryDate.add(Calendar.HOUR, lifetime);
    }
    
    void setEditable(boolean newEditMode) {
        isAdmin = newEditMode;
    }
    
    public String getStandard() {
        return "not implemented.";
    }
    
    protected String getItem() {
        return "not implemented.";
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    public String getNewsFeed() {
        Connection con;
        PreparedStatement getNewsFeed;
        ResultSet results;
        StringBuilder h = new StringBuilder();
        boolean startedList = false;
        try {
            con = dataSrc.getConnection();
            if (site.length() > 0) {
                getNewsFeed = con.prepareStatement(sqlGetSiteNews);
                getNewsFeed.clearParameters();
                getNewsFeed.setString(1, site);
            } else {
                getNewsFeed = con.prepareStatement(sqlGetGlobalNews);
            }
            results = getNewsFeed.executeQuery();
            while (results.next()) {
                if (!startedList) {
                    h.append("<dl class=\"msgs\">");
                    startedList = true;
                }
                h.append("<dt class=\"headline\">");
                h.append(results.getString("headline"));
                h.append("</dt>");
                if (!isAdmin) {
                    long age = System.currentTimeMillis() - results.getTimestamp("issued").getTime();
                    if (age < 28800000) {
                        h.append("<dd class=\"latest\">");
                    } else {
                        h.append("<dd class=\"msgs\">");
                    }
                }
                h.append(EncodedTextField.getEncodedText(results.getString("story")));
                if (results.getDate("issued") != null) {
                    h.append("<br><span class=\"date\">");
                    h.append(HouseKeeper.itemDateTime.format(
                        results.getTimestamp("issued")));
                    h.append("</span>");
                } else {
                    h.append("<br><span class=\"date\">");
                    h.append("&nbsp;</span>");
                }
                if (isAdmin) {
                    h.append("<form method=\"post\" action=\"/noticeeditor\">");
                    h.append("<input type=\"hidden\" name=\"storynum\" value=\"");
                    h.append(results.getInt("storynum") + "\">");
                    h.append("<input type=\"radio\" name=\"action\" value=\"edit\" checked >Edit ");
                    h.append("<input type=\"radio\" name=\"action\" value=\"delete\">Delete ");
                    h.append("<input type=\"submit\" name=\"submit\"value=\"Go\"></form>");
                }
                h.append("</dd>");
            }
            if (startedList) {
                h.append("</dl>");
            } else {
                h.append("<p class=\"notImportant\">There are no notices.</p>");
            }
            if (false) { // FIXME: change to !isAdmin
                h.append("<p class=\"button\">");
                h.append("<a href=\"/noticeeditor\" class=\"button\">Add a notice</a>");
                h.append("</p>");
            }
            results.close();
            getNewsFeed.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("NewsBean, getNewsFeed", sqlex);
            h.append(sqlex);
        }
        results = null;
        getNewsFeed = null;
        con = null;
        return h.toString();
    }
    
    /** 
     * Returns an HTML page for the user.
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (submitPressed) {
            if (validateData()) {
                if (writeToDatabase()) {
                    h.append("<p>Your notice has been saved.</p>");
                } else {
                    h.append("<p>Unable to save your notice to the database.</p>");
                }
                submitPressed = false;
                
            } else {
                h.append(getEditForm());
            }
        } else {
            h.append(getNoticeList());
            h.append(getEditForm());
        }
        return h.toString();
    }
    
    void enableAdmin() {
        isAdmin = true;
    }
    
    protected boolean validateData() {
        boolean valid = true;
        if (headline.length() == 0) {
            valid = false;
        }
        if (story.length() == 0) {
            valid = false;
        }
        return valid;
    }
    
    public boolean writeToDatabase() {
        Connection con;
        PreparedStatement saveNotice;
        boolean noticeSaved = false;
        try {
            con = dataSrc.getConnection();
            if (storyId > 0) {
                saveNotice = con.prepareStatement(sqlUpdateNotice);
                saveNotice.clearParameters();
                saveNotice.setInt(6, storyId);
            } else {
                saveNotice = con.prepareStatement(sqlAddNotice);
                saveNotice.clearParameters();
                saveNotice.setString(6, userName);
                saveNotice.setString(7, src);
            }
            saveNotice.setString(1, headline);
            saveNotice.setString(2, story);
            saveNotice.setString(3, site);
            saveNotice.setString(4, team);
            saveNotice.setTimestamp(5, new java.sql.Timestamp(expiryDate.getTimeInMillis()));
            if (saveNotice.executeUpdate() == 1) {
                noticeSaved = true;
            }
            saveNotice.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("NewsBean, writeToDatabase", sqlex);
        }
        saveNotice = null;
        con = null;
        return noticeSaved;
    }
    
    public String getEditForm() {
        StringBuilder h = new StringBuilder();
        DropDownBean teams = new DropDownBean("teams", "team");
        teams.allowNullValue(true);
        teams.setNotifyOnChange(true);
        DropDownBean office = new DropDownBean("sites", "office");
        office.allowNullValue(true);
        office.setNotifyOnChange(true);
        h.append("<form method=\"post\" action=\"notices\" name=\"notice\">\n");
        h.append("<table><tbody><tr>");
        // headline
        h.append("<tr><td class=\"grey\">Subject:</td><td><input type=\"text\" name=\"headline\" size=\"40\" maxlength=\"80\" value=\"");
        h.append(headline);
        h.append("\"");
        if (errors.indexOf("headline") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // story 
        h.append("<tr><td  class=\"grey\"style=\"vertical-align:top;\">Notice:</td>");
        h.append("<td><textarea cols=\"50\" rows=\"6\" wrap=\"soft\">");
        h.append(story);
        h.append("</textarea>\n");
        // audience
        h.append("<tr><td class=\"grey\">Audience:</td><td>");
        // audience = all
        h.append("<input type=\"radio\" name=\"audience\" value=\"all\"");
        if (audience.equalsIgnoreCase("all")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('all');\">All ");
        // audience = office
        h.append("<input type=\"radio\" name=\"audience\" value=\"office\"");
        if (audience.equalsIgnoreCase("office")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('office');\">Office ");
        // audience = team
        h.append("<input type=\"radio\" name=\"audience\" value=\"team\"");
        if (audience.equalsIgnoreCase("team")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('team');\">Team ");
        h.append("</td>");
        // site
        office.setSelectedValue(site);
        h.append("<tr><td class=\"grey\">Office:</td><td>");
        h.append(office.getSelectControl());
        h.append("</td></tr>\n");
        // team
        teams.setSelectedValue(team);
        h.append("<tr><td class=\"grey\">Team:</td><td>");
        h.append(teams.getSelectControl());
        h.append("</td></tr>\n");
        // buttons and hidden items
        h.append("<tr><td  class=\"grey\" colspan=\"2\" style=\"text-align:right;\">");
        h.append("<input type=\"hidden\" name=\"storynum\" value=\"" + storyId + "\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\"> ");
        h.append("<input type=\"reset\" value=\"Reset\" class=\"button\">\n");
        h.append("</td></tr></tbody></table></form>\n");
        // errors
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct the errors indicated.</strong></p>\n");
            if (errors.indexOf("headline") > -1) {
                h.append("<p>A subject must be included.</p>");
            }
        }
        return h.toString();
    }
    
    public String getNoticeList() {
        StringBuilder h = new StringBuilder();
        return h.toString();
    }
}

