/* 
 * @(#) Teams.java    0.1 2008/03/22
 *
 * A servlet to instantiate the TeamBean.
 * Copyright (C) 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
/**
 * Servlet to instantiate a TeamBean.
 *
 * @version		0.1     22 March 2008
 * @author		Steven Lilley
 */
public class Teams extends HttpServlet { 

	private ServletContext context;
    private Logger log;

	public void init() {
        log = Logger.getLogger("itres");
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        TeamBean teamBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session != null) {
                if (session.getAttribute("userIdentity") != null) {
                    userId = (IdentityBean)session.getAttribute("userIdentity");
                    if (session.getAttribute("teamBean") == null) {
                        teamBean = new TeamBean();
                        teamBean.setUserId(userId.getUserName());
                        session.setAttribute("teamBean", teamBean);
                    }
                }
            }
            handler = context.getRequestDispatcher("/teams.jsp");
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doGet(rqst, resp);
    }
}

