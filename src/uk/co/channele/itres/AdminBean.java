/* 
 * @(#) AdminBean.java    0.3 2008/06/21
 * 
 * Copyright (C) 2003-2008 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * A bean that presents and interface to allow the administator to access underlying tables and edit them.
 *
 * @version       0.3    21 June 2008
 * @author        Steven Lilley
 */
public class AdminBean extends GenericBean { 

    /** 
     * Creates a new AdminBean object.
     */
    public AdminBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("AdminBean constructor", nex);
        }
    }
    
    protected void clearValues() {
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    public String getEditForm() {
        return "";
    }
    
    public String getItem() {
        return "";
    }
    
    public String getList() {
        return "";
    }
    
    public String getSearchForm() {
        return "";
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getStandard() {
        return "";
    }
    
    protected boolean validateData() {
        return false;
    }
    
    protected boolean writeToDatabase() {
        return false;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
}

