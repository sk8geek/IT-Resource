/* 
 * @(#) SiteBean.java    0.1 2008/04/13
 *
 * A Bean to provide interaction with the Sites table.
 * Copyright (C) 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Bean to allow interaction with the Sites table.
 *
 * @version       0.1    22 March 2008
 * @author        Steven Lilley
 */
public class SiteBean extends GenericBean {
    
    private final String sqlGetSiteList = "SELECT * FROM sites ORDER BY listord";
    private final String sqlGetItem = "SELECT * FROM sites WHERE sitecode=? LIMIT 1";
    private final String sqlAddSite = "INSERT INTO sites (sitecode, sitename, sitetype, lat, lon, listord) VALUES (?, ?, ?, ?, ?, ?)";
    private final String sqlUpdateSite = "UPDATE sites SET sitecode=?, sitename=?, sitetype=?, lat=?, lon=?, listord=? WHERE sitecode=? LIMIT 1";
    private final String sqlDeleteSite = "DELETE FROM sites WHERE sitecode=? LIMIT 1";
    private final String sqlCheckUniqueCode = "SELECT COUNT(sitecode) AS qty FROM sites WHERE sitecode=?";
    private final String googleMapPrefix = "http://maps.google.co.uk/maps/ms?hl=en&gl=uk&ie=UTF8&oe=UTF8&msa=0&msid=105269384409824857593.000447c459d3cd1744144&ll=";
    private final String googleMapSuffix = "&spn=0.004748,0.020943&z=16";
    private String siteCode = "";
    private String siteName = "";
    private String siteType = "";
    private float mapLat = 0;
    private float mapLong = 0;
    private int listOrder = 127;

    public SiteBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("SiteBean constructor", nex);
        }
    }
    
    /**
     * Set the siteCode. The database is queried and if the code already 
     * exists the other object variables are set from the database. 
     * If the code does not exist the other object variables are cleared.
     * @param code the siteCode
     */
    public void setSiteCode(String code) {
        Connection con;
        PreparedStatement getItem;
        ResultSet result;
        siteCode = code;
        try {
            con = dataSrc.getConnection();
            getItem = con.prepareStatement(sqlGetItem);
            getItem.clearParameters();
            getItem.setString(1, siteCode);
            result = getItem.executeQuery();
            if (result.next()) {
                siteName = result.getString("sitename");
                siteType = result.getString("sitetype");
                mapLat = result.getFloat("lat");
                mapLong = result.getFloat("lon");
                listOrder = result.getInt("listord");
                newItem = false;
            } else {
                siteName = "";
                siteType = "";
                mapLat = 0;
                mapLong = 0;
                listOrder = 127;
                newItem = true;
            }
            result.close();
            getItem.close();
            con.close();
            itemSelected = true;
        } catch  (SQLException sqlex) {
            log.error("SiteBean: setSiteCode", sqlex);
        }
        result = null;
        con = null;
        getItem = null;
    }
    
    /**
     * Set the siteName.
     * @param name new value for siteName
     */
    public void setSiteName(String name) {
        siteName = name;
    }
    
    /**
     * Set the siteType.
     * @param type new value for siteType
     */
    public void setSiteType(String type) {
        siteType = type;
    }
    
    /**
     * Set the site latitude with a string value.
     * @param lat new value for latitude
     */
    public void setMapLat(String lat) {
        try {
            mapLat = Float.parseFloat(lat);
        } catch (NumberFormatException nfex) {
            log.error("SiteBean, setMapLat", nfex);
            mapLat = 0;
        }
    }
    
    /**
     * Set the site longitude with a string value.
     * @param lon new value for longitude
     */
    public void setMapLong(String lon) {
        try{
            mapLong = Float.parseFloat(lon);
            log.debug("SiteBean, setMapLong: mapLong=" + mapLong);
        } catch (NumberFormatException nfex) {
            log.error("SiteBean, setMapLong", nfex);
            mapLong = 0;
        }
    }
    
    /**
     * Set the list order value.
     * @param order the list order sort value
     */
    public void setListOrder(int order) {
        if (order < 0) {
            listOrder = 0;
        } else 
        if (order > 127) {
            listOrder = 127;
        } else {
            listOrder = order;
        }
    }
    
    protected String getEditForm() {
        StringBuilder h = new StringBuilder();
        h.append("<form method=\"post\" action=\"sites\" name=\"siteForm\">\n");            
        h.append("<table><tr><th>Code</th><td>");
        if (siteCode.length() > 0) {
            h.append(siteCode);
            h.append("<input type=\"hidden\" name=\"siteCode\" value=\"");
            h.append(siteCode);
            h.append("\">\n");
        } else {
            h.append("<input type=\"text\" name=\"siteCode\" ");
            h.append("size=\"8\" maxlength=\"8\" value=\"");
            h.append(siteCode);
            h.append("\"");
            if (errors.indexOf("code") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append(">");
        }
        h.append("</td></tr>\n");
        // site name
        h.append("<tr><th>Site</th><td>");
        h.append("<input type=\"text\" name=\"siteName\" ");
        h.append("size=\"20\" maxlength=\"40\" value=\"");
        h.append(siteName);
        h.append("\"></td></tr>\n");
        // site type
        h.append("<tr><th>Type</th><td>");
        h.append("<input type=\"text\" name=\"siteType\" ");
        h.append("size=\"8\" maxlength=\"8\" value=\"");
        h.append(siteType);
        h.append("\"></td></tr>\n");
        // map lat
        h.append("<tr><th>Latitude</th><td>");
        h.append("<input type=\"text\" name=\"mapLat\" ");
        h.append("size=\"9\" maxlength=\"9\" value=\"");
        h.append(mapLat);
        h.append("\"");
        if (errors.indexOf("lat") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // map long
        h.append("<tr><th>Longitude</th><td>");
        h.append("<input type=\"text\" name=\"mapLong\" ");
        h.append("size=\"9\" maxlength=\"9\" value=\"");
        h.append(mapLong);
        h.append("\"");
        if (errors.indexOf("long") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // list order
        h.append("<tr><th>Order</th><td>");
        h.append("<input type=\"text\" name=\"listOrder\" ");
        h.append("size=\"3\" maxlength=\"3\" value=\"");
        h.append(listOrder);
        h.append("\"");
        if (errors.indexOf("code") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // buttons
        h.append("<tr><td colspan=\"2\">");
        h.append("<input type=\"submit\" name=\"delete\" value=\"Delete\" class=\"button\" ");
        h.append("onclick=\"return(confirm('Are you sure?'))\">\n");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\" >\n");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\" >\n");
        h.append("</td></tr></table></form>\n");
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct the errors indicated.</strong></p>\n");
            if (errors.indexOf("qty") > -1) {
                h.append("<p>The site code is already in use.  Site codes must be unique.</p>\n");
            }
        }
        h.append("<a href=\"sites?siteCode=\" class=\"button\">List</a>");
        return h.toString();
    }
    
    protected String getItem() {
        StringBuilder h = new StringBuilder();
        h.append("<table><tr><th>Code</th><td>");
        h.append(siteCode);
        h.append("</td></tr>");
        h.append("<tr><th>Site</th><td>");
        h.append(siteName);
        h.append("</td></tr>");
        h.append("<tr><th>Type</th><td>");
        h.append(siteType);
        h.append("</td></tr>");
        h.append("<tr><th>Latitude</th><td>");
        h.append(mapLat);
        h.append("</td></tr>");
        h.append("<tr><th>Longitude</th><td>");
        h.append(mapLong);
        h.append("</td></tr>");
        h.append("<tr><th>Order</th><td>");
        h.append(listOrder);
        h.append("</td></tr></table>");
        h.append("<a href=\"sites?siteCode=\" class=\"button\">List</a>");
        return h.toString();
    }
    
    /**
     * Clear the Bean values.
     */
    protected void clearValues() {
        siteCode = "";
        siteName = "";
        siteType = "";
        mapLat = 0;
        mapLong = 0;
        listOrder = 127;
    }
    
    public String getStandard() {
        StringBuilder h = new StringBuilder();
        h.append(getList());
        if (isAdmin) {
            h.append("<p><a href=\"sites?action=add\" class=\"button\">Add New</a></p>");
        }
        return h.toString();
    }
    
    public String getSearchForm() {
        return "";
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getList() {
        Connection con;
        PreparedStatement getSiteList;
        ResultSet results;
        int recordCount = 0;
        StringBuilder h = new StringBuilder();
        boolean headingDone = false;
        try {
            con = dataSrc.getConnection();
            getSiteList = con.prepareStatement(sqlGetSiteList);
            results = getSiteList.executeQuery();
            while (results.next()) {
                if (!headingDone) {
                    h.append("<table><thead><tr><th>Code</th><th>Site</th>");
                    h.append("<th>Type</th><th>Map Link</th><th>Order</th></tr></thead>");
                    h.append("\n<tbody>");
                    headingDone = true;
                }
                h.append("<tr><td>");
                h.append("<a href=\"sites?siteCode=");
                h.append(results.getString("sitecode"));
                h.append("\">");
                h.append(results.getString("sitecode"));
                h.append("</td><td>");
                h.append(results.getString("sitename"));
                h.append("</td><td>");
                h.append(results.getString("sitetype"));
                h.append("</td><td>");
                if (results.getFloat("lat") > 0) {
                    h.append("<a href=\"");
                    h.append(googleMapPrefix);
                    h.append(results.getFloat("lat") + ",");
                    h.append(results.getFloat("lon"));
                    h.append(googleMapSuffix + "\">map</a>");
                } else {
                    h.append("&nbsp;");
                }
                h.append("</td><td>");
                h.append(results.getInt("listord"));
                h.append("</td></tr>\n");
            }
            if (headingDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p>No sites.</p>");
            }
            results.close();
            getSiteList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SiteBean: getStandard", sqlex);
        }
        results = null;
        getSiteList = null;
        con = null;
        return h.toString();
    }
    
    protected boolean validateData() {
        Connection con;
        PreparedStatement checkUniqueCode;
        ResultSet result;
        int recordCount = 0;
        boolean dataValid = true;
        StringBuilder errorList = new StringBuilder();
        errors = "";
        if (siteCode.length() == 0) {
            dataValid = false;
            errorList.append("code,");
        } else {
            try {
                con = dataSrc.getConnection();
                checkUniqueCode = con.prepareStatement(sqlCheckUniqueCode);
                checkUniqueCode.clearParameters();
                checkUniqueCode.setString(1, siteCode);
                result = checkUniqueCode.executeQuery();
                if (result.next()) {
                    if (result.getInt("qty") > 1) {
                        dataValid = false;
                        errorList.append("notUnique,");
                    }
                }
                result.close();
                checkUniqueCode.close();
                con.close();
            } catch (SQLException sqlex) {
                log.error("SiteBean: validateData", sqlex);
            }
        }
        if (siteName.length() < 3) {
            dataValid = false;
            errorList.append("name,");
        }
        if (mapLat != 0 && (mapLat < 53.654813 || mapLat > 53.815247)) {
            dataValid = false;
            errorList.append("lat,");
        }
        if (mapLong != 0 && (mapLong < -2.15538 || mapLong > -1.733093)) {
            dataValid = false;
            errorList.append("long,");
        }
        result = null;
        checkUniqueCode = null;
        con = null;
        errors = errorList.toString();
        return dataValid;
    }
    
    protected boolean writeToDatabase() {
        Connection con;
        PreparedStatement writeToDatabase;
        boolean writeSuccessful = false;
        try {
            con = dataSrc.getConnection();
            if (newItem) {
                writeToDatabase = con.prepareStatement(sqlAddSite);
                writeToDatabase.clearParameters();
            } else {
                writeToDatabase = con.prepareStatement(sqlUpdateSite);
                writeToDatabase.clearParameters();
                writeToDatabase.setString(7, siteCode);
            }
            writeToDatabase.setString(1, siteCode);
            writeToDatabase.setString(2, siteName);
            writeToDatabase.setString(3, siteType);
            writeToDatabase.setFloat(4, mapLat);
            writeToDatabase.setFloat(5, mapLong);
            writeToDatabase.setInt(6, listOrder);
            if (writeToDatabase.executeUpdate() == 1) {
                writeSuccessful = true;
                newItem = false;
            }
            writeToDatabase.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SiteBean: writeToDatabase", sqlex);
        }
        writeToDatabase = null;
        con = null;
        return writeSuccessful;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    protected boolean deleteFromDatabase() {
        Connection con;
        PreparedStatement deleteSite;
        int recordCount = 0;
        boolean deleteSuccessful = false;
        try {
            con = dataSrc.getConnection();
            deleteSite = con.prepareStatement(sqlDeleteSite);
            deleteSite.clearParameters();
            deleteSite.setString(1, siteCode);
            if (deleteSite.executeUpdate() == 1) {
                deleteSuccessful = true;
                newItem = true;
            }
            deleteSite.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SiteBean: deleteFromDatabase", sqlex);
        }
        deleteSite = null;
        con = null;
        return deleteSuccessful;
    }
    
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append(super.getDebug());
        h.append("<p style=\"font-size:xx-small;\"><strong>SiteBean values</strong></p>");
        h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
        h.append("<li>siteCode = " + siteCode);
        h.append("<li>siteName = " + siteName);
        h.append("<li>siteType = " + siteType);
        h.append("<li>mapLat =" + mapLat);
        h.append("<li>mapLong =" + mapLong);
        h.append("<li>listOrder= " + listOrder);
        h.append("</ul>");
        return h.toString();
    }
}

