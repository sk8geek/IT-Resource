/* 
 * @(#) DocObject.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import uk.co.channele.itres.DocObject;
import java.util.Date;
import java.util.Calendar;
/**
 * Part of my IT Resource suite of servlets.
 *
 * @version		0.1 6 February 2004
 * @author		Steven Lilley
 */
public class DocObject implements Comparable{

	public String fileName;
	public String docTitle;
	public java.util.Date docModified;

	DocObject(String fileName) {
		this.fileName = fileName;
		StringBuffer docT = new StringBuffer();
		if (fileName.lastIndexOf("-") > 0) {
			docT.append(fileName.substring(0,fileName.lastIndexOf("-")));
			int separator = fileName.lastIndexOf("-");
			int year = Integer.parseInt(fileName.substring((separator+1), 
				(separator+5)));
			int month = Integer.parseInt(fileName.substring((separator+5), 
				(separator+7))) - 1;
			int date = Integer.parseInt(fileName.substring((separator+7), 
				(separator+9)));
			Calendar c = Calendar.getInstance();
			c.set(year, month, date);
			docModified = c.getTime();
		} else {
			docT .append(fileName.substring(0, fileName.lastIndexOf(".")));
		}
		while (docT.toString().indexOf("_") > -1) {
			docT.setCharAt(docT.toString().indexOf("_"),' ');
		}
		docTitle = docT.toString();
	}
	
	public int compareTo(Object o) {
		return compareTo((DocObject)o);
	}
	
	public int compareTo(DocObject compTo) {
		return this.docModified.compareTo(compTo.docModified);
	}
}
