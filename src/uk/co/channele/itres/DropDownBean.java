/* 
 * @(#) DropDownBean.java    0.3.1 2012/01/17
 * 
 * Copyright (C) 2006 Steven J Lilley
 * Copyright (C) 2012 Calderdale Council
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

// TODO: add features from cyril bean
// TODO: if source table is empty then the select control should return a message
// TODO: make GPL3

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.DropDownItem;
/**
 * Gets a Drop Down list from the DropDownStoreBean.
 *
 * @version       0.3.1    17 January 2012
 * @author        Steven Lilley
 */
public class DropDownBean implements Serializable { 

    final SimpleDateFormat mysqlDate = new SimpleDateFormat("yyyy-MM-dd");
    final SimpleDateFormat mysqlDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    final SimpleDateFormat itemDay = new SimpleDateFormat("EEEE");
    final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    private InitialContext initCntx;
    private Context cntx;
    private DataSource dataSrc;
    private ArrayList<DropDownItem> dropDownList;
    private String tableName;
    private String controlName = "";
    private String homeValue = "";
    private String selectedValue = "";
    private boolean allowMultipleChoice = false;
    private boolean allowNullValue = false;
    private boolean autoSubmit = false;
    private boolean notifyOnChange = false;
    private boolean returnHumanValues = false;
    private boolean listPopulated = false;
    private Logger log;
    
    public DropDownBean() {
        log = Logger.getLogger("itres");
        dropDownList = new ArrayList<DropDownItem>();
        try {
            initCntx = new InitialContext();
            cntx = (Context)initCntx.lookup("java:comp/env");
            dataSrc = (DataSource)cntx.lookup("jdbc/itres");
        } catch (NamingException nex) {
            System.out.println("DropDownBean: makeDropDownList: " + nex);
        }
    }
    
    public DropDownBean(String home) {
        this();
        setHomeValue(home);
    }
    
    public DropDownBean(String tableName, String controlName) {
        this();
        setTableName(tableName);
        setControlName(controlName);
    }
    
    public DropDownBean(String tableName, String control, String selected) {
        this();
        setTableName(tableName);
        controlName = control;
        selectedValue = selected;
    }
    
    public void setTableName(String table) {
        tableName = table;
        if (!listPopulated) {
            populateDropDownList();
            listPopulated = true;
        }
    }
    
    public void setControlName(String name) {
        controlName = name;
    }
    
    public void setHomeValue(String home) {
        homeValue = home;
        selectedValue = home;
    }
    
    public void setSelectedValue(String currentValue) {
        selectedValue = currentValue;
    }
    
    public void setHumanValues(boolean forHumans) {
        returnHumanValues = forHumans;
    }
    
    public void setGoHome() {
        selectedValue = homeValue;
    }
    
    public void setAllowNullValue(String allowNull) {
        if (allowNull.equalsIgnoreCase("true")) {
            allowNullValue = true;
        } else {
            allowNullValue = false;
        }
    }
    
    void allowNullValue(boolean allowNull) {
        allowNullValue = allowNull;
    }
    
    void setNotifyOnChange(boolean notify) {
        notifyOnChange = notify;
        if (notifyOnChange) {
            autoSubmit = false;
        }
    }
    
    public String getSelectControl() {
        StringBuilder h = new StringBuilder();
        int i = 0;
        h.append("<select name=\"");
        h.append(controlName);
        h.append("\"");
        if (autoSubmit) {
            h.append(" onChange=\"submit();\"");
        } 
        if (notifyOnChange) {
            h.append(" onChange=\"dropDownChanged('" + controlName + "', this.value);\"");
        }
        h.append(">");
        if (allowNullValue) {
            h.append("<option value=\"\"");
            if (selectedValue.length() == 0 || selectedValue == null) {
                h.append(" selected");
            }
            h.append(">");
        }
        for (i = 0; i < dropDownList.size(); i++) {
            DropDownItem listValue = (DropDownItem)dropDownList.get(i);
            h.append("<option value=\"");
            if (returnHumanValues) {
                h.append(listValue.desc);
            } else {
                h.append(listValue.code);
            }
            h.append("\"");
            if (selectedValue.length() > 0) {
                if (listValue.code.equalsIgnoreCase(selectedValue) ) {
                    h.append(" selected");
                }
            }
            h.append(">" + listValue.desc + "</option>\n");
        }
        h.append("</select>");
        return h.toString();
    }
    
    public String getReturningSelectControl() {
        StringBuilder h = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        int dayCounter = 0;
        if (cal.get(Calendar.HOUR_OF_DAY) > 15) {
            selectedValue = "Tomorrow";
        }
        h.append("<select name=\"returning\">");
        cal.add(Calendar.MINUTE, 30);
        h.append(addOption(mysqlDateTime.format(cal.getTime()), "Soon"));
        cal.add(Calendar.MINUTE, 30);
        cal.set(Calendar.MINUTE, 0);
        if (cal.get(Calendar.HOUR_OF_DAY) < 16) {
            if (cal.get(Calendar.HOUR_OF_DAY) < 8) {
                cal.set(Calendar.HOUR_OF_DAY, 8);
            }
            do {
                cal.add(Calendar.HOUR_OF_DAY, 1);
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemTime.format(cal.getTime())));
            } while (cal.get(Calendar.HOUR_OF_DAY) < 18);
        }
        cal.set(Calendar.HOUR_OF_DAY, 1);
        // insert tomorrow as a special value
        if (cal.get(Calendar.DAY_OF_WEEK) >= Calendar.MONDAY && cal.get(Calendar.DAY_OF_WEEK) <= Calendar.THURSDAY) {
            cal.add(Calendar.DATE, 1);
            h.append(addOption(mysqlDateTime.format(cal.getTime()), "Tomorrow"));
        } else {
            if (selectedValue.equalsIgnoreCase("Tomorrow")) {
                selectedValue = "Monday";
            }
        }
        do {
            cal.add(Calendar.DATE, 1);
            dayCounter++;
            while (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                cal.add(Calendar.DATE, 1);
                dayCounter++;
            }
            if (dayCounter < 7) {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDay.format(cal.getTime())));
            } else {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDate.format(cal.getTime())));
            }
        } while (!(dayCounter > 16 && cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY));
        h.append("</select>");
        return h.toString();
    }
    
    private String addOption(String value, String text) {
        StringBuilder h = new StringBuilder();
        h.append("<option value=\"");
        h.append(value);
        h.append("\"");
        if (selectedValue.length() > 0) {
            if (selectedValue.equals(value) || selectedValue.equals(text)) {
                h.append(" selected");
            }
        }
        h.append(">");
        h.append(text);
        h.append("</option>\n");
        return h.toString();
    }
    
    private void populateDropDownList() {
        Connection con;
        PreparedStatement getTableValues;
        ResultSet results;
        String sqlGetTableValues;
        try {
            con = dataSrc.getConnection();
            sqlGetTableValues = "SELECT * FROM " + tableName + " ORDER BY listord";
            getTableValues = con.prepareStatement(sqlGetTableValues);
            results = getTableValues.executeQuery();
            while (results.next()) {
                dropDownList.add(new DropDownItem(results.getString(1), results.getString(2)));
            }
            dropDownList.trimToSize();
            results.close();
            getTableValues.close();
            con.close();
            listPopulated = true;
        } catch (SQLException sqlex) {
            System.out.println("DropDownBean: makeDropDownList: " + sqlex);
        }
        results = null;
        getTableValues = null;
        con = null;
    }
}

