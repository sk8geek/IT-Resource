/* 
 * @(#) TeamBean.java    0.1 2008/03/22
 *
 * A Bean to provide interaction with the Teams table.
 * Copyright (C) 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Bean to allow interaction with the Teams table.
 *
 * @version       0.1    22 March 2008
 * @author        Steven Lilley
 */
public class TeamBean extends GenericBean {
    
    private final String sqlGetTeamList = "SELECT * FROM teams ORDER BY teamname";
    private final String sqlGetItem = "SELECT * FROM teams WHERE tcode=? LIMIT 1";
    private final String sqlAddTeam = "INSERT INTO teams (tcode, teamname, listord) VALUES (?, ?, ?)";
    private final String sqlUpdateTeam = "UPDATE teams SET tcode=?, teamname=?, listord=? WHERE tcode=? LIMIT 1";
    private final String sqlDeleteTeam = "DELETE FROM teams WHERE tcode=? LIMIT 1";
    private final String sqlCheckUniqueCode = "SELECT COUNT(tcode) AS qty FROM teams WHERE tcode=?";
    private String teamCode = "";
    private String teamName = "";
    private int listOrder = 127;

    public TeamBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("TeamBean constructor", nex);
        }
    }
    
    /**
     * Set the teamCode. The database is queried and if the code already 
     * exists the other object variables are set from the database. 
     * If the code does not exist the other object variables are cleared.
     * @param code the teamCode
     */
    public void setTeamCode(String code) {
        Connection con;
        PreparedStatement getItem;
        ResultSet result;
        teamCode = code;
        try {
            con = dataSrc.getConnection();
            getItem = con.prepareStatement(sqlGetItem);
            getItem.clearParameters();
            getItem.setString(1, teamCode);
            result = getItem.executeQuery();
            if (result.next()) {
                teamName = result.getString("teamname");
                listOrder = result.getInt("listord");
                newItem = false;
            } else {
                teamName = "";
                listOrder = 127;
                newItem = true;
            }
            result.close();
            getItem.close();
            con.close();
            itemSelected = true;
        } catch  (SQLException sqlex) {
            log.error("TeamBean: setTeamCode", sqlex);
        }
        result = null;
        con = null;
        getItem = null;
    }
    
    /**
     * Set the teamName.
     * @param name new value for teamName
     */
    public void setTeamName(String name) {
        teamName = name;
    }
    
    /**
     * Set the list order value.
     * @param order the list order sort value
     */
    public void setListOrder(int order) {
        if (order < 0) {
            listOrder = 0;
        } else 
        if (order > 127) {
            listOrder = 127;
        } else {
            listOrder = order;
        }
    }
    
    protected String getEditForm() {
        StringBuilder h = new StringBuilder();
        h.append("<form method=\"post\" action=\"teams\" name=\"teamForm\">\n");            
        h.append("<table><tr><th>Code</th><td>");
        if (teamCode.length() > 0) {
            h.append(teamCode);
            h.append("<input type=\"hidden\" name=\"teamCode\" value=\"");
            h.append(teamCode);
            h.append("\">\n");
        } else {
            h.append("<input type=\"text\" name=\"teamCode\" ");
            h.append("size=\"8\" maxlength=\"8\" value=\"");
            h.append(teamCode);
            h.append("\"");
            if (errors.indexOf("code") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append(">");
        }
        h.append("</td></tr>\n");
        h.append("<tr><th>Team</th><td>");
        h.append("<input type=\"text\" name=\"teamName\" ");
        h.append("size=\"20\" maxlength=\"40\" value=\"");
        h.append(teamName);
        h.append("\"></td></tr>\n");
        h.append("<tr><th>Order</th><td>");
        h.append("<input type=\"text\" name=\"listOrder\" ");
        h.append("size=\"3\" maxlength=\"3\" value=\"");
        h.append(listOrder);
        h.append("\"");
        if (errors.indexOf("code") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        h.append("<tr><td colspan=\"2\">");
        h.append("<input type=\"submit\" name=\"delete\" value=\"Delete\" class=\"button\" ");
        h.append("onclick=\"return(confirm('Are you sure?'))\">\n");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\" >\n");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\" >\n");
        h.append("</td></tr></table></form>\n");
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct the errors indicated.</p>\n");
            if (errors.indexOf("qty") > -1) {
                h.append("<p>The team code is already in use.  Team codes must be unique.</p>\n");
            }
        }
        h.append("<a href=\"teams?teamCode=\" class=\"button\">List</a>");
        return h.toString();
    }
    
    protected String getItem() {
        StringBuilder h = new StringBuilder();
        h.append("<table><tr><th>Code</th><td>");
        h.append(teamCode);
        h.append("</td></tr>");
        h.append("<tr><th>Team</th><td>");
        h.append(teamName);
        h.append("</td></tr>");
        h.append("<tr><th>Order</th><td>");
        h.append(listOrder);
        h.append("</td></tr></table>");
        h.append("<a href=\"teams?teamCode=\" class=\"button\">List</a>");
        return h.toString();
    }
    
    /**
     * Clear the Bean values.
     */
    protected void clearValues() {
        teamCode = "";
        teamName = "";
        listOrder = 127;
    }
    
    public String getStandard() {
        StringBuilder h = new StringBuilder();
        h.append(getList());
        if (isAdmin) {
            h.append("<p><a href=\"teams?action=add\" class=\"button\">Add New</a></p>");
        }
        return h.toString();
    }
    
    public String getSearchForm() {
        return "";
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getList() {
        Connection con;
        PreparedStatement getTeamList;
        ResultSet results;
        int recordCount = 0;
        StringBuilder h = new StringBuilder();
        boolean headingDone = false;
        try {
            con = dataSrc.getConnection();
            getTeamList = con.prepareStatement(sqlGetTeamList);
            results = getTeamList.executeQuery();
            while (results.next()) {
                if (!headingDone) {
                    h.append("<table><thead><tr><th>Code</th><th>Team</th>");
                    h.append("<th>Order</th></tr></thead>");
                    h.append("\n<tbody>");
                    headingDone = true;
                }
                h.append("<tr><td>");
                h.append("<a href=\"teams?teamCode=");
                h.append(results.getString("tcode"));
                h.append("\">");
                h.append(results.getString("tcode"));
                h.append("</td><td>");
                h.append(results.getString("teamname"));
                h.append("</td><td>");
                h.append(results.getInt("listord"));
                h.append("</td></tr>\n");
            }
            if (headingDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p>No teams.</p>");
            }
            results.close();
            getTeamList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("TeamBean: getStandard", sqlex);
        }
        results = null;
        getTeamList = null;
        con = null;
        return h.toString();
    }
    
    protected boolean validateData() {
        Connection con;
        PreparedStatement checkUniqueCode;
        ResultSet result;
        int recordCount = 0;
        boolean dataValid = true;
        StringBuilder errorList = new StringBuilder();
        errors = "";
        if (teamCode.length() == 0) {
            dataValid = false;
            errorList.append("code,");
        } else {
            try {
                con = dataSrc.getConnection();
                checkUniqueCode = con.prepareStatement(sqlCheckUniqueCode);
                checkUniqueCode.clearParameters();
                checkUniqueCode.setString(1, teamCode);
                result = checkUniqueCode.executeQuery();
                if (result.next()) {
                    if (result.getInt("qty") > 1) {
                        dataValid = false;
                        errorList.append("notUnique,");
                    }
                }
                result.close();
                checkUniqueCode.close();
                con.close();
            } catch (SQLException sqlex) {
                log.error("TeamBean: validateData", sqlex);
            }
        }
        if (teamName.length() < 3) {
            dataValid = false;
            errorList.append("name,");
        }
        result = null;
        checkUniqueCode = null;
        con = null;
        errors = errorList.toString();
        return dataValid;
    }
    
    protected boolean writeToDatabase() {
        Connection con;
        PreparedStatement writeToDatabase;
        boolean writeSuccessful = false;
        try {
            con = dataSrc.getConnection();
            if (newItem) {
                writeToDatabase = con.prepareStatement(sqlAddTeam);
                writeToDatabase.clearParameters();
            } else {
                writeToDatabase = con.prepareStatement(sqlUpdateTeam);
                writeToDatabase.clearParameters();
                writeToDatabase.setString(4, teamCode);
            }
            writeToDatabase.setString(1, teamCode);
            writeToDatabase.setString(2, teamName);
            writeToDatabase.setInt(3, listOrder);
            if (writeToDatabase.executeUpdate() == 1) {
                writeSuccessful = true;
                newItem = false;
            }
            writeToDatabase.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("TeamBean: writeToDatabase", sqlex);
        }
        writeToDatabase = null;
        con = null;
        return writeSuccessful;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    protected boolean deleteFromDatabase() {
        Connection con;
        PreparedStatement deleteTeam;
        int recordCount = 0;
        boolean deleteSuccessful = false;
        try {
            con = dataSrc.getConnection();
            deleteTeam = con.prepareStatement(sqlDeleteTeam);
            deleteTeam.clearParameters();
            deleteTeam.setString(1, teamCode);
            if (deleteTeam.executeUpdate() == 1) {
                deleteSuccessful = true;
                newItem = true;
            }
            deleteTeam.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("TeamBean: deleteFromDatabase", sqlex);
        }
        deleteTeam = null;
        con = null;
        return deleteSuccessful;
    }
    
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append(super.getDebug());
        h.append("<p style=\"font-size:xx-small;\"><strong>TeamBean values</strong></p>");
        h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
        h.append("<li>teamCode = " + teamCode);
        h.append("<li>teamName = " + teamName);
        h.append("<li>listOrder= " + listOrder);
        h.append("</ul>");
        return h.toString();
    }
}

