/* 
 * @(#) WebFaultBean.java    0.1 2006/10/14
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * Stores reported web faults.
 *
 * @version       0.1    14 October 2006
 * @author        Steven Lilley
 */
public class WebFaultBean implements Serializable { 

    private final String sqlRecordWebFault = "INSERT INTO webfaultlog VALUES (?, ?, ?, ?)";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String host = "";
    private String fault = "";
    private String other = "";
    private Logger log;
    
    public WebFaultBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("WebFaultBean constructor", nex);
        }
    }
    
    public void setRemoteHost(String remoteHost) {
        host = remoteHost;
    }
    
    public void setWebFault(String newFault) {
        fault = newFault;
    }
    
    public void setOther(String otherFault) {
        other = otherFault;
    }
    
    public String getRecordWebFault() {
        StringBuilder h = new StringBuilder();
        h.append("<p>Thank you for reporting this fault.</p>");
        Connection con;
        PreparedStatement recordWebFault;
        try {
            con = dataSrc.getConnection();
            recordWebFault = con.prepareStatement(sqlRecordWebFault);
            recordWebFault.clearParameters();
            recordWebFault.setString(1, host);
            recordWebFault.setString(2, fault);
            recordWebFault.setString(3, other);
            recordWebFault.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            if (recordWebFault.executeUpdate() == 1) {
                h.append("<p>The fault has been successfully recorded.</p>");
            } else {
                h.append("<p>Unfortunately a problem with the database has ");
                h.append("prevented this fault from being recorded.</p>");
            }
            con.close();
        } catch (SQLException sqlex) {
            log.error("WebFaultBean, getRecordWebFault", sqlex);
            h.append("<p>An database error occurred:<br>" + sqlex + "</p>");
        }
        con = null;
        return h.toString();
    }
    
    void setHost(String userHost) {
        host = userHost;
    }
}

