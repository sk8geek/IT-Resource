/* 
 * @(#) HouseKeeper.java    3.0 2008/03/21
 * 
 * Copyright (C) 2003-2008 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package uk.co.channele.itres;

import java.beans.Beans;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
/**
 * Controls the running of regular jobs, such as checking services and 
 * updating tables.
 *
 * @version       3.0   21 March 2008
 * @author        Steven Lilley
 */
public class HouseKeeper extends HttpServlet implements Runnable {

    static final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    static final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    static final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    /** A simple date format used for hardware cover start/end dates */
    static final private SimpleDateFormat coverDate = new SimpleDateFormat("MMM yy");
    static final DecimalFormat currency = new DecimalFormat("##,##0.00");
    static final int errorDelay = 20;
    static final int secureDelay = 300;
    static final int normalDelay = 600;
    /** 
     * Query to get a resource of the specified type.
     * The table can hold multiple instances of each service.  
     * Each instance of a service should have a unique <tt>priority</tt> 
     * value.  This allows alternative service providers to be selected.
     */
    static final String sqlGetResourceOfType = "SELECT * FROM resources WHERE service = ? AND priority > ? ORDER BY priority LIMIT 1";
    private static String staticURL;
    private static String imageFilePath;
    private static InternetAddress sysAdmin;
    // Timer identifiers
    private final int timer_testServices = 1;
    private final int timer_workerMaintenance = 2;
    private final int timer_expireNews = 4;
    private final int timer_makeLists = 5;
    private final int timer_calcStats = 6;
    private final int timer_feedback = 7;
    private final int timer_librarian = 9;
    /** Query to get chores that are due to be done. */
    private final String sqlGetDueChores = 
        "SELECT choreid FROM chores WHERE state = -1 "
        + "AND DATE_ADD(lastrun, INTERVAL latency SECOND) < NOW()";
    /** Query to get a list of services to check. */
    private final String sqlGetServicesToCheck = 
        "SELECT sid, hostname, portnum, status FROM services WHERE last_check=null OR "
        + "(status='OKAY' AND DATE_ADD(last_check, INTERVAL check_freq SECOND)<NOW()) OR "
        + "(status='DOWN' AND DATE_ADD(last_check, INTERVAL fail_freq SECOND)<NOW()) ORDER BY sid";
    /** Query to get a list ALL services to check (forced). */
    private final String sqlGetAllServices = "SELECT sid, hostname, portnum, status FROM services ORDER BY sid";
    /** Query to update the server status and check time/date. */
    private final String sqlUpdateServiceStatus = "UPDATE services SET status=?, last_check=NOW() WHERE "
        + "sid=? LIMIT 1";
    /** Query to get list of admin who want to know this servers status. */
    private final String sql_getInterestedAdmins = 
        "SELECT services.sid, cname, mail_addr FROM services "
        + "INNER JOIN notify ON services.sid = notify.sid WHERE "
        + "services.sid = ?";
    /** Query to mark workers as UNKNOWN presence. */
    private final String sqlWorkerCheckIn = 
        "UPDATE register SET presence = 0 WHERE UNIX_TIMESTAMP(returning) < UNIX_TIMESTAMP(NOW()) OR returning IS NULL";
    /** Query to expire long term non-users. */
    private final String sqlExpireWorkers = 
        "DELETE FROM register WHERE lastmod < DATE_SUB(NOW(), INTERVAL 1 MONTH)";
    /** Query to add distinguished names to users table. */
    private final String sql_addDN = "INSERT IGNORE INTO users (dn) VALUES (?)";
    /** Query to expire old news items. */
    private final String sqlExpireOldNews = "DELETE FROM notices WHERE expires < NOW()";
    /** Update to expire service issues. */
    private final String sqlExpireServiceIssues = "UPDATE services SET issue = NULL WHERE issexpire < NOW()";
    /** Query to get street wardens reports. */
    private final String sql_getWardenReports = "SELECT * FROM reports ORDER BY warden";
    /** Query to update a chore's last run time. */
    private final String sql_setLastRunTime = 
        "UPDATE chores SET lastrun = NOW() WHERE choreid = ? LIMIT 1";
    private final String sqlGetNewUsers = "SELECT dn FROM users WHERE query=0";
    private final String sqlGetUserDNs = "SELECT * FROM users WHERE src='n'";
    private final String sqlUpdateUserDetails = "UPDATE users SET given = ?, "
        + "surname = ?, lid = ?, title = ?, email = ?, phone = ?, "
        + "fax = ?, location = ?, query = ?, fullname = ?, src = 'n' WHERE dn = ? LIMIT 1";
    private final String sqlExpireUser = "DELETE FROM users WHERE dn = ? LIMIT 1";
    private final String sqlRecordStateChange = "INSERT INTO service_history VALUES (?, ?, ?)";
    private final String sqlFindNewHardware = "SELECT device_id, device_desc, location_name, section_name, "
        + "fl_maint_code, purchase_price, purchase_date, installed_date, warranty_ex_date, 7 FROM "
        + "hd_v_devices WHERE dept_code=\"L\" AND device_id > ?";
    private Calendar nextAutoCheckin = Calendar.getInstance();
    private File watchdogFlag;
    private Thread watchdog;
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String wardenDB;
    private String LDAPBaseDomain;
    private Resource LDAPServer;
    private SystemsStatusBean systemsStatus;
    private Logger log;
    private boolean firstRun = true;
    
    public void init() {
        context = getServletContext();
        log = Logger.getLogger("itres");
        staticURL = context.getInitParameter("staticURL");
        imageFilePath = context.getInitParameter("imageFiles");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
            String sysAdminAddress = context.getInitParameter("sysAdmin");
            sysAdmin = new InternetAddress(sysAdminAddress);
            systemsStatus = new SystemsStatusBean();
            context.setAttribute("serviceStatus", systemsStatus);
        } catch (AddressException aex) {
            log.warn("Invalid email address for system administrator.", aex);
        } catch (NamingException nex) {
            log.fatal("Naming exception, unable to find data source.", nex);
        }
        try { 
            watchdogFlag = new File(System.getProperty("java.io.tmpdir") + "/itres_HouseKeeper");
            if (watchdogFlag.createNewFile()) {
                watchdog = new Thread(this);
                watchdog.setPriority(Thread.MIN_PRIORITY);
                watchdog.start();
                log.info("HouseKeeper started doing chores.");
            }
        } catch (IOException ioex) {
            log.error("Unable to create flag (file) for itres Housekeeper.", ioex);
        }
    }

    public void run() {
        Connection con;
        PreparedStatement getDueChores;
        ResultSet results;
        try {
            while (watchdogFlag.exists()) {
                con = dataSrc.getConnection();
                if (firstRun) {
                    // services to be run immediately
                    testServices(true);
                    firstRun = false;
                }
                getDueChores = con.prepareStatement(sqlGetDueChores);
                results = getDueChores.executeQuery();
                while (results.next()) {
                    switch (results.getInt("choreid")) {
                        case  1 : testServices(false); break;
                        case  2 : workerMaintenance(false); break;
                        case  4 : expireNews(false); break;
                        case  6 : calcStats(false); break;
                        case  9 : maintainLibrary(false); break;
                    }
                }
                results.close();
                getDueChores.close();
                con.close();
                watchdog.sleep(60000);
            }
        } catch (InterruptedException iex) {
            log.info("HouseKeeper, run caught an interrupt exception");
        } catch (SQLException sqlex) {
            log.error("HouseKeeper, run", sqlex);
        }
        results = null;
        getDueChores = null;
        con = null;
    }
    
    public void destroy() {
        watchdogFlag.delete();
        log.info("HouseKeeper stopped doing chores.");
    }
    
    /**
     * Run the specified chore immediately.
     * @param rqst HTTP servlet request
     * @param resp HTTP servlet response
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        int choreID = 0;
        if (rqst.getAttribute("runChore") != null) {
            choreID = ((Integer)rqst.getAttribute("runChore")).intValue();
        }
        switch (choreID) {
            case  1 : testServices(true); break;
            case  2 : workerMaintenance(true); break;
            case  4 : expireNews(true); break;
            case  6 : calcStats(true); break;
            case  9 : maintainLibrary(true); break;
        }
    }
    
    /** 
     * Update lastrun field for a service.
     * @param choreid the number of the chore that has been run
     */
    private void setLastRunTime(int choreid) {
        Connection con;
        PreparedStatement setLastRunTime;
        try {
            con = dataSrc.getConnection();
            setLastRunTime = con.prepareStatement(sql_setLastRunTime);
            setLastRunTime.setInt(1, choreid);
            if (setLastRunTime.executeUpdate() != 1) {
                log.warn("HouseKeeper failed to set run time "
                    + "for chore " + Integer.toString(choreid) + ".");
            }
            setLastRunTime.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("HouseKeeper, setLastRunTime", sqlex);
        }
        setLastRunTime = null;
        con = null;
    }

    /** 
     * Check each server in <tt>services</tt>, record status changes.
     * The check is an attempt to open a TCP/IP connection to the given port.
     * Status changes are recorded in the <tt>service_history</tt> table.
     * If a server is listed in the <tt>notify</tt> table then the status 
     * change is also sent to the given email address.
     * 
     */
    private void testServices(boolean forcedRun) {
        Connection con1;
        Connection con2;
        PreparedStatement getServicesToCheck;
        PreparedStatement updateServiceStatus;
        ResultSet results;
        String previousState;
        String currentState;
        synchronized (this) {
            int failedServiceCount = 0;
            try {
                con1 = dataSrc.getConnection();
                con2 = dataSrc.getConnection();
                if (forcedRun) {
                    log.debug("FORCED run of testServices");
                    getServicesToCheck = con1.prepareStatement(sqlGetAllServices);
                } else {
                    log.debug("Scheduled run of testServices");
                    getServicesToCheck = con1.prepareStatement(sqlGetServicesToCheck);
                }
                updateServiceStatus = con2.prepareStatement(sqlUpdateServiceStatus);
                results = getServicesToCheck.executeQuery();
                while (results.next()) {
                    previousState = results.getString("status");
                    if (testService(results.getString("hostname"), results.getInt("portnum"))) {
                        currentState = "OKAY";
                    } else {
                        currentState = "DOWN";
                        failedServiceCount++;
                    }
                    updateServiceStatus.clearParameters();
                    updateServiceStatus.setString(1, currentState);
                    updateServiceStatus.setInt(2, results.getInt("sid"));
                    if (updateServiceStatus.executeUpdate() != 1) {
                        log.warn("Unable to set status for service ID " + results.getInt("sid"));
                    }
                    if (!previousState.equalsIgnoreCase(currentState)) {
                        notifyAdmins(results.getInt("sid"), previousState, currentState);
                    }
                }
                results.close();
                getServicesToCheck.close();
                updateServiceStatus.close();
                con2.close();
                con1.close();
                context.setAttribute("lastScanTime", itemTime.format((new Date())));
                setServiceSummary(failedServiceCount);
                systemsStatus.updateSystemStatus();
                setLastRunTime(timer_testServices);
            } catch (SQLException sqlex) {
                log.error("HouseKeeper, testServices", sqlex);
            }
            results = null;
            getServicesToCheck = null;
            updateServiceStatus = null;
            con2 = null;
            con1 = null;
        } // end sync
    }

    /**
     * Set the context wide summary of services. 
     * @param failedServiceCount number of services that are down
     */
    private void setServiceSummary(int failedServiceCount) {
        if (failedServiceCount == 0) {
//            context.setAttribute("serviceSummary", "All <a href=\"servicestatus\">monitored systems</a> are running.");
        } else 
        if (failedServiceCount == 1) {
//            context.setAttribute("serviceSummary", "1 system is down.");
        } else {
//            context.setAttribute("serviceSummary", new Integer(failedServiceCount).toString() + " systems are down.");
        }
    }

    /** 
     * Attempt to open a TCP/IP connection to the given host/port.
     * This is our most basic method of testing server availability.
     * @param host the (resolvable) host name or IP address
     * @param port the port number to attempt connecting to
     * @return true if the connection was successful, false otherwise
     */
    private boolean testService(String host, int port) {
        try {
            Socket testSocket = new Socket(host, port);
            testSocket.close();
            return true;
        } catch (UnknownHostException uhex) {
            log.debug("testService reports " + host + " as unknown.");
            return false;
        } catch (IOException ioex) {
            log.debug("testService gets an IOEexception on " + host);
            return false;
        }
    }
     
    /**
     * Send an email to registered address giving current server status.
     */
    private void notifyAdmins(int serviceID, String previousState, String currentState) {
        log.debug("HouseKeeper,notifyAdmins called with:" + serviceID + " from " + previousState +
            " to " + currentState);
        String subjectLine = "";
        String messageBody = "";
        Connection con;
        PreparedStatement getInterestedAdmins;
        PreparedStatement recordStateChange;
        ResultSet results;
        try {
            con = dataSrc.getConnection();
            getInterestedAdmins = con.prepareStatement(sql_getInterestedAdmins);
            getInterestedAdmins.clearParameters(); // DEBUG: added 5 Oct 2011
            getInterestedAdmins.setInt(1, serviceID);
            results = getInterestedAdmins.executeQuery();
            while (results.next()) {
                subjectLine = "SYSMON: " + results.getString("cname");
                messageBody = "Status change from: " + previousState + " to " + currentState 
                    + "\nat " + itemDateTime.format(new Date());
                if (!sendMail(results.getString("mail_addr"), subjectLine, messageBody)) {
                    log.warn("Failed to send status change message to " + results.getString("mail_addr"));
                }
            }
            getInterestedAdmins.close();
            recordStateChange = con.prepareStatement(sqlRecordStateChange);
            recordStateChange.clearParameters();
            recordStateChange.setInt(1, serviceID);
            recordStateChange.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            recordStateChange.setString(3, currentState);
            recordStateChange.executeUpdate();
            results.close();
            recordStateChange.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("HouseKeeper, notifyAdmins", sqlex);
        }
        getInterestedAdmins = null;
        results = null;
        recordStateChange = null;
        con = null;
    }
    
    /** 
     * Check workers back in in the early hours of the morning.
     * This method performs two functions:
     * 1. Where users are checked out they are checked back in.  
     *    (Unless the user specified not to be)
     * 2. Users who haven't used the system for a month are deleted.
     */
    private void workerMaintenance(boolean forcedRun) {
        // don't run this task unless the time is between 1am and 2am.
        if (!hourMatches(3) && !forcedRun) {
            return;
        }
        Connection con;
        PreparedStatement workerCheckIn;
        PreparedStatement expireWorkers;
        int modifiedRecords = 0;
        try {
            con = dataSrc.getConnection();
            workerCheckIn = con.prepareStatement(sqlWorkerCheckIn);
            modifiedRecords = workerCheckIn.executeUpdate();
            log.debug("Worker maintenance run affecting " + modifiedRecords + " records.");
            // also remove non users
            expireWorkers = con.prepareStatement(sqlExpireWorkers);
            modifiedRecords = expireWorkers.executeUpdate();
            log.debug("Worker expiry run affecting " + modifiedRecords + " records.");
            setLastRunTime(timer_workerMaintenance);
            workerCheckIn.close();
            expireWorkers.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("HouseKeeper, workerMaintenace", sqlex);
        }
        workerCheckIn = null;
        expireWorkers = null;
        con = null;
    }
    
    /** 
     * Expire old news items and old service issues.
     */
    private void expireNews(boolean forcedRun) {
        Connection con = null;
        PreparedStatement expireOldNews = null;
        PreparedStatement expireServiceIssues = null;
        try {
            con = dataSrc.getConnection();
            expireOldNews = con.prepareStatement(sqlExpireOldNews);
            expireOldNews.executeUpdate();
            expireServiceIssues = con.prepareStatement(sqlExpireServiceIssues);
            expireServiceIssues.executeUpdate();
            setLastRunTime(timer_expireNews);
            expireServiceIssues.close();
            expireServiceIssues = null;
            expireOldNews.close();
            expireOldNews = null;
            con.close();
            con = null;
            log.debug("Expire notices ran.");
        } catch (SQLException sqlex) {
            log.error("HouseKeeper, expireNews", sqlex);
        } finally {
            if (expireServiceIssues != null) {
                try { expireServiceIssues.close(); } catch (SQLException sqlex) {}
                expireServiceIssues = null;
            }
            if (expireOldNews != null) {
                try { expireOldNews.close(); } catch (SQLException sqlex) {}
                expireOldNews = null;
            }
            if (con != null) {
                try { con.close(); } catch (SQLException sqlex) {}
                con = null;
            }
        }
    }
    
    /** 
     * Calculate statistics.
     */
    private void calcStats(boolean forcedRun) {
        // don't run this task unless the time is between 4am and 5am.
        if (!hourMatches(4)) {
            return;
        }
    }
    
    private void maintainLibrary(boolean forcedRun) {
        class libDocs implements FileFilter {
            public boolean accept(File path) {
                return true;
            }
        }
        
    }
    
    /** 
     * Get a resource of the specified type.
     * @param type the type of service (e.g. SMTP, LDAP) that we want
     * @return a Resource object of the requested type if possible
     */
    protected Resource getResource(String type) {
        Resource resource;
        Connection con;
        PreparedStatement getResource;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getResource = con.prepareStatement(sqlGetResourceOfType);
            getResource.setString(1, type);
            getResource.setInt(2, 0);
            result = getResource.executeQuery();
            if (result.next()) {
                resource = new Resource(
                    result.getString("service"), 
                    result.getString("host"), 
                    result.getInt("port"), 
                    result.getString("user"), 
                    result.getString("pw"), 
                    result.getString("info1"));
            } else {
                resource = null;
            }
            result.close();
            getResource.close();
            con.close();
        } catch (SQLException sqlex) {
            // can't use log here as this method is static
            System.out.print(itemDateTime.format(new Date()));
            System.err.println(" HouseKeeper, getResource:" + sqlex);
            resource = null;
        }
        result = null;
        getResource = null;
        con = null;
        return resource;
    }
    
    /**
     * Send an email via the application context.
     * @param recipient the email recipient
     * @param subject the subject line of the email
     * @param body the body text of the email
     * @return true if the email was sent successfully
     */
    private boolean sendMail(String recipient, String subject, String body) {
        Session mailSession;    
        MimeMessage message;
        boolean success = false;
        try {
            mailSession = (Session)nameCnxt.lookup("mail/smtp");
            message = new MimeMessage(mailSession);
            message.setSender(new InternetAddress("ls-db@calderdale.gov.uk"));
            message.addRecipients(MimeMessage.RecipientType.TO, recipient);
            message.setSubject(subject);
            message.setText(body, "UTF-8");
            Transport.send(message);
            success = true;
        } catch (NamingException nex) {
            System.out.println("HouseKeeper, sendMail: " + nex);
        } catch (AddressException aex) {
            System.out.println("HouseKeeper, sendMail: " + aex);
        } catch (MessagingException mex) {
            System.out.println("HouseKeeper, sendMail: " + mex);
        }
        return success;
    }
    
    /**
     * Check the current hour and return true if it matches the submitted 
     * hour.
     * @param targetHour the desired hour
     */
    private boolean hourMatches(int targetHour) {
        Calendar currentTime = Calendar.getInstance();
        if (currentTime.get(Calendar.HOUR_OF_DAY) == targetHour) {
            return true;
        }
        return false;
    }
}

