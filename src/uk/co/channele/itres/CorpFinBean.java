/* 
 * @(#) CorpFinBean.java    0.1 2010/06/24
 *
 * A Bean to provide interaction with the Teams table.
 * Copyright (C) 2010 Calderdale MBC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Bean to allow interaction with Corporate Financials (queries only).
 *
 * @version       0.1    24 June 2010
 * @author        Steven Lilley
 */
public class CorpFinBean extends GenericBean {
    
    private final String sqlGetMySupervisor = "SELECT FORENAMES, SURNAME FROM FN.USER_LIDS JOIN "
        + "FN.USER_NAMES ON FN.USER_LIDS.MAIN_SUPER = FN.USER_NAMES.LID "
        + "WHERE FN.USER_NAMES.END_DATE IS NULL AND FN.USER_LIDS.LID = ?";
    private String lid = "LA04";
    private String password = "GreenP3N";
    private String lidToCheck = "";

    /**
     * NB Don't use a DataSource as we want the user to log in so that
     * we can use their credentials to connect to the database.
     */
    public CorpFinBean() {
        log = Logger.getLogger("itres");
    }
    
    /**
     * Set the users LID
     * @param lid user's LID
     */
    public void setLid(String userLid) {
        lid = userLid.toUpperCase();
        log.debug("CorpFinBean, setLid called with " + lid);
    }
    
    /**
     * Set password.
     * @param ps users password
     */
    public void setPassword(String pw) {
        password = pw;
    }
    
    /**
     * Set the LID to check
     * @param lid user's LID
     */
    public void setLidToCheck(String checkLid) {
        lidToCheck = checkLid.toUpperCase();
        criteria = lidToCheck;
    }
    
    protected String getEditForm() {
        return "";
    }
    
    protected String getItem() {
        return "";
    }
    
    /**
     * Clear the Bean values.
     */
    protected void clearValues() {
        lid = "";
        password = "";
        lidToCheck = "";
    }
    
    public String getStandard() {
        StringBuilder h = new StringBuilder();
        h.append(getSearchForm());
        return h.toString();
    }
    
    public String getSearchForm() {
        StringBuilder h = new StringBuilder();
        h.append("<p>Use this form to identify a supervisor in Corporate Financials.</p>");
        h.append("<form method=\"post\" action=\"whosmysuper.jsp\" name=\"findmysuper\">\n");
        // user name  
        h.append("<p>LID to check: <input type=\"text\" name=\"lidToCheck\" size=\"4\" maxlength=\"4\" ");
        h.append("onMouseOver=\"this.focus();\">");
        /* h.append("<p>Enter your Corporate Financials LID/password.</p>");
        h.append("<p>LID: <input type=\"text\" name=\"lid\" size=\"4\" maxlength=\"4\" ");
        h.append("onMouseOver=\"this.focus();\"> \n");
        h.append("Password: <input type=\"password\" name=\"password\" size=\"8\" maxlength=\"16\" ");
        h.append("onMouseOver=\"this.focus();\"> \n"); */
        h.append("<input type=\"hidden\" name=\"action\" value=\"search\">");
        h.append("<input type=\"submit\" value=\"Search\" class=\"button\"></p>\n");
        h.append("</form>\n");
        return h.toString();
    }
    
    public String getSearchResults() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getMySupervisor;
        ResultSet res;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection("jdbc:oracle:thin:@corpfins:2001:fn",lid,password);
            getMySupervisor = con.prepareStatement(sqlGetMySupervisor);
            getMySupervisor.setString(1, lidToCheck);
            res = getMySupervisor.executeQuery();
            if (res.next()) {
                h.append("<p>The supervisor for <strong>");
                h.append(lidToCheck);
                h.append("</strong> is <strong>");
                h.append(res.getString("FORENAMES") + " ");
                h.append(res.getString("SURNAME"));
                h.append("</strong></p>");
            } else {
                h.append("<p>Sorry, unable to find your LID in the system.</p>");
            }
            res.close();
            getMySupervisor.close();
            con.close();
        } catch (ClassNotFoundException cnfex) {
            log.error("CorpFinBean, getSearchResults", cnfex);
            h.append("<p>Class not found.</p>");
        } catch (SQLException sqlex) {
            log.error("CorpFinBean, getSearchResults", sqlex);
            h.append("<p>SQL exception: ");
            h.append(sqlex.toString());
            h.append("</p>");
        }
        con = null;
        res = null;
        getMySupervisor = null;
        return h.toString();
    }
    
    public String getList() {
        return "";
    }
    
    protected boolean validateData() {
        return false;
    }
    
    protected boolean writeToDatabase() {
        return false;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append(super.getDebug());
        h.append("<p style=\"font-size:xx-small;\"><strong>CorpFinBean values</strong></p>");
        h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
        h.append("<li>LID = " + lid);
        h.append("</ul>");
        return h.toString();
    }
}

