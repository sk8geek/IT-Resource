/* 
 * @(#) GetDocument.java	0.1 2003/07/11
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
/**
 * Forwards the request to a static document..
 *
 * @version		0.1 11 July 2003
 * @author		Steven Lilley
 */
public class GetDocument extends HttpServlet{ 

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler = rqst.getRequestDispatcher("/documents/" + rqst.getParameter("filename"));
        handler.forward(rqst, resp);
    }
}
