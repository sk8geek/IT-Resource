/* 
 * @(#) FAQBean.java    0.2 2006/10/29
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * Returns results from the selfhelp (FAQ) table.
 *
 * @version       0.2    29 October 2006
 * @author        Steven Lilley
 */
public class FAQBean implements Serializable { 

    final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    public static final String[] FAQLISTORDERS = {"alpha", "entry", "request"};
    public static final int FAQ_ALPHA_ORDER = 0;
    public static final int FAQ_ENTRY_ORDER = 1;
    public static final int FAQ_REQUEST_ORDER = 2;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private final String sqlGetCategoryList = 
        "SELECT category, COUNT(category) AS items FROM selfhelp GROUP BY category ORDER BY category";
    private final String sqlGetCategoryListEntryOrder = 
        "SELECT category, COUNT(category) AS items FROM selfhelp GROUP BY category ORDER BY COUNT(category) DESC, category";
    private final String sqlGetCategoryListRequestOrder = 
        "SELECT category, SUM(requests) AS items FROM selfhelp GROUP BY category ORDER BY SUM(category) DESC, category";
    private final String sqlGetSymptomList = 
        "SELECT docref, symptoms FROM selfhelp WHERE category=? ORDER BY docref";
    private final String sqlGetDocument = 
        "SELECT * FROM selfhelp WHERE docref=?";
    private final String sqlGetFullList = 
        "SELECT * FROM selfhelp ORDER BY category, symptoms";
    private final String sqlIncrementRequestCount = 
        "UPDATE selfhelp SET requests=requests+1 WHERE docref=? LIMIT 1";
    private final String sqlSaveNewDocument = 
        "INSERT INTO selfhelp (category, symptoms, fix, techlevel) VALUES " +
        "(?, ?, ?, 0)";
    private final String sqlUpdateDocument = 
        "UPDATE selfhelp SET category=?, symptoms=?, fix=? WHERE docref=? LIMIT 1";
    private int listOrder = FAQ_ALPHA_ORDER;
    private boolean showItemCount = true;
    private int documentRef = 0;
    private String category = "";
    private String symptoms = "";
    private String fix = "";
    private int requests = 0;
    private int techLevel = 0;
    private Date lastMod;
    private boolean validDocument = false;
    private boolean adminEnabled = false;
    private Logger log;
    
    public FAQBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("FAQBean constructor", nex);
        }
    }
    
    public void setShowItemCount(boolean showCount) {
        showItemCount = showCount;
    }
    
    public void setListOrder(String newOrder) {
        if (newOrder.equalsIgnoreCase("entry") || Integer.parseInt(newOrder) == 1) {
            listOrder = FAQ_ENTRY_ORDER;
        } else 
        if (newOrder.equalsIgnoreCase("request") || Integer.parseInt(newOrder) == 2) {
            listOrder = FAQ_REQUEST_ORDER;
        } else {
            listOrder = FAQ_ALPHA_ORDER;
        }
    }
    
    public void setDocRef(int docRef) {
        documentRef = docRef;
    }
    
    public void setCategory(String newCategory) {
        category = newCategory;
    }
    
    public String getCategory() {
        return category;
    }
    
    public void setSymptoms(String newSymptoms) {
        symptoms = newSymptoms;
    }
    
    public void setFix(String newFix) {
        fix = newFix;
    }
    
    protected void setRequests(int reqs) {
        requests = reqs;
    }
    
    public void setTechLevel(int level) {
        techLevel = level;
    }
    
    protected void setModDate(Date lastmod) {
        lastMod = lastmod;
    }
    
    public String getResponse() {
        if (category.length() > 0) {
            return getSymptomList();
        } else {
            if (documentRef > 0) {
                return getDocument();
            } else {
                return getFullList();
            }
        }
    }
    
    private String getFullList() {
        if (category.length() > 0 || documentRef > 0) {
            return "";
        }
        Connection con;
        PreparedStatement getFullList;
        ResultSet results;
        StringBuffer h = new StringBuffer();
        String currentCategory = "";
        try {
            con = dataSrc.getConnection();
            getFullList = con.prepareStatement(sqlGetFullList);
            results = getFullList.executeQuery();
            while (results.next()) {
                if (!currentCategory.equalsIgnoreCase(results.getString("category"))) {
                    if (currentCategory.length() > 0) {
                        h.append("</ul>");
                    }
                    currentCategory = results.getString("category");
                    h.append("<h3>");
                    h.append(currentCategory);
                    h.append("</h3><ul>");
                }
                h.append("<li class=\"faqs\">");
                h.append("<a href=\"faqs.jsp?docRef=");
                h.append(results.getString("docref"));
                h.append("\">");
                h.append(results.getString("symptoms"));
                h.append("</a>");
                h.append("</li>");
            }
            h.append("</ul>");
            results.close();
            getFullList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, getFullList", sqlex);
            h.append(sqlex);
        }
        results = null;
        getFullList = null;
        con = null;
        return h.toString();
    }
    
    public String getCategoryList() {
        Connection con;
        PreparedStatement getCategoryList;
        ResultSet results;
        StringBuffer h = new StringBuffer();
        boolean startedList = false;
        try {
            con = dataSrc.getConnection();
            switch (listOrder) {
                case FAQ_ENTRY_ORDER : 
                    getCategoryList = con.prepareStatement(sqlGetCategoryListEntryOrder);
                case FAQ_REQUEST_ORDER : 
                    getCategoryList = con.prepareStatement(sqlGetCategoryListRequestOrder);
                default :
                    getCategoryList = con.prepareStatement(sqlGetCategoryList);
            }
            results = getCategoryList.executeQuery();
            while (results.next()) {
                if (!startedList) {
                    h.append("<ul class=\"faqs\">");
                    startedList = true;
                }
                h.append("<li>");
                h.append("<a href=\"faqs.jsp?category=");
                h.append(results.getString("category"));
                h.append("\">");
                h.append(results.getString("category"));
                h.append("</a>");
                if (showItemCount) {
                    h.append(" (");
                    h.append(results.getInt("items"));
                    h.append(")");
                }
                h.append("</li>\n");
            }
            if (startedList) {
                h.append("</ul>\n");
            }
            results.close();
            getCategoryList.close();
            con.close();
        } catch (SQLException sqlex) {
            h.append(sqlex);
        }
        results = null;
        getCategoryList = null;
        con = null;
        return h.toString();
    }
    
    public String getCategorySelectControl() {
        StringBuffer h = new StringBuffer();
        Connection con;
        PreparedStatement getCategoryList;
        ResultSet results;
        try {
            h.append("<select name=\"category\">");
            h.append("<option value=\"category\">category</option>");
            con = dataSrc.getConnection();
            getCategoryList = con.prepareStatement(sqlGetCategoryList);
            results = getCategoryList.executeQuery();
            while (results.next()) {
                h.append("<option value=\"" + results.getString("category"));
                h.append("\"");
                if (results.getString("category").equals(category)) {
                    h.append(" selected");
                }
                h.append(">");
                h.append(results.getString("category"));
                h.append("</option>");
            }
            h.append("</select>");
            results.close();
            getCategoryList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, getCategorySelectControl", sqlex);
        }
        results = null;
        getCategoryList = null;
        con = null;
        return h.toString();
    }
    
    protected void enableAdmin() {
        adminEnabled = true;
    }
    
    protected String getAdminList() {
        Connection con;
        PreparedStatement getFullList;
        ResultSet results;
        StringBuffer h = new StringBuffer();
        String currentCategory = "";
        boolean headerDone = false;
        try {
            con = dataSrc.getConnection();
            getFullList = con.prepareStatement(sqlGetFullList);
            results = getFullList.executeQuery();
            while (results.next()) {
                if (!headerDone) {
                    h.append("<table><thead><tr><th>Symptoms</th><th>Requests</th>");
                    h.append("<th>Modified</th></tr></thead>\n<tbody>");
                    headerDone = true;
                }
                if (!currentCategory.equalsIgnoreCase(results.getString("category"))) {
                    currentCategory = results.getString("category");
                    h.append("<tr><td colspan=\"3\" class=\"grey\">");
                    h.append(currentCategory);
                    h.append("</td></tr>\n");
                }
                h.append("<tr><td><a href=\"admin.jsp?docRef=");
                h.append(results.getString("docref"));
                h.append("\">");
                h.append(results.getString("symptoms"));
                h.append("</a></td><td>");
                h.append(results.getInt("requests"));
                h.append("</td><td>");
                h.append(itemDateTime.format(results.getTimestamp("moddate")));
                h.append("</td></tr>\n");
            }
            if (headerDone) {
                h.append("</tbody></table>\n");
            }
            if (adminEnabled) {
                h.append("<p style=\"font-size: smaller;\">");
                h.append("<a href=\"admin.jsp?docRef=0\" class=\"button\">Add a new FAQ</a></p>");
            }
            results.close();
            getFullList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, getAdminList", sqlex);
            h.append(sqlex);
        }
        results = null;
        getFullList = null;
        con = null;
        return h.toString();
    }
    
    private String getSymptomList() {
        if (category.length() == 0) {
            return "";
        }
        Connection con;
        PreparedStatement getSymptomList;
        ResultSet results;
        StringBuffer h = new StringBuffer();
        boolean startedList = false;
        try {
            con = dataSrc.getConnection();
            getSymptomList = con.prepareStatement(sqlGetSymptomList);
            getSymptomList.clearParameters();
            getSymptomList.setString(1, category);
            results = getSymptomList.executeQuery();
            while (results.next()) {
                if (!startedList) {
                    h.append("<h3>");
                    h.append(category);
                    h.append("</h3>");
                    h.append("<ul class=\"faqs\">");
                    startedList = true;
                }
                h.append("<li>");
                h.append("<a href=\"faqs.jsp?docRef=");
                h.append(results.getString("docref"));
                h.append("\">");
                h.append(results.getString("symptoms"));
                h.append("</a>");
                h.append("</li>");
            }
            if (startedList) {
                h.append("</ul>");
            }
            results.close();
            getSymptomList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, getSymptomList", sqlex);
            h.append(sqlex);
        }
        results = null;
        getSymptomList = null;
        con = null;
        return h.toString();
    }
    
    private void populateFromDatabase() {
        Connection con;
        PreparedStatement getDocument;
        ResultSet result;
        category = "";
        symptoms = "";
        fix = "";
        requests = 0;
        techLevel = 0;
        lastMod = new Date();
        validDocument = false;
        if (documentRef > 0) {
            try {
                con = dataSrc.getConnection();
                getDocument = con.prepareStatement(sqlGetDocument);
                getDocument.clearParameters();
                getDocument.setInt(1, documentRef);
                result = getDocument.executeQuery();
                if (result.next()) {
                    category = result.getString("category");
                    symptoms = result.getString("symptoms");
                    fix = result.getString("fix");
                    requests = result.getInt("requests");
                    techLevel = result.getInt("techlevel");
                    lastMod = (Date)result.getTimestamp("moddate");
                    validDocument = true;
                }
                result.close();
                getDocument.close();
                con.close();
            } catch (SQLException sqlex) {
                log.error("FAQBean, populateFromDatabase", sqlex);
            }
            result = null;
            getDocument = null;
            con = null;
        }
    }
    
    protected boolean writeToDatabase() {
        Connection con;
        PreparedStatement saveDocument;
        boolean docSaved = false;
        try {
            con = dataSrc.getConnection();
            if (documentRef > 0) {
                saveDocument = con.prepareStatement(sqlUpdateDocument);
                saveDocument.setInt(4, documentRef);
            } else {
                saveDocument = con.prepareStatement(sqlSaveNewDocument);
            }
            saveDocument.setString(1, category);
            saveDocument.setString(2, symptoms);
            saveDocument.setString(3, fix);
            if (saveDocument.executeUpdate() == 1) {
                docSaved = true;
            }
            saveDocument.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, writeToDatabase", sqlex);
        }
        saveDocument = null;
        con = null;
        return docSaved;
    }
    
    protected String getDocument() {
        StringBuffer h = new StringBuffer();
        populateFromDatabase();
        if (adminEnabled) {
            h.append("<form name=\"faqEdit\" method=\"post\" action=\"admin.jsp\">");
            h.append("<p>Category: ");
            h.append(getCategorySelectControl());
            h.append("</p>\n");
            h.append("<p>New Category: ");
            h.append("<input type=\"text\" name=\"newCategory\" size=\"30\" ");
            h.append("maxlength=\"30\" onMouseOver=\"this.focus();\"></p>\n");
        } else {
            h.append("<h3>Issue</h3>\n");
        }
        h.append("<p class=\"faqSymptom\">");
        if (adminEnabled) {
            h.append("Symptom: <input type=\"text\" name=\"symptoms\" size=\"60\" ");
            h.append("maxlength=\"120\" value=\"");
            h.append(symptoms);
            h.append("\" onMouseOver=\"this.focus();\">");
        } else {
            h.append(symptoms);
        }
        h.append("</p>\n");
        if (!adminEnabled) {
            h.append("<h3>Solution</h3>\n");
        }
        h.append("<p class=\"faqSolution\">");
        if (adminEnabled) {
            h.append("Solution:<br>");
            h.append("<textarea name=\"fix\" cols=\"80\" rows=\"20\" ");
            h.append("wrap=\"virtual\">");
            h.append(fix);
            h.append("</textarea>\n");
        } else {
            h.append(fix);
        }
        h.append("</p>");
        if (adminEnabled) {
            h.append("<p><input type=\"hidden\" name=\"docRef\" value=\"" + documentRef + "\">");
            h.append("<input type=\"submit\" name=\"saveFAQ\" value=\"Save FAQ\" class=\"button\"> ");
            h.append("<input type=\"reset\" value=\"Reset Values\" class=\"button\"></p>\n");
            h.append("</form>");
        } else {
            incrementDocRequestCount();
        }
        return h.toString();
    }
    
    private void incrementDocRequestCount() {
        Connection con;
        PreparedStatement incRequestCount;
        try {
            con = dataSrc.getConnection();
            incRequestCount = con.prepareStatement(sqlIncrementRequestCount);
            incRequestCount.clearParameters();
            incRequestCount.setInt(1, documentRef);
            incRequestCount.executeUpdate();
            incRequestCount.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("FAQBean, incrementDocRequestCount", sqlex);
        }
        incRequestCount = null;
        con = null;
    }
}

