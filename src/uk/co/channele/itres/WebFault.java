/* 
 * @(#) WebFault.java	0.1 2006/10/14
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to create a web fault bean.
 *
 * @version		1.2 24 September 2005
 * @author		Steven Lilley
 */
public class WebFault extends HttpServlet { 
    
    private ServletContext context;
    
    public void init() {
        context = getServletContext();
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        WebFaultBean faultBean;
        synchronized (this) {
            faultBean = new WebFaultBean();
            faultBean.setHost(rqst.getRemoteHost());
            handler = context.getRequestDispatcher("/recordwebfault.jsp");
            handler.forward(rqst, resp);
        } // end sync
	}
}

