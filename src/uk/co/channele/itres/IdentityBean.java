/* 
 * @(#) IdentityBean.java    0.4 2012/01/15
 * 
 * A bean to store a users identity and provide tools for it's upkeep.
 * Copyright (C) 2006-2009,2012 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Stores a visitors identity.
 * In version 0.4, added usual start/finish times
 *
 * @version       0.4    15 January 2012
 * @author        Steven Lilley
 */
public class IdentityBean extends GenericBean {

    // TODO: build in admin funcationality, inc. user status setting
    // IDEA: allow creation of a new user
    /** User levels */
    public final static int USER_NORMAL = 1;
    public final static int USER_SUPER = 5;
    public final static int USER_ADMIN = 9;
    /** Presence values */
    public static final int USER_PRESENCE_IN = 2;
    public static final int USER_PRESENCE_SEEN = 1;
    public static final int USER_PRESENCE_UNKNOWN = 0;
    public static final int USER_PRESENCE_OUT = -1;
    final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    /** A date/time format that only return the name of the day. */
    final SimpleDateFormat itemDay = new SimpleDateFormat("E");
    public static final String[] USER_PRESENCE = {"OUT", "Out", "In", "IN"};
    private final String sqlUserCheckout = "UPDATE itres.register "
        + "SET presence=?, returning=?, whereto=? WHERE uname = ? LIMIT 1";
    /** SQL query to check to see if a username exists in the database. */
    private final String sqlCheckForExistingUser = 
        "SELECT count(uname) AS qty FROM itres.register WHERE uname = ?";
    /** SQL query to add a new user. */
    private final String sqlInsertNewUser = 
        "INSERT INTO itres.register (given, surname, email, team, site, scope, uname, pw) "
        + "VALUES (?, ?, ?, ?, ?, ?, PASSWORD(?))";
    /** SQL query to update all of a user's data. */
    private final String sqlUpdatePrefsIncludingPassword = 
        "UPDATE itres.register SET given=?, surname=?, email=?, team=?, site=?, scope=?, "
        + "pw=PASSWORD(?) WHERE uname=? LIMIT 1";
    /** SQl query to update user data, exluding the password. */
    private final String sqlUpdatePrefsExcludingPassword = 
        "UPDATE itres.register SET given=?, surname=?, email=?, team=?, site=?, scope=? "
        + "WHERE uname=? LIMIT 1";
    private final String sqlGetUserDetails = "SELECT * FROM itres.register WHERE uname=?";
    private final String sqlSetPresence = 
        "UPDATE itres.register SET presence=?, returning=null, whereto=null WHERE uname=? LIMIT 1";
    private final String sqlGetPresence = 
        "SELECT presence FROM itres.register WHERE uname=?";
    private final String sqlCheckPassword = 
        "SELECT uname FROM itres.register WHERE uname=? AND pw=password(?)";
    private final String sqlSetPassword = "UPDATE itres.register SET pw=PASSWORD(?) WHERE uname=? LIMIT 1";
    private final String sqlGetUserPresenceList = 
        "SELECT * FROM itres.user_presence_view ORDER BY sitename, teamname";
    private final String sqlGetUserPresenceSummary = 
        "SELECT uname, given, surname, presence, returning FROM itres.register WHERE active='y' AND site=? ORDER BY given";
    private final String sqlGetUserList = "SELECT uname, given, surname FROM itres.register ORDER BY surname";
    private final String sqlReadPresence = "SELECT presence FROM register WHERE uname=? LIMIT 1";
    private String userTeam = "";
    private String userSite = "";
    private String userGiven = "";
    private String userSurname = "";
    private String userEmail = "";
    private String currentPassword = "";
    private String newPassword = "";
    private String confirmPassword = "";
    private String scope = "team";
    private String currentPage = "";
    private String viewingPage = "";
    private int userPresence = 0;
    private int previousPresence = 0;
    private boolean changeProcessStarted = false;
    private boolean registeredUser = false;
    private boolean loggedIn = false;
    private boolean allowCookie = true;
    private boolean dataValid = false;
    private boolean forgotPassword = false;
    // starting in version 0.4, usual working morning and afternoon start and finish times, Monday to Friday.
    // This is an array of integers representing the minutes from midnight, so from 0 to 1439.
    private int[][] usualWorkTimes = new int[5][4];
    protected static int DAY_MONDAY = 0;
    protected static int DAY_TUESDAY = 1;
    protected static int DAY_WEDNESDAY = 2;
    protected static int DAY_THURSDAY = 3;
    protected static int DAY_FRIDAY = 4;
    protected static int STARTAM = 0;
    protected static int FINISHAM = 1;
    protected static int STARTPM = 2;
    protected static int FINISHPM = 3;
    protected static int TIME_0900 = 9 * 60;
    protected static int TIME_1230 = 12 * 60 + 30;
    protected static int TIME_1330 = 13 * 60 + 30;
    protected static int TIME_1700 = 17 * 60;
    protected static int TIME_1730 = 17 * 60 + 30;
    
    public IdentityBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("IdentityBean constructor", nex);
        }
    }
    
    public void setUserName(String newUser) {
        userId = newUser;
    }
    
    public void setCurrentPassword(String pw) {
        currentPassword = pw;
    }
    
    public void setNewPassword(String pw) {
        newPassword = pw;
    }
    
    public void setConfirmPassword(String pw) {
        confirmPassword = pw;
    }
    
    public void setTeam(String newTeam) {
        userTeam = newTeam;
    }
    
    public void setSite(String newSite) {
        userSite = newSite;
    }
    
    public void setScope(String newScope) {
        scope = newScope;
    }
    
    public void setGiven(String newGivenName) {
        userGiven = newGivenName;
    }
    
    public void setSurname(String newSurname) {
        userSurname = newSurname;
    }
    
    public void setEmail(String newEmail) {
        userEmail = newEmail;
    }
    
    public void setForgotPassword(String forgotPass) {
        if (forgotPass.equals("true")) {
            forgotPassword = true;
        }
    }
    
    public void setGuestComputer(String guestPC) {
        if (guestPC.equals("true")) {
            allowCookie = false;
        }
    }
    
    public String getList() {
        // FIXME: populate this method
        return "";
    }
    
    protected void clearValues() {
        userLevel = 0;
        userTeam = "";
        userSite = "";
        userGiven = "";
        userSurname = "";
        userEmail = "";
        currentPassword = "";
        newPassword = "";
        confirmPassword = "";
        scope = "team";
        currentPage = "";
        viewingPage = "";
        userPresence = 0;
        previousPresence = 0;
        changeProcessStarted = false;
        registeredUser = false;
        loggedIn = false;
        allowCookie = true;
        dataValid = false;
        forgotPassword = false;
        // since 0.4
        usualWorkTimes[DAY_MONDAY][STARTAM] = TIME_0900;
        usualWorkTimes[DAY_MONDAY][FINISHAM] = TIME_1230;
        usualWorkTimes[DAY_MONDAY][STARTPM] = TIME_1330;
        usualWorkTimes[DAY_MONDAY][FINISHPM] = TIME_1730;
        usualWorkTimes[DAY_TUESDAY][STARTAM] = TIME_0900;
        usualWorkTimes[DAY_TUESDAY][FINISHAM] = TIME_1230;
        usualWorkTimes[DAY_TUESDAY][STARTPM] = TIME_1330;
        usualWorkTimes[DAY_TUESDAY][FINISHPM] = TIME_1730;
        usualWorkTimes[DAY_WEDNESDAY][STARTAM] = TIME_0900;
        usualWorkTimes[DAY_WEDNESDAY][FINISHAM] = TIME_1230;
        usualWorkTimes[DAY_WEDNESDAY][STARTPM] = TIME_1330;
        usualWorkTimes[DAY_WEDNESDAY][FINISHPM] = TIME_1730;
        usualWorkTimes[DAY_THURSDAY][STARTAM] = TIME_0900;
        usualWorkTimes[DAY_THURSDAY][FINISHAM] = TIME_1230;
        usualWorkTimes[DAY_THURSDAY][STARTPM] = TIME_1330;
        usualWorkTimes[DAY_THURSDAY][FINISHPM] = TIME_1730;
        usualWorkTimes[DAY_FRIDAY][STARTAM] = TIME_0900;
        usualWorkTimes[DAY_FRIDAY][FINISHAM] = TIME_1230;
        usualWorkTimes[DAY_FRIDAY][STARTPM] = TIME_1330;
        usualWorkTimes[DAY_FRIDAY][FINISHPM] = TIME_1700;
    }
    
    /**
     * Since 0.4
     * @return array of integers representing start/finish times during the working week
     */
    public int[][] getUsualWorkTimes() {
        return usualWorkTimes;
    }
    
    /**
     * Since 0.4, returns the value requested from the usualWorkTimes array.
     * If the requested elements is out of bounds returns -1.
     * @param day the work day
     * @param transition the start or finish time
     * @return the requested value from the array or -1 if request is invalid
     */
    public int getUsualWorkTime(int day, int transition) {
        int returnValue = -1;
        if (day >= 0 && day <= 4) {
            if (transition >= 0 && transition <= 3) {
                returnValue = usualWorkTimes[day][transition];
            }
        }
        return returnValue;
    }
                
    public void setPresence(int newPresence) {
        userPresence = newPresence;
        Connection con;
        PreparedStatement setPresence;
        PreparedStatement getPresence;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getPresence = con.prepareStatement(sqlGetPresence);
            getPresence.clearParameters();
            getPresence.setString(1, userId);
            result = getPresence.executeQuery();
            if (result.next()) {
                previousPresence = result.getInt("presence");
            }
            result.close();
            setPresence = con.prepareStatement(sqlSetPresence);
            setPresence.clearParameters();
            setPresence.setInt(1, newPresence);
            setPresence.setString(2, userId);
            setPresence.executeUpdate();
            result.close();
            getPresence.close();
            setPresence.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: setPresence", sqlex);
        }
        result = null;
        getPresence = null;
        setPresence = null;
        con = null;
    }
    
    public void setSaveSettings(String submitted) {
        if (submitted.equals("Save")) {
            submitPressed = true;
        }
    }
    
    void setAsSeen() {
        if (userPresence == USER_PRESENCE_UNKNOWN) { 
            setPresence(USER_PRESENCE_SEEN);
        }
    }
    
    /**
     * Get an HTML <code>select</code> control for when a user is returning to the office.
     * @return the HTML control
     */
    public String getReturningSelectControl() {
        StringBuilder h = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        int dayCounter = 0;
        h.append("<select name=\"returning\">");
        cal.add(Calendar.MINUTE, 30);
        h.append(addOption(mysqlDateTime.format(cal.getTime()), "Soon", false));
        cal.add(Calendar.MINUTE, 30);
        cal.set(Calendar.MINUTE, 0);
        if (cal.get(Calendar.HOUR_OF_DAY) < 16) {
            if (cal.get(Calendar.HOUR_OF_DAY) < 8) {
                cal.set(Calendar.HOUR_OF_DAY, 8);
            }
            do {
                cal.add(Calendar.HOUR_OF_DAY, 1);
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemTime.format(cal.getTime()), false));
            } while (cal.get(Calendar.HOUR_OF_DAY) < 18);
        }
        cal.set(Calendar.HOUR_OF_DAY, 1);
        // insert tomorrow as a special value
        if (cal.get(Calendar.DAY_OF_WEEK) >= Calendar.MONDAY && cal.get(Calendar.DAY_OF_WEEK) <= Calendar.THURSDAY) {
            cal.add(Calendar.DATE, 1);
            h.append(addOption(mysqlDateTime.format(cal.getTime()), "Tomorrow", false));
        }
        do {
            cal.add(Calendar.DATE, 1);
            dayCounter++;
            while (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                cal.add(Calendar.DATE, 1);
                dayCounter++;
            }
            if (dayCounter < 7) {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDay.format(cal.getTime()), false));
            } else {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDate.format(cal.getTime()), false));
            }
        } while (!(dayCounter > 16 && cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY));
        h.append("</select>");
        return h.toString();
    }
    
    /**
     * Sets the page that the user is currently viewing.
     * <p>Also checks to see if the user has moved away from the settings page 
     * halfway through the process of changing their preferrences.  If this 
     * occurs then the <code>changeProcessStarted</code> and 
     * <code>submitPressed</code> flags is reset.  This 
     * will cause the user to be presented with the settings form if they 
     * return to the settings page.
     * @param thisPage the name of the page that the user has just opened
     */
    public void setCurrentPage(String thisPage) {
        if (currentPage.equals("settings.jsp")  && !thisPage.equals("settings.jsp")) {
            changeProcessStarted = false;
            submitPressed = false;
        }
        currentPage = thisPage;
        viewingPage = thisPage;
    }
    
    public void setViewingPage(String thisPage) {
        viewingPage = thisPage;
    }
    
    public String getRegisteredUserSelectControl() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getUserList;
        ResultSet results;
        try {
            h.append("<select name=\"userName\">");
            h.append("<option value=\"user\">user</option>");
            con = dataSrc.getConnection();
            getUserList = con.prepareStatement(sqlGetUserList);
            results = getUserList.executeQuery();
            while (results.next()) {
                if (!results.getString("uname").equals(userId)) {
                    h.append("<option value=\"" + results.getString("uname"));
                    h.append("\">");
                }
                h.append(results.getString("given") + " " + results.getString("surname"));
                h.append("</option>");
            }
            h.append("</select>");
            results.close();
            getUserList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: getRegisteredUserSelectControl", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    /**
     * Get the userId.
     * @return userId
     */
    public String getUserName() {
        return userId;
    }
    
    public String getTeam() {
        return userTeam;
    }
    
    public String getSite() {
        return userSite;
    }
    
    public String getScope() {
        return scope;
    }
    
    public String getGiven() {
        return userGiven;
    }
    
    public String getSurname() {
        return userSurname;
    }
    
    public String getEmail() {
        return userEmail;
    }
    
    public int getPresence() {
        return userPresence;
    }
    
    public boolean isRegistered() {
        return registeredUser;
    }
    
    public String getCurrentPage() {
        return currentPage;
    }
    
    public String getViewingPage() {
        return viewingPage;
    }
    
    public String getViewingPageControl() {
        return "<input type=\"hidden\" name=\"viewingPage\" value=\"" + viewingPage + "\">";
    }
    
    public String getViewingPageLink() {
        return "<a href=\"" + viewingPage + "\" class=\"button\">Return to previous page</a>";
    }
    
    public String getCurrentPageLink() {
        return "<a href=\"" + currentPage + "\" class=\"button\">Return to previous page</a>";
    }
    
    public String getRegistrationProcess() {
        String action = "register";  // FIXME: should this be register.jsp
        if (!changeProcessStarted) {
            changeProcessStarted = true;
            return getSettingsForm(action);
        } else {
            if (validateData()) {
                if (writeToDatabase()) {
                    registeredUser = true;
                    changeProcessStarted = false;
                    return getRegistrationSuccessPage();
                } else {
                    changeProcessStarted = false;
                    return "<p>error saving</p>";
                }
            } else {
                return getSettingsForm(action);
            }
        }
    }
    
    public String getStandard() {
        return getWelcome();
    }
    
    public String getItem() {
        return "Not written yet."; //todo: getItem could return basic user details
    }
    
    /**
     * Controls the process of changing user settings via the 
     * settings.jsp page.
     * <p>This method is called repeatedly and returns different responses 
     * depending on the state of the Bean.  
     * When the user first visits the page they are not logged in.  
     * The <code>loggedIn</code> and <code>changeProcessStarted</code> flags 
     * are not set. A login form is returned.
     * <p>When the user logs in (<code>loggedIn</code> is set but 
     * <code>changeProcessStarted</code> is still false.  At this point the 
     * <code>changeProcessStarted</code> is set and the settings form is returned.  
     * The user can now edit their settings.  
     * <p>When the user submits the settings form this method is called with 
     * <code>changeProcessStarted</code> set.  The data is the form is 
     * validated.  If valid it is written to the database and the 
     * <code>changeProcessStarted</code> flag reset.  I not valid the form is 
     * returned showing errors.
     * @return HTML page dependant on the state of the process
     */
    public String getSettingsProcess() {
        String action = "settings.jsp";
        if (!changeProcessStarted) {
            if (loggedIn) {
                changeProcessStarted = true;
                submitPressed = false;
                return getSettingsForm(action);
            } else {
                if (forgotPassword) {
                    return mailNewPassword();
                } else {
                    return getLoginForm();
                }
            }
        } else {
            if (submitPressed) {
                if (validateData()) {
                    if (writeToDatabase()) {
                        changeProcessStarted = false;
                        submitPressed = false;
                        return "<p>Thank you.  Your settings have been saved.</p>";
                    } else {
                        changeProcessStarted = false;
                        submitPressed = false;
                        return "<p>Sorry, an error trying to save your settings.</p>";
                    }
                }
            }
            return getSettingsForm(action);
        }
    }
    
    public String getWelcome() {
        StringBuilder h = new StringBuilder();
        String greeting = "Good morning ";
        Calendar timeOfDay = Calendar.getInstance();
        if (timeOfDay.get(Calendar.HOUR_OF_DAY) >= 12) {
            greeting = "Good afternoon ";
        }
        if (timeOfDay.get(Calendar.HOUR_OF_DAY) >= 17) {
            greeting = "Good evening ";
        }
        if (registeredUser) {
            if (userPresence == USER_PRESENCE_SEEN) {
                h.append("<p class=\"greeting\">");
                h.append(greeting);
                h.append(userGiven);
                h.append("</p>");
            } else 
            if (userPresence == USER_PRESENCE_IN && previousPresence == USER_PRESENCE_OUT) {
                h.append("<p class=\"greeting\">");
                h.append("Hi, welcome back");
                h.append("</p>");
                updatePreviousPresence();
            }
            if (userPresence == USER_PRESENCE_OUT) {
                h.append("<p class=\"greeting\">");
                h.append("You are checked out");
                h.append("</p>");
            }
        }
        return h.toString();
    }
            
    public String getUserNameInput() {
        StringBuilder h = new StringBuilder();
        h.append("<input type=\"text\" maxlength=\"16\" name=\"username\" ");
        h.append("size=\"6\" value=\"");
        h.append(userId);
        h.append("\">");
        return h.toString();
    }
    
    /**
     * Returns an HTML String with user login/checkin/checkout buttons as appropraite.
     * @return HTML String containing buttons
     */
    public String getCheckInOutButtons() {
        StringBuilder h = new StringBuilder();
        if (!registeredUser) {
            h.append("<a href=\"login\" class=\"button\">Log In</a> ");
            h.append("<a href=\"register\" class=\"button\">Register</a>");
        } else {
            if (loggedIn) {
                h.append("<a href=\"logout\" class=\"button\">Log Out</a>&nbsp;");
            }
            if (userId.length() > 0) {
                if (userPresence < USER_PRESENCE_IN) {
                    h.append("<a class=\"button\" href=\"checkin\" title=\"Check In\">Check In</a> ");
                }
                if (userPresence >= USER_PRESENCE_UNKNOWN) {
                    h.append("<a class=\"button\" href=\"checkout.jsp\" title=\"Check Out\">Check Out</a>");
                }
                if (!loggedIn && registeredUser) {
                    h.append(" <a href=\"login.jsp\" class=\"button\">Login</a>");
                }
            }
        }
        return h.toString();
    }
    
    /**
     * Returns an HTML String with user registration/login controls as appropraite.
     * @return HTML String containing buttons
     */
    public String getLoginControls() {
        StringBuilder h = new StringBuilder();
        if (!registeredUser) {
            h.append("<a href=\"login\" class=\"button\">Log In</a> ");
            h.append("<a href=\"register\" class=\"button\">Register</a>");
        } else {
            if (loggedIn) {
                h.append("<a href=\"logout\" class=\"button\">Log Out</a>&nbsp;");
            } else {
                if (userId.length() > 0) {
//                    if (userPresence == USER_PRESENCE_IN && editLevel > 4) {
//                        h.append(" <a href=\"login.jsp\" class=\"button\">Login</a>");
                // replaced this bit
                    h.append("<form method=\"post\" action=\"login\" name=\"login\">\n");
                    // user name  
                    h.append("<p>Username: <input type=\"text\" name=\"userName\" size=\"4\" maxlength=\"4\" value=\"");
                    h.append(userId);
                    h.append("\"");
                    h.append(" onMouseOver=\"this.focus();\"></p>\n");
                    // current password
                    h.append("<p>Password: <input type=\"password\" name=\"currentPassword\" size=\"16\" maxlength=\"16\"");
                    h.append(" onMouseOver=\"this.focus();\"></p>\n");
                    h.append("</form>\n");
                }
            }
        }
        return h.toString();
    }
    
    public void readPresence() {
        Connection con;
        PreparedStatement readPresence;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            readPresence = con.prepareStatement(sqlReadPresence);
            readPresence.clearParameters();
            readPresence.setString(1, userId);
            result = readPresence.executeQuery();
            if (result.next()) {
                userPresence = result.getInt("presence");
            }
            result.close();
            readPresence.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: readPresence", sqlex);
        }
        result = null;
        readPresence = null;
        con = null;
    }
    
    public String getUserPresence() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getUserPresenceList;
        ResultSet results;
        Calendar now = Calendar.getInstance();
        Calendar returning = Calendar.getInstance();
        boolean headerDone = false;
        Date lastMod;
        try {
            con = dataSrc.getConnection();
            getUserPresenceList = con.prepareStatement(sqlGetUserPresenceList);
            results = getUserPresenceList.executeQuery();
            while (results.next()) {
                if (!headerDone) {
                    h.append("<table><thead><tr><th>Name</th><th>Team</th>");
                    h.append("<th>Site</th><th>Presence</th><th>Destination</th>");
                    h.append("<th>Returning</th><th>Modified</th></tr></thead><tbody>");
                    headerDone = true;
                }
                if (results.getString("uname").equals(userId)) {
                    h.append("<tr class=\"itsMe\">");
                } else {
                    h.append("<tr>");
                }
                h.append("<td>");
                h.append(results.getString("given") + " " + results.getString("surname"));
                h.append("</td><td>");
                h.append(results.getString("teamname"));
                h.append("</td><td>");
                h.append(results.getString("sitename"));
                h.append("</td><td>");
                h.append(IdentityBean.getPresenceAsText(results.getInt("presence")));
                if (results.getInt("presence") > -1) {
                    h.append("<td class=\"greytext\">");
                } else {
                    h.append("<td>");
                }
                if (results.getString("whereto") != null) {
                    h.append(results.getString("whereto"));
                }
                h.append("</td>\n");
                if (results.getInt("presence") > -1) {
                    h.append("<td class=\"greytext\">");
                } else {
                    h.append("<td>");
                }
                if (results.getTimestamp("returning") != null) {
                    returning.setTime(results.getTimestamp("returning"));
                    if (returning.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {
                        h.append(itemTime.format(returning.getTime()));
                    } else {
                        h.append(itemDate.format(returning.getTime()));
                    }
                    if (returning.compareTo(now) < 0 && returning.get(Calendar.HOUR) != 1) {
                        h.append(" *overdue*");
                    }
                }
                h.append("</td><td>");
                lastMod = new Date(results.getTimestamp("lastmod").getTime());
                if (System.currentTimeMillis() - lastMod.getTime() > 43200000) {
                    // yesterday
                    h.append(itemDate.format(lastMod));
                } else {
                    // today
                    h.append(itemTime.format(lastMod));
                }
                h.append("</td></tr>\n");
            }
            if (headerDone) {
                h.append("</tbody></table>\n");
            }
            results.close();
            getUserPresenceList.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: getUserPresence", sqlex);
        }
        results = null;
        getUserPresenceList = null;
        con = null;
        return h.toString();
    }
    
    public String getUserPresenceSummary() {
        if (!registeredUser) {
            return "";
        }
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getUserPresenceSummary;
        ResultSet results;
        Calendar now = Calendar.getInstance();
        Calendar returning = Calendar.getInstance();
        boolean headerDone = false;
        try {
            con = dataSrc.getConnection();
            getUserPresenceSummary = con.prepareStatement(sqlGetUserPresenceSummary);
            getUserPresenceSummary.clearParameters();
            getUserPresenceSummary.setString(1, userSite);
            results = getUserPresenceSummary.executeQuery();
            while (results.next()) {
                if (!headerDone) {
                    h.append("<table class=\"summary\"><thead><tr><th>Name</th>");
                    h.append("<th>Presence</th><th>Returning</th></tr></thead><tbody>");
                    headerDone = true;
                }
                if (results.getString("uname").equals(userId)) {
                    h.append("<tr class=\"itsMe\">");
                } else {
                    h.append("<tr>");
                }
                h.append("<td>");
                h.append(results.getString("given") + " " + results.getString("surname"));
                h.append("</td><td>");
                h.append(IdentityBean.getPresenceAsText(results.getInt("presence")));
                h.append("</td>\n");
                if (results.getInt("presence") > -1) {
                    h.append("<td class=\"greytext\">");
                } else {
                    h.append("<td>");
                }
                if (results.getTimestamp("returning") != null) {
                    returning.setTime(results.getTimestamp("returning"));
                    if (returning.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {
                        h.append(itemTime.format(returning.getTime()));
                    } else {
                        h.append(itemDate.format(returning.getTime()));
                    }
                    if (returning.compareTo(now) < 0) {
                        h.append(" *overdue*");
                    }
                }
                h.append("</td></tr>\n");
            }
            if (headerDone) {
                h.append("</tbody></table>\n");
            }
            results.close();
            getUserPresenceSummary.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: getUserPresenceSummary", sqlex);
        }
        results = null;
        getUserPresenceSummary = null;
        con = null;
        return h.toString();
    }
    
    /**
     * Provides a log in form.
     * @return HTML log in form
     */
    public String getLoginForm() {
        StringBuilder h = new StringBuilder();
        if (userGiven.length() > 0) {
            h.append("<p class=\"greeting\">Welcome ");
            h.append(userGiven);
            h.append(", please log in</p>");
        } else {
            h.append("<p class=\"greeting\">Please log in</p>\n");
        }
        h.append("<form method=\"post\" action=\"login\" name=\"login\">\n");
        // user name  
        h.append("<p>Username: <input type=\"text\" name=\"userName\" size=\"4\" maxlength=\"4\" value=\"");
        h.append(userId);
        h.append("\"");
        if (errors.indexOf("uname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></p>\n");
        // current password
        h.append("<p>Password: <input type=\"password\" name=\"currentPassword\" size=\"16\" maxlength=\"16\"");
        if (errors.indexOf("oldpw") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></p>\n");
        // forgotten password
        h.append("<p><input type=\"checkbox\" name=\"forgotPassword\" value=\"true\">");
        h.append(" Forgotten password?</p>\n");
        // guest computer (don't set a cookie)
        h.append("<p><input type=\"checkbox\" name=\"guestComputer\" value=\"true\">");
        h.append(" This isn't your usual computer?</p>\n");
        // buttons 
        h.append("<p><input type=\"submit\" value=\"Submit\" class=\"button\">\n ");
        h.append("<input type=\"reset\" value=\"Reset\" class=\"button\">\n");
        // action 
        h.append("</form>\n");
        // errors
        if (errors.length() > 0) {
            if (errors.indexOf("invpw") > -1) {
                // password was invalid for account 
                h.append("<p class=\"nb\">Invalid password for ");
                h.append("this account.</p>\n");
            }
        }
        h.append("<div class=\"function\"><p>If you have forgotten your password ");
        h.append("enter your user name.  A new password will be sent to your registered ");
        h.append("email address.</p>");
        h.append("<p>If this isn't the computer that you usually use then a ");
        h.append("'cookie' won't be left.</p></div>");
        return h.toString();
    }
    
    /**
     * Returns the internal state of the Bean as HTML for debug purposes.
     * @return internal state as HTML
     */
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append(super.getDebug());
        h.append("<p style=\"font-size:xx-small;\"><strong>IdentityBean values</strong></p>");
        h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
        h.append("<li>bean id= " + this.toString());
        h.append("</li>\n<li>userLevel= " + userLevel);
        h.append("</li>\n<li>userTeam= " + userTeam);
        h.append("</li>\n<li>userSite= " + userSite);
        h.append("</li>\n<li>userGiven= " + userGiven);
        h.append("</li>\n<li>userSurname= " + userSurname);
        h.append("</li>\n<li>userEmail= " + userEmail);
        if (currentPassword.length() > 0) {
            h.append("</li>\n<li>currentPassword is set");
        }
        if (newPassword.length() > 0) {
            h.append("</li>\n<li>newPassword is set");
        }
        if (confirmPassword.length() > 0) {
            h.append("</li>\n<li>confirmPassword is set");
        }
        h.append("</li>\n<li>scope= " + scope);
        h.append("</li>\n<li>currentPage= " + currentPage);
        h.append("</li>\n<li>viewingPage= " + viewingPage);
        h.append("</li>\n<li>userPresence= " + userPresence);
        h.append("</li>\n<li>prevPresence= " + previousPresence);
        h.append("</li>\n<li>registeredUser= " + registeredUser);
        h.append("</li>\n<li>loggedIn= " + loggedIn);
        h.append("</li>\n<li>allowCookie= " + allowCookie);
        h.append("</li>\n<li>forgotPassword= " + forgotPassword);
        h.append("</li>\n<li>changeProcessStarted= " + changeProcessStarted);
        h.append("</li>\n<li>dataValid= " + dataValid + "</li></ul>");
        return h.toString();
    }
    
    void enableAdmin() {
        isAdmin = true;
    }
    
    void checkInUser() {
        setPresence(USER_PRESENCE_IN);
    }
    
    void checkOutUser(String backat, String destination) {
        Connection con;
        PreparedStatement userCheckout;
        try {
            con = dataSrc.getConnection();
            userCheckout = con.prepareStatement(sqlUserCheckout);
            userCheckout.clearParameters();
            userCheckout.setInt(1, IdentityBean.USER_PRESENCE_OUT);
            userCheckout.setString(2, backat);
            userCheckout.setString(3, destination);
            userCheckout.setString(4, userId);
            if (userCheckout.executeUpdate() == 1) {
                userPresence = IdentityBean.USER_PRESENCE_OUT;
            }
            userCheckout.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: checkOutUser", sqlex);
        }
        userCheckout = null;
        con = null;
    }
    
    public void cookieMonster() {
        Connection con;
        PreparedStatement checkForExistingUser;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            checkForExistingUser = con.prepareStatement(sqlCheckForExistingUser);
            checkForExistingUser.clearParameters();
            checkForExistingUser.setString(1, userId);
            result = checkForExistingUser.executeQuery();
            if (result.next()) {
                populateFromDatabase();
                log.debug("IdentityBean, cookieMonster: user found, NOM! NOM!");
            }
            result.close();
            checkForExistingUser.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: cookieMonster", sqlex);
        }
        result = null;
        checkForExistingUser = null;
        con = null;
    }
    
    public boolean attemptLogin() {
        Connection con;
        PreparedStatement checkPassword;
        PreparedStatement checkForExistingUser;
        ResultSet result;
        clearErrors();
        try {
            con = dataSrc.getConnection();
            checkForExistingUser = con.prepareStatement(sqlCheckForExistingUser);
            checkForExistingUser.clearParameters();
            checkForExistingUser.setString(1, userId);
            result = checkForExistingUser.executeQuery();
            if (result.next()) {
                populateFromDatabase();
                checkPassword = con.prepareStatement(sqlCheckPassword);
                checkPassword.clearParameters();
                checkPassword.setString(1, userId);
                checkPassword.setString(2, currentPassword);
                result = checkPassword.executeQuery();
                if (result.next()) {
                    loggedIn = true;
                    setPresence(USER_PRESENCE_IN);
                } else {
                    setError("password");
                }
                checkPassword.close();
            } else {
                currentPage = "register.jsp";
            }
            result.close();
            checkForExistingUser.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: attemptLogin", sqlex);
        }
        con = null;
        checkPassword = null;
        checkForExistingUser = null;
        currentPassword = "";
        return loggedIn;
    }
    
    /**
     * Marks the user as logged out.
     * <p>Also resets the <code>changeProcessStarted</code> flag.
     */
    public void logoutUser() {
        loggedIn = false;
    }
    
    /**
     * Test to see if cookies are allowed.
     * @return true if cookies are allowed, false otherwise
     */
    boolean allowCookie() {
        return allowCookie;
    }
    
    public String getEditForm() {
        return "not written yet"; // todo: get an edit form
    }
    
    public boolean deleteFromDatabase() {
        return false;
    }
    
    /**
     * Present a settings form.
     * @param action the form action to perform on submission
     * @return the HTML form
     */
    private String getSettingsForm(String action) {
        StringBuilder h = new StringBuilder();
        DropDownBean teams = new DropDownBean("teams", "team");
        DropDownBean sites = new DropDownBean("sites", "site");
        h.append("<form method=\"post\" action=\"");
        h.append(action);
        h.append("\" name=\"userdetails\">\n");
        h.append("<table><tbody><tr>");
        // given name
        h.append("<tr><td>Given name:</td><td><input type=\"text\" name=\"given\" size=\"30\" maxlength=\"30\" value=\"");
        h.append(userGiven);
        h.append("\"");
        if (errors.indexOf("given") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // surname 
        h.append("<tr><td>Surname:</td><td><input type=\"text\" name=\"surname\" size=\"30\" maxlength=\"40\" value=\"");
        h.append(userSurname);
        h.append("\"");
        if (errors.indexOf("surname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // email
        h.append("<tr><td>Email address:</td><td><input type=\"text\" name=\"email\" size=\"40\" maxlength=\"120\" value=\"");
        h.append(userEmail);
        h.append("\"");
        if (errors.indexOf("email") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // team
        teams.setSelectedValue(userTeam);
        h.append("<tr><td>Team:</td><td>");
        h.append(teams.getSelectControl());
        h.append("</td></tr>\n");
        // site
        sites.setSelectedValue(userSite);
        h.append("<tr><td>Site:</td><td>");
        h.append(sites.getSelectControl());
        h.append("</td></tr>\n");
        // TODO: add scope control
        // user name  
        if (registeredUser) {
            h.append("<tr><td>Username:</td><td> &nbsp; <b>" + userId + "</b></td></tr>");
            h.append("<input type=\"hidden\" name=\"uname\" value=\"");
            h.append(userId);
            h.append("\">");
        } else {
            h.append("<tr><td>Username:</td><td><input type=\"text\" name=\"userName\" size=\"16\" maxlength=\"16\" value=\"");
            h.append(userId);
            h.append("\"");
            if (errors.indexOf("username") > -1 || errors.indexOf("inuse") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        }
        // current password 
        if (registeredUser) {
            h.append("<tr><td>Current password:</td><td><input type=\"password\" name=\"currentPassword\" size=\"16\" maxlength=\"16\"");
            if (errors.indexOf("oldpw") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        }
        // new password
        h.append("<tr><td>New password:</td><td><input type=\"password\" name=\"newPassword\" size=\"16\" maxlength=\"16\"");
        if (errors.indexOf("newPassword") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // confirm password 
        h.append("<tr><td>Confirm password:</td><td><input type=\"password\" name=\"confirmPassword\" size=\"16\" maxlength=\"16\"");
        if (errors.indexOf("confirmPassword") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // buttons 
        h.append("<tr><td colspan=\"2\"><input type=\"submit\" name=\"saveSettings\" value=\"Save\" class=\"button\"> ");
        h.append("<input type=\"reset\" value=\"Reset\" class=\"button\">\n");
        h.append("</td></tr></tbody></table></form>\n");
        // errors
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct the errors indicated.</strong></p>\n");
            if (errors.indexOf("given") > -1) {
                h.append("<p>The given name may only contain letters.</p>");
            }
            if (errors.indexOf("surname") > -1) {
                h.append("<p>The surname may only contain letters.</p>");
            }
            if (errors.indexOf("email") > -1) {
                h.append("<p>The email address seems invalid.</p>");
            }
            if (errors.indexOf("inuse") > -1) {
                // UNAME is valid but already in use 
                h.append("<p>The user name is valid but has ");
                h.append("been taken.  Please try something different.</p>\n");
            }
            if (errors.indexOf("invpw") > -1) {
                // password was invalid for account 
                h.append("<p>Invalid password for ");
                h.append("this account.</p>\n");
            }
            if (errors.indexOf("newpw,confirmpw") > -1) {
                // passwords differ 
                h.append("<p>The passwords entered do not ");
                h.append("match.</p>\n");
            }
            if (errors.indexOf("pwlen") > -1) {
                // password is too short 
                h.append("<p class=\"nb\">Passwords must to be at least ");
                h.append("four characters long.</p>\n");
            }
        }
        return h.toString();
    }
    
    public String getSearchForm() {
        return ""; // todo: allow to search for a person by team/name/site
    }
    
    public String getSearchResults() {
        return ""; // todo: return search results
    }
    
    /**
     * Returns an HTML page indicating successful registration.
     * @return successful registration page
     */
    private String getRegistrationSuccessPage() {
        StringBuilder h = new StringBuilder();
        h.append("<p class=\"greeting\">Thank you for registering.</p>");
        h.append("<p>I hope you find this site useful.  Now you are registered ");
        h.append("you can use e-Register to show if you are in or out of the ");
        h.append("office.  You can also post news items.</p>");
        return h.toString();
    }
    
    private void updatePreviousPresence() {
        previousPresence = userPresence;
    }
    
    protected boolean populateFromDatabase() {
        Connection con;
        PreparedStatement getUserDetails;
        ResultSet result;
        boolean userFound = false;
        if (userId.length() > 0) {
            try {
                con = dataSrc.getConnection();
                getUserDetails = con.prepareStatement(sqlGetUserDetails);
                getUserDetails.setString(1, userId);
                result = getUserDetails.executeQuery();
                if (result.next()) {
                    setUserLevel(result.getInt("ulevel"));
                    setTeam(result.getString("team"));
                    setSite(result.getString("site"));
                    setScope(result.getString("scope"));
                    setGiven(result.getString("given"));
                    setSurname(result.getString("surname"));
                    setEmail(result.getString("email"));
                    setPresence(result.getInt("presence"));
                    setAsSeen();
                    registeredUser = true;
                    dataValid = true;
                    userFound = true;
                    result.close();
                }
                result.close();
                getUserDetails.close();
                con.close();
            } catch (SQLException sqlex) {
                log.error("IdentityBean: populateFromDatabase", sqlex);
            }
            con = null;
        }
        return userFound;
    }
    
    protected boolean writeToDatabase() {
        Connection con;
        PreparedStatement saveUserDetails;
        boolean userSaved = false;
        try {
            con = dataSrc.getConnection();
            if (!registeredUser) {
                saveUserDetails = con.prepareStatement(sqlInsertNewUser);
                saveUserDetails.clearParameters();
                saveUserDetails.setString(7, userId);
                saveUserDetails.setString(8, newPassword);
            } else {
                if (newPassword.length() > 0 && !newPassword.equals(currentPassword)) {
                    saveUserDetails = con.prepareStatement(sqlUpdatePrefsIncludingPassword);
                    saveUserDetails.clearParameters();
                    saveUserDetails.setString(7, newPassword);
                    saveUserDetails.setString(8, userId);
                } else {
                    saveUserDetails = con.prepareStatement(sqlUpdatePrefsExcludingPassword);
                    saveUserDetails.clearParameters();
                    saveUserDetails.setString(7, userId);
                }
            }
            saveUserDetails.setString(1, userGiven);
            saveUserDetails.setString(2, userSurname);
            saveUserDetails.setString(3, userEmail);
            saveUserDetails.setString(4, userTeam);
            saveUserDetails.setString(5, userSite);
            saveUserDetails.setString(6, scope);
            if (saveUserDetails.executeUpdate() == 1) {
                userSaved = true;
                setAsSeen();
            }
            saveUserDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("IdentityBean: writeToDatabase", sqlex);
        }
        con = null;
        return userSaved;
    }
    
    /** 
     * Validates the information entered into the form.
     * Errors are added to a String and returned.
     * The string can be used by the caller or by the doUserForm method. 
     */ 
    protected boolean validateData() {
        Connection con;
        PreparedStatement checkForUsername;
        PreparedStatement checkPassword;
        ResultSet results;
        StringBuilder errorList = new StringBuilder();
        if (!isAlphaOnly(userGiven)) { errorList.append("given,"); }
        if (!isAlphaOnly(userSurname)) { errorList.append("surname,"); }
        if (!emailAddrOkay(userEmail)) { errorList.append("email,"); }
        if (!isAlphaNumeric(userTeam)) { errorList.append("team,"); }
        if (!isAlphaNumeric(userSite)) { errorList.append("site,"); }
        if (!isAlphaNumeric(userId)) { errorList.append("username,"); }
        if (newPassword.compareTo(confirmPassword) != 0) { 
            errorList.append("newPassword,confirmPassword,"); 
        }
        if (newPassword.length() > 0) {
            if (newPassword.length() < 5) { errorList.append("pwlen,"); }
        }
        try {
            con = dataSrc.getConnection();
            checkForUsername = con.prepareStatement(sqlCheckForExistingUser);
            checkForUsername.setString(1, userId);
            results = checkForUsername.executeQuery();
            if (results.next()) {
                if(results.getInt("qty") > 0) { 
                    if (!registeredUser) {
                        errorList.append("inuse,");
                    }
                } else {
                    if (registeredUser) {
                        errorList.append("nofind");
                    }
                }
            }
            if (newPassword.length() != 0 && registeredUser) {
                checkPassword = con.prepareStatement(sqlCheckPassword);
                checkPassword.clearParameters();
                checkPassword.setString(1, userId);
                checkPassword.setString(2, currentPassword);
                results = checkPassword.executeQuery();
                if (results.next()) {
                    if(results.getInt("qty") != 1) { 
                        errorList.append("invpw,");
                    }
                }
                checkPassword.close();
            }
            results.close();
            checkForUsername.close();
            con.close();
        } catch (SQLException sqlex) {
            System.out.println("IdentityBean, validateData:" + sqlex);
        }
        con = null;
        errors = errorList.toString();
        if (errors.length() == 0) {
            dataValid = true;
        }
        return dataValid;
    }

    private String mailNewPassword() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement setPassword;
        boolean sentMessage = false;
        String replacementPassword;
        try {
            replacementPassword = generatePasscode();
            sentMessage = sendMail(userEmail, "New Password for LS-DB", 
                "Your password has been reset to " + replacementPassword);
            if (sentMessage) {
                con = dataSrc.getConnection();
                setPassword = con.prepareStatement(sqlSetPassword);
                setPassword.clearParameters();
                setPassword.setString(1, replacementPassword);
                setPassword.setString(2, userId);
                if (setPassword.executeUpdate() == 1) {
                    h.append("<p>Your password has been reset.</p>");
                    h.append("<p>The new password has been sent to your ");
                    h.append("registered email address.</p>");
                    forgotPassword = false;
                } else {
                    h.append("<p><strong>Unable to reset your password!</strong></p>");
                    h.append("<p>Please <em>disregard</em> the email stating ");
                    h.append("that it has been reset.</p>");
                }
                setPassword.close();
                con.close();
            } else {
                h.append("<p><strong>Unable to send email at this time.</strong></p>");
                h.append("<p>Please try again later.</p>");
            }
        } catch (SQLException sqlex) {
            log.error("IdentityBean: mailNewPassword", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    /**
     * Generates a random String, eight characters in length.
     * Vowels are never used in order to prevent the potential creation 
     * of recognisable English words.
     * @return String of eight random characters
     */ 
    private String generatePasscode() {
        StringBuilder code = new StringBuilder();
        for (int loop = 0; loop < 8; loop++) {
            int ch = (int)(98 + Math.random() * 24);
            if (ch == 101 | ch == 105 | ch == 111 | ch== 117) {
                ch++;
            }
            code.append(Character.valueOf((char)ch));
        }
        return code.toString();
    }
    
    public static String getPresenceAsText(int presence) {
        int p = presence + 1;
        if (p < 0) { p = 0; }
        if (p > 3) { p = 3; }
        return USER_PRESENCE[p];
    }
    
    /* TO-DO: this method might need to be in GenericBean.  This isn't the right place for it. */
    /**
     * Return an HTML <code>option</code> tag using the values supplied.
     * @param value the option value
     * @param text the human readable option text
     * @param selected true if the option should be marked as selected
     * @return the HTML option tag
     */
    private String addOption(String value, String text, boolean selected) {
        StringBuilder h = new StringBuilder();
        h.append("<option value=\"");
        h.append(value);
        h.append("\"");
        if (selected) {
            h.append(" selected");
        }
        h.append(">");
        h.append(text);
        h.append("</option>\n");
        return h.toString();
    }
}

