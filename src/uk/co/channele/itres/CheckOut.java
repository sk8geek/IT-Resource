/* 
 * @(#) CheckOut.java	0.2 2006/10/28
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to mark the user as checked out.
 *
 * @version		0.2    28 October 2006
 * @author		Steven Lilley
 */
public class CheckOut extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    /**
     * Handle POST requests.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        MessageBean msgBean;
        String returning = "";
        String destination = "";
        boolean noAutoCheckIn = false;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                if (rqst.getParameter("returning") != null) {
                    returning = rqst.getParameter("returning");
                }
                if (rqst.getParameter("destination") != null) {
                    destination = rqst.getParameter("destination");
                }
                userId.checkOutUser(returning, destination);
                if (session.getAttribute("msgBean") != null) {
                    msgBean = (MessageBean)session.getAttribute("msgBean");
                    msgBean.setUserPresence(userId.getPresence());
                }
                handler = context.getRequestDispatcher("/" + userId.getCurrentPage());
                handler.forward(rqst, resp);
            }
        } // end sync
	}
}

