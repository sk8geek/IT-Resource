/* 
 * @(#) IOBean.java    0.1 2012/01/14
 * 
 * Copyright (C) 2006 Steven J Lilley
 * Copyright (C) 2011-12 Calderdale MBC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
import uk.co.channele.LiteGenericBean;

/**
 * A bean to provide a whiteboard for users to check in/out.
 *
 * @version     0.1    14 January 2012
 * @author      Steven Lilley
 */
public class IOBean extends LiteGenericBean {
    
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    // user field variables
    private String username = "";
    private Date leavingAt = new Date();
    private Date arrivingBack = new Date();
    private String destination = "";
    private boolean autoCheckIn = true;
    private String editor = "";
    // form variables
    private String monAM = "";
    private String monPM = "";
    private String tuesAM = "";
    private String tuesPM = "";
    private String wednesAM = "";
    private String wednesPM = "";
    private String thursAM = "";
    private String thursPM = "";
    private String friAM = "";
    private String friPM = "";
    private boolean monAll = false;
    private boolean tuesAll = false;
    private boolean wednesAll = false;
    private boolean thursAll = false;
    private boolean friAll = false;
    private boolean weekAll = false;
    // other variables
    private String given = "";
    private String surname = "";
    private int[][] usualWorkTimes = new int[5][4];
    protected static int DAY_MONDAY = 0;
    protected static int DAY_TUESDAY = 1;
    protected static int DAY_WEDNESDAY = 2;
    protected static int DAY_THURSDAY = 3;
    protected static int DAY_FRIDAY = 4;
    protected static int STARTAM = 0;
    protected static int FINISHAM = 1;
    protected static int STARTPM = 2;
    protected static int FINISHPM = 3;
    
    /**
     * Constructs an IOBean.
     * <p>Instatiates the log and data source.
     */
    public IOBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("IOBean constructor", nex);
        }
        log.debug("IOBean constructor:" + this);
    }
    
    public void readUser(GenericBean userBean) {
        setUserLevel(userBean.getUserLevel());
        isOwner = true;
        // FIXME: I don't know where to set isOwner. Depends on if user is found, so cookie should exist.
    }
    
    public void setUsualWorkTimes(int[][] workTimes) {
        usualWorkTimes = workTimes;
    }
    
    public void clearValues() {
        super.clearValues();
        username = "";
        leavingAt = new Date();
        arrivingBack = new Date();
        destination = "";
        autoCheckIn = true;
        editor = "";
        monAM = "";
        monPM = "";
        tuesAM = "";
        tuesPM = "";
        wednesAM = "";
        wednesPM = "";
        thursAM = "";
        thursPM = "";
        friAM = "";
        friPM = "";
        monAll = false;
        tuesAll = false;
        wednesAll = false;
        thursAll = false;
        friAll = false;
        weekAll = false;
        given = "";
        surname = "";
    }
    
    public String getStandard() {
        return getEditForm();
    }
    
    public String getEditForm() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"inout\" name=\"presence\">\n");
        h.append("<table><tbody>");
        // thead
        h.append("<thead><tr><th>" + given + " " + surname + "</th>");
        h.append("<th>All Day</th><th>Morning</th><th>Afternoon</th></tr>\n</thead>");
        h.append("<tbody>");
        // Monday
        h.append("<tr><th>Monday</th>");
        h.append("<td><input id=\"monAll\" type=\"checkbox\" name=\"mondayAll\" value=\"");
        h.append(Boolean.toString(monAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("<td><input id=\"monAM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"mondayMorning\" ");
        h.append("value=\"" + monAM + "\"></td>");
        h.append("<td><input id=\"monPM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"mondayAfternoon\" ");
        h.append("value=\"" + monPM + "\"");
        if (monAll || weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("</tr>\n");
        // Tuesday
        h.append("<tr><th>Tuesday</th>");
        h.append("<td><input id=\"tuesAll\" type=\"checkbox\" name=\"tuesdayAll\" value=\"");
        h.append(Boolean.toString(tuesAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("<td><input id=\"tuesAM\" id=\"tuesAM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"tuesdayMorning\" ");
        h.append("value=\"" + tuesAM + "\"");
        if (weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("<td><input id=\"tuesPM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"tuesdayAfternoon\" ");
        h.append("value=\"" + tuesPM + "\"");
        if (tuesAll || weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("</tr>\n");
        // Wednesday
        h.append("<tr><th>Wednesday</th>");
        h.append("<td><input id=\"wednesAll\" type=\"checkbox\" name=\"wednesdayAll\" value=\"");
        h.append(Boolean.toString(wednesAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("<td><input id=\"wednesAM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"wednesdayMorning\" ");
        h.append("value=\"" + wednesAM + "\"");
        if (weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("<td><input id=\"wednesPM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"wednesdayAfternoon\" ");
        h.append("value=\"" + wednesPM + "\"");
        if (wednesAll || weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("</tr>\n");
        // Thursday
        h.append("<tr><th>Thursday</th>");
        h.append("<td><input id=\"thursAll\" type=\"checkbox\" name=\"thursdayAll\" value=\"");
        h.append(Boolean.toString(thursAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("<td><input id=\"thursAM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"thursdayMorning\" ");
        h.append("value=\"" + thursAM + "\"");
        if (weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("<td><input id=\"thursPM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"thursdayAfternoon\" ");
        h.append("value=\"" + thursPM + "\"");
        if (thursAll || weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("</tr>\n");
        // Friday
        h.append("<tr><th>Friday</th>");
        h.append("<td><input id=\"friAll\" type=\"checkbox\" name=\"fridayAll\" value=\"");
        h.append(Boolean.toString(friAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("<td><input id=\"friAM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"fridayMorning\" ");
        h.append("value=\"" + friAM + "\"");
        if (weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("<td><input id=\"friPM\" type=\"text\" size=\"40\" maxlength=\"255\" name=\"fridayAfternoon\" ");
        h.append("value=\"" + friPM + "\"");
        if (friAll || weekAll) {
            h.append(" disabled");
        }
        h.append("></td>");
        h.append("</tr>\n");
        // All week
        h.append("<tr><th>All week</th>");
        h.append("<td colspan=\"4\"><input id=\"weekAll\" type=\"checkbox\" name=\"weekAll\" value=\"");
        h.append(Boolean.toString(weekAll) + "\" onClick=\"setEntryBoxes();\"></td>");
        h.append("</tr>\n");
        // close table
        h.append("<tr><td colspan=\"4\" class=\"right\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\">");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\">");
        h.append("</td></tr>\n");
        h.append("</tbody></table></form>");
        return h.toString();
    }
    
    public String getList() {
        return "getList";
    }
    
    public String getItem() {
        StringBuffer h = new StringBuffer();
        return h.toString();
    }
    
    public boolean deleteFromDatabase() {
        return false;
    }
    
    public boolean writeToDatabase() {
        boolean successful = false;
        return successful;
    }
    
    public boolean validateData() {
        boolean valid = true;
        StringBuffer h = new StringBuffer();
        errors = h.toString();
        return valid;
    }
    
    /**
     * Analyse the request and provide a response.
     * @return the HTML response
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (action.equalsIgnoreCase("new")) {
            if (isAdmin) {
                clearValues();
                newItem = true;
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("reset")) {
            clearValues();
            h.append(getStandard());
        } else  
        if (action.equalsIgnoreCase("save")) {
            if (isAdmin || isOwner) {
                if (validateData()) {
                    if (writeToDatabase()) {
                        h.append(getSaveSuccessful());
                    } else {
                        h.append(getSaveFailed());
                    }
                    h.append(getStandard());
                } else {
                    h.append(getEditForm());
                }
            }
        } else
        if (action.equalsIgnoreCase("edit")) {
            if (isAdmin && itemSelected) {
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("delete")) {
            if (isAdmin && itemSelected) {
                if (deleteFromDatabase()) {
                    h.append(getDeleteSuccessful());
                    clearValues();
                } else {
                    h.append(getDeleteFailed());
                }
            }
        } else
        if (itemSelected) {
            h.append(getItem());
        } else {
            h.append(getStandard());
        }
        clearValues();
        action = "";
        submitPressed = false;
        return h.toString();
    }
    
    protected boolean populateFromDatabase() {
        boolean success = false;
        return success;
    }
    
    public void setMondayAll() {
        monAll = true;
    }
    
    public void setTuesdayAll() {
        tuesAll = true;
    }
    
    public void setWednesdayAll() {
        wednesAll = true;
    }
    
    public void setThursdayAll() {
        thursAll = true;
    }
    
    public void setFridayAll() {
        friAll = true;
    }
    
    public void setWeekAll() {
        weekAll = true;
    }
    
    public void setMondayMorning(String location) {
        monAM = location;
    }
    
    public void setMondayAfternoon(String location) {
        monPM = location;
    }
    
    public void setTuesdayMorning(String location) {
        tuesAM = location;
    }
    
    public void setTuesdayAfternoon(String location) {
        tuesPM = location;
    }
    
    public void setWednesdayMorning(String location) {
        wednesAM = location;
    }
    
    public void setWednesdayAfternoon(String location) {
        wednesPM = location;
    }
    
    public void setThursdayMorning(String location) {
        thursAM = location;
    }
    
    public void setThursdayAfternoon(String location) {
        thursPM = location;
    }
    
    public void setFridayMorning(String location) {
        friAM = location;
    }
    
    public void setFridayAfternoon(String location) {
        friPM = location;
    }
    
    public void setGiven(String givenName) {
        given = givenName;
        log.debug("IOBean, setGiven: " + givenName);
    }
    
    public void setSurname(String sname) {
        surname = sname;
    }
    
    /**
     * Get information useful for debugging.
     * @return HTML list of internal variables and values
     */
     public String getDebug() {
         StringBuilder h = new StringBuilder();
         h.append("<p style=\"font-size:xx-small;\"><strong>IOBean values</strong></p>\n");
         h.append("<ul style=\"font-size:xx-small;\">");
         h.append("\n<li>given= "+ given);
         h.append("\n<li>surname= " + surname);
         h.append("\n<li>monAM= " + monAM);
         h.append("\n<li>monPM= " + monPM);
         h.append("\n<li>tuesAM= " + tuesAM);
         h.append("\n<li>tuesPM= " + tuesPM);
         h.append("\n<li>wednesAM= " + wednesAM);
         h.append("\n<li>wednesPM= " + wednesPM);
         h.append("\n<li>thursAM= " + thursAM);
         h.append("\n<li>thursPM= " + thursPM);
         h.append("\n<li>friAM= " + friAM);
         h.append("\n<li>friPM= " + friPM);
         h.append("\n<li>monAll= " + monAll);
         h.append("\n<li>tuesAll= " + tuesAll);
         h.append("\n<li>wednesAll= " + wednesAll);
         h.append("\n<li>thursAll= " + thursAll);
         h.append("\n<li>friAll= " + friAll);
         h.append("\n<li>weekAll= " + weekAll);
         h.append("</ul>");
         h.append(super.getDebug());
         return h.toString();
     }
}

