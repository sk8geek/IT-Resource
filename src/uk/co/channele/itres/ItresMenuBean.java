/* 
 * @(#) ItresMenuBean.java    0.1 2007/03/17
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import uk.co.channele.MenuBean;
/**
 * Searches various sources and returns the results.
 *
 * @version       0.1    17 March 2007
 * @author        Steven Lilley
 */
public class ItresMenuBean extends MenuBean implements Serializable { 

    private String[] pageNames = {"index", "inout", "people", "inventory", "systems", "faqs.jsp", "notices", "settings.jsp", "admin" };
    private String[] optionNames = {"Home", "In/Out", "People", "Inventory", "Systems", "FAQs", "Notices", "Settings", "Admin" };
    
    public ItresMenuBean() {
        setPageNames(pageNames);
        setOptionNames(optionNames);
        hasAdminTab = true;
    }
}

