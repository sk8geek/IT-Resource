/* 
 * @(#) People.java    0.1 2011/09/19
 *
 * Copyright (C) 2008 Steven J Lilley
 * Copyright (C) 2011 Calderdale Council
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 * steven.lilley@calderdale.gov.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to instantiate a PeopleBean.
 *
 * @version		0.1     19 September 2011
 * @author		Steven Lilley
 */
public class People extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler = context.getRequestDispatcher("/people.jsp");
        IdentityBean userId;
        PeopleBean peopleBean;
        Cookie[] cookies;
        boolean userFound = false;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("UserIdentity") != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
            } else {
                userId = new IdentityBean();
                cookies = rqst.getCookies();
                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        if ((cookies[i].getName()).equalsIgnoreCase("eregid")) {
                            userId.setUserName(cookies[i].getValue());
                        }
                    }
                    if (userId.getUserName().length() > 0) {
                        userId.cookieMonster();
                    }
                    session.setAttribute("userIdentity", userId);
                    userFound = true;
                }
            }       
            if (session.getAttribute("peopleBean") == null) {
                peopleBean = new PeopleBean();
                peopleBean.readUser(userId);
                session.setAttribute("peopleBean", peopleBean);
            }
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler = context.getRequestDispatcher("/people.jsp");
        IdentityBean userId;
        PeopleBean peopleBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                if (session.getAttribute("UserIdentity") != null) {
                    userId = (IdentityBean)session.getAttribute("userIdentity");
                } else {
                    userId = new IdentityBean();
                }
                if (session.getAttribute("peopleBean") == null) {
                    peopleBean = new PeopleBean();
                    peopleBean.readUser(userId);
                    session.setAttribute("peopleBean", peopleBean);
                }
                handler.include(rqst, resp);
            }
        } // end sync
    }
}

