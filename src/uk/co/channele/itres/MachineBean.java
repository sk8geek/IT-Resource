/* 
 * @(#) MachineBean.java    0.1 2006/11/02
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * A bean to represent a piece of equipment.
 *
 * @version     0.1    2 November 2006
 * @author      Steven Lilley
 */
public class MachineBean implements Serializable {
    
    public static final String[] LIVE_VALUES = {"Unkn", "LIVE", "Mute", "Surp", "Dead"}; 
    private final SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd");
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private final String sqlGetMachineDetails = "SELECT * FROM hw_badged WHERE badge=?";
    private final String sqlUpdateMachine = "UPDATE hw_badged SET "
        + "live=?, type=?, descrip=?, sn=?, location=?, team=?, user=?, parent=?, "
        + "requis=?, ordnum=?, ccentre=?, cost=?, orddate=?, delivdate=?, installed=?, action=?, "
        + "action_date=?, covernotes=?, notes=?, os=?, ipa=? WHERE badge=? LIMIT 1";
    private final String sqlAddNewMachine = "INSERT INTO hw_badged (live, "
        + "type, descrip, sn, location, team, user, parent, requis, "
        + "ordnum, ccentre, cost, orddate, delivdate, installed, action, "
        + "action_date, covernotes, notes, os, ipa, badge) VALUES "
        + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private InitialContext initCnxt;
    private Context cnxt;
    private DataSource dataSrc;
    private Logger log;
    private boolean isAdmin = false;
    private boolean newMachine = true;
    // hw_badged field variables
    private int badge = 0;
    private String live = "";
    private String type = "";
    private String descrip = "";
    private String serialNumber = "";
    private String location = "";
    private String team = "";
    private String user = "";
    private int parent = 0;
    private String requisition = "";
    private String orderNumber = "";
    private String costCentre = "";
    private float cost = 0;
    private Date orderDate;
    private Date deliveryDate;
    private Date installDate;
    private String action = "";
    private Date actionDate;
    private String coverNotes = "";
    private String notes = "";
    private String opSys = "";
    private String ipAddress = "";
    private Date lastMod;
    
    /**
     * Constructs a MachineBean.
     * <p>Instatiates the log and data source.
     */
    public MachineBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            cnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)cnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("MachineBean constructor", nex);
        }
    }
    
    public MachineBean(int badgeNo) {
        this();
        setBadge(badgeNo);
    }
    
    public void setBadge(String badgeNo) {
        try {
            setBadge(Integer.parseInt(badgeNo));
        } catch (NumberFormatException nfex) {
            log.debug("MachineBean, setBadge: NumberFormatException");
            badge = 0;
        }
    }
    
    public void setBadge(int badgeNo) {
        badge = badgeNo;
        populateFromDatabase();
    }
    
    public void setLive(String liveMachine) {
        live = liveMachine;
    }
    
    public void setType(String equipType) {
        type = equipType;
    }
    
    public void setDescrip(String desc) {
        descrip = desc;
    }
    
    public void setSn(String sn) {
        serialNumber = sn;
    }
    
    public void setLocation(String loc) {
        location = loc;
    }
    
    public void setTeam(String tm) {
        team = tm;
    }
    
    public void setUser(String usr) {
        user = usr;
    }
    
    public void setParent(String parnt) {
        try {
            parent = Integer.parseInt(parnt);
        } catch (NumberFormatException nfex) {
            log.debug("MachineBean, setParent: NumberFormatException");
            parent = 0;
        }
    }
    
    public void setRequis(String req) {
        requisition = req;
    }
    
    public void setOrdnum(String ordnum) {
        orderNumber = ordnum;
    }
    
    public void setCcentre(String cc) {
        costCentre = cc;
    }
    
    public void setCost(String cst) {
        try {
            cost = Float.parseFloat(cst);
        } catch (NumberFormatException nfex) {
            log.debug("MachineBean, setCost: NumberFormatException");
            cost = 0;
        }
    }
    
    public void setOrddate(String ordered) {
        try {
            orderDate = inputDate.parse(ordered);
        } catch (ParseException pex) {
            log.debug("MachineBean, setOrddate: ParseException");
        }
    }   
    
    public void setDelivdate(String delivered) {
        try {
            deliveryDate = inputDate.parse(delivered);
        } catch (ParseException pex) {
            log.debug("MachineBean, setDelivdate: ParseException");
        }
    }   
    
    public void setInstalled(String installed) {
        try {
            installDate = inputDate.parse(installed);
        } catch (ParseException pex) {
            log.debug("MachineBean, setInstalled: ParseException");
        }
    }   
    
    public void setAction(String act) {
        action = act;
    }
    
    public void setActionDate(String actiondate) {
        try {
            actionDate = inputDate.parse(actiondate);
        } catch (ParseException pex) {
            log.debug("MachineBean, setActionDate: ParseException");
        }
    }   
    
    public void setCovernotes(String covnotes) {
        coverNotes = covnotes;
    }
    
    public void setNotes(String nts) {
        notes = nts;
    }
    
    public void setOs(String os) {
        opSys = os;
    }
    
    public void setIpa(String ipa) {
        ipAddress = ipa;
    }
    
    boolean machineExists() {
        return !newMachine;
    }
    
    public String getMachineDetailsTable() {
        StringBuilder h = new StringBuilder();
        DropDownBean hardwareTypes = new DropDownBean("hwtypes", "type", type);
        h.append("<table class=\"boxed\"><tbody><tr>");
        if (isAdmin) {
            h.append("<form name=\"machineEdit\" method=\"post\" action=\"savemachine.jsp\">");
        }
        // badge
        h.append("<td class=\"grey\">Badge</td>");
        if (isAdmin && newMachine) {
            h.append("<td><input type=\"text\" name=\"badge\" ");
            h.append("size=\"5\" maxlength=\"5\" value=\"");
            h.append(badge + "\"></td>");
        } else {
            h.append("<td><strong>" + badge + "</strong></td>");
        }
        h.append("</tr>\n<tr>");
        // live
        h.append("<td class=\"grey\">Live</td>");
        if (isAdmin) {
            h.append("<td>" + getLiveSelectControl() + "</td>");
        } else {
            if (live.equalsIgnoreCase("live")) {
                h.append("<td class=\"inv_live\">");
            } else 
            if (live.equalsIgnoreCase("mute")) {
                h.append("<td class=\"inv_mute\">");
            } else 
            if (live.equalsIgnoreCase("surp")) {
                h.append("<td class=\"inv_surp\">");
            } else 
            if (live.equalsIgnoreCase("dead")) {
                h.append("<td class=\"inv_dead\">");
            } else {
                h.append("<td>");
            }
            h.append(live + "</td>");
        }
        h.append("</tr>\n<tr>");
        // type
        h.append("<td class=\"grey\">Type</td>");
        if (isAdmin) {
            h.append("<td>" + hardwareTypes.getSelectControl() + "</td>");
        } else {
            h.append("<td>" + type + "</td>");
        }
        h.append("</tr>\n<tr>");
        // descrip
        h.append("<td class=\"grey\">Description</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"descrip\" ");
            h.append("size=\"30\" maxlength=\"50\" value=\"");
            h.append(descrip + "\"></td>");
        } else {
            h.append("<td>" + descrip + "</td>");
        }
        h.append("</tr>\n<tr>");
        // serialNumber
        h.append("<td class=\"grey\">S/Number</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"sn\" ");
            h.append("size=\"30\" maxlength=\"70\" value=\"");
            h.append(serialNumber + "\"></td>");
        } else {
            h.append("<td>" + serialNumber + "</td>");
        }
        h.append("</tr>\n<tr>");
        // location
        h.append("<td class=\"grey\">Office</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"location\" ");
            h.append("size=\"30\" maxlength=\"60\" value=\"");
            h.append(location + "\"></td>");
        } else {
            h.append("<td>" + location + "</td>");
        }
        h.append("</tr>\n<tr>");
        // team
        h.append("<td class=\"grey\">Team</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"team\" ");
            h.append("size=\"30\" maxlength=\"40\" value=\"");
            h.append(team + "\"></td>");
        } else {
            h.append("<td>" + team + "</td>");
        }
        h.append("</tr>\n<tr>");
        // user
        h.append("<td class=\"grey\">User</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"user\" ");
            h.append("size=\"30\" maxlength=\"30\" value=\"");
            h.append(user + "\"></td>");
        } else {
            h.append("<td>" + user + "</td>");
        }
        h.append("</tr>\n<tr>");
        // parent
        h.append("<td class=\"grey\">Parent</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"parent\" ");
            h.append("size=\"5\" maxlength=\"5\" value=\"");
            h.append(parent + "\"></td>");
        } else {
            h.append("<td>" + parent + "</td>");
        }
        h.append("</tr>\n<tr>");
        // requisition
        h.append("<td class=\"grey\">Requisition</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"requis\" ");
            h.append("size=\"20\" maxlength=\"20\" value=\"");
            h.append(requisition + "\"></td>");
        } else {
            h.append("<td>" + requisition + "</td>");
        }
        h.append("</tr>\n<tr>");
        // orderNumber
        h.append("<td class=\"grey\">Order#</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"ordnum\" ");
            h.append("size=\"10\" maxlength=\"10\" value=\"");
            h.append(orderNumber + "\"></td>");
        } else {
            h.append("<td>" + orderNumber + "</td>");
        }
        h.append("</tr>\n<tr>");
        // costCentre
        h.append("<td class=\"grey\">Cost Centre</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"ccentre\" ");
            h.append("size=\"5\" maxlength=\"5\" value=\"");
            h.append(costCentre + "\"></td>");
        } else {
            h.append("<td>" + costCentre + "</td>");
        }
        h.append("</tr>\n<tr>");
        // cost
        h.append("<td class=\"grey\">Cost</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"cost\" ");
            h.append("size=\"5\" maxlength=\"5\" value=\"");
            h.append(cost + "\"></td>");
        } else {
            h.append("<td>" + cost + "</td>");
        }
        h.append("</tr>\n<tr>");
        // orderDate
        h.append("<td class=\"grey\">Order Date (yyyy-mm-dd)</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"orddate\" ");
            h.append("size=\"10\" maxlength=\"10\" value=\"");
            if (orderDate != null) {
                h.append(inputDate.format(orderDate));
            }
            h.append("\"></td>");
        } else {
            h.append("<td>");
            if (orderDate != null) {
                h.append(inputDate.format(orderDate));
            }
            h.append("</td>");
        }
        h.append("</tr>\n<tr>");
        // deliveryDate
        h.append("<td class=\"grey\">Delivery Date (yyyy-mm-dd)</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"delivdate\" ");
            h.append("size=\"10\" maxlength=\"10\" value=\"");
            if (deliveryDate != null) {
                h.append(inputDate.format(deliveryDate));
            }
            h.append("\"></td>");
        } else {
            h.append("<td>");
            if (deliveryDate != null) {
                h.append(inputDate.format(deliveryDate));
            }
            h.append("</td>");
        }
        h.append("</tr>\n<tr>");
        // installDate
        h.append("<td class=\"grey\">Install Date (yyyy-mm-dd)</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"installed\" ");
            h.append("size=\"10\" maxlength=\"10\" value=\"");
            if (installDate != null) {
                h.append(inputDate.format(installDate));
            }
            h.append("\"></td>");
        } else {
            h.append("<td>");
            if (installDate != null) {
                h.append(inputDate.format(installDate));
            }
            h.append("</td>");
        }
        h.append("</tr>\n<tr>");
        // action
        h.append("<td class=\"grey\">Action</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"action\" ");
            h.append("size=\"4\" maxlength=\"4\" value=\"");
            h.append(action + "\"></td>");
        } else {
            h.append("<td>" + action + "</td>");
        }
        h.append("</tr>\n<tr>");
        // actionDate
        h.append("<td class=\"grey\">Action Date (yyyy-mm-dd)</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"actionDate\" ");
            h.append("size=\"5\" maxlength=\"5\" value=\"");
            if (actionDate != null) {
                h.append(inputDate.format(actionDate));
            }
            h.append("\"></td>");
        } else {
            h.append("<td>");
            if (actionDate != null) {
                h.append(inputDate.format(actionDate));
            }
            h.append("</td>");
        }
        h.append("</tr>\n<tr>");
        // coverNotes
        h.append("<td class=\"grey\">Cover Notes</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"covernotes\" ");
            h.append("size=\"30\" maxlength=\"100\" value=\"");
            h.append(coverNotes + "\"></td>");
        } else {
            h.append("<td>" + coverNotes + "</td>");
        }
        h.append("</tr>\n<tr>");
        // opSys
        h.append("<td class=\"grey\">Op.Sys</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"os\" ");
            h.append("size=\"3\" maxlength=\"3\" value=\"");
            h.append(opSys + "\"></td>");
        } else {
            h.append("<td>" + opSys + "</td>");
        }
        h.append("</tr>\n<tr>");
        // ipAddress
        h.append("<td class=\"grey\">IP Address</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"ipa\" ");
            h.append("size=\"15\" maxlength=\"15\" value=\"");
            h.append(ipAddress + "\"></td>");
        } else {
            h.append("<td>" + ipAddress + "</td>");
        }
        h.append("</tr>\n<tr>");
        // lastMod
        h.append("<td class=\"grey\">Modified</td>");
        h.append("<td>" + itemDateTime.format(lastMod) + "</td>");
        h.append("</tr>\n<tr>");
        // notes
        h.append("<td colspan=\"2\" class=\"grey\">Notes</td></tr>\n<tr><td colspan=\"2\">");
        if (isAdmin) {
            h.append("<textarea name=\"notes\" ");
            h.append("rows=\"4\" columns=\"30\">");
            h.append(notes + "</textarea></td>");
        } else {
            h.append(notes + "</td>");
        }
        h.append("</tr>");
        if (isAdmin) {
            h.append("<tr><td colspan=\"2\" style=\"text-align: right\">");
            h.append("<input type=\"submit\" name=\"saveMachine\" value=\"Save\">");
            h.append("<input type=\"reset\" value=\"Reset\"></td></tr>");
        }
        h.append("</tbody></table>");
        return h.toString();
    }
    
    public String getLiveSelectControl() {
        StringBuilder h = new StringBuilder();
        h.append("<select name=\"live\">");
        for (int i = 0; i < LIVE_VALUES.length; i++) {
            h.append("<option value=\"" + LIVE_VALUES[i]);
            if (live.equalsIgnoreCase(LIVE_VALUES[i])) {
                h.append(" selected");
            }
            h.append(">" + LIVE_VALUES[i] + "</option>\n");
        }
        h.append("</select");
        return h.toString();
    }
    
    private boolean populateFromDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement getMachineDetails;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getMachineDetails = con.prepareStatement(sqlGetMachineDetails);
            getMachineDetails.clearParameters();
            getMachineDetails.setInt(1, badge);
            result = getMachineDetails.executeQuery();
            if (result.next()) {
                badge = result.getInt("badge");
                live = result.getString("live");
                type = result.getString("type");
                descrip = result.getString("descrip");
                serialNumber = result.getString("sn");
                location = result.getString("location");
                team = result.getString("team");
                user = result.getString("user");
                parent = result.getInt("parent");
                requisition = result.getString("requis");
                orderNumber = result.getString("ordnum");
                costCentre = result.getString("ccentre");
                cost = result.getFloat("cost");
                orderDate = result.getDate("orddate");
                deliveryDate = result.getDate("delivdate");
                installDate = result.getDate("installed");
                action = result.getString("action");
                actionDate = result.getDate("action_date");
                coverNotes = result.getString("covernotes");
                notes = result.getString("notes");
                opSys = result.getString("os");
                ipAddress = result.getString("ipa");
                lastMod = result.getTimestamp("modified");
            }
            success = true;
            newMachine = false;
            result.close();
            getMachineDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("MachineBean, populateFromDatabase", sqlex);
        }
        result = null;
        getMachineDetails = null;
        con = null;
        return success;
    }
    
    private boolean writeToDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement saveMachineDetails;
        try {
            con = dataSrc.getConnection();
            if (newMachine) {
                saveMachineDetails = con.prepareStatement(sqlAddNewMachine);
            } else {
                saveMachineDetails = con.prepareStatement(sqlUpdateMachine);
            }
            saveMachineDetails.clearParameters();
            saveMachineDetails.setString(1, live);
            saveMachineDetails.setString(2, type);
            saveMachineDetails.setString(3, descrip);
            saveMachineDetails.setString(4, serialNumber);
            saveMachineDetails.setString(5, location);
            saveMachineDetails.setString(6, team);
            saveMachineDetails.setString(7, user);
            saveMachineDetails.setInt(8, parent);
            saveMachineDetails.setString(9, requisition);
            saveMachineDetails.setString(10, orderNumber);
            saveMachineDetails.setString(11, costCentre);
            saveMachineDetails.setFloat(12, cost);
            saveMachineDetails.setDate(13, new java.sql.Date(orderDate.getTime()));
            saveMachineDetails.setDate(14, new java.sql.Date(deliveryDate.getTime()));
            saveMachineDetails.setDate(15, new java.sql.Date(installDate.getTime()));
            saveMachineDetails.setString(16, action);
            saveMachineDetails.setDate(17, new java.sql.Date(actionDate.getTime()));
            saveMachineDetails.setString(18, coverNotes);
            saveMachineDetails.setString(19, notes);
            saveMachineDetails.setString(20, opSys);
            saveMachineDetails.setString(21, ipAddress);
            saveMachineDetails.setInt(22, badge);
            if (saveMachineDetails.executeUpdate() == 1) {
                success = true;
                newMachine = false;
                log.debug("MachineBean, writeToDatabase: badge " + badge + "written to database.");
            }
            saveMachineDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("MachineBean, writeToDatabase", sqlex);
        }
        saveMachineDetails = null;
        con = null;
        return success;
    }
    
    void setAdminEnabled(boolean admin) {
        isAdmin = admin;
    }
}

