/* 
 * @(#) Resource.java	0.1 2005/08/01
 * 
 * Copyright (C) 2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

/**
 * A (server) resource object to contain connection information.
 *
 * @version		0.1 1 August 2005
 * @author		Steven Lilley
 */
public class Resource {

    /** The type pf service provided, such as LDAP or SMTP. */
    private String serviceType = new String();
    /** The host name of the server that provides this service. */
    private String hostName = new String();
    /** The port number on the server that provides this service. */
    private int portNumber = 0;
    /** A valid username for this service. */
    private String userName = new String();
    /** A corresponding (valid) password for this service. */
    private String password = new String();
    /** Additional service related information.  
     * Expected uses for common service types are as follows.<br>
     * LDAP - The base domain to use in searches
     * SMTP - The From address to use in messages
     */
    private String moreInfo = new String();

    /**
     * A no argument constructor.
     */
    Resource() {
    }
    
    /**
     * A minimal constructor method.
     * @param type the service type offered by this resource
     * @param host the server host name
     * @param port the port number of the service
     */
    Resource(String type, String host, int port) {
        serviceType = type;
        hostName = host;
        portNumber = port;
    }
    
    /**
     * A full constructor method.
     * @param type     the service type offered by this resource
     * @param host     the server host name
     * @param port     the port number of the service
     * @param user     a (valid) user name for the service
     * @param pw       a corresponding password for the service
     * @param moreinfo additional service information (e.g. LDAP base domain)
     */
    Resource(String type, String host, int port, String user, String pw, 
        String moreinfo) {
        this(type, host, port);
        userName = user;
        password = pw;
        moreInfo = moreinfo;
    }
    
    /** 
     * Get the common service type of this resource.
     * Expected values are SMTP or LDAP.
     * @return the service type for this resource
     */
    public String getType() {
        return serviceType;
    }
    
    /** 
     * Get the server host name for this resource.
     * @return the host name for this resource
     */
    public String getHostName() {
        return hostName;
    }
    
    /** 
     * Get the port number used by the server for this service.
     * @return the port number to use for this resource
     */
    public int getPortNumber() {
        return portNumber;
    }
    
     /**
     * Get the (valid) user name for this resource.
     * @return the user name for this resource
     */
     public String getUserName() {
         return userName;
     }

     /**
     * Get the password corresponding to the user name this resource.
     * @return the password for this resource
     */
     public String getPassword() {
         return password;
     }

    /**
     * Get additional information for this resource.
     * Expected uses for common service types are as follows.<br>
     * LDAP - The base domain to use in searches
     * SMTP - The From address to use in messages
     * @return the additional information for this resource
     */
     public String getMoreInfo() {
         return moreInfo;
     }

}

