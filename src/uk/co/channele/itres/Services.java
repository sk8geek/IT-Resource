/* 
 * @(#) Services.java	0.1 2007/08/19
 * 
 * A servlet to instantiate the SYstemsStatusBean.
 * Copyright (C) 2006-2007 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@sk8hx.org.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to initialise/set editable a ServicesStatusBean.
 *
 * @version		0.1     19 August 2007
 * @author		Steven Lilley
 */
public class Services extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        SystemsStatusBean serviceBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("userIdentity") != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
            } else {
                userId = new IdentityBean();
            }
            if (userId.isRegistered()) {
                if (session.getAttribute("serviceBean") == null) {
                    serviceBean = new SystemsStatusBean();
                    session.setAttribute("serviceBean", serviceBean);
                } else {
                    serviceBean = (SystemsStatusBean)session.getAttribute("serviceBean");
                }
                serviceBean.setUserId(userId.getUserName());
                serviceBean.setRemoteHost(rqst.getRemoteHost());
            }
            handler = context.getRequestDispatcher("/systems.jsp");
            handler.include(rqst, resp);
        } // end sync
    }
}

