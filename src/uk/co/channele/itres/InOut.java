/* 
 * @(#) InOut.java    0.1 2012/01/14
 *
 * Copyright (C) 2008-2011 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 * steven.lilley@calderdale.gov.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * @version		0.1     14 January 2012
 * @author		Steven Lilley
 */
public class InOut extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler = context.getRequestDispatcher("/inout.jsp");
        IdentityBean userId;
        IOBean ioBean;
        Cookie[] cookies;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("UserIdentity") != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
            } else {
                userId = new IdentityBean();
                cookies = rqst.getCookies();
                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        if ((cookies[i].getName()).equalsIgnoreCase("eregid")) {
                            userId.setUserName(cookies[i].getValue());
                        }
                    }
                    if (userId.getUserName().length() > 0) {
                        userId.cookieMonster();
                    }
                    session.setAttribute("userIdentity", userId);
                }
            }       
            if (session.getAttribute("ioBean") == null) {
                ioBean = new IOBean();
            } else {
                ioBean = (IOBean)session.getAttribute("ioBean");
            }
            ioBean.readUser(userId);
            ioBean.setGiven(userId.getGiven());
            ioBean.setSurname(userId.getSurname());
            // FIXME: ioBean.setUsualWorkTimes(userId.getUsualWorkTimes());
            session.setAttribute("ioBean", ioBean);
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler = context.getRequestDispatcher("/inout.jsp");
        IdentityBean userId;
        IOBean ioBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                if (session.getAttribute("UserIdentity") != null) {
                    userId = (IdentityBean)session.getAttribute("userIdentity");
                } else {
                    userId = new IdentityBean();
                }
                if (session.getAttribute("ioBean") == null) {
                    ioBean = new IOBean();
                    ioBean.readUser(userId);
                    session.setAttribute("ioBean", ioBean);
                }
                handler.include(rqst, resp);
            }
        } // end sync
    }
}

