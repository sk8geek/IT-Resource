/* 
 * @(#) CheckIn.java	0.1 2006/10/19
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to set a cookie when a user checks in.
 *
 * @version		0.1     19 October 2006
 * @author		Steven Lilley
 */
public class CheckIn extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        MessageBean msgBean;
        String returnPage = "/index.jsp";
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                returnPage = "/" + userId.getCurrentPage();
                userId.checkInUser();
                if (userId.allowCookie() && userId.isRegistered()) {
                    Cookie eregCookie = new Cookie("eregid", userId.getUserName());
                    eregCookie.setMaxAge(5184000);
                    resp.addCookie(eregCookie);
                }
                if (userId.isRegistered()) {
                    if (session.getAttribute("msgBean") == null) {
                        msgBean = new MessageBean();
                        session.setAttribute("msgBean", msgBean);
                    } else {
                        msgBean = (MessageBean)session.getAttribute("msgBean");
                    }
                    msgBean.setUserName(userId.getUserName());
                }
            }
            handler = context.getRequestDispatcher(returnPage);
            handler.include(rqst, resp);
        } // end sync
    }
}

