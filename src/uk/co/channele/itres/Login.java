/* 
 * @(#) Login.java	0.1 2006/10/18
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
/**
 * Servlet to log in a user.
 *
 * @version		0.1     18 October 2006
 * @author		Steven Lilley
 */
public class Login extends HttpServlet { 

	private ServletContext context;
    private Logger log;

	public void init() {
        log = Logger.getLogger("itres");
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        String returnPage = "/login.jsp";
        log.debug("Login, doGet called");
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                if (session.getAttribute("userIdentity") != null) {
                    userId = (IdentityBean)session.getAttribute("userIdentity");
                    log.debug("Login, userId = " + userId.getUserName());
                    log.debug("Login, currentPage = " + userId.getCurrentPage());
                    if (userId.getUserName().length() > 0) {
                        returnPage = "/" + userId.getCurrentPage();
                    }
                }
            }
            handler = context.getRequestDispatcher(returnPage);
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        ItresMenuBean menuBean;
        AdminBean admin;
        String password = "";
        String returnPage = "/login.jsp";
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                log.debug("Login, doPost: session is not null.");
                userId = (IdentityBean)session.getAttribute("userIdentity");
                returnPage = "/" + userId.getCurrentPage();
                if (rqst.getParameter("userName") != null) {
                    userId.setUserName(rqst.getParameter("userName"));
                    log.debug("Login, doPost: userName parameter = " + userId.getUserName());
                }
                if (rqst.getParameter("currentPassword") != null) {
                    userId.setCurrentPassword(rqst.getParameter("currentPassword"));
                    if (userId.attemptLogin()) {
                        log.debug("Login, doPost user logged in (" + userId.getUserName());
                        returnPage="/index.jsp";
                        if (userId.allowCookie() && userId.isRegistered()) {
                            Cookie eregCookie = new Cookie("eregid", userId.getUserName());
                            eregCookie.setMaxAge(5184000);
                            resp.addCookie(eregCookie);
                        }
                        if (userId.getUserLevel() > 4) {
                            log.debug("Login, doPost, user is above level 4");
                            if (session.getAttribute("admin") == null) {
                                // no AdminBean created yet, create one
                                admin = new AdminBean();
                                session.setAttribute("admin", admin);
                                if (session.getAttribute("menu") != null) {
                                    menuBean = (ItresMenuBean)session.getAttribute("menu");
                                    menuBean.setAdminTab(true);
                                }
                            }
                        }
                    } else {
                        // login failed
                        log.debug("Login, doPost: login failed.");
                    }
                }
            } else {
                log.debug("Login, doPost: session IS NULL.");
            }
            handler = context.getRequestDispatcher(returnPage);
            handler.include(rqst, resp);
        } // end sync
    }
}

