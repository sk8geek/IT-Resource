/* 
 * @(#) PeopleBean.java    0.1 2011/09/19
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;

/**
 * A bean to represent people (in estPeople table)
 *
 * @version     0.1    19 September 2011
 * @author      Steven Lilley
 */
public class PeopleBean extends GenericBean {
    
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private final String sqlGetPerson = "SELECT * FROM estPeople WHERE id=?";
    private final String sqlGetPersonView = "SELECT * FROM estUserView WHERE id=?";
    private final String sqlAddNewPerson = "INSERT INTO estPeople (lid, dir, given, surname, startDate) VALUES "
        + "(?, ?, ?, ?, ?)";
    private final String sqlGetLatestId = "SELECT LAST_INSERT_ID()";
    private final String sqlUpdatePerson = "UPDATE estPeople SET lid=?,dir=?,given=?,surname=?,startDate=? "
        + "WHERE id=? LIMIT 1";
    private final String sqlEndPerson = "UPDATE estPeople SET endDate=? WHERE id=? LIMIT 1";
    private final String sqlEndPerson2 = "INSERT INTO estLeavers SELECT * FROM estPeople WHERE id=? LIMIT 1";
    private final String sqlDeletePerson = "DELETE FROM estPeople WHERE id=? LIMIT 1";
    private final String sqlGetPeopleListByLid = "SELECT * FROM estPeople ORDER BY lid, startDate";
    private final String sqlSearchPeople = "SELECT * FROM estPeople WHERE lid LIKE ? OR "
        + "given LIKE ? OR surname LIKE ? ORDER BY lid,id";
    private final String sqlDisownDevice = "DELETE FROM estUserDevices WHERE personId=? AND deviceId=? "
        + "LIMIT 1";
    private final String sqlOwnDevice = "INSERT INTO estUserDevices (personId,deviceId) VALUES (?,?)";
    private final String sqlGetCmsRoles = "SELECT * FROM cmsUsers WHERE lid=? ORDER BY community,role";
    // user field variables
    private int id = 0;
    private String lid = "";
    private String dir = "COM";
    private String given = "";
    private String surname = "";
    private Date startDate;
    private Date endDate;
    private int badge = 0;
    
    /**
     * Constructs a PeopleBean.
     * <p>Instatiates the log and data source.
     */
    public PeopleBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("PeopleBean constructor", nex);
        }
    }
    
    public void readUser(GenericBean userBean) {
        setUserLevel(userBean.getUserLevel());
        log.debug("PeopleBean, readUser set user level to " + getUserLevel());
    }
    
    public void clearValues() {
        super.clearValues();
        id = 0;
        lid = "";
        given = "";
        surname = "";
        startDate = null;
        endDate = null;
    }
    
    public String getStandard() {
        return getSearchForm();
    }
    
    /**
     * Gets search results based on the criteria set.
     * Expanded to include search in cmsUsers.
     * @return HTML string
     */
    public String getSearchResults() {
        StringBuffer h = new StringBuffer();
        h.append(getSearchForm());
        boolean tableHeaderDone = false;
        Connection con;
        Connection cmsCon;
        PreparedStatement getSearchResults;
        PreparedStatement getCmsResults;
        ResultSet results;
        ResultSet cmsResults;
        try {
            con = dataSrc.getConnection();
            cmsCon = dataSrc.getConnection();
            getSearchResults = con.prepareStatement(sqlSearchPeople);
            getCmsResults = con.prepareStatement(sqlGetCmsRoles);
            getSearchResults.clearParameters();
            getSearchResults.setString(1, criteria);
            getSearchResults.setString(2, criteria);
            getSearchResults.setString(3, criteria);
            results = getSearchResults.executeQuery();
            while (results.next()) {
                if (!tableHeaderDone) {
                    h.append("<table><thead><tr><th>LID</th><th>Given</th><th>Surname</th>");
                    h.append("<th class=\"right\">Start date</th><th>CMS Roles</th></tr></thead><tbody>");
                    tableHeaderDone = true;
                }
                h.append("<tr>");
                h.append("<td><a href=\"people?id=" + results.getInt("id") + "\" ");
                h.append("title=\"" + results.getInt("id") + "\">");
                h.append(results.getString("lid") + "</a></td>");
                h.append("<td>" + results.getString("given") + "</td>");
                h.append("<td>" + results.getString("surname") + "</td>");
                if (results.getDate("startDate") != null) {
                    h.append("<td class=\"right\">" + itemDate.format(results.getDate("startDate")) + "</td>");
                } else {
                    h.append("<td class=\"right\">[null]</td>");
                }
                // include CMS results if any
                h.append("<td>");
                getCmsResults.clearParameters();
                getCmsResults.setString(1, results.getString("lid"));
                cmsResults = getCmsResults.executeQuery();
                while (cmsResults.next()) {
                    try {
                        h.append(cmsResults.getString("role").substring(0, 2));
                    } catch (IndexOutOfBoundsException ex) {
                        log.error("PeopleBean, getSearchResults", ex);
                        h.append("xx");
                    }
                }
                cmsResults.close();
                h.append("</td>");
                h.append("</tr>");                
            }
            if (tableHeaderDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p>No results found.</p>");
            }
            results.close();
            getSearchResults.close();
            getCmsResults.close();
            con.close();
            cmsCon.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, getSearchResults", sqlex);
            h.append(sqlex);
        }
        results = null;
        cmsResults = null;
        getSearchResults = null;
        getCmsResults = null;
        con = null;
        cmsCon = null;
        return h.toString();
    }
    
    public String getEditForm() {
        StringBuffer h = new StringBuffer();
        String started = "";
        String ended = "";
        if (startDate != null) {
            started = inputDate.format(startDate);
        }
        if (endDate != null) {
            ended = inputDate.format(endDate);
        }
        h.append("<form method=\"post\" action=\"people\" name=\"editForm\">\n");
        h.append("<table><tbody>");
        // LID
        h.append("<tr><th>LID</th><td>");
        h.append("<input type=\"text\" size=\"4\" maxlength=\"4\" name=\"lid\" ");
        h.append("value=\"" + lid + "\">");
        h.append("</td></tr>");
        // dir
        h.append("<tr><th>Directorate</th><td>");
        h.append("<input type=\"text\" size=\"4\" maxlength=\"4\" name=\"directorate\" ");
        h.append("value=\"" + dir + "\">");
        h.append("</td></tr>");
        // given
        h.append("<tr><th>Given name</th><td>");
        h.append("<input type=\"text\" size=\"20\" maxlength=\"20\" name=\"given\" ");
        h.append("value=\"" + given + "\">");
        h.append("</td></tr>");
        // surname
        h.append("<tr><th>Surname</th><td>");
        h.append("<input type=\"text\" size=\"30\" maxlength=\"30\" name=\"surname\" ");
        h.append("value=\"" + surname + "\">");
        h.append("</td></tr>");
        // startDate
        h.append("<tr><th>Start date (yyyy-mm-dd)</th><td>");
        h.append("<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"startDate\" ");
        h.append("value=\"" + started + "\">");
        h.append("</td></tr>");
        // close table
        h.append("<tr><td colspan=\"2\" class=\"right\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\">");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\">");
        h.append("</td></tr>");
        h.append("</tbody></table></form>");
        return h.toString();
    }
    
    public String getLeaverForm() {
        StringBuffer h = new StringBuffer();
        String ending = inputDate.format(new Date());
        if (endDate != null) {
            ending = inputDate.format(endDate);
        }
        h.append("<form method=\"post\" action=\"people\" name=\"editForm\">\n");
        h.append("<table><tbody>");
        // LID
        h.append("<tr><th>LID</th><td>");
        h.append(lid);
        h.append("</td></tr>");
        // given
        h.append("<tr><th>Name</th><td>");
        h.append(given + " " + surname);
        h.append("</td></tr>");
        // endDate
        h.append("<tr><th>End date (yyyy-mm-dd)</th><td>");
        h.append("<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"endDate\" ");
        h.append("value=\"" + ending + "\">");
        h.append("</td></tr>");
        // close table
        h.append("<tr><td colspan=\"2\" class=\"right\">");
        h.append("<input type=\"hidden\" name=\"id\" value=\"" + id + "\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"End person\" class=\"button\">");
        h.append("</td></tr>");
        h.append("</tbody></table></form>");
        return h.toString();
    }
    
    public String getSearchForm() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"people\" name=\"peopleSearchForm\">\n");            
        h.append("<p>Search for: <input type=\"text\" name=\"searchFor\" ");
        h.append("size=\"10\" maxlength=\"20\" onMouseOver=\"this.focus();\">\n");
        h.append("<input type=\"hidden\" name=\"action\" value=\"search\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"button\">");
        h.append("<a href=\"people?action=new\" class=\"button\">Add New</a>\n");
        h.append("</p></form>\n");
        return h.toString();
    }
    
    public String getList() {
        return "getList";
    }
    
    public String getItem() {
        StringBuffer h = new StringBuffer();
        h.append(getSearchForm());
        boolean tableHeaderDone = false;
        int deviceId = 0;
        Connection con;
        PreparedStatement getPerson;
        ResultSet results;
        try {
            con = dataSrc.getConnection();
            getPerson = con.prepareStatement(sqlGetPersonView);
            getPerson.clearParameters();
            getPerson.setInt(1, id);
            results = getPerson.executeQuery();
            while (results.next()) {
                if (!tableHeaderDone) {
                    h.append("<table>");
                    h.append("<tr>");
                    h.append("<th>ID</th>");
                    h.append("<td>" + id + "</td>");
                    h.append("</tr>");
                    h.append("<tr>");
                    h.append("<th>LID</th>");
                    h.append("<td>" + lid + "</td>");
                    h.append("</tr>");
                    h.append("<tr>");
                    h.append("<th>Given name</th>");
                    h.append("<td>" + given + "</td>");
                    h.append("</tr>");
                    h.append("<tr>");
                    h.append("<th>Surname</th>");
                    h.append("<td>" + surname + "</td>");
                    h.append("</tr>");
                    h.append("<tr>");
                    h.append("<th>Start date</th>");
                    if (startDate != null) {
                        h.append("<td>" + itemDate.format(startDate) + "</td>");
                    } else {
                        h.append("<td>[null]</td>");
                    }
                    h.append("</tr>");
                    h.append("<tr>");
                    h.append("<th>Site</th>");
                    h.append("<td>" + results.getString("siteName") + "</td>");
                    h.append("</tr>\n");
                    if (isAdmin) {
                        h.append("<tr><td colspan=\"2\" class=\"right\">");
                        h.append("<a href=\"people?action=edit&id=");
                        h.append(id);
                        h.append("\" class=\"button\">Edit</a>");
                        h.append("<a href=\"people?action=end&id=");
                        h.append(id);
                        h.append("\" class=\"button\">");
                        h.append("End</a>");
                        h.append("</td></tr>");
                    }
                    h.append("</table>\n");
                    if (results.getInt("deviceId") > 0) {
                        h.append("<table><thead><tr><th>Badge</th><th>Office</th>");
                        if (isAdmin) {
                            h.append("<th>Actions</th>");
                        }
                        h.append("</tr></thead>");
                        h.append("<tbody>");
                    }
                    tableHeaderDone = true;
                }
                if (tableHeaderDone && results.getInt("deviceId") > 0) {
                    deviceId = results.getInt("deviceId");
                    h.append("<tr><td>");
                    h.append("<a href=\"/inventory?id=" + deviceId + "\">" + deviceId + "</td>");
                    h.append("<td>" + results.getString("officeName") + "</td>");
                    if (isAdmin) {
                        h.append("<td><a href=\"/people?badge=" + deviceId + "&id=" + id);
                        h.append("&action=disown\">[disown]</a></td>");
                    }

                    h.append("</tr>\n");
                }
            }
            if (tableHeaderDone) {
                h.append("</tbody></table>\n");
            } else {
                h.append("<p>[Record not found.]</p>\n");
            }
            if (isAdmin) {
                h.append("<form method=\"post\" action=\"people\" name=\"associateDeviceForm\">\n");            
                h.append("<p>Badge to link to user: <input type=\"text\" name=\"badge\" ");
                h.append("size=\"6\" maxlength=\"6\" onMouseOver=\"this.focus();\">\n");
                h.append("<input type=\"hidden\" name=\"action\" value=\"link\">\n");
                h.append("<input type=\"hidden\" name=\"id\" value=\"" + id + "\">\n");
                h.append("<input type=\"submit\" name=\"submit\" value=\"Link\" class=\"button\" >\n");
                h.append("</form>\n");
            }
            results.close();
            getPerson.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, getItem", sqlex);
            h.append(sqlex);
        }
        results = null;
        getPerson = null;
        con = null;
        return h.toString();
    }
    
    public boolean deleteFromDatabase() {
        return false;
    }
    
    public boolean writeToDatabase() {
        boolean successful = false;
        Connection con;
        PreparedStatement writePerson;
        PreparedStatement getLatestId;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            if (newItem) {
                writePerson = con.prepareStatement(sqlAddNewPerson);
                writePerson.clearParameters();
                writePerson.setString(1, lid);
                writePerson.setString(2, dir);
                writePerson.setString(3, given);
                writePerson.setString(4, surname);
                if (startDate != null) {
                    writePerson.setDate(5, new java.sql.Date(startDate.getTime()));
                } else {
                    writePerson.setDate(5, null);
                }
            } else {
                writePerson = con.prepareStatement(sqlUpdatePerson);
                writePerson.clearParameters();
                writePerson.setString(1, lid);
                writePerson.setString(2, dir);
                writePerson.setString(3, given);
                writePerson.setString(4, surname);
                if (startDate != null) {
                     writePerson.setDate(5, new java.sql.Date(startDate.getTime()));
                } else {
                    writePerson.setDate(5, null);
                }
                writePerson.setInt(6, id);
            }
            if (writePerson.executeUpdate() == 1) {
                if (newItem) {
                    getLatestId = con.prepareStatement(sqlGetLatestId);
                    result = getLatestId.executeQuery();
                    while (result.next()) {
                        id = result.getInt(1);
                    }
                    if (id > 0) {
                        successful = true;
                        newItem = false;
                    } else {
                        newItem = true;
                    }
                    result.close();
                    getLatestId.close();
                } else {
                    successful = true;
                }
            }
            writePerson.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, writeToDatabase", sqlex);
        }
        result = null;
        getLatestId = null;
        writePerson = null;
        con = null;
        return successful;
    }
    
    public boolean validateData() {
        boolean valid = true;
        StringBuffer h = new StringBuffer();
        if (lid.length() != 4) {
            h.append("lid,");
            valid = false;
        }
        if (dir.isEmpty()) {
            h.append("dir,");
            valid = false;
        }
        if (given.isEmpty()) {
            h.append("given,");
            valid = false;
        }
        errors = h.toString();
        return valid;
    }
    
    public void setId(String newId) {
        log.debug("PeopleBean, setId");
        try {
            id = Integer.parseInt(newId);
            log.debug("PeopleBean, setId sets id=" + id);
            if (!submitPressed) {
                if (!populateFromDatabase()) {
                    log.debug("PeopleBean, setId, populate failed");
                } else {
                    log.debug("PeopleBean, setId, populate OKAY");
                }
            }            
        } catch (NumberFormatException ex) {
            id = 0;
            itemSelected = false;
            log.error("PeopleBean, setId", ex);
        }
    }
    
    public void setSearchFor(String snippet) {
        criteria = "%" + snippet + "%";
    }
    
    public void setLid(String logicalId) {
        lid = logicalId.toUpperCase();
    }
    
    public void setDirectorate(String directorate) {
        dir = directorate;
    }
    
    public void setGiven(String givenName) {
        given = givenName;
    }
    
    public void setSurname(String sname) {
        surname = sname;
    }
    
    public void setStartDate(Date started) {
        startDate = new Date(started.getTime());
    }
    
    public void setStartDate(String startDateAsString) {
        try {
            startDate = inputDate.parse(startDateAsString);
        } catch (ParseException pex) {
            log.error("PeopleBean, setStartDate(String)", pex);
        }
    }

    public void setEndDate(String endDateAsString) {
        try {
            endDate = inputDate.parse(endDateAsString);
        } catch (ParseException pex) {
            log.error("PeopleBean, setEndDate(String)", pex);
        }
    }
    
    public void setBadge(String deviceId) {
        try {
            badge = Integer.parseInt(deviceId);
        } catch (NumberFormatException ex) {
            badge = 0;
            log.error("PeopleBean, setBadge", ex);
        }
    }

    /**
     * Analyse the request and provide a response.
     * @return the HTML response
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (action.equalsIgnoreCase("new")) {
            if (isAdmin) {
                clearValues();
                newItem = true;
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("reset")) {
            clearValues();
            h.append(getStandard());
        } else  
        if (action.equalsIgnoreCase("save")) {
            if (isAdmin) {
                if (validateData()) {
                    if (writeToDatabase()) {
                        h.append(getSaveSuccessful());
                    } else {
                        h.append(getSaveFailed());
                    }
                    h.append(getStandard());
                } else {
                    h.append(getEditForm());
                }
            }
        } else
        if (action.equalsIgnoreCase("end")) {
            if (isAdmin && itemSelected) {
                h.append(getLeaverForm());
            }
        } else
        if (action.equalsIgnoreCase("end person")) {
            if (isAdmin && itemSelected) {
                h.append(evictPerson());
                h.append(getStandard());
            }
        } else
        if (action.equalsIgnoreCase("link")) {
            if (isAdmin && itemSelected) {
                if (badge > 0) {
                    h.append(ownDevice());
                    h.append(getItem());
                }
            }
        } else
        if (action.equalsIgnoreCase("disown")) {
            if (isAdmin && itemSelected) {
                if (badge > 0) {
                    h.append(disownDevice());
                    h.append(getItem());
                }
            }
        } else
        if (action.equalsIgnoreCase("edit")) {
            if (isAdmin && itemSelected) {
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("delete")) {
            if (isAdmin && itemSelected) {
                if (deleteFromDatabase()) {
                    h.append(getDeleteSuccessful());
                    clearValues();
                } else {
                    h.append(getDeleteFailed());
                }
            }
        } else
        if (criteria.length() > 0) {
            h.append(getSearchResults());
            criteria = "";
        } else {
            if (itemSelected) {
                h.append(getItem());
            } else {
                h.append(getStandard());
            }
            clearValues();
        }
        action = "";
        submitPressed = false;
        return h.toString();
    }

    private String evictPerson() {
        StringBuffer h = new StringBuffer();
        Connection con;
        PreparedStatement endPerson;
        try {
            con = dataSrc.getConnection();
            endPerson = con.prepareStatement(sqlEndPerson);
            endPerson.clearParameters();
            if (endDate != null) {
                endPerson.setDate(1, new java.sql.Date(endDate.getTime()));
            } else {
                endPerson.setDate(1, new java.sql.Date(new Date().getTime()));
            }
            endPerson.setInt(2, id);
            if (endPerson.executeUpdate() == 1) {
                endPerson = con.prepareStatement(sqlEndPerson2);
                endPerson.clearParameters();
                endPerson.setInt(1, id);
                if (endPerson.executeUpdate() == 1) {
                    endPerson = con.prepareStatement(sqlDeletePerson);
                    endPerson.clearParameters();
                    endPerson.setInt(1, id);
                    if (endPerson.executeUpdate() == 1) {
                        h.append("<p class=\"success\">" + given + " " + surname);
                        h.append(" has left the building!</p>");
                        clearValues();
                    } else {
                        h.append("<p class=\"failure\">There was a problem deleting ");
                        h.append(given + " " + surname + " from the people table.</p>");
                    }
                } else {
                    h.append("<p class=\"failure\">There was a problem moving ");
                    h.append(given + " " + surname + " to the leavers table.</p>");
                }
            } else {
                h.append("<p class=\"failure\">Unable to end date for ");
                h.append(given + " " + surname + ".</p>");
            }
            endPerson.close();
            con.close();
        } catch (SQLException sqlex) {
            h.append("<p class=\"failure\">Oops! Database problem: " + sqlex);
            log.error("PeopleBean, evictPerson", sqlex);
        } catch (Exception ex) {
            h.append("<p class=\"failure\">An problem occured: " + ex);
            log.error("PeopleBean, evictPerson", ex);
        }
        endPerson = null;
        con = null;
        return h.toString();
    }
    
    protected boolean populateFromDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement getPerson;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getPerson = con.prepareStatement(sqlGetPerson);
            getPerson.clearParameters();
            getPerson.setInt(1, id);
            log.debug("PeopleBean, populateFromDatebase called with id=" + id);
            result = getPerson.executeQuery();
            if (result.next()) {
                lid = result.getString("lid");
                given = result.getString("given");
                surname = result.getString("surname");
                if (result.getDate("startDate") != null) {
                    startDate = result.getDate("startDate");
                }
                newItem = false;
                itemSelected = true;
                success = true;
            }   
            result.close();
            getPerson.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, populateFromDatabase", sqlex);
        }
        result = null;
        getPerson = null;
        con = null;
        return success;
    }
    
    /**
     * Disassociate a device from a user
     * @return HTML message indicating success or failure of the request
     */
    private String disownDevice() {
        String outcome = "";
        Connection con;
        PreparedStatement disownDevice;
        try {
            con = dataSrc.getConnection();
            disownDevice = con.prepareStatement(sqlDisownDevice);
            disownDevice.clearParameters();
            disownDevice.setInt(1, id);
            disownDevice.setInt(2, badge);
            if (disownDevice.executeUpdate() == 1) {
                outcome = "<p class=\"success\">Device disassociated.</p>";
            } else {
                outcome = "<p class=\"failure\">Device disassociation failed.</p>";
            }
            disownDevice.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, disownDevice", sqlex);
            outcome = "<p class=\"failure\">Device disassociation failed due to a database error.</p>";
        }
        disownDevice = null;
        con = null;
        return outcome;
    }
    
    /**
     * Disassociate a device from a user
     * @return HTML message indicating success or failure of the request
     */
    private String ownDevice() {
        String outcome = "";
        Connection con;
        PreparedStatement ownDevice;
        if (id > 0 && badge > 0) {
            try {
                con = dataSrc.getConnection();
                ownDevice = con.prepareStatement(sqlOwnDevice);
                ownDevice.clearParameters();
                ownDevice.setInt(1, id);
                ownDevice.setInt(2, badge);
                if (ownDevice.executeUpdate() == 1) {
                    outcome = "<p class=\"success\">Device associated.</p>";
                } else {
                    outcome = "<p class=\"failure\">Device association failed.</p>";
                }
                ownDevice.close();
                con.close();
            } catch (SQLException sqlex) {
                log.error("PeopleBean, ownDevice", sqlex);
                outcome = "<p class=\"failure\">Device association failed due to a database error.</p>";
            }
        } else {
            outcome = "<p class=\"failure\">Device association failed due to zero value.</p>";
        }
        ownDevice = null;
        con = null;
        return outcome;
    }
    
    private boolean addNewPerson() {
        boolean success = false;
        Connection con;
        PreparedStatement setPerson;
        try {
            con = dataSrc.getConnection();
            setPerson = con.prepareStatement(sqlAddNewPerson);
            setPerson.clearParameters();
            setPerson.setString(1, lid);
            setPerson.setString(2, dir);
            setPerson.setString(3, given);
            setPerson.setString(4, surname);
            setPerson.setDate(5, new java.sql.Date(startDate.getTime()));
            if (setPerson.executeUpdate() == 1) {
                success = true;
                log.debug("PeopleBean, addNewPerson: person " + lid + " added.");
            }
            setPerson.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PeopleBean, addNewPerson", sqlex);
        }
        setPerson = null;
        con = null;
        return success;
    }
    
    /**
     * Get information useful for debugging.
     * @return HTML list of internal variables and values
     */
     public String getDebug() {
         StringBuilder h = new StringBuilder();
         h.append("<p style=\"font-size:xx-small;\"><strong>PeopleBean values</strong></p>\n");
         h.append("<ul style=\"font-size:xx-small;\">");
         h.append("</li>\n<li>id =" + id);
         h.append("</li>\n<li>lid = " + lid);
         h.append("</li>\n<li>given= "+ given);
         h.append("</li>\n<li>surname= " + surname);
         h.append("</li>\n<li>startDate= " + startDate);
         h.append("</li>\n<li>endDate= " + endDate);
         h.append("</li>\n<li>badge= " + badge);
         h.append("</li></ul>");
         h.append(super.getDebug());
         return h.toString();
     }
}

