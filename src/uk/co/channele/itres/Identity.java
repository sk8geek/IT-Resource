/* 
 * @(#) Identity.java    0.1 2006/10/08
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Instantiates an IdentityBean for this session.
 *
 * @version     0.1    8 October 2006
 * @author      Steven J Lilley
 */
public class Identity extends HttpServlet { 
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doGet(rqst, resp);
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Cookie[] cookies;
        IdentityBean userId;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("userIdentity") == null) {
                // no IdentityBean created yet, create one
                userId = new IdentityBean();
                cookies = rqst.getCookies();
                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        if ((cookies[i].getName()).equalsIgnoreCase("eregid")) {
                            userId.setUserName(cookies[i].getValue());
                        }
                    }
                    if (userId.getUserName().length() > 0) {
                        // found a valid cookie so the user should be registered
                        // check for existing user
                        userId.cookieMonster();
                    }
                    session.setAttribute("userIdentity", userId);
                }
            }
        } // end sync
    }
}

