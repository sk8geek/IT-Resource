/* 
 * @(#) SearchBean.java    0.1 2006/10/25
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * Searches various sources and returns the results.
 *
 * @version       0.1    25 October 2006
 * @author        Steven Lilley
 */
public class SearchBean implements Serializable { 
    
    final private String sqlSearchUsers = 
        "SELECT * FROM estPeople WHERE given LIKE ? OR surname LIKE ? OR lid LIKE ? "
        + "OR nickname LIKE ? ORDER BY surname";
    final private String sqlSearchUsersOffices = 
        "SELECT * FROM users WHERE location LIKE ? ORDER BY location";
    final private String sqlSearchUsersContexts = 
        "SELECT * FROM users WHERE dn LIKE ? ORDER BY surname";
    final private String sqlSearchHardware = 
        "SELECT * FROM hw_badged WHERE badge LIKE ? OR descrip LIKE ? OR location LIKE ? "
        + "OR team LIKE ? OR user LIKE ? ORDER BY badge";
    final private String sqlSearchHardwareOffices = 
        "SELECT * FROM hw_badged WHERE location LIKE ? ORDER BY location";
    final private String sqlSearchHardwareBadge = 
        "SELECT * FROM hw_badged WHERE badge LIKE ? ORDER BY badge";
    final private String sqlSearchHardwareAddress = 
        "SELECT * FROM hw_badged WHERE ipa LIKE ? ORDER BY badge";
    final private String sqlSearchFAQs = 
        "SELECT docref, symptoms FROM selfhelp WHERE symptoms LIKE ? OR fix LIKE ? ORDER BY symptoms";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String searchCriteria = "";
    private String user = "";
    private int badge = 0;
    private int docref = 0;
    private boolean includeHardware = false;
    private boolean includePeople = false;
    private boolean includeFAQs = false;
    private boolean isAdmin = false;
    private Logger log;
    
    public SearchBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("SearchBean constructor", nex);
        }
    }
    
    public void setSearchFor(String lookFor) {
        searchCriteria = lookFor;
    }
    
    public void setQuickSearchFor (String lookFor) {
        searchCriteria = lookFor;
        includeHardware = true;
        includePeople = true;
        includeFAQs = true;
    }
    
    public void setIncludeHardware(String incHardware) {
        if (incHardware.equals("true")) {
            includeHardware = true;
        }
    }
    
    public void setIncludePeople(String incPeople) {
        if (incPeople.equals("true")) {
            includePeople = true;
        }
    }
    
    public void setIncludeFAQs(String incFAQs) {
        if (incFAQs.equals("true")) {
            includeFAQs = true;
        }
    }
    
    public void setUser(String person) {
        user = person;
    }
    
    public void setBadge(String badgeNo) {
        try {
            badge = Integer.parseInt(badgeNo);
        } catch (NumberFormatException nfex) {
            log.debug("SearchBean, setBadge: NumberFormatException");
        }
    }
    
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (user.length() == 0 && badge == 0) {
            if (searchCriteria.length() == 0) {
                return getSearchForm(false);
            } else 
            if (searchCriteria.length() < 3) {
                return getSearchForm(true);
            } else {
                if (includePeople) {
                    h.append(getPeopleSearch());
                }
                if (includeHardware) {
                    h.append(getHardwareSearch());
                }
                if (includeFAQs) {
                    h.append(getFAQSearch());
                }
            }
        } else {
            if (user.length() > 0) {
                h.append(getPersonDetails());
            } else
            if (badge > 0) {
                h.append(getMachineDetails());
            }
        }
        h.append("<form name=\"startOver\" method=\"get\" action=\"search\">");
        h.append("<p><input type=\"hidden\" name=\"searchFor\" value=\"\">");
        h.append("<input type=\"submit\" value=\"Start Over\" class=\"button\"></p></form>");
        return h.toString();
    }
    
    void setAdminEnabled(boolean admin) {
        isAdmin = admin;
    }
    
    private String getPeopleSearch() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement searchUsers;
        ResultSet results;
        boolean headingDone = false;
        try {
            con = dataSrc.getConnection();
            if (searchCriteria.charAt(0) == '@') {
                // locations only
                searchUsers = con.prepareStatement(sqlSearchUsersOffices);
                searchUsers.clearParameters();
                searchUsers.setString(1, "%" + searchCriteria.substring(1) + "%");
            } else
            if (searchCriteria.charAt(0) == '=') {
                // context only
                searchUsers = con.prepareStatement(sqlSearchUsersContexts);
                searchUsers.clearParameters();
                searchUsers.setString(1, "%" + searchCriteria.substring(1) + "%");
            } else {
                searchUsers = con.prepareStatement(sqlSearchUsers);
                searchUsers.clearParameters();
                searchUsers.setString(1, "%" + searchCriteria + "%");
                searchUsers.setString(2, "%" + searchCriteria + "%");
                searchUsers.setString(3, "%" + searchCriteria + "%");
                searchUsers.setString(4, "%" + searchCriteria + "%");
            }
            results = searchUsers.executeQuery();
            while (results.next()) {
                if (!headingDone) {
                    h.append("<h3>People</h3>");
                    h.append("<table><thead><tr>");
                    h.append("<th>Name</th><th>LID</th></tr></thead><tbody>\n");
                    headingDone = true;
                }
                h.append("<tr><td>");
                h.append(results.getString("given") + " ");
                h.append(results.getString("surname"));
                if (results.getString("email") != null) {
                    if (results.getString("email").length() > 0) {
                        h.append("<a href=\"mailto:");
                        h.append(results.getString("email"));
                        h.append("\"><img src=\"images/mailto.png\" ");
                        h.append("style=\"width:12px;height:8px;float:none;margin-left:4px;\" alt=\"email\">");
                        h.append("</a>");
                    }
                }
                h.append("</td>");
                h.append("<td>" + results.getString("lid") + "</td>");
                h.append("<td>" + results.getString("title") + "</td>");
                h.append("<td>" + results.getString("phone") + "</td>");
                h.append("<td>" + results.getString("location") + "</td></tr>\n");
            }
            if (headingDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p class=\"notImportant\">Person search returned null.</p>");
            }
            results.close();
            searchUsers.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SearchBean, getPeopleSearch", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    private String getHardwareSearch() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement searchHardware;
        ResultSet results;
        boolean headingDone = false;
        String live = "";
        try {
            con = dataSrc.getConnection();
            if (searchCriteria.charAt(0) == '@') {
                // locations only
                searchHardware = con.prepareStatement(sqlSearchHardwareOffices);
                searchHardware.clearParameters();
                searchHardware.setString(1, "%" + searchCriteria.substring(1) + "%");
            } else
            if (searchCriteria.charAt(0) == '#' && validBadge(searchCriteria.substring(1))) {
                // badge only
                searchHardware = con.prepareStatement(sqlSearchHardwareBadge);
                searchHardware.clearParameters();
                searchHardware.setString(1, "%" + searchCriteria.substring(1) + "%");
            } else
            if (searchCriteria.charAt(0) == '.' && ipAddrPair(searchCriteria)) {
                // TCP/IP address only
                searchHardware = con.prepareStatement(sqlSearchHardwareAddress);
                searchHardware.clearParameters();
                searchHardware.setString(1, "%" + searchCriteria);
            } else {
                searchHardware = con.prepareStatement(sqlSearchHardware);
                searchHardware.clearParameters();
                searchHardware.setString(1, "%" + searchCriteria + "%");
                searchHardware.setString(2, "%" + searchCriteria + "%");
                searchHardware.setString(3, "%" + searchCriteria + "%");
                searchHardware.setString(4, "%" + searchCriteria + "%");
                searchHardware.setString(5, "%" + searchCriteria + "%");
            }
            results = searchHardware.executeQuery();
            while (results.next()) {
                if (!headingDone) {
                    h.append("<h3>Hardware</h3>");
                    h.append("<table><thead><tr>");
                    h.append("<th>Badge</th><th>LIVE</th><th>Description</th>");
                    h.append("<th>Office</th><th>Team</th><th>User</th></tr></thead><tbody>\n");
                    headingDone = true;
                }
                h.append("<tr><td><a href=\"search?badge=");
                h.append(results.getInt("badge"));
                h.append("\">");
                h.append(results.getInt("badge") + "</a>");
                h.append("</td>");
                live = results.getString("live");
                if (live.equalsIgnoreCase("live")) {
                    h.append("<td class=\"inv_live\">");
                } else 
                if (live.equalsIgnoreCase("mute")) {
                    h.append("<td class=\"inv_mute\">");
                } else 
                if (live.equalsIgnoreCase("surp")) {
                    h.append("<td class=\"inv_surp\">");
                } else 
                if (live.equalsIgnoreCase("dead")) {
                    h.append("<td class=\"inv_dead\">");
                } else {
                    h.append("<td>");
                }
                h.append(live + "</td>");
                h.append("<td>" + results.getString("descrip") + "</td>");
                h.append("<td>" + results.getString("location") + "</td>");
                h.append("<td>" + results.getString("team") + "</td>");
                h.append("<td>" + results.getString("user") + "</td></tr>\n");
            }
            if (headingDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p class=\"notImportant\">Hardware search returned null.</p>");
            }
            results.close();
            searchHardware.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SearchBean, getHardwareSearch", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    private String getFAQSearch() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement searchSelfhelp;
        ResultSet results;
        boolean headingDone = false;
        try {
            con = dataSrc.getConnection();
            searchSelfhelp = con.prepareStatement(sqlSearchFAQs);
            searchSelfhelp.clearParameters();
            searchSelfhelp.setString(1, "%" + searchCriteria + "%");
            searchSelfhelp.setString(2, "%" + searchCriteria + "%");
            results = searchSelfhelp.executeQuery();
            while (results.next()) {
                if (!headingDone) {
                    h.append("<h3>Frequently Asked Questions</h3>");
                    h.append("<ul class=\"faqs\">");
                    headingDone = true;
                }
                h.append("<li><a href=\"faqs.jsp?docRef=");
                h.append(results.getString("docref"));
                h.append("\">");
                h.append(results.getString("symptoms") + "</a></li>");
            }
            if (headingDone) {
                h.append("</ul>\n");
            } else {
                h.append("<p class=\"notImportant\">FAQ search returned null.</p>");
            }
            results.close();
            searchSelfhelp.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SearchBean, getFAQSearch", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    private String getPersonDetails() {
        PersonBean person = new PersonBean(user);
        person.setAdminEnabled(isAdmin);
        if (person.personExists()) {
            return person.getPersonDetailsTable();
        } else {
            return "<p>No user with that distinguished name can be found.</p>";
        }
    }
    
    private String getMachineDetails() {
        MachineBean machine = new MachineBean(badge);
        machine.setAdminEnabled(isAdmin);
        if (machine.machineExists()) {
            return machine.getMachineDetailsTable();
        } else {
            return "<p>No machine with that badge number can be found.</p>";
        }
    }
    
/*
    // Test for distinguished name
    if (find.indexOf("ou=") == 0) {
        handler = context.getNamedDispatcher("OutOUSummary");
        rqst.setAttribute("find", find);
        handler.include(rqst, resp);
    } else if (find.indexOf("cn=") > -1) {
        handler = context.getNamedDispatcher("OutPersonDetails");
        rqst.setAttribute("find", find);
        handler.include(rqst, resp);
    } else {
        // We have a string so look for names and LIDs
        // Machines by location, team or user
        handler = context.getNamedDispatcher("OutMachineSummary");
        rqst.setAttribute("find", find);
        handler.include(rqst, resp);
        // People by name
        handler = context.getNamedDispatcher("OutPersonSummary");
        rqst.setAttribute("find", find);
        handler.include(rqst, resp);
        // LIDs (but only if the length is 4 characters)
        if (Pattern.matches("[a-zA-Z]\\w\\w\\w", find)) {
            handler = context.getNamedDispatcher("OutLIDDetails");
            rqst.setAttribute("find", find);
            handler.include(rqst, resp);
*/
    
    private String getSearchForm(boolean searchStringTooShort) {
        StringBuilder h = new StringBuilder();
        h.append("<p>Use this form for search for people and/or equipment.</p>");
        h.append("<form name=\"search\" action=\"search\" method=\"post\">");
        h.append("<p>Search for: <input type=\"text\" name=\"searchFor\" ");
        h.append("size=\"8\" maxlength=\"16\" onMouseOver=\"this.focus();\"></p>");
        if (searchStringTooShort) {
            h.append("<p class=\"reqd\">The search text must be at least three characters.</p>\n");
        }
        h.append("<p><input type=\"checkbox\" name=\"includePeople\"  value=\"true\" checked>");
        h.append(" Search for people.</p>\n");
        h.append("<p><input type=\"checkbox\" name=\"includeFAQs\" value=\"true\" checked>");
        h.append(" Search Frequently Asked Questions.</p>\n");
        h.append("<p><input type=\"checkbox\" name=\"includeHardware\"  value=\"true\" checked>");
        h.append(" Search for hardware.</p>\n");
        h.append("<p><input type=\"submit\" value=\"Go\" class=\"button\"></p>\n");
        h.append("</form>\n");
        h.append("<div class=\"function\">\n");
        h.append("<p>You can search for specific types of information using these patterns:</p>\n");
        h.append("<ul class=\"faq\">\n");
        h.append("<li>Offices, @ followed by anything</li>\n");
        h.append("<li>Users in an Novell context, = followed by the context name</li>\n");
        h.append("<li>TCP/IP address final pair, a fullstop followed by a number between 1 and 254</li>\n");
        h.append("<li>Hardware by badge number, # followed by a number</li>\n");
        h.append("</ul></div>\n");
        return h.toString();
    }
    
    private boolean validBadge(String test) {
        boolean validBadge = false;
        try {
            int x = Integer.parseInt(test);
            validBadge = true;
        } catch (NumberFormatException nfex) {
            // Not a badge number, this error can be ignored. 
        }
        return validBadge;
    }
    
    private boolean ipAddrPair(String test) {
        boolean validIpPair = false;
        if (test.charAt(0) == '.' && test.length() < 5) {
            try {
                int ipa = Integer.parseInt(test.substring(1));
                if (ipa >= 0 && ipa <= 255) {
                    validIpPair = true;
                }
            } catch (NumberFormatException nfex) {
                // not an IP address, this error can be ignored.
            }
        }
        return validIpPair;
    }
    
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append("<p style=\"font-size:x-small;\"><strong>SearchBean values</strong></p>");
        h.append("<p style=\"font-size:x-small;\">searchCriteria = " + searchCriteria);
        h.append("<br>user = " + user);
        h.append("<br>badge = " + badge);
        h.append("<br>docRef = " + docref);
        h.append("<br>includeHardware = " + includeHardware);
        h.append("<br>includePeople = " + includePeople);
        h.append("<br>includeFAQs = " + includeFAQs);
        h.append("<br>isAdmin = " + isAdmin);
        h.append("</p>");
        return h.toString();
    }
}

