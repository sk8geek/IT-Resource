/* 
 * @(#) LibraryBean.java    0.1 2006/10/22
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * A Bean to provide access to a library of staric documents.
 *
 * @version       0.1 22 October 2006
 * @author        Steven Lilley
 */
public class LibraryBean implements Serializable { 

    private final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private final String sqlGetCategoryList = 
        "SELECT category, COUNT(docref) AS items FROM docs GROUP BY category ORDER BY category";
    private final String sqlGetDocsByCategory = 
        "SELECT * FROM docs WHERE category=? ORDER BY title";
    private String libraryFolder = "";
    private int docRef = 0;
    private String title = "";
    private String category= "";
    private String secondary = "";
    private String scope = "";
    private String team = "";
    private String owner = "";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    public LibraryBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("LibraryBean constructor", nex);
        }
    }
    
    public void setLibraryFolder(String library) {
        libraryFolder = library;
    }
    
    public String getDocument() {
        return "<p>Nothing to see here, move along please.</p>";
    }
    
    public String getResponse() {
        if (category.length() > 0) {
            return getDocsByCategory();
        } else {
            return "List something for the user.";
        }
    }
    
    public String getDocsByCategory() {
        if (category.length() == 0) {
            return "";
        }
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getDocsByCategory;
        ResultSet results;
        boolean listStarted = false;
        try {
            con = dataSrc.getConnection();
            getDocsByCategory = con.prepareStatement("sqlGetDocsByCategory");
            getDocsByCategory.clearParameters();
            getDocsByCategory.setString(1, category);
            results = getDocsByCategory.executeQuery();
            while (results.next()) {
                if (!listStarted) {
                    h.append("<ul class=\"docList\">");
                    listStarted = true;
                }
                h.append("<li>");
                h.append(documentLink(results.getString("title"), results.getString("filename")));
                h.append("</li>");
            }
            if (listStarted) {
                h.append("</ul>");
            }
            results.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("LibraryBean, getDocsByCategory", sqlex);
        }
        con = null;
        return h.toString();
    }
    
    public String getDocumentList() {
        StringBuilder h = new StringBuilder();
/*        int c = 0;
        File documentDir = new File(libraryFolder);
        String[] fileList = documentDir.list(new docFileFilter());
        DocObject[] documentList = new DocObject[fileList.length];
        for (c = 0; c < documentList.length; c++) {
            documentList[c] = new DocObject(fileList[c]);
        }
        h.append("<p>");
        for (c = (documentList.length-1); c >= 0 ; c--) {
            h.append("<a href=\"" + libraryFolder);
            h.append(documentList[c].fileName + "\">");
            h.append(documentList[c].docTitle + "</a>");
            // identify file type 
//                if ((documentList[c].fileName).endsWith(".html")) {
//                    h.append(" <span class=\"htmlfile\">html</span>");
//                }
            if ((documentList[c].fileName).endsWith(".pdf")) {
                h.append(" <span class=\"pdffile\">pdf</span>");
            }
            if (c > 0) {
                h.append("<br>\n");
            }
        }
        h.append("</p>");
*/        return h.toString();
    }
    
    private String documentLink(String title, String filename) {
        return documentLink(title, filename, "", "", new Date(), new Date(), true);
    }
    
    private String documentLink(String title, String filename, String descrip, 
        String warning, Date submitted, Date expires, boolean compact) {
        StringBuilder h = new StringBuilder();
        h.append("<a href=\"");
        h.append(filename);
        h.append("\">");
        h.append(title);
        h.append("</a>");
        return h.toString();
    }
}

