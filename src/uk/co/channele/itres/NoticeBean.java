/* 
 * @(#) NoticeBean.java    0.4 2008/10/04
 *
 * A Bean to provide interaction with the Notices table.
 * Copyright (C) 2006 - 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * Provides an interface for notices.
 *
 * @version       0.4    4 October 2008
 * @author        Steven Lilley
 */
public class NoticeBean extends GenericBean { 

    private final String sqlGetGlobalNews = "SELECT * FROM notices WHERE site IS NULL AND team IS NULL ORDER BY issued DESC";
    private final String sqlGetSiteNews = "SELECT * FROM notices WHERE (site IS NULL OR site=?) AND team IS NULL ORDER BY issued DESC";
    private final String sqlGetTeamNews = "SELECT * FROM notices WHERE (team IS NULL OR team=?) AND site IS NULL ORDER BY issued DESC";
    private final String sqlGetSiteTeamNews = "SELECT * FROM notices WHERE (site=? AND team=?) OR (site=? AND team IS NULL) OR (site IS NULL and team=?) OR (site IS NULL AND team IS NULL) ORDER BY issued DESC";
    private final String sqlGetOwnedNews = "SELECT * FROM notices WHERE poster=? ORDER BY issued DESC";
    private final String sqlAddNotice = "INSERT INTO notices (headline, story, site, team, expires, poster, src) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final String sqlUpdateNotice = "UPDATE notices SET headline=?, story=?, site=?, team=?, expires=? WHERE storynum=? LIMIT 1";
    private final String sqlDeleteNotice = "DELETE FROM notices WHERE storynum=? LIMIT 1";
    private final String sqlSearchNotices = "SELECT * FROM notices WHERE headline LIKE ? OR story LIKE ? ORDER BY issued DESC";
    private int storyId = 0;
    private String headline = "";
    private String story = "";
    private String site = "";
    private String team = "";
    private String scope = "";
    private boolean scopeIsSite = false;
    private boolean scopeIsAll = false;
    private String poster = "";
    private String src = "";
    private Calendar expires = Calendar.getInstance();
    
    public NoticeBean() {
        log = Logger.getLogger("itres");
        expires.add(Calendar.MINUTE, 15);
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("NoticeBean constructor", nex);
        }
    }
    
    public void setStoryId(String story_id) {
        try {
            setStoryId(Integer.parseInt(story_id));
        } catch (NumberFormatException nfex) {
            log.debug("NoticeBean, setStoryId", nfex);
            storyId = 0;
        }
    }
    
    public void setStoryId(int story_id) {
        storyId = story_id;
    }
    
    public void setHeadline(String new_headline) {
        headline = new_headline;
    }
    
    public void setStory(String new_story) {
        story = new_story;
    }
    
    public void setSite(String newSite) {
        site = newSite;
    }
    
    public void setTeam(String newTeam) {
        team = newTeam;
    }
    
    public void setScope(String newScope) {
        scope = newScope;
        if (scope.equalsIgnoreCase("site")) {
            scopeIsSite = true;
            scopeIsAll = false;
        } else if (scope.equalsIgnoreCase("team")) {
            scopeIsSite = false;
            scopeIsAll = false;
        } else {
            scopeIsSite = false;
            scopeIsAll = true;
        }
    }
    
    public void setSource(String hostname) {
        src = hostname;
    }
    
    public void setSqlExpiryDate(String expiryDate) {
        // TODO: set these to 1am on the given date
    }
    
    public void setSqlExpiryTime(String expiryDate) {
        // TODO: set these to date is specified, otherwise today, at the given hour
    }
    
    public void setLifetimeHours(String life) {
        int lifetime = 1;
        try {
            lifetime = Integer.parseInt(life);
        } catch (NumberFormatException nfex) {
            log.debug("NoticeBean, setLifetimeHours", nfex);
        }
        expires.add(Calendar.HOUR, lifetime);
    }
    
    public String getStandard() {
        Connection con;
        PreparedStatement getNewsFeed;
        ResultSet results;
        StringBuilder h = new StringBuilder();
        boolean startedList = false;
        try {
            con = dataSrc.getConnection();
            if (scopeIsAll) {
                getNewsFeed = con.prepareStatement(sqlGetGlobalNews);
            } else {
                if (scopeIsSite && site.length() > 0) {
                    getNewsFeed = con.prepareStatement(sqlGetSiteNews);
                    getNewsFeed.clearParameters();
                    getNewsFeed.setString(1, site);
                } else if (team.length() > 0) {
                    getNewsFeed = con.prepareStatement(sqlGetTeamNews);
                    getNewsFeed.clearParameters();
                    getNewsFeed.setString(1, team);
                } else {
                    getNewsFeed = con.prepareStatement(sqlGetGlobalNews);
                }
            }
            results = getNewsFeed.executeQuery();
            while (results.next()) {
                if (!startedList) {
                    h.append("<dl class=\"msgs\">");
                    startedList = true;
                }
                h.append("<dt class=\"headline\">");
                h.append(results.getString("headline"));
                h.append("</dt>");
                if (!isAdmin) {
                    long age = System.currentTimeMillis() - results.getTimestamp("issued").getTime();
                    if (age < 28800000) {
                        h.append("<dd class=\"latest\">");
                    } else {
                        h.append("<dd class=\"msgs\">");
                    }
                }
                h.append(EncodedTextField.getEncodedText(results.getString("story")));
                if (results.getDate("issued") != null) {
                    h.append("<br><span class=\"date\">");
                    h.append(HouseKeeper.itemDateTime.format(
                        results.getTimestamp("issued")));
                    h.append("</span>");
                } else {
                    h.append("<br><span class=\"date\">");
                    h.append("&nbsp;</span>");
                }
                if (isAdmin) {
                    h.append("<form method=\"post\" action=\"/noticeeditor\">");
                    h.append("<input type=\"hidden\" name=\"storynum\" value=\"");
                    h.append(results.getInt("storynum") + "\">");
                    h.append("<input type=\"radio\" name=\"action\" value=\"edit\" checked >Edit ");
                    h.append("<input type=\"radio\" name=\"action\" value=\"delete\">Delete ");
                    h.append("<input type=\"submit\" name=\"submit\"value=\"Go\"></form>");
                }
                h.append("</dd>");
            }
            if (startedList) {
                h.append("</dl>");
            } else {
                h.append("<p class=\"notImportant\">There are no notices");
                if (!scopeIsAll) {
                    if (scopeIsSite) {
                        h.append(" for this site");
                    } else {
                        h.append(" for this team");
                    }
                }
                h.append(".</p>");
            }
            if (false) { // FIXME: change to !isAdmin
                h.append("<p class=\"button\">");
                h.append("<a href=\"/noticeeditor\" class=\"button\">Add a notice</a>");
                h.append("</p>");
            }
            results.close();
            getNewsFeed.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("NoticeBean, getNewsFeed", sqlex);
            h.append(sqlex);
        }
        results = null;
        getNewsFeed = null;
        con = null;
        return h.toString();
    }
    
    void enableAdmin() {
        isAdmin = true;
    }
    
    private boolean validate() {
        boolean valid = true;
        if (headline.length() == 0) {
            valid = false;
        }
        if (story.length() == 0) {
            valid = false;
        }
        return valid;
    }
    
    protected boolean writeToDatabase() {
        Connection con;
        PreparedStatement saveNotice;
        boolean noticeSaved = false;
        try {
            con = dataSrc.getConnection();
            if (storyId > 0) {
                saveNotice = con.prepareStatement(sqlUpdateNotice);
                saveNotice.clearParameters();
                saveNotice.setInt(6, storyId);
            } else {
                saveNotice = con.prepareStatement(sqlAddNotice);
                saveNotice.clearParameters();
                saveNotice.setString(6, userId);
                saveNotice.setString(7, src);
            }
            saveNotice.setString(1, headline);
            saveNotice.setString(2, story);
            saveNotice.setString(3, site);
            saveNotice.setString(4, team);
            saveNotice.setTimestamp(5, new java.sql.Timestamp(expires.getTimeInMillis()));
            if (saveNotice.executeUpdate() == 1) {
                noticeSaved = true;
            }
            saveNotice.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("NoticeBean, writeToDatabase", sqlex);
        }
        saveNotice = null;
        con = null;
        return noticeSaved;
    }
    
    protected String getEditForm() {
        StringBuilder h = new StringBuilder();
        DropDownBean teams = new DropDownBean("teams", "team");
        teams.allowNullValue(true);
        teams.setNotifyOnChange(true);
        DropDownBean sites = new DropDownBean("sites", "site");
        sites.allowNullValue(true);
        sites.setNotifyOnChange(true);
        h.append("<form method=\"post\" action=\"notices\" name=\"notice\">\n");
        h.append("<table><tbody><tr>");
        // headline
        h.append("<tr><th>Subject:</th><td><input type=\"text\" name=\"headline\" size=\"40\" maxlength=\"80\" value=\"");
        h.append(headline);
        h.append("\"");
        if (errors.indexOf("headline") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append(" onMouseOver=\"this.focus();\"></td></tr>\n");
        // story 
        h.append("<tr><th style=\"vertical-align:top;\">Notice:</th>");
        h.append("<td><textarea cols=\"50\" rows=\"6\" wrap=\"soft\">");
        h.append(story);
        h.append("</textarea>\n");
        // scope
        h.append("<tr><th>Audience:</th><td>");
        // audience = all
        h.append("<input type=\"radio\" name=\"scope\" value=\"all\"");
        if (scope.equalsIgnoreCase("all")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('all');\">All ");
        // audience = office
        h.append("<input type=\"radio\" name=\"scope\" value=\"site\"");
        if (scope.equalsIgnoreCase("site")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('site');\">Site ");
        // audience = team
        h.append("<input type=\"radio\" name=\"scope\" value=\"team\"");
        if (scope.equalsIgnoreCase("team")) {
            h.append(" checked");
        }
        h.append(" onClick=\"setAudience('team');\">Team ");
        h.append("</td>");
        // site
        sites.setSelectedValue(site);
        h.append("<tr><th>Site:</th><td>");
        h.append(sites.getSelectControl());
        h.append("</td></tr>\n");
        // team
        teams.setSelectedValue(team);
        h.append("<tr><th>Team:</th><td>");
        h.append(teams.getSelectControl());
        h.append("</td></tr>\n");
        // user password
        h.append("<tr><th>Password:</th><td>");
        h.append("<input type=\"password\" name=\"checkPassword\" value=\"\">");
        // buttons and hidden items
        h.append("<tr><td  class=\"grey\" colspan=\"2\" style=\"text-align:right;\">");
        h.append("<input type=\"hidden\" name=\"storynum\" value=\"" + storyId + "\">");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Save\" class=\"button\"> ");
        h.append("<input type=\"reset\" value=\"Reset\" class=\"button\">\n");
        h.append("</td></tr></tbody></table></form>\n");
        // errors
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct the errors indicated.</strong></p>\n");
            if (errors.indexOf("headline") > -1) {
                h.append("<p>A subject must be included.</p>");
            }
        }
        return h.toString();
    }
    
    protected void clearValues() {
        storyId = 0;
        headline = "";
        story = "";
        site = "";
        team = "";
        scope = "";
        poster = "";
        src = "";
        expires = Calendar.getInstance();
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    public String getSearchForm() {
        return "";
    }
    
    protected boolean validateData() {
        return false;
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getItem() {
        return "";
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
    
    public String getList() {
        return "";
    }
    
    private String getNoticeList() {
        StringBuilder h = new StringBuilder();
        return h.toString();
    }
}

