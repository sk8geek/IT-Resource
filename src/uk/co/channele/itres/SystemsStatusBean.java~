/* 
 * @(#) SystemsStatusBean.java    0.3 2007/08/19
 * 
 * A bean to provide the status of monitored systems.
 * Copyright (C) 2006-2007 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@sk8hx.org.uk
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * A bean to provide the status of monitored systems.
 *
 * @version     0.3    19 August 2007
 * @author      Steven Lilley
 */
public class SystemsStatusBean extends GenericBean {
    
    // TODO: system list for presentation to admin, allow notes per system
    // IDEA: build in start/stop checking of a system
    static final public SimpleDateFormat simpleTime = new SimpleDateFormat("HH:mm");
    static final public DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    /** SQL command to get a list of monitored services in hostname order. */
    private final String sqlGetServices = "SELECT services.*, COUNT(event_time) AS events, "
        + "MAX(event_time) AS most_recent_event FROM services LEFT JOIN service_history USING (sid) "
        + "GROUP BY sid ORDER BY hostname";
    private final String sqlGetServiceHistory = 
        "SELECT sid, cname, event_time, event_desc FROM services LEFT JOIN service_history "
        + "USING (sid) WHERE sid=? ORDER BY event_time DESC";
    private final String sqlGetStatusSummary = "SELECT status, issue FROM services";
    private final String sqlGetLatestFault = 
        "SELECT faultdesc, moment FROM webfaultlog LEFT JOIN webfaults ON (fault=faultcode) "
        + "ORDER BY moment DESC LIMIT 1";
    private final String sqlAddSystemEvent = "INSERT INTO service_history (sid, event_desc) VALUES (?, ?)";
    private int healthySystems = 0;
    private int sicklySystems = 0;
    private int knownDeadSystems = 0;
    private int deadSystems = 0;
    private int serviceId = 0;
    private String serviceComment = "";
    
    public SystemsStatusBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("SystemsStatusBean constructor", nex);
        }
        updateSystemStatus();
    }
    
    public String getSystemStatus() {
        StringBuilder h = new StringBuilder();
        String systemText = "systems";
        if (sicklySystems == 0 && knownDeadSystems == 0 && deadSystems == 0 && healthySystems == 0 ) {
            h.append("No monitored systems.");
        } else {
            h.append("<a href=\"systems.jsp\" class=\"invisible\">");
            if (deadSystems == 0) {
                if (knownDeadSystems == 0) {
                    if (sicklySystems == 0) {
                        if (healthySystems == 1) {
                            systemText = "system";
                        }
                        h.append("<span class=\"healthySystem\">&nbsp;");
                        h.append(healthySystems);
                        h.append(" healthy " + systemText + "&nbsp;</span>");
                    } else {
                        if (sicklySystems == 1) {
                            systemText = "system";
                        }
                        h.append("<span class=\"sicklySystem\">&nbsp;NOTE (");
                        h.append(sicklySystems);
                        h.append(") " + systemText + "&nbsp;</span>");
                    }
                } else {
                    if (knownDeadSystems == 1) {
                        systemText = "system";
                    }
                    h.append("<span class=\"knownDeadSystem\">&nbsp;WARNING (");
                    h.append(knownDeadSystems);
                    h.append(") " + systemText + "&nbsp;</span>");
                }
            } else {
                if (deadSystems == 1) {
                    systemText = "system";
                }
                h.append("<span class=\"deadSystem\">&nbsp;FAILURE (");
                h.append(deadSystems);
                h.append(") " + systemText + "&nbsp;</span>");
            }
            h.append("</a>");
        }
        return h.toString();
    }
    
    public String getStandard() {
        if (serviceId == 0) {
            return getSystemList();
        } else {
            return getServiceHistory();
        }
    }
    
    private String getServiceHistory() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getServiceHistory;
        ResultSet results;
        boolean headerDone = false;
        DateFormat dateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        try {
            con = dataSrc.getConnection();
            getServiceHistory = con.prepareStatement(sqlGetServiceHistory);
            getServiceHistory.clearParameters();
            getServiceHistory.setInt(1, serviceId);
            results = getServiceHistory.executeQuery();
            while (results.next()) {
                if (!headerDone) {
                    h.append("<h3>" + results.getString("cname") + "</h3>");
                    h.append("<table>");
                    h.append("<tr><thead><th>Date/Time</th><th>Event</th></tr></thead>\n<tbody>");
                    headerDone = true;
                }
                h.append("<tr><td>" + dateTime.format(results.getTimestamp("event_time")) + "</td>");
                h.append("<td>" + results.getString("event_desc") + "</td>");
                h.append("</tr>\n");
            }
            if (headerDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p>No results found.</p>");
            }
            results.close();
            getServiceHistory.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SystemsStatusBean, getServiceHistory", sqlex);
        }
        results = null;
        getServiceHistory = null;
        con = null;
        return h.toString();
    }
    
    /**
     * Returns a (tabled) list of services and their current status.  If an administrator is logged in the
     * table is wrapped in a form to allow comments to be attached to services. FIXME: this doesn't work yet.
     * @return the HTML table
     */
    private String getSystemList() {
        StringBuilder h = new StringBuilder();
        Connection con;
        PreparedStatement getServices;
        ResultSet results;
        boolean headerDrawn = false;
        SimpleDateFormat hoursMins = new SimpleDateFormat("HH:mm");
        DateFormat dateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        String status = "";
        String issue = "";
        String lastCheck= "";
        Date issueExpires = new Date();
        long issexpire = 0;
        long msRemaining = 0;
        try {
            con = dataSrc.getConnection();
            getServices = con.prepareStatement(sqlGetServices);
            results = getServices.executeQuery();
            h.append("<table class=\"boxed\">");
            if (adminEnabled) {
                h.append("<form method=\"post\" name=\"servicenotes\" action=\"systems\">\n");
            }
            while (results.next()) {
                if (!headerDrawn) {
                    h.append("<tr><thead><th>System</th><th>Checked</th>");
                    h.append("<th class=\"centre\">Status</th></tr></thead>\n<tbody>");
                    headerDrawn = true;
                }
                status = "";
                issue = "";
                if (results.getString("status") != null) {
                    status = results.getString("status");
                }
                if (results.getString("issue") != null) {
                    issue = results.getString("issue");
                }
                if (results.getTimestamp("last_check") != null) {
                    if (System.currentTimeMillis() - results.getTimestamp("last_check").getTime() < 57600000l) {
                        lastCheck = hoursMins.format(results.getTimestamp("last_check"));
                    } else {
                        lastCheck = dateTime.format(results.getTimestamp("last_check"));
                    }
                } else {
                    lastCheck = "Never";
                }
                h.append("<tr><td");
                if (status.indexOf("OK") > -1) {
                    if (issue.length() == 0) {
                        h.append(" class=\"healthySystem\"");
                    } else {
                        h.append(" class=\"sicklySystem\"");
                    }
                } else 
                if (status.indexOf("DOWN") > -1 ) {
                    if (issue.length() == 0) {
                        h.append(" class=\"deadSystem\"");
                    } else {
                        h.append(" class=\"knownDeadSystem\"");
                    }
                }
                h.append(">");
                h.append(results.getString("cname"));
                h.append("</td>");
                h.append("<td>" + lastCheck + "</td>");
                h.append("<td class=\"systemstatus\">");
                if (status.indexOf("OK") > -1) {
                    if (issue.length() == 0) {
                        h.append("<img src=\"images/up.png\" alt=\"Healthy\" title=\"Healthy\" class=\"systemstatus\">");
                    } else {
                        h.append("<img src=\"images/warn.png\" alt=\"Sickly\" title=\"Sickly\" class=\"systemstatus\">");
                    }
                } else 
                if (status.indexOf("DOWN") > -1) {
                    h.append("<img src=\"images/down.png\" alt=\"DEAD\" title=\"DEAD\" class=\"systemstatus\">");
                } else {
                    h.append("<img src=\"images/ukn.png\" alt=\"Unknown\" title=\"Unknown\" class=\"systemstatus\">");
                }
                if (adminEnabled) {
                    h.append("<a href=\"systems?action=edit&serviceId=" + results.getInt("sid") + "\" title=\"Add New Event\">");
                    h.append("<img src=\"images/new_event.png\" alt=\"New Event\" class=\"systemstatus\"></a>");
                }
                if (results.getInt("events") > 0) {
                    h.append("<a href=\"systems.jsp?serviceId=" + results.getInt("sid") + "\" title=\"Event history\">");
                    h.append("<img src=\"images/history.png\" alt=\"History\" class=\"systemstatus\"></a> ");
                    h.append(getNiceDate(results.getTimestamp("most_recent_event")));
                }
                h.append("</td></tr>\n");
                if (adminEnabled) {
                    h.append("<tr class=\"grey\"><td colspan=\"2\">");
                    h.append("Note: <input type=\"text\" name=\"s");
                    h.append(results.getInt("sid") + "\" ");
                    h.append("maxlength=\"200\" size=\"40\" ");
                    if (results.getString("issue") != null) {
                        h.append("value=\"");
                        h.append(results.getString("issue"));
                        h.append("\"> Clear ");
                        h.append("<input type=\"checkbox\" name=\"c");
                        h.append(results.getInt("sid") + "\" ");
                    }
                    h.append("> ");
                    try {
                        issueExpires = results.getTimestamp("issexpire");
                    } catch (SQLException sqlex) {
                        if (sqlex.toString().indexOf("can not be represented") == -1) {
                            throw sqlex;
                        }
                    }
                    issexpire = issueExpires.getTime();
                    msRemaining = (issexpire - System.currentTimeMillis());
                    // convert to hours remaining, rounding up
                    msRemaining = (msRemaining + 1800000) / 3600000;
                    int hours = (int)msRemaining;
                    if (hours < 0) {
                        hours = 0;
                    }
                    h.append("Life(hrs): <select name=\"l");
                    h.append(results.getInt("sid") + "\">");
                    for (int i = 1; i < 11; i++) {
                        h.append("<option value=\"" + i + "\" ");
                        if (i == hours) {
                            h.append("selected ");
                        }
                        h.append("/>");
                        h.append(i + "</option>");
                    }
                    h.append("</select> ");
                    h.append("</td></tr>\n");
                } else {
                    if (results.getString("issue") != null) {
                        h.append("<tr><td colspan=\"2\" ");
                        h.append("class=\"notImportant\">");
                        h.append(results.getString("issue"));
                        h.append("</td></tr>\n");
                    }
                }
            }
            if (adminEnabled) {
                h.append("<tr><td colspan=\"2\" class=\"right\">");
                h.append("<input type=\"submit\" value=\"Save\">");
                h.append("</tr>\n</form>\n");
            }
            if (headerDrawn) {
                h.append("</tbody></table>\n");
            }
            results.close();
            getServices.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SystemStatusBean, getSystemList", sqlex);
        }
        results = null;
        getServices = null;
        con = null;
        return h.toString();
    }
    
    public String getWebFaultSummary() {
        StringBuilder h = new StringBuilder();
        String[] latestFault = getTimeSinceLatestForHumans();
        h.append("<p><span class=\"notImportant\">Latest fault:</span> ");
        h.append(latestFault[0]);
        h.append("<br><span class=\"notImportant\">Occurred:</span> ");
        h.append(latestFault[1]);
        h.append("</p>");
        return h.toString();
    }
    
    /**
     * Returns a String representing the given date in a pleasant human readable form.
     * @param dateTime the date that needs humanizing
     * @return the pleasantly formatted date
     */
    private String getNiceDate(Date dateTime) {
        Calendar now = Calendar.getInstance();
        Calendar eventTime = Calendar.getInstance();
        String formattedDate = "";
        eventTime.setTime(dateTime);
        if (now.get(Calendar.DAY_OF_YEAR) == eventTime.get(Calendar.DAY_OF_YEAR)) {
            formattedDate = simpleTime.format(dateTime);
        } else {
            formattedDate = itemDate.format(dateTime);
        }
        return formattedDate;
    }
   
    private String[] getTimeSinceLatestForHumans() {
        Connection con;
        PreparedStatement getLatestFault;
        ResultSet result;
        long latestFault = 0;
        long timeSince = 0;
        String[] latestFaultDetails = new String[2];
        String timeSinceAsText = "";
        try {
            con = dataSrc.getConnection();
            getLatestFault = con.prepareStatement(sqlGetLatestFault);
            result = getLatestFault.executeQuery();
            if (result.next()) {
                latestFault = (result.getTimestamp("moment")).getTime();
                timeSince = (new Date()).getTime() - latestFault;
                timeSince = timeSince / 1000;
                // timeSince is now in seconds
                if (timeSince < 600) {
                    timeSinceAsText = "Just now";
                } else 
                if (timeSince < 3600) {
                    timeSinceAsText = "A bit ago";
                } else {
                    // convert to hours
                    timeSince = timeSince / 3600;
                    if (timeSince < 24) {
                        timeSinceAsText = timeSince + " hours ago";
                    } else 
                    if (timeSince < 48) {
                        timeSinceAsText = "Yesterday";
                    } else {
                        // convert to days
                        timeSince = timeSince / 24;
                        if (timeSince < 14) {
                            timeSinceAsText = timeSince + " days ago";
                        } else {
                            timeSinceAsText = "Ages ago";
                        }
                    }
                }
                latestFaultDetails[0] = result.getString("faultdesc");
                latestFaultDetails[1] = timeSinceAsText;
            }
            result.close();
            getLatestFault.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("SystemStatusBean, getTimeSinceLatestForHumans: ", sqlex);
        }
        con = null;
        return latestFaultDetails;
    }
    
    /**
     * Returns the internal state of the Bean as HTML for debug purposes.
     * @return internal state as HTML
     */
    public String getDebug() {
        StringBuilder h = new StringBuilder();
        h.append("<p style=\"font-size:x-small;\"><strong>SystemsStatusBean values</strong></p>");
        h.append("<p style=\"font-size:x-small;\">healthySystems = " + healthySystems);
        h.append("<br>sicklySystems = " + sicklySystems);
        h.append("<br>knownDeadSystems = " + knownDeadSystems);
        h.append("<br>deadSystems = " + deadSystems);
        h.append("<br>serviceId = " + serviceId);
        h.append("<br>serviceComment = " + serviceComment);
        h.append("</p>");
        h.append(super.getDebug());
        return h.toString();
    }
    
    void updateSystemStatus() {
        Connection con;
        PreparedStatement getStatusSummary;
        ResultSet results;
        int healthy = 0;
        int sickly = 0;
        int knowndead = 0;
        int dead = 0;
        try {
            con = dataSrc.getConnection();
            getStatusSummary = con.prepareStatement(sqlGetStatusSummary);
            results = getStatusSummary.executeQuery();
            while (results.next()) {
                if (results.getString("status").equalsIgnoreCase("down")) {
                    if (results.getString("issue") != null) {
                        knowndead++;
                    } else {
                        dead++;
                    }
                } else {
                    if (results.getString("issue") != null) {
                        sickly++;
                    } else {
                        healthy++;
                    }
                }
            }
            results.close();
            getStatusSummary.close();
            con.close();
            healthySystems = healthy;
            sicklySystems = sickly;
            knownDeadSystems = knowndead;
            deadSystems = dead;
        } catch (SQLException sqlex) {
            log.error("SystemsStatusBean: updateServiceStatus", sqlex);
        }
        con = null;
    }
    
    public void setServiceId(String id) {
        try {
            serviceId = Integer.parseInt(id);
        } catch (NumberFormatException nfex) {
            log.warn("SystemStatusBean, setServiceId choked on " + id);
        }
    }
    
    public void setServiceComment(String comment) {
        serviceComment = comment;
    }
    
    public String getList() {
        // FIXME: populate this method
        return "";
    }
    
    protected void clearValues() {
        // FIXME: populate this method
    }
    
    public String getSearchForm() {
        return "";
    }
    
    public String getSearchResults() {
        return "";
    }
    
    public String getItem() {
        return "";
    }
    
    public String getEditForm() {
        return "";
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    protected boolean writeToDatabase() {
        return false;
    }
    
    protected boolean validateData() {
        return false;
    }
    
    protected boolean populateFromDatabase() {
        return false;
    }
}

