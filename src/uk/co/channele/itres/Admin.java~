/* 
 * @(#) Admin.java    0.1 2006/10/29
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
/**
 * Instantiates an AdminBean for this session.
 *
 * @version     0.1    29 October 2006
 * @author      Steven J Lilley
 */
public class Admin extends HttpServlet { 
    
	private ServletContext context;
    private Logger log;
    
	public void init() {
        log = Logger.getLogger("itres");
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        AdminBean admin;
        String returnPage = "/index.jsp";
        log.debug("Admin, doGet called");
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("userIdentity") != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                returnPage = "/" + userId.getCurrentPage();
                if (userId.getUserLevel() > 4) {
                    log.debug("Admin, doGet: user exceeds level 4.");
                    if (session.getAttribute("admin") == null) {
                        // no AdminBean created yet, create one
                        log.debug("Admin, doGet: AdminBean created");
                        admin = new AdminBean();
                        admin.setValid();
                        session.setAttribute("admin", admin);
                    }
                    handler = context.getRequestDispatcher("/admin.jsp");
                    handler.include(rqst, resp);
                } else {
                    handler = context.getRequestDispatcher(returnPage);
                    handler.include(rqst, resp);
                }
            } else {
                handler = context.getRequestDispatcher(returnPage);
                handler.include(rqst, resp);
            }
        } // end sync
    }
}

