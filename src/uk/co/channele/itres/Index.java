/* 
 * @(#) Index.java	0.2 2011/10/06
 * 
 * An index servlet.
 * Copyright (C) 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * An index servlet.
 *
 * @version		0.2     6 October 2011
 * @author		Steven Lilley
 */
public class Index extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    /**
     * Handle GET request.
     * <p>If no session exists one is created.</p>
     * <p>If an <code>IdentityBean</code> exists in the session it is destroyed and recreated.
     * This is the chnage from 0.1 and fixes the issue of user presence not updating on both
     * all machines is a user is logged into more than one machine.
     * A check is made for a cookie.  If one is found then the username is set in the bean.</p>
     * <p>A <code>NoticeBean</code> is also created for this <em>request</em>. 
     * If the username is known then this is set.</p>
     * @param rqst the HTTP request
     * @param resp the HTTP response
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler = context.getRequestDispatcher("/index.jsp");
        IdentityBean userId;
        NoticeBean noticeBean;
        Cookie[] cookies;
        synchronized (this) {
            HttpSession session = rqst.getSession(true);
            if (session.getAttribute("userIdentity") != null) {
                session.removeAttribute("userIdentity");
            }
            userId = new IdentityBean();
            cookies = rqst.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    if ((cookies[i].getName()).equalsIgnoreCase("eregid")) {
                        userId.setUserName(cookies[i].getValue());
                    }
                }
                if (userId.getUserName().length() > 0) {
                    // found a valid cookie so the user should be registered
                    // check for existing user
                    userId.cookieMonster();
                }
                session.setAttribute("userIdentity", userId);
            }
            userId.readPresence();
            if (rqst.getAttribute("noticeBean") == null) {
                noticeBean = new NoticeBean();
                noticeBean.setUserId(userId.getUserId());
                noticeBean.setTeam(userId.getTeam());
                noticeBean.setSite(userId.getSite());
                noticeBean.setScope(userId.getScope());
                rqst.setAttribute("noticeBean", noticeBean);
            }
        } // end sync
        handler.include(rqst, resp);
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        doGet(rqst, resp);
    }
}

