/* 
 * @(#) Notices.java	0.4 2008/09/09
 * 
 * A servlet to instantiate a NoticeBean.
 * Copyright (C) 2006 - 2008 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.itres;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to instantiate a NoticeBean.
 *
 * @version		0.4     9 September 2008
 * @author		Steven Lilley
 */
public class Notices extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        NoticeBean noticeBean;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                if (userId.isRegistered()) {
                    if (session.getAttribute("noticeBean") == null) {
                        noticeBean = new NoticeBean();
                        session.setAttribute("noticeBean", noticeBean);
                    } else {
                        noticeBean = (NoticeBean)session.getAttribute("noticeBean");
                    }
                    noticeBean.setUserId(userId.getUserName());
                    noticeBean.setRemoteHost(rqst.getRemoteHost());
                    // noticeBean.setEditable(true);  FIXME: Don't set editable unless user is poster
                    // TODO: need to get current user site/team if registered - in order to match newsfeed
                    handler = context.getRequestDispatcher("/notices.jsp");
                } else {
                    handler = context.getRequestDispatcher("/register.jsp");
                }
            } else {
                handler = context.getRequestDispatcher("/index.jsp");
            }
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) throws ServletException, IOException {
        doGet(rqst, resp);
    }
}

