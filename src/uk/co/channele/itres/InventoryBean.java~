/* 
 * @(#) InventoryBean.java    0.1 2006/10/14
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import uk.co.channele.GenericBean;
/**
 * A bean to provide the status of monitored systems.
 *
 * @version     0.1 14 October 2006
 * @author      Steven Lilley
 */
public class InventoryBean extends GenericBean {
    
    private int badge = 0;
    private String device = "";
    private String deviceType = "";
    private String userName = "";
    private String officeName = "";
    private int deviceStatus = 0;
    public final static int STATUS_LIVE = 1;
    public final static int STATUS_DEAD = 2;
    public final static int STATUS_SURPLUS = 3;
    private Date confirmedDate = new Date();
    
    private final String sqlAddNewDevice = "INSERT INTO estDevices "
        + "(device,devType,userName,officeName,status,confDate, badge) VALUES (?,?,?,?,?,?,?)";
    private final String sqlUpdateDevice = "UPDATE estDevices SET device=?, devType=?, "
        + "userName=?, officeName=?, status=?, confDate=? WHERE badge=? LIMIT 1";
    private final String sqlGetDevice = "SELECT * FROM estDevices WHERE badge=?";
    private final String sqlEndDevice = "UPDATE estDevices SET status=3 WHERE badge=? LIMIT 1";
    private final String sqlGetDeviceList = "SELECT * FROM estDevices ORDER BY badge";
    private final String sqlSearchDevices = "SELECT * FROM estDevices WHERE badge LIKE ? OR "
        + "userName LIKE ? OR officeName LIKE ? ORDER BY badge";
    
    /**
     * Constructs an InventoryBean.
     */
    public InventoryBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
            mailFrom = "ls-db@calderdale.gov.uk";
        } catch (NamingException nex) {
            log.error("InventoryBean constructor", nex);
        }
    }
    
    public void readUser(GenericBean userBean) {
        setUserLevel(userBean.getUserLevel());
    }
    
    public void setBadge(String id) {
        try {
            badge = Integer.parseInt(id);
            if (!submitPressed) {
                if (!populateFromDatabase()) {
                    log.debug("InventoryBean, setBadge, populate failed");
                } else {
                    log.debug("InventoryBean, setBadge, populate OKAY");
                }
            }            
        } catch (NumberFormatException nfex) {
            badge = 0;
            itemSelected = false;
            newItem = true;
            log.error("InventoryBean, setId", nfex);
        }
    }
    
    public void setDevice(String dev) {
        device = dev;
    }
    
    public void setDeviceType(String devType) {
        deviceType = devType;
    }
    
    public void setUserName(String user) {
        userName = user;
    }
    
    public void setOffice(String office) {
        officeName = office;
    }
    
    public void setStatus(String status) {
        int newStatus = 0;
        try {
            newStatus = Integer.parseInt(status);
            if (newStatus < 1) {
                newStatus = 1;
            } else
            if (newStatus > 3) {
                newStatus = 3;
            } else {
                deviceStatus = newStatus;
            }
        } catch (NumberFormatException nfex) {
            log.error("InventoryBean, setStatus", nfex);
        }
    }
    
    public void setConfirmed(String confDate) {
        try {
            confirmedDate = inputDate.parse(confDate);
        } catch (ParseException pex) {
            log.error("InventoryBean, setConfirmed", pex);
            confirmedDate = new Date();
        }
    }
        
        /**
     * Analyse the request and provide a response.
     * @return the HTML response
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (action.equalsIgnoreCase("new")) {
            if (isAdmin) {
                clearValues();
                newItem = true;
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("reset")) {
            clearValues();
            h.append(getStandard());
        } else  
        if (action.equalsIgnoreCase("save")) {
            if (isAdmin) {
                if (validateData()) {
                    if (writeToDatabase()) {
                        h.append(getSaveSuccessful());
                    } else {
                        h.append(getSaveFailed());
                    }
                    h.append(getStandard());
                } else {
                    h.append(getEditForm());
                }
            }
        } else
        if (action.equalsIgnoreCase("edit")) {
            if (isAdmin && itemSelected) {
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("delete")) {
            if (isAdmin && itemSelected) {
                if (deleteFromDatabase()) {
                    h.append(getDeleteSuccessful());
                    clearValues();
                } else {
                    h.append(getDeleteFailed());
                }
            }
        } else
        if (criteria.length() > 0) {
            h.append(getSearchResults());
            itemSelected = false;
            newItem = true;
            criteria = "";
        } else {
            if (itemSelected) {
                h.append(getItem());
            } else {
                h.append(getStandard());
            }
            clearValues();
        }
        action = "";
        submitPressed = false;
        return h.toString();
    }

    protected String getEditForm() {
        StringBuilder h = new StringBuilder();
        h.append("<form method=\"post\" action=\"inventory\" name=\"inventoryAddForm\">\n");            
        h.append("<table>");
        // badge
        h.append("<tr><th>Badge</th><td>");
        h.append("<input required=\"required\" name=\"badge\" size=\"5\" maxlength=\"5\" ");
        h.append("type=\"number\" value=\"" + badge + "\"");
        h.append("onMouseOver=\"this.focus();\"></td></tr>\n");
        // device
        h.append("<tr><th>Device</th><td>");
        h.append("<input required=\"required\" name=\"device\" size=\"10\" maxlength=\"20\" ");
        h.append("value=\"" + device + "\" type=\"number\" onMouseOver=\"this.focus();\"></td></tr>\n");
        // deviceType
        h.append("<tr><th>Device type</th><td>");
        h.append("<input type=\"text\" name=\"deviceType\" size=\"10\" maxlength=\"10\" ");
        h.append("value=\"" + deviceType + "\" onMouseOver=\"this.focus();\"></td></tr>\n");
        // userName
        h.append("<tr><th>Username</th><td>");
        h.append("<input type=\"text\" name=\"userName\" size=\"20\" maxlength=\"30\" ");
        h.append("value=\"" + userName + "\" onMouseOver=\"this.focus();\"></td></tr>\n");
        // officeName
        h.append("<tr><th>Office</th><td>");
        h.append("<input type=\"text\" name=\"office\" size=\"20\" maxlength=\"20\" ");
        h.append("value=\"" + officeName + "\" onMouseOver=\"this.focus();\"></td></tr>\n");
        // deviceStatus
        h.append("<tr><th>Status</th><td>");
        h.append("<input name=\"status\" size=\"1\" maxlength=\"1\" value=\"" + deviceStatus + "\"");
        h.append("type=\"number\" min=\"1\" max=\"3\" onMouseOver=\"this.focus();\"></td></tr>\n");
        // confirmedDate
        h.append("<tr><th>Confirmed date</th><td>");
        h.append("<input type=\"text\" name=\"confirmed\" value=\"2011-12-16\" ");
        h.append("value=\"");
        if (confirmedDate != null) {
            h.append(inputDate.format(confirmedDate));
        }
        h.append("\" onMouseOver=\"this.focus();\"></td></tr>\n");
        h.append("</table>");
        h.append("<input type=\"hidden\" name=\"action\" value=\"save\">\n");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"button\" >\n");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\">\n");
        h.append("</form>\n");
        return h.toString();
    }
    
    protected String getItem() {
        StringBuffer h = new StringBuffer();
        h.append(getSearchForm());
        h.append("<table>");
        h.append("<tr>");
        h.append("<th>Badge</th>");
        h.append("<td>" + badge + "</td>");
        h.append("</tr>");
        h.append("<tr>");
        h.append("<th>Device</th>");
        h.append("<td>" + device + "</td>");
        h.append("</tr>");
        h.append("<tr>");
        h.append("<th>Device Type</th>");
        h.append("<td>" + deviceType + "</td>");
        h.append("</tr>");
        h.append("<tr>");
        h.append("<th>User</th>");
        h.append("<td>" + userName + "</td>");
        h.append("</tr>");
        h.append("<tr>");
        h.append("<th>Office</th>");
        h.append("<td>" + officeName + "</td>");
        h.append("</tr>");
        if (isAdmin) {
            h.append("<tr><td colspan=\"2\" class=\"right\">");
            h.append("<a href=\"inventory?action=edit&badge=");
            h.append(badge);
            h.append("\" class=\"button\">Edit</a>");
            h.append("</td></tr>");
        }
        h.append("</table>");
        return h.toString();
    }
    
    public String getList() {
        // FIXME: populate this method
        return "";
    }
    
    protected void clearValues() {
        super.clearValues();
        badge = 0;
        device = "";
        deviceType = "";
        userName = "";
        officeName = "";
        deviceStatus = 0;
        confirmedDate = new Date();
    }
    
    public String getStandard() {
        StringBuffer h = new StringBuffer();
        h.append(getSearchForm());
        h.append("<p><a class=\"button\" href=\"inventory?action=new\">Add Device</a></p>");
        return h.toString();
    }
    
    public void setSearchFor(String snippet) {
        criteria = "%" + snippet + "%";
    }
    
    public String getSearchForm() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"inventory\" name=\"inventorySearchForm\">\n");            
        h.append("<p>Search for: <input type=\"text\" name=\"searchFor\" ");
        h.append("size=\"10\" maxlength=\"20\" onMouseOver=\"this.focus();\">\n");
        h.append("<input type=\"hidden\" name=\"action\" value=\"search\">\n");
        h.append("<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"button\" >\n");
        h.append("<input type=\"reset\" name=\"reset\" value=\"Reset\" class=\"button\">\n");
        h.append("</form>\n");
        return h.toString();
    }
    
    public String getSearchResults() {
        StringBuffer h = new StringBuffer();
        h.append(getSearchForm());
        boolean tableHeaderDone = false;
        Connection con;
        PreparedStatement getSearchResults;
        ResultSet results;
        try {
            con = dataSrc.getConnection();
            getSearchResults = con.prepareStatement(sqlSearchDevices);
            getSearchResults.clearParameters();
            getSearchResults.setString(1, criteria);
            getSearchResults.setString(2, criteria);
            getSearchResults.setString(3, criteria);
            results = getSearchResults.executeQuery();
            while (results.next()) {
                if (!tableHeaderDone) {
                    h.append("<table><thead><tr><th>Badge</th><th>Device</th><th>User Name</th>");
                    h.append("<th>Office Name</th></tr></thead><tbody>");
                    tableHeaderDone = true;
                }
                h.append("<tr>");
                h.append("<td><a href=\"/inventory?badge=" + results.getInt("badge") + "\">");
                h.append(results.getInt("badge") + "</a></td>");
                h.append("<td>" + results.getString("device") + "</td>");
                h.append("<td>" + results.getString("userName") + "</td>");
                h.append("<td>" + results.getString("officeName") + "</td>");
                h.append("</tr>");                
            }
            if (tableHeaderDone) {
                h.append("</tbody></table>");
            } else {
                h.append("<p>No results found.</p>");
            }
            results.close();
            getSearchResults.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("InventoryBean, getSearchResults", sqlex);
            h.append(sqlex);
        }
        results = null;
        getSearchResults = null;
        con = null;
        return h.toString();
    }
    
    protected boolean validateData() {
        boolean valid = true;
        if (badge < 3000 || badge > 30000) {
            valid = false;
        }
        if (device.length() < 4) {
            valid = false;
        }
        if (deviceStatus < 0 || deviceStatus > 3) {
            valid = false;
        }
        try {
            if (confirmedDate.getTime() < inputDate.parse("2011-10-01").getTime()) {
                valid = false;
            }
        } catch (ParseException pex) {
            // should never happen as we are parsing an internal String value
            log.error("InventoryBean, validateDate", pex);
            log.debug("==== FIX THIS! ====");
        }
        return valid;
    }
    
    protected boolean writeToDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement saveDevice;
        try {
            con = dataSrc.getConnection();
            if (newItem) {
                saveDevice = con.prepareStatement(sqlAddNewDevice);
            } else {
                saveDevice = con.prepareStatement(sqlUpdateDevice);
            }
            saveDevice.clearParameters();
            saveDevice.setString(1, device);
            saveDevice.setString(2, deviceType);
            saveDevice.setString(3, userName);
            saveDevice.setString(4, officeName);
            saveDevice.setInt(5, deviceStatus);
            saveDevice.setDate(6, new java.sql.Date(confirmedDate.getTime()));
            saveDevice.setInt(7, badge);
            if (saveDevice.executeUpdate() == 1) {
                success = true;
                log.debug("InventoryBean, writeToDatabase: badge " + badge + " saved.");
            }
            saveDevice.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("InventoryBean, writeToDatabase", sqlex);
        }
        saveDevice = null;
        con = null;
        return success;
    }
    
    protected boolean deleteFromDatabase() {
        return false;
    }
    
    protected boolean populateFromDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement getDevice;
        ResultSet result;
        String devStatusString = "";
        try {
            con = dataSrc.getConnection();
            getDevice = con.prepareStatement(sqlGetDevice);
            getDevice.clearParameters();
            getDevice.setInt(1, badge);
            result = getDevice.executeQuery();
            if (result.next()) {
                device = result.getString("device");
                if (result.getString("devType") != null) {
                    deviceType = result.getString("devType");
                }
                if (result.getString("userName") != null) {
                    userName = result.getString("userName");
                }
                if (result.getString("officeName") != null) {
                    officeName = result.getString("officeName");
                }
                devStatusString = result.getString("status");
                if (devStatusString != null) {
                    if (devStatusString.equalsIgnoreCase("live")) {
                        deviceStatus = 1;
                    } else
                    if (devStatusString.equalsIgnoreCase("surplus")) {
                        deviceStatus = 2;
                    } else
                    if (devStatusString.equalsIgnoreCase("dead")) {
                        deviceStatus = 3;
                    } else {
                        deviceStatus = 0;
                    }
                }
                if (result.getTimestamp("confDate") != null) {
                    confirmedDate = result.getTimestamp("confDate");
                }
                success = true;
                newItem = false;
                itemSelected = true;
            }
            result.close();
            getDevice.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("InventoryBean, populateFromDatabase", sqlex);
            success = false;
            newItem = true;
            itemSelected = false;
        }
        result = null;
        getDevice = null;
        con = null;
        return success;
    }
    
    public String getDebug() {
        StringBuffer h = new StringBuffer();
        h.append(super.getDebug());
        h.append("<p style=\"font-size:xx-small;\"><strong>InventoryBean values</strong></p>\n");
        h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
        h.append("<li>badge= " + badge);
        h.append("</li>\n<li>device= " + device);
        h.append("</li>\n<li>deviceType= " + deviceType);
        h.append("</li>\n<li>userName= " + userName);
        h.append("</li>\n<li>officeName= " + officeName);
        h.append("</li>\n<li>deviceStatus= " + deviceStatus);
        h.append("</li>\n<li>confirmedDate= " + confirmedDate);
        h.append("</li></ul>");
        return h.toString();
    }
}

