/* 
 * @(#) PersonBean.java    0.1 2006/11/03
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * A bean to represent a person.
 *
 * @version     0.1    3 November 2006
 * @author      Steven Lilley
 */
public class PersonBean implements Serializable {
    
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private final String sqlGetPersonDetails = "SELECT * FROM users WHERE dn=?";
    private final String sqlUpdatePerson = "UPDATE users SET admin=?, team=?, "
        + "section=?, service=? WHERE dn=? LIMIT 1";
    private InitialContext initCnxt;
    private Context cnxt;
    private DataSource dataSrc;
    private Logger log;
    private boolean isAdmin = false;
    private boolean newPerson = true;
    // user field variables
    private String dn = "";
    private String given = "";
    private String surname = "";
    private String fullname = "";
    private String lid = "";
    private String title = "";
    private String email = "";
    private String phone = "";
    private String fax = "";
    private String location = "";
    private String admin = "";
    private String team = "";
    private String section = "";
    private String service = "";
    private String src = "";
    private int query = 0;
    private Date lastMod;
    
    /**
     * Constructs a PersonBean.
     * <p>Instatiates the log and data source.
     */
    public PersonBean() {
        log = Logger.getLogger("itres");
        try {
            initCnxt = new InitialContext();
            cnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)cnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.error("PersonBean constructor", nex);
        }
    }
    
    public PersonBean(String distName) {
        this();
        setDn(distName);
    }
    
    public void setDn(String distName) {
        dn = distName;
        log.debug("dn=" + dn);
        populateFromDatabase();
    }
    
    public void setGiven(String givenName) {
        given = givenName;
    }
    
    public void setSurname(String sname) {
        surname = sname;
    }
    
    public void setFullname(String name) {
        fullname = name;
    }
    
    public void setLid(String logicalId) {
        lid = logicalId.toUpperCase();
    }
    
    public void setTitle(String jobTitle) {
        title = jobTitle;
    }
    
    public void setEmail(String emailAddr) {
        email = emailAddr;
    }
    
    public void setPhone(String phoneNumber) {
        phone = phoneNumber;
    }
    
    public void setFax(String faxNumber) {
        fax = faxNumber;
    }
    
    public void setLocation(String loc) {
        location = loc;
    }
    
    public void setAdmin(String adm) {
        admin = adm;
    }
    
    public void setTeam(String tm) {
        team = tm;
    }
    
    public void setSection(String sect) {
        section = sect;
    }
    
    public void setService(String svc) {
        service = svc;
    }
    
    public void setSrc(String source) {
        src = source;
    }
    
    public void setQuery(String qry) {
        try {
            query = Integer.parseInt(qry);
        } catch (NumberFormatException nfex) {
            log.debug("PersonBean, setQuery: NumberFormatException");
            query = 0;
        }
    }
    
    boolean personExists() {
        return !newPerson;
    }
    
    public String getPersonDetailsTable() {
        StringBuilder h = new StringBuilder();
        h.append("<table class=\"boxed\"><tbody><tr>");
        if (isAdmin) {
            h.append("<form name=\"personEdit\" method=\"post\" action=\"saveperson.jsp\">");
        }
        // dn
        h.append("<td class=\"grey\">Distinguished Name</td>");
        h.append("<td><strong>" + dn + "</strong></td>");
        h.append("</tr>\n<tr>");
        // given
        h.append("<td class=\"grey\">Given Name</td>");
        h.append("<td>" + given + "</td>");
        h.append("</tr>\n<tr>");
        // surname
        h.append("<td class=\"grey\">Surname</td>");
        h.append("<td>" + surname + "</td>");
        h.append("</tr>\n<tr>");
        // fullname
        h.append("<td class=\"grey\">Full Namer</td>");
        h.append("<td>" + fullname + "</td>");
        h.append("</tr>\n<tr>");
        // lid
        h.append("<td class=\"grey\">LID</td>");
        h.append("<td>" + lid + "</td>");
        h.append("</tr>\n<tr>");
        // title
        h.append("<td class=\"grey\">Title</td>");
        h.append("<td>" + title + "</td>");
        h.append("</tr>\n<tr>");
        // email
        h.append("<td class=\"grey\">Email</td>");
        h.append("<td>" + email + "</td>");
        h.append("</tr>\n<tr>");
        // phone
        h.append("<td class=\"grey\">Phone</td>");
        h.append("<td>" + phone + "</td>");
        h.append("</tr>\n<tr>");
        // fax
        h.append("<td class=\"grey\">Fax</td>");
        h.append("<td>" + fax + "</td>");
        h.append("</tr>\n<tr>");
        // location
        h.append("<td class=\"grey\">Location</td>");
        h.append("<td>" + location + "</td>");
        h.append("</tr>\n<tr>");
        // admin
        h.append("<td class=\"grey\">Administrator</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"admin\" ");
            h.append("size=\"16\" maxlength=\"16\" value=\"");
            h.append(admin + "\"></td>");
        } else {
            h.append("<td>" + admin + "</td>");
        }
        h.append("</tr>\n<tr>");
        // team
        h.append("<td class=\"grey\">Team</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"team\" ");
            h.append("size=\"30\" maxlength=\"40\" value=\"");
            h.append(team + "\"></td>");
        } else {
            h.append("<td>" + team + "</td>");
        }
        h.append("</tr>\n<tr>");
        // section
        h.append("<td class=\"grey\">Section</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"section\" ");
            h.append("size=\"30\" maxlength=\"40\" value=\"");
            h.append(section + "\"></td>");
        } else {
            h.append("<td>" + section + "</td>");
        }
        h.append("</tr>\n<tr>");
        // service
        h.append("<td class=\"grey\">Service</td>");
        if (isAdmin) {
            h.append("<td><input type=\"text\" name=\"service\" ");
            h.append("size=\"30\" maxlength=\"40\" value=\"");
            h.append(service + "\"></td>");
        } else {
            h.append("<td>" + service + "</td>");
        }
        h.append("</tr>\n<tr>");
        // lastMod
        h.append("<td class=\"grey\">Modified</td>");
        h.append("<td>" + itemDateTime.format(lastMod) + "</td>");
        h.append("</tr>");
        if (isAdmin) {
            h.append("<tr><td colspan=\"2\" style=\"text-align: right\">");
            h.append("<input type=\"submit\" name=\"savePerson\" value=\"Save\">");
            h.append("<input type=\"reset\" value=\"Reset\"></td></tr>");
        }
        h.append("</tbody></table>");
        return h.toString();
    }
    
    private boolean populateFromDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement getPersonDetails;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getPersonDetails = con.prepareStatement(sqlGetPersonDetails);
            getPersonDetails.clearParameters();
            getPersonDetails.setString(1, dn);
            result = getPersonDetails.executeQuery();
            if (result.next()) {
                given = result.getString("given");
                surname = result.getString("surname");
                fullname = result.getString("fullname");
                lid = result.getString("lid");
                title = result.getString("title");
                email = result.getString("email");
                phone = result.getString("phone");
                fax = result.getString("fax");
                location = result.getString("location");
                admin = result.getString("admin");
                team = result.getString("team");
                section = result.getString("section");
                service = result.getString("service");
                src = result.getString("src");
                query = result.getInt("query");
                try {
                    lastMod = result.getTimestamp("lastmod");
                } catch (SQLException sqlex) {
                    lastMod = new Date(0);
                }
            }
            success = true;
            newPerson = false;
            result.close();
            getPersonDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PersonBean, populateFromDatabase", sqlex);
        }
        result = null;
        getPersonDetails = null;
        con = null;
        return success;
    }
    
    private boolean writeToDatabase() {
        boolean success = false;
        Connection con;
        PreparedStatement savePersonDetails;
        if (newPerson) {
            return false;
        }
        try {
            con = dataSrc.getConnection();
            savePersonDetails = con.prepareStatement(sqlUpdatePerson);
            savePersonDetails.clearParameters();
            savePersonDetails.setString(1, admin);
            savePersonDetails.setString(2, team);
            savePersonDetails.setString(3, section);
            savePersonDetails.setString(4, service);
            savePersonDetails.setString(5, dn);
            if (savePersonDetails.executeUpdate() == 1) {
                success = true;
                log.debug("PersonBean, writeToDatabase: person " + dn + "updated.");
            }
            savePersonDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            log.error("PersonBean, writeToDatabase", sqlex);
        }
        savePersonDetails = null;
        con = null;
        return success;
    }
    
    void setAdminEnabled(boolean admin) {
        isAdmin = admin;
    }
}

