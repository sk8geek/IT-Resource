/* 
 * @(#) GenericBean.java    2.0 2011/12/10
 * 
 * An abstract Bean to provide a basic framework.
 * Copyright (C) 2006-2011 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * An abstract Bean to provide a basic framework.
 *
 * @version       2.0   12 December 2011
 * @author        Steven Lilley
 */
abstract public class GenericBean extends LiteGenericBean implements Serializable { 
    
    /** The search criteria when looking for records. */
    protected String criteria = "";
    /** @deprecated from version 1.2 The username of the current user. */
    protected String currentUserName = "";
    /** @deprecated from version 1.0 The current edit mode. */
    protected boolean editMode = false;
    /** @deprecated from version 1.2 Current edit level, 0 is read-only, 9 is admin */
    protected int editLevel = 0;
    /** @deprecated from version 1.2 Flag to set if the current users is a system administrator. */
    protected boolean adminEnabled = false;
    /** @deprecated from version 1.2 Flag set once the user has started editing an item. */
    protected boolean startedEdit = false;
    
    /**
     * @deprecated As of 1.2, use {@link #setUserId(String user)}.
     * Set the username of the current user.
     * @param user the username of the current user
     */
    @Deprecated
    public void setCurrentUser(String user) {
        currentUserName = user;
        userId = user;
    }
    
    /**
     * @deprecated As of 1.2, use {@link #getUserId()}.
     * Get the username of the current user.
     * @return the username
     */
    @Deprecated
    public String getCurrentUser() {
        return currentUserName;
    }
    
    /**
     * Set the search criteria.
     * @param searchFor the item to search for
     */
    public void setSearchFor(String searchFor) {
        criteria = searchFor;
        action = "search";
    }
    
    /**
     * @deprecated As of 1.0, use {@link #setEditLevel(int)}.
     * Set the edit mode of the bean.
     * @param newEditMode true to enable edit mode
     */
    @Deprecated
    public void setEditable(boolean newEditMode) {
        // editMode = newEditMode;
        editLevel = 1;
    }
    
    /**
     * @deprecated As of 1.2, use {@link #setUserLevel(int level)}.
     * Set the edit level where 0 is read-only and 9 is administrator. Most users should be level 1, super-users 5.
     * @param level the new edit level to set.
     */
    @Deprecated
    public void setEditLevel(int level) {
        if (level < 0) {
            editLevel = 0;
        } else
        if (level > 9) {
            editLevel = 9;
        } else {
            editLevel = level;
        }
    }
    
    /**
     * @deprecated As of 1.2, use {@link #setUserLevel(int level)}.
     * Set admin mode of the bean.
     * @param admin true to enable admin mode
     */
    @Deprecated
    public void setAdmin(boolean admin) {
        adminEnabled = admin;
    }
    
    /**
     * @deprecated As of 1.0, use {@link #getEditLevel()}.
     * @deprecated As of 1.3, use {@link LiteGenericBean#getUserLevel()}.
     * Get the edit mode.
     * @return returns true if editing is taking place
     */
    @Deprecated
    public boolean isEditMode() {
        boolean canEdit = false;
        if (editLevel > 0) {
            canEdit = true;
        }
        return canEdit;
    }
    
    /**
     * @deprecated As of 1.3, use {@link LiteGenericBean#getUserLevel()}.
     * Get the edit mode.
     * @return returns the current edit level
     */
    public int getEditLevel() {
        return editLevel;
    }
    
    /**
     * @deprecated As of 1.2, use {@link LiteGenericBean#isAdministrator()}.
     * Get the admin flag.
     * @return returns true if the current user is a system administrator
     */
    @Deprecated
    public boolean isAdminEnabled() {
        return adminEnabled;
    }
    
    /**
     * @deprecated As of 0.9, use {@link #isEditMode()}.
     * Get the editing flag.
     * @return returns true if the user is currently editing an item.
     */
    @Deprecated
    public boolean isEditStarted() {
        return startedEdit;
    }
    
    /**
     * @deprecated As of 0.2, use {@link #isError(String error)}
     * <p>Get the comma separated list of errors that have been identified by the validation process.
     * @return returns a comma separated list of fields that have failed validation
     */
    @Deprecated
    public String getErrors() {
        return errors;
    }
    
    /**
     * Analyse the request and provide a response.
     * @return the HTML response
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (action.equalsIgnoreCase("new") && isAdmin) {
            clearValues();
            newItem = true;
            h.append(getEditForm());
        } else 
        if (action.equalsIgnoreCase("save") && (isAdmin || isOwner)) {
            if (validateData()) {
                if (writeToDatabase()) {
                    h.append(getSaveSuccessful());
                } else {
                    h.append(getSaveFailed());
                }
                h.append(getStandard());
            } else {
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("edit") && itemSelected && (isAdmin || isOwner)) {
            h.append(getEditForm());
        } else
        if (action.equalsIgnoreCase("delete") && itemSelected && (isAdmin || isOwner)) {
            if (deleteFromDatabase()) {
                h.append(getDeleteSuccessful());
                clearValues();
            } else {
                h.append(getDeleteFailed());
            }
        } else 
        if (action.equalsIgnoreCase("search")) {
            h.append(getSearchResults());
        } else {
            if (itemSelected) {
                h.append(getItem());
            } else {
                h.append(getStandard());
            }
        }
        action = "";
        submitPressed = false;
        return h.toString();
    }
    
    /**
     * Display a list of items in an HTML format.
     * @return list of items
     */
    abstract protected String getList();
    
    /**
     * Get search results.
     * @return a list records matching the search criteria
     */
    abstract protected String getSearchResults();
    
    /**
     * An HTML search form to enable the user to search the table of these items.
     * @return an HTML form
     */
    abstract public String getSearchForm();
    
    /**
     * Get information useful for debugging.
     * @return HTML list of internal variables and values
     */
     public String getDebug() {
         StringBuilder h = new StringBuilder();
         h.append(super.getDebug());
         h.append("<p style=\"font-size:xx-small;\"><strong>GenericBean values</strong></p>\n");
         h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
         h.append("<li>bean id =" + this.toString());
         h.append("</li>\n<li>criteria= " + criteria);
         h.append("</li>\n<li>adminEnabled= " + adminEnabled);
         h.append("</li>\n<li>itemSelected= " + itemSelected);
         h.append("</li></ul>");
         return h.toString();
     }
}

