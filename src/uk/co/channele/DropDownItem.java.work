/* 
 * @(#) DropDownItem.java	0.1 2004/07/04
 * 
 * An item in a drop down list.
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@sk8hx.org.uk
 */
package uk.co.channele;

import java.io.Serializable;
/**
 * An item in a Drop Down list.
 *
 * @version		0.1 4 July 2004
 * @author		Steven Lilley
 */
public class DropDownItem implements Serializable {
    
    /** The short code for this item. */
    public String code;
    /** The human readable description for this item. */
    public String desc;
    
    /**
     * Constructor.
     * @param code the short code
     * @param desc the human readable description
     */
    public DropDownItem(String code, String desc){
        this.code = code;
        this.desc = desc;
    }
}