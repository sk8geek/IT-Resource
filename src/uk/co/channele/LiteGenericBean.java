/* 
 * @(#) LiteGenericBean.java    1.0.1 2012/01/15
 * 
 * An abstract Bean to provide a lite basic framework.
 * Copyright (C) 2006-2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
/**
 * An abstract Bean to provide a lite basic framework.
 *
 * @version       1.0.1    15 January 2012
 * @author        Steven Lilley
 */
abstract public class LiteGenericBean implements Serializable { 
    
    /** A commonly used date/time format. */
    public final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
    /** A commonly used date format. */
    public final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    /** A commonly used time format. */
    public final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    /** A date format used for data entry. */
    public final SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd");
    /** A date/time format that matches the MySQL Timestamp format. */
    public final SimpleDateFormat mysqlDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /** A date/time format that only return the name of the day. */
    public final SimpleDateFormat itemDay = new SimpleDateFormat("E");
    /** The ucurrent user. */
    protected String userId = "";
    /** The senders email address, used if this bean sends email. */
    protected String mailFrom = "";
    /** The IP address or host name of the user's computer. */
    protected String remoteHost = "";
    /** The requested action. */
    protected String action = "";
    /** A comma separated list of errors found when validated the data entry form. */
    protected String errors = "";
    /** Flag to indicate that an item is selected. */
    protected boolean itemSelected = false;
    /** Current edit level, 0 is read-only, 9 is admin */
    protected int userLevel = 0;
    /** Level at which user has edit rights. */
    protected int adminLevel = 9;
    /** Boolean to indicate if the user has admin rights. */
    public boolean isAdmin = false;
    /** Boolean to indicate if the user owns the current item. */
    public boolean isOwner = false;
    /** Flag to indicate that the user wants to perform an action. */
    protected boolean submitPressed = false;
    /** Flag to indicate that the record is new. */
    protected boolean newItem = true;
    /** The initial naming context. */
    protected InitialContext initCnxt;
    /** The context in which the bean is stored. This should be set (to <code>java:comp/env</code>) when the bean is instantiated. */
    protected Context nameCnxt;
    /** The data source to use. */
    protected DataSource dataSrc;
    /** The logger to be used by the bean. */
    protected Logger log;
    
    /**
     * Set the action that the bean should perform.
     * <p>This will be one of three values: edit, save or delete.
     * @param act the requested action
     */
    public void setAction(String act) {
        action = act;
    }
    
    /**
     * Determine what action is requested from a submitted form.
     * If the action 
     * @param submit the value from the form's submit button
     */
    public void setSubmit(String submit) {
        submitPressed = true;
        action = submit.toLowerCase();
    }
    
    /**
     * Set the username of the current user.
     * @param user the username of the current user
     */
    public void setUserId(String user) {
        userId = user;
    }
    
    /**
     * Get the username of the current user.
     * @return the username
     */
    public String getUserId() {
        return userId;
    }
    
    /**
     * Set the IP address or host name of the computer that the current user is using.
     * @param host the IP address or host name of the client computer
     */
    public void setRemoteHost(String host) {
        remoteHost = host;
    }
    
    /**
     * Set the user level. 0 is read-only and 9 is God. 
      * If the user level is greater than or equal to adminLevel then the user can edit items.
     * @param level the new edit level to set.
     */
    public void setUserLevel(int level) {
        if (level < 0) {
            userLevel = 0;
        } else
        if (level > 9) {
            userLevel = 9;
        } else {
            userLevel = level;
        }
    }
    
    /**
     * Set the logger to be used by the bean.
     * @param useLog the name of the logger to use
     */
    protected void setLogger(Logger useLog) {
        log = useLog;
    }
    
    /**
     * Get the edit mode.
     * @return returns the current edit level
     */
    public int getUserLevel() {
        return userLevel;
    }
    
    /**
     * Test to see if the item is a new item. An item is considered new if 
     * values are set but no record has been stored in the database.
     * @return returns true if the record is new
     */
    public boolean isNewItem() {
        return newItem;
    }
    
    /**
     * Get the admin flag.
     * @return returns true if the current user is a system administrator
     */
    public boolean isAdministrator() {
        if (userLevel >= adminLevel) {
            isAdmin = true;
        } else {
            isAdmin = false;
        }
        return isAdmin;
    }
    
    /**
     * Get the flag that indicates if the user has requested an action.
     * @return returns true if the user has clicked submit
     */
    public boolean isSubmitPressed() {
        return submitPressed;
    }
    
    /**
     * Add an error to the error list.
     * @param error the error to add
     */
    protected void setError(String error) {
        errors = errors + "," + error;
    }
    
    /**
     * Reset the error list.
     */
    protected void clearErrors() {
        errors = "";
    }
    
    /**
     * Test to see if the given error has occurred.
     * @param error the error to test for
     * @return true if the error is present, false otherwise
     */
    public boolean isError(String error) {
        if (error.indexOf(error) > -1) {
            return true;
        }
        return false;
    }
    
    /**
     * Analyse the request and provide a response.
     * @return the HTML response
     */
    public String getResponse() {
        StringBuilder h = new StringBuilder();
        if (action.equalsIgnoreCase("new") && isAdmin) {
            clearValues();
            newItem = true;
            h.append(getEditForm());
        } else 
        if (action.equalsIgnoreCase("save") && (isAdmin || isOwner)) {
            if (validateData()) {
                if (writeToDatabase()) {
                    h.append(getSaveSuccessful());
                } else {
                    h.append(getSaveFailed());
                }
                h.append(getStandard());
            } else {
                h.append(getEditForm());
            }
        } else
        if (action.equalsIgnoreCase("edit") && itemSelected && (isAdmin || isOwner)) {
            h.append(getEditForm());
        } else
        if (action.equalsIgnoreCase("delete") && itemSelected && (isAdmin || isOwner)) {
            if (deleteFromDatabase()) {
                h.append(getDeleteSuccessful());
                clearValues();
            } else {
                h.append(getDeleteFailed());
            }
        } else {
            if (itemSelected) {
                h.append(getItem());
            } else {
                h.append(getStandard());
            }
        }
        action = "";
        submitPressed = false;
        return h.toString();
    }
    
    /**
     * Validates the data for this objects.
     * @return true is dataq valid, false otherwise
     */
    abstract protected boolean validateData();
    
    /** 
     * Read the object from the database table.
     * @return true if the record was found, false otherwise
     */
    abstract protected boolean populateFromDatabase();
     
    /** 
     * Write the object back to the database table.
     * @return true if the record was successfully written
     */
    abstract protected boolean writeToDatabase();
     
    /** 
     * Delete the object from the database table.
     * @return true if the record was successfully deleted
     */
    abstract protected boolean deleteFromDatabase();
     
    /** 
     * Get an HTML form to allow editing this object.
     * @return an HTML edit form
     */
    abstract protected String getEditForm();
    
    /**
     * Display the item in an HTML format.
     * @return details of this item
     */
    abstract protected String getItem();
    
    /**
     * The standard response for this item type.
     * <p>For a small set of items a list might typically be presented.
     * With a large set presentation of a search box or a top ten list might be more appropriate.
     * @return a top ten HTML list or table
     */
    abstract public String getStandard();
    
    /**
     * Clear the Bean values. This method should be overridden. The this.super() method should always be called.
     */
    protected void clearValues() {
        itemSelected = false;
        newItem = true;
        isAdmin = false;
        isOwner = false;
        clearErrors();
    }
    
    /**
     * A generic successful save message.
     * @return message indicating a successful save to the database
     */
    protected String getSaveSuccessful() {
        return "<p class=\"success\">Item saved to database.</p>";
    }
    
    /**
     * A generic failed save message.
     * @return message indicating a failed save to the database
     */
    protected String getSaveFailed() {
        return "<p class=\"failure\">Failed to save item.</p>";
    }
    
    /**
     * A generic successful deletion message.
     * @return message indicating a successful deletion from the database
     */
    protected String getDeleteSuccessful() {
        return "<p class=\"success\">Item deleted from the database.</p>";
    }
    
    /**
     * A generic failed to delete message.
     * @return message indicating a failed deletion from the database
     */
    protected String getDeleteFailed() {
        return "<p class=\"failure\">Item could not be from the database.</p>";
    }
    
    /** 
     * Check that the String is only made up of numbers and/or letters.
     * @param testMe the String to check
     * @return true is the String is only letters and/or numbers
     */ 
    protected boolean isAlphaNumeric(String testMe) {
        for (int c = 0; c < testMe.length(); c++) {
            if ( !Character.isLetterOrDigit(testMe.charAt(c))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Check that the email address is valid.
     * @return true is the email address is valid
     */ 
    protected boolean emailAddrOkay(String emailAddr) {
        boolean validAddress = true;
        try {
            InternetAddress test = new InternetAddress(emailAddr);
        } catch (AddressException aex) {
            validAddress = false;
        }
        return validAddress;
    }
    
    /**
     * Send an email via the application context.
     * @param recipient the email recipient
     * @param subject the subject line of the email
     * @param body the body text of the email
     * @return true if the email was sent successfully
     */
    protected boolean sendMail(String recipient, String subject, String body) {
        Session mailSession;    
        MimeMessage message;
        boolean success = false;
        try {
            mailSession = (Session)nameCnxt.lookup("mail/smtp");
            message = new MimeMessage(mailSession);
            message.setSender(new InternetAddress(mailFrom));
            message.addRecipients(MimeMessage.RecipientType.TO, recipient);
            message.setSubject(subject);
            message.setText(body, "UTF-8");
            Transport.send(message);
            success = true;
        } catch (NamingException nex) {
            log.error("LiteGenericBean, sendMail: ", nex);
        } catch (AddressException aex) {
            log.error("LiteGenericBean, sendMail: ", aex);
        } catch (MessagingException mex) {
            log.error("LiteGenericBean, sendMail: ", mex);
        }
        return success;
    }
    
    /** 
     * Check that the String is only made up of only letters.
     * @param testMe the String to check
     * @return true is the String is only letters
     */ 
    protected boolean isAlphaOnly(String testMe) {
        for (int c = 0; c < testMe.length(); c++) {
            if ( !Character.isLetter(testMe.charAt(c))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Get information useful for debugging.
     * @return HTML list of internal variables and values
     */
     public String getDebug() {
         StringBuilder h = new StringBuilder();
         h.append("<p style=\"font-size:xx-small;\"><strong>LiteGenericBean values</strong></p>\n");
         h.append("<ul style=\"font-size:xx-small;color: #006; background-color: #eee;\">");
         h.append("<li>bean id =" + this.toString());
         h.append("\n<li>currentUserId =" + userId);
         h.append("\n<li>remoteHost= "+ remoteHost);
         h.append("\n<li>userLevel= " + userLevel);
         h.append("\n<li>submitPressed= " + submitPressed);
         h.append("\n<li>action= " + action);
         h.append("\n<li>errors= " + errors);
         h.append("</ul>");
         return h.toString();
     }
}

