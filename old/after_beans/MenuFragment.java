/* 
 * @(#) MenuFragment.java    1.3 2005/11/16
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Produces the menu and footer buttons for all pages.
 * All pages must set a <code>page</code> attribute.  This can be used to 
 * guide options based on the page in which the menu is being displayed.
 *
 * @version       1.3 16 November 2005
 * @author        Steven J Lilley
 */
public class MenuFragment extends HttpServlet { 

    private final String sql_getUserStatus = 
        "SELECT presence FROM register WHERE uname = ?";
    private final String sql_markAsSeen = 
        "UPDATE register SET presence=1 WHERE presence=0 AND uname=? LIMIT 1";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:MenuFragment, init: Unable to get logger from context.");
        } else {
            log.finer("MenuFragment servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("MenuFragment", "init", nex);
        }
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp)
        throws ServletException, IOException {
        doGet(rqst, resp);
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getUserStatus;
        PreparedStatement markAsSeen;
        ResultSet result;
        String serviceSummary = (String)context.getAttribute("serviceSummary");
        String callingPage = "index";
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            HttpSession session = rqst.getSession(false);
            if (rqst.getAttribute("page") != null) {
                callingPage = (String)rqst.getAttribute("page");
            }
            boolean seenRegisteredUser = false;
            if (serviceSummary == null) {
                // this might happen when the system is starting up 
                serviceSummary = "[Systems]";
            }
            StringBuffer h = new StringBuffer();
            String eregid = "";
            Cookie[] cks = rqst.getCookies();
            if (cks != null) {
                for (int i = 0; i < cks.length; i++) {
                    if ((cks[i].getName()).equalsIgnoreCase("eregid")) {
                        eregid = cks[i].getValue();
                    }
                }
            }
            h.append("<!-- content ends -->\n");
            h.append("<div class=\"footer\">\n");
            h.append("<div class=\"usermenu\">\n");
            if (session == null) {
                // no valid session found
                h.append("<form class=\"menuitem\" method=\"post\" ");
                h.append("action=\"userops\" name=\"login\" ");
                h.append("onMouseOver=\"loginFieldFocus();\">\n\t");
                // if we've found a cookie with a user name then show a 
                // login user/password boxes
                if (eregid.length() > 0) {
                    h.append("<p class=\"menuform\">");
                    h.append("<input type=\"text\" maxlength=\"16\" ");
                    h.append("name=\"uname\" size=\"8\" ");
                    h.append("value=\"" + eregid + "\">\n");
                    h.append("<input type=\"password\" maxlength=\"16\" ");
                    h.append("name=\"oldpw\" size=\"8\">");
//                    h.append("<br>\n");
                }
                h.append("<input type=\"submit\" ");
                h.append("value=\"Log in\"><input type=\"hidden\" ");
                h.append("name=\"userop\" value=\"login\">");
                h.append("</p></form>\n");
                if (eregid.length() == 0) {
                    // no cookie found, offer registration
                    h.append("<form class=\"menuitem\" method=\"get\" ");
                    h.append("action=\"useradmin\" ");
                    h.append("name=\"register\">\n");
                    h.append("<p class=\"menuform\">");
                    h.append("<input type=\"submit\" value=\"Register\">");
                    h.append("</p></form>\n");
                } else {
                    // cookie found, mark as seen
                    seenRegisteredUser = true;
                }
            } else {
                // logged in (we have a session) 
                // news editor button 
                h.append("<form class=\"menuitem\" method=\"post\" ");
                h.append("action=\"newsdesk\" ");
                h.append("name=\"newsdesk\">\n");
                h.append("<p class=\"menuform\">");
                h.append("<input type=\"submit\" ");
                h.append("value=\"Edit News\"><input type=\"hidden\" ");
                h.append("name=\"action\" value=\"editnews\">");
                h.append("</p></form>\n");
                // user self admin 
                if (!callingPage.equalsIgnoreCase("useradmin")) {
                    h.append("<form class=\"menuitem\" method=\"post\" ");
                    h.append("action=\"useradmin\" ");
                    h.append("name=\"useradmin\">\n");
                    h.append("<p class=\"menuform\">");
                    h.append("<input type=\"submit\" ");
                    h.append("value=\"Settings\"><input type=\"hidden\" ");
                    h.append("name=\"userop\" value=\"editprefs\">");
                    h.append("</p></form>\n");
                }
                // log out button 
                h.append("<form class=\"menuitem\" method=\"post\" ");
                h.append("action=\"userops\" ");
                h.append("name=\"logout\">\n\t");
                h.append("<p class=\"menuform\">");
                h.append("<input id=\"logout\" type=\"submit\" value=\"Log out\">\n\t");
                h.append("<input type=\"hidden\" ");
                h.append("name=\"userop\" value=\"logout\">");
                h.append("</p></form>\n");
                // system admin
                if (session.getAttribute("ulevel") != null) {
                    if (((Integer)session.getAttribute("ulevel")).intValue() == 9) {
                        h.append("<form class=\"menuitem\" method=\"get\" ");
                        h.append("action=\"admin\" name=\"admin\">\n\t");
                        h.append("<p class=\"menuform\">");
                        h.append("<input type=\"submit\" value=\"Admin\">\n");
                        h.append("</p></form>\n");
                        h.append("<form class=\"menuitem\" method=\"get\" ");
                        h.append("action=\"directory\" name=\"directory\">\n\t");
                        h.append("<p class=\"menuform\">");
                        h.append("<input type=\"submit\" value=\"Directory\">\n");
                        h.append("</p></form>\n");
                    }
                }
            }
            try {
                con = dataSrc.getConnection();
                getUserStatus = con.prepareStatement(sql_getUserStatus);
                getUserStatus.setString(1, eregid);
                result = getUserStatus.executeQuery();
                if (result.next()) {
                    // got the users in/out presence
                    int presence = result.getInt("presence");
                    if (presence < 2) {
                        h.append(checkInButton(eregid, presence == -1 ? true : false));
                    } 
                    if (presence > -1) {
                        h.append(checkOutButton(eregid));
                    }
                }
                result.close();
                // mark the user as seen if they are registered
                if (seenRegisteredUser) {
                    markAsSeen = con.prepareStatement(sql_markAsSeen);
                    markAsSeen.setString(1, eregid);
                    markAsSeen.executeUpdate();
                }
                con.close();
            } catch (SQLException sqlex) {
                context.log("MenuFragment, doGet:", sqlex);
            }
            con = null;
            h.append("<form class=\"menuitem\" method=\"post\" ");
            h.append("action=\"find\" name=\"find\" ");
            h.append("onMouseOver=\"document.find.findtext.focus();\" />");
            h.append("<p class=\"menuform\">");
            h.append("\nFind <input type=\"text\" name=\"findtext\" ");
            h.append("size=\"8\" maxlength=\"16\"></p></form>\n");            
            h.append("</div> <!-- usermenu ends -->\n");
            h.append("<p class=\"buttons\">\n");
            h.append("<a href=\"http://www.spreadfirefox.com/?q=affiliates");
            h.append("&amp;id=13541&amp;t=68\">\n");
            h.append("<img alt=\"Get Firefox! \" title=\"Get Firefox!\" ");
            h.append("class=\"button\" ");
            h.append("src=\"/itres/images/firefox.png\"></a>\n");
            h.append("<a href=\"http://www.redhat.com\">\n");
            h.append("<img src=\"/itres/images/poweredby.png\" ");
            h.append("class=\"button\" alt=\"powered by RedHat\" ");
            h.append("title=\"Powered by RedHat\"></a></p>\n");
            h.append("</div> <!-- footer ends -->\n</body>\n</html>\n");
            out.println(h.toString());
            out.close();
        }
    }

    /** 
     * check IN button and form.
     */
    private String checkInButton(String uname, boolean knownToBeOut) {
        StringBuffer h = new StringBuffer();
        h.append("<form class=\"menuitem\" method=\"post\" ");
        h.append("action=\"userops\" name=\"checkin\">");
        h.append("<p class=\"menuform\">");
        h.append("<input type=\"submit\" ");
        if (knownToBeOut) {
            h.append("class=\"alert\" ");
        }
        h.append("value=\"Check In\">");
        h.append("<input type=\"hidden\" name=\"uname\" ");
        h.append("value=\"" + uname + "\">");
        h.append("<input type=\"hidden\" name=\"userop\" ");
        h.append("value=\"checkin\"></p></form>\n");
        return h.toString();
    }

    /** 
     * check OUT button and form.
     */
    private String checkOutButton(String uname) {
        StringBuffer h = new StringBuffer();
        h.append("<form class=\"menuitem\" method=\"post\" ");
        h.append("action=\"userops\" name=\"checkout\">");
        h.append("<p class=\"menuform\">");
        h.append("<input type=\"submit\" ");
        h.append("value=\"Check Out\">");
        h.append("<input type=\"hidden\" name=\"uname\" ");
        h.append("value=\"" + uname + "\">");
        h.append("<input type=\"hidden\" name=\"userop\" ");
        h.append("value=\"checkoutform\"></p></form>\n");
        return h.toString();
    }
}

