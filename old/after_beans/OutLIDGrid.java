/* 
 * @(#) OutLIDGrid.java    0.2 2006/10/06
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Output a table of LIDs who have the current user as their administrator.
 *
 * @version       0.2    6 October 2006
 * @author        Steven Lilley
 */
public class OutLIDGrid extends HttpServlet {

    private final String sqlGetLidsForAdmin = 
        "SELECT dn, lid, given, surname FROM users WHERE admin = ? ORDER BY lid";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutLIDGrid, init: Unable to get logger from context.");
        } else {
            log.finer("OutLIDGrid servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutLIDGrid", "init", nex);
        }
    }
    
    /**
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        Connection con;
        PreparedStatement getLidsForAdmin;
        ResultSet results;
        synchronized (this) {
            StringBuffer h = new StringBuffer();
            PrintWriter out = resp.getWriter();
            int colCount = 0;
            int recordCount = 0;
            String adminName = "";
            String given = "";
            String surname = "";
            String LID = "";
            String lidRange = "";
            StringBuffer initials = new StringBuffer();
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                if (session.getAttribute("ulevel") != null) {
                    if (((Integer)session.getAttribute("ulevel")).intValue() == 9) {
                        adminName = (String)session.getAttribute("uname");
                    }
                }
            }
            try {
                con = dataSrc.getConnection();
                getLidsForAdmin = con.prepareStatement(sqlGetLidsForAdmin);
                getLidsForAdmin.clearParameters();
                getLidsForAdmin.setString(1, adminName);
                results = getLidsForAdmin.executeQuery();
                while (results.next()) {
                    if (recordCount == 0) {
                        h.append("<h3>LID Grid</h3>\n");
                    }
                    if (results.getString("lid") != null) {
                        LID = results.getString("lid");
                    } else {
                        LID = "____";
                    }
                    if (!lidRange.equalsIgnoreCase(LID.substring(0, 2))) {
                        lidRange = LID.substring(0, 2);
                        if (recordCount > 0) {
                            h.append("</tr>\n</tbody></table>\n");
                        }
                        h.append("<table class=\"boxed\">");
                        h.append("<tbody>\n");
                        colCount = 0;
                    }
                    given = "";
                    surname = "";
                    initials.setLength(0);
                    if (results.getString("given") != null) {
                        given = results.getString("given");
                    }
                    if (results.getString("surname") != null) {
                        surname = results.getString("surname");
                    }
                    initials.append(given.substring(0, given.length() > 0 ? 1 : given.length()));
                    initials.append(surname.substring(0, surname.length() > 0 ? 1 : surname.length()));
                    if (colCount == 0) {
                        h.append("<tr>");
                    }
                    h.append("<td>");
                    h.append("<abbrev title=\""  + given + " " + surname + "\"><b>");
                    h.append(initials.toString());
                    h.append("</b></abbr> ");
                    h.append("<a href=\"directory?dn=");
                    h.append(results.getString("dn"));
                    h.append("\">");
                    if (results.getString("lid") != null) {
                        h.append(results.getString("lid"));
                    } else {
                        h.append("____");
                    }
                    h.append("</a>");
                    h.append("</td>");
                    colCount++;
                    if (colCount > 9 || LID.indexOf("9") == 3) {
                        h.append("</tr>\n");
                        colCount = 0;
                    }
                    recordCount++;
                }
                h.append("</tbody></table>\n");
                if (recordCount > 0) {
                    h.append("<p class=\"date\">" + recordCount + " records returned</p>\n");
                } else {
                    h.append("<p class=\"date\">No data returned from the query.</p>\n");
                }
                out.println(h);
                con.close();
                con = null;
            } catch (SQLException sqlex) {
                log.throwing("OutLIDGrid", "doGet", sqlex);
            }
        } // end sync
    }
}



