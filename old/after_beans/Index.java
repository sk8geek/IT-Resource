/* 
 * @(#) Index.java	1.2 2005/09/24
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Index servlet.
 *
 * @version		1.2 24 September 2005
 * @author		Steven Lilley
 */
public class Index extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    /**
     * Handle POST requests.
     * These are passed to doGet.
     * @param rqst the HTTP request.
     * #param resp the HTTP response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
        doGet(rqst, resp);
    }
    
	public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "index");
            PrintWriter out = resp.getWriter();
            RequestDispatcher handler;
            String serviceSummary = "";
            if (context.getAttribute("serviceSummary") != null) {
                serviceSummary = (String)context.getAttribute("serviceSummary");
            }
            out.println(HouseKeeper.doHead("LS-DB Welcome", 
                "Welcome", "index", HouseKeeper.normalDelay));
            // system status
            if (serviceSummary.length() > 0) {
                if (serviceSummary.startsWith("All")) {
                    out.print("<p class=\"okay\">");
                } else {
                    out.print("<p class=\"warn\">");
                }
                out.print(serviceSummary + "</p>");
            }
            out.println("<h3>News</h3>");
            handler = context.getNamedDispatcher("OutNewsFeed");
            handler.include(rqst, resp);
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
	}
}

