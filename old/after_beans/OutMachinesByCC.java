/* 
 * @(#) OutMachinesByCC.java	0.5 2005/11/16
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Outputs a tabular list of machines by cost centre.
 *
 * @version       0.5 16 November 2005
 * @author        Steven Lilley
 */
public class OutMachinesByCC extends HttpServlet {

    private final String sql_createTempTable = 
        "CREATE TEMPORARY TABLE cover (badge int, start_date date)";
    private final String sql_getLatestCover = 
        "INSERT INTO cover SELECT cover_history.badge, "
        + "MAX(start_date) FROM cover_history "
        + "LEFT JOIN hw_badged USING (badge) "
        + "WHERE ccentre LIKE ? GROUP BY badge ORDER BY badge";
    private final String sql_listHardware = 
        "SELECT hw_badged.badge, live, type, descrip, location, team, "
        + "user, ccentre, cover.start_date, provider, charge "
        + "FROM (hw_badged LEFT JOIN cover USING (badge)) "
        + "LEFT JOIN cover_history USING (badge, start_date) "
        + "WHERE ccentre LIKE ? AND type like ? "
        + "ORDER BY ccentre, hw_badged.badge";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:Admin, init: Unable to get logger from context.");
        } else {
            log.finer("Admin servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.severe("Naming exception, unable to find data source.");
            log.throwing("HouseKeeper", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement createTempTable;
        PreparedStatement getLatestCover;    
        PreparedStatement listHardware;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer("<!-- List by Cost Centre -->\n");
            StringBuffer costCentre = new StringBuffer();
            String filterType = "";
            String title = "";
            if (rqst.getParameter("filterhwbytype") != null) {
                if (rqst.getParameter("hwtype") != null) {
                    filterType = rqst.getParameter("hwtype");
                }
            } else {
                filterType = "%";
            }
            if (rqst.getParameter("ccentre") != null) {
                costCentre.append(rqst.getParameter("ccentre"));
            }
            if (costCentre.length() == 2) {
                costCentre.append("%");
            }
            try {
                con = dataSrc.getConnection();
                createTempTable = con.prepareStatement(sql_createTempTable);
                getLatestCover = con.prepareStatement(sql_getLatestCover);
                getLatestCover.clearParameters();
                getLatestCover.setString(1, costCentre.toString());
                listHardware = con.prepareStatement(sql_listHardware);
                listHardware.setString(1, costCentre.toString());
                listHardware.setString(2, filterType);
                createTempTable.executeUpdate();
                getLatestCover.executeUpdate();
                results = listHardware.executeQuery();
                title = "<h3>Hardware by Cost Centre: "
                    + costCentre.toString() + "</h3>\n";
                h.append(HouseKeeper.doMachineSummary(title, results, false));
                out.println(h.toString());
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutMachinesByCC, doPost:", sqlex);
            }
        } // end sync
        con = null;
    }
}

