/* 
 * @(#) OutMachineDetails.java	0.2 2005/10/15
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Output the details of a piece of hardware.
 *
 * @version		0.2 15 Oct 2005
 * @author		Steven Lilley
 */
public class OutMachineDetails extends HttpServlet {

    private final String sql_getHardwareByBadge = 
        "SELECT * FROM hw_badged WHERE badge = ?";
    private final String sql_getAssociatedHardware = 
        "SELECT badge, type, descrip FROM hw_badged WHERE parent = ?";
    private final String sql_getCoverHistory = "SELECT * FROM cover_history WHERE badge = ? "
        + "ORDER BY start_date DESC";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutMachineDetails, init: Unable to get logger from context.");
        } else {
            log.finer("OutMachineDetails servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutMachineDetails", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getHardwareByBadge;
        PreparedStatement getCoverHistory;
        PreparedStatement getAssociatedHardware;
        ResultSet result;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer("<!-- Hardware Detail -->\n");
            int badge = 0;
            String type = "";
            String descrip = "";
            String location = "";
            String team = "";
            String user = "";
            String ccentre = "";
            String coverStartDate = "";
            String coverProvider = "";
                String itemStatus = "";
            double coverCharge = 0;
            boolean doneTitle = false;
            try {
                if (rqst.getParameter("badge") != null) {
                    badge = Integer.parseInt(rqst.getParameter("badge"));
                } else
                if (rqst.getAttribute("badge") != null) {
                    badge = ((Integer)rqst.getAttribute("badge")).intValue();
                }
                con = dataSrc.getConnection();
                getHardwareByBadge = con.prepareStatement(sql_getHardwareByBadge);
                getHardwareByBadge.setInt(1, badge);
                result = getHardwareByBadge.executeQuery();
                if (result.next()) {
                    h.append("<h3>Machine Details, Badge#: ");
                    h.append(badge + "</h3>\n");
                    if (session != null) {
                        if (session.getAttribute("ulevel") != null) {
                            if (((Integer)session
                                .getAttribute("ulevel")).intValue() >= 5) {
                                h.append(doMachineDetailForm(result));
                            }
                        }
                    } else {
                        h.append(doMachineDetailTable(result));
                    }
                    // get insurance cover records
                    getCoverHistory = con.prepareStatement(sql_getCoverHistory);
                    getCoverHistory.setInt(1, badge);
                    result = getCoverHistory.executeQuery();
                    while (result.next()) {
                        if (!doneTitle) {
                            h.append("<h3>Premature Failure Insurance</h3>\n");
                            h.append("<table class=\"boxed\"><tr>");
                            h.append("<th>Start</th><th>End</th>");
                            h.append("<th>Provider</th><th>Cost</th>");
                            h.append("</tr>\n");
                            doneTitle=true;
                        }
                        h.append("<tr><td class=\"right\">");
                        try {
                            h.append(HouseKeeper.itemDate.format(result.getDate("start_date")));
                        } catch (SQLException sqlex) {
                            h.append("bad date");
                        } catch (NullPointerException npex) {
                            // could insert non-breaking space
                        }
                        h.append("</td><td class=\"right\">");
                        try {
                            h.append(HouseKeeper.itemDate.format(
                                result.getDate("end_date")));
                        } catch (SQLException sqlex) {
                            h.append("bad date");
                        } catch (NullPointerException npex) {
                            // could insert non-breaking space
                        }
                        h.append("</td><td>");
                        h.append(result.getString("provider"));
                        h.append("</td><td class=\"right\">");
                        h.append(result.getShort("charge"));
                        h.append("</td></tr>\n");
                    }
                    if (doneTitle) {
                        h.append("</table>\n");
                    }
                    // look for associated hardware
                    getAssociatedHardware = 
                        con.prepareStatement(sql_getAssociatedHardware);
                    getAssociatedHardware.setInt(1, badge);
                    result = getAssociatedHardware.executeQuery();
                    doneTitle = false;
                    while (result.next()) {
                        if (!doneTitle) {
                            h.append("<h3>Associated Items</h3>\n");
                            h.append("<table class=\"boxed\"><tr>");
                            h.append("<th>Badge</th><th>Type</th>");
                            h.append("<th>Description</th></tr>\n");
                            doneTitle=true;
                        }
                        h.append("<tr><td><a href=\"inventory?badge=");
                        h.append(result.getString("badge") + "\">");
                        h.append(result.getString("badge") + "</a></td>");
                        h.append("<td>" + result.getString("type"));
                        h.append("</td><td>");
                        h.append(result.getString("descrip"));
                        h.append("</td></tr>\n");
                    }
                    if (doneTitle) {
                        h.append("</table>\n");
                    }
                    result.close();
                } else {
                    h.append("<p><b>No equipment with badge number ");
                    h.append(badge + " can be found.</b></p>");
                }
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutMachineDetails", sqlex);
            }
            con = null;
            out.println(h);
        } // end sync
    }
    
    /**
     * Returns an HTML table of the given machine details.
     * @param record the result set containing the hardware record
     * @return the HTML table
     */
    private String doMachineDetailTable(ResultSet record) 
        throws SQLException {
        StringBuffer h = new StringBuffer();
        String type = (record.getString("type")).toUpperCase();
        String itemStatus = record.getString("live");
        String descrip = record.getString("descrip");
        String location = record.getString("location");
        String team = record.getString("team");
        String user = record.getString("user");
        String os = record.getString("os");
        String ipAddress = record.getString("ipa");
        String serialNumber = record.getString("sn");
        String processor = record.getString("processor");
        String memory = record.getString("memory");
        String ccentre = record.getString("ccentre");
        h.append("<table class=\"boxed\">\n");
        h.append("<tr><td class=\"grey\">Type:</td>");
        h.append("<td>" + type + "</td>");
        h.append("<td class=\"grey\">Live:</td>");
        h.append("<td");
        if (itemStatus.equalsIgnoreCase("live")) {
            h.append(" class=\"inv_live\"");
        } else 
        if (itemStatus.equalsIgnoreCase("mute")) {
            h.append(" class=\"inv_mute\"");
        } else 
        if (itemStatus.equalsIgnoreCase("dead")) {
            h.append(" class=\"inv_dead\"");
        } else 
        if (itemStatus.equalsIgnoreCase("away")) {
            h.append(" class=\"inv_away\"");
        }
        h.append(">" + itemStatus + "</td>");
        h.append("</tr>\n");
        h.append("<tr><td class=\"grey\">Description:</td>");
        h.append("<td colspan=\"3\">");
        h.append(descrip + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Location:</td>");
        h.append("<td colspan=\"3\">");
        h.append(location + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Team:</td>");
        h.append("<td colspan=\"3\">");
        h.append(team + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">User:</td>");
        h.append("<td colspan=\"3\">");
        h.append(user + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Op Sys:</td>");
        h.append("<td>" + os + "</td>");
        h.append("<td class=\"grey\">IP Addr:</td>");
        h.append("<td>" + ipAddress + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">SN:</td>");
        h.append("<td colspan=\"3\">");
        h.append(serialNumber + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Processor:</td>");
        h.append("<td>" + processor + "</td>");
        h.append("<td class=\"grey\">Memory: </td>");
        h.append("<td>" + memory + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Cost Centre:</td>");
        h.append("<td>" + ccentre + "</td>");
        h.append("<td class=\"grey\">Modified:</td>");
        h.append("<td>");
        h.append(HouseKeeper.itemDateTime.format(
            record.getTimestamp("modified")));
        h.append("</td></tr>\n");
        h.append("</table>\n");
        return h.toString();
    }
    
    /**
     * Returns an HTML form to allow editing of machine details.
     * 
     * @param record the result set containing the hardware record
     * @return the HTML table
     */
    private String doMachineDetailForm(ResultSet record) 
        throws SQLException {
        StringBuffer h = new StringBuffer();
        int badge = record.getInt("badge");
        String type = (record.getString("type")).toUpperCase();
        String itemStatus = record.getString("live");
        String descrip = record.getString("descrip");
        String location = record.getString("location");
        String team = record.getString("team");
        String user = record.getString("user");
        String os = record.getString("os");
        String ipAddress = record.getString("ipa");
        String serialNumber = record.getString("sn");
        String processor = record.getString("processor");
        String memory = record.getString("memory");
        String ccentre = record.getString("ccentre");
        h.append("<form  method=\"post\" ");
        h.append("action=\"inventory\" ");
        h.append("name=\"hardware\">\n");
        h.append("<table class=\"borderless\">\n");
        h.append("<tr><td class=\"grey\">Type:</td>");
        h.append("<td>" + type + "</td></tr>\n");
        h.append("<tr><td");
        if (itemStatus.equalsIgnoreCase("live")) {
            h.append(" class=\"inv_live\"");
        } else 
        if (itemStatus.equalsIgnoreCase("mute")) {
            h.append(" class=\"inv_mute\"");
        } else 
        if (itemStatus.equalsIgnoreCase("dead")) {
            h.append(" class=\"inv_dead\"");
        } else 
        if (itemStatus.equalsIgnoreCase("away")) {
            h.append(" class=\"inv_away\"");
        }
        h.append(">Live:</td>");
        h.append("<td><input type=\"text\" name=\"live\" size=\"4\" ");
        h.append("maxlength=\"4\" value=\"" + itemStatus + "\"></td>");
        h.append("</tr>\n");
        h.append("<tr><td class=\"grey\">Description: </td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"descrip\" size=\"20\" maxlength=\"50\" value=\"");
        h.append(descrip + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">Location:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"location\" size=\"20\" ");
        h.append("maxlength=\"60\" value=\"");
        h.append(location + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">Team:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"team\" size=\"20\" ");
        h.append("maxlength=\"40\" value=\"");
        h.append(team + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">User:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"user\" size=\"20\" ");
        h.append("maxlength=\"30\" value=\"");
        h.append(user + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">Op Sys:</td>");
        h.append("<td>" + os + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">IP Addr:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"ipa\" size=\"15\" ");
        h.append("maxlength=\"15\" value=\"");
        h.append(ipAddress + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">SN:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"sn\" size=\"20\" ");
        h.append("maxlength=\"70\" value=\"");
        h.append(serialNumber + "\"></td></tr>\n");
        h.append("<tr><td class=\"grey\">Processor:</td>");
        h.append("<td>" + processor + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Memory:</td>");
        h.append("<td>" + memory + "</td></tr>\n");
        h.append("<tr><td class=\"grey\">Cost Centre:</td>");
        h.append("<td><input type=\"text\" ");
        h.append("name=\"ccentre\" size=\"4\" ");
        h.append("maxlength=\"4\" value=\"");
        h.append(ccentre + "\"></td></tr>\n");
        h.append("<td class=\"grey\">Modified:</td>");
        h.append("<td>");
        h.append(HouseKeeper.itemDateTime.format(record.getTimestamp("modified")));
        h.append("</td></tr>\n");
        h.append("<tr><td colspan=\"2\" class=\"right\">\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("<input type=\"hidden\" name=\"badge\" ");
        h.append("value=\"" + badge + "\">\n");
        h.append("<input type=\"hidden\" name=\"querytype\" ");
        h.append("value=\"updatehw\">\n</td></tr>");
        h.append("</table>\n</form>\n");
        return h.toString();
    }
}

