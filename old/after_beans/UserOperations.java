/* 
 * @(#) UserOperations.java	0.1 2006/10/12
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to mark the user as checked out.
 *
 * @version		1.2 24 September 2005
 * @author		Steven Lilley
 */
public class UserOperations extends HttpServlet { 

	private ServletContext context;

	public void init() {
		context = getServletContext();
	}
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        IdentityBean userId;
        String action = "";
        String password = "";
        String returnPage = "/index.jsp";
        synchronized (this) {
            if (rqst.getParameter("action") != null) {
                action = rqst.getParameter("action");
            }
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                userId = (IdentityBean)session.getAttribute("userIdentity");
                returnPage = "/" + userId.getCurrentPage();
                if (action.equalsIgnoreCase("checkout")) {
                    checkOut(rqst, userId);
                } else 
                if (action.equalsIgnoreCase("checkin")) {
                    userId.checkInUser();
                }
            }
            handler = context.getRequestDispatcher(returnPage);
            handler.forward(rqst, resp);
        } // end sync
	}
    
    private void checkOut(HttpServletRequest rqst, IdentityBean userId) {
        String returning = "";
        String destination = "";
        boolean noAutoCheckIn = false;
        if (rqst.getParameter("backat") != null) {
            returning = rqst.getParameter("backat");
        }
        if (rqst.getParameter("destination") != null) {
            returning = rqst.getParameter("destination");
        }
        if (rqst.getParameter("noautocheckin") != null) {
            noAutoCheckIn = true;;
        }
        userId.checkOutUser(returning, destination, noAutoCheckIn);
    }
   
}

