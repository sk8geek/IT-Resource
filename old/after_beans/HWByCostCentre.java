/* 
 * @(#) HWByCostCentre.java    0.3 2006/08/27
 * 
 * Copyright (C) 2003,2004,2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Page to show hardware by (financial) cost centre.
 *
 * @version       0.3 27 August 2006
 * @author        Steven Lilley
 */
public class HWByCostCentre extends HttpServlet { 
    
    /** The context in which this servlet is running. */
    private ServletContext context;
    
    public void init() throws ServletException {
        context = getServletContext();
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        resp.setContentType("text/html");
        rqst.setAttribute("page", "hwbycostcentre");
        PrintWriter out = resp.getWriter();
        RequestDispatcher handler;
        out.println(HouseKeeper.doHead("Hardware Inventory", 
            "Hardware Inventory", "inventory", HouseKeeper.normalDelay));
        StringBuffer h = new StringBuffer();
        if ((String)rqst.getParameter("ccentre") != null) {
            handler = context.getNamedDispatcher("OutHWByCC");
            handler.include(rqst, resp);
        } else {
            h.append("<p>Cost centre specified is null.</p>");
        }
        h.append("<p class=\"date\">(Find results as at ");
        h.append(HouseKeeper.itemDateTime.format(new Date()));
        h.append(")</p>\n");
        out.println(h);
        out.println("</div>");
        handler = context.getNamedDispatcher("MenuFragment");
        handler.include(rqst, resp);
        out.close();
    }
}

