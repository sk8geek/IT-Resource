/* 
 * @(#) OutWebFaultStats.java	0.1 2006/06/30
 * 
 * Copyright (C) 2006 Calderdale MBC
 * parts Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Outputs statistics from the web failure log.
 *
 * @version       0.1 20 June 2006
 * @author        Steven Lilley
 */
public class OutWebFaultStats extends HttpServlet {
    
    private final String sqlFaultFrequency = 
        "SELECT faultdesc, COUNT(moment) AS events FROM webfaultlog "
        + "LEFT JOIN webfaults ON fault=faultcode GROUP BY fault";
    private final String sqlFirstFaultDate = 
        "SELECT MIN(moment) FROM webfaultlog";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutWebFaultStats, init: Unable to get logger from context.");
        } else {
            log.finer("OutWebFaultStats servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutWebFaultStats", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement faultFrequency;
        PreparedStatement firstFaultDate;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            boolean headerDone = false;
            long duration = 0;
            Date firstFault = new Date();
            try {
                con = dataSrc.getConnection();
                firstFaultDate = con.prepareStatement(sqlFirstFaultDate);
                results = firstFaultDate.executeQuery();
                if (results.next()) {
                    duration = (System.currentTimeMillis() - results.getTimestamp("min(moment)").getTime()) / 3600000l;
                }
                faultFrequency = con.prepareStatement(sqlFaultFrequency);
                results = faultFrequency.executeQuery();
                while (results.next()) {
                    if (!headerDone) {
                        h.append("<table class=\"boxed\"><thead><th>Fault</th>");
                        h.append("<th>Frequency</th></thead><tbody>");
                        headerDone = true;
                    }
                    h.append("<tr><td>");
                    h.append(results.getString("faultdesc"));
                    h.append("</td><td>");
                    h.append(results.getInt("events"));
                    h.append("</td></tr>");
                }
                if (headerDone) {
                    h.append("</tbody></table>");
                    h.append("</p>Over " + duration + " hours.</p>");
                } else {
                    h.append("<p>There are no results to display, which is a good thing :-)</p>");
                }
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                System.out.println("OutWebFaultStats, doGet: " + sqlex);
                context.log("OutWebFaultStats, doGet:", sqlex);
            }
            con = null;
            out.println(h.toString());
        } // end sync
    }
}

