/* 
 * @(#) OutOwnedMachineSummary.java    0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Part of ITRES suite.
 *
 * @version       0.1 6 February 2004
 * @author        Steven Lilley
 */

public class OutOwnedMachineSummary extends HttpServlet {

    private final String sqlGetHardwareByUser = 
        "SELECT badge,descrip,user FROM hw_badged WHERE user LIKE ? ORDER BY user";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutOwnedMachineSummary, init: Unable to get logger from context.");
        } else {
            log.finer("OutOwnedMachineSummary servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutOwnedMachineSummary", "init", nex);
        }
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getHWbyUser;
        PreparedStatement getAssoc;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            try {
                con = dataSrc.getConnection();
                getHWbyUser = con.prepareStatement(sqlGetHardwareByUser);
                String userLike = new String("%" + (String)(rqst.getAttribute("find")) + "%");
                getHWbyUser.setString(1, userLike);
                results = getHWbyUser.executeQuery();
                boolean titleDone = false;
                while (results.next()) {
                    if ( titleDone == false ) {
                        h.append("<h3>Machine Summary</h3>");
                        h.append("<table cellspacing=\"0\" cellpadding=\"4\" class=\"boxed\">");
                        h.append("<tr><th>Badge</th><th>Description</th><th>User</th></tr>");
                        titleDone = true;
                    }
                    h.append("<tr><td><a href=\"/itres/machinedetails?badgeno=");
                    h.append(results.getString("badge") + "\">");
                    h.append(results.getString("badge") + "</a></td>");
                    h.append("<td>" + results.getString("descrip") + "</td>");
                    String name = results.getString("user");
                    if ( name != "" & name.lastIndexOf(" ") != -1 ) {
                        String surname = name.substring((name.lastIndexOf(" ") + 1));
                        String firstname = name.substring(0,name.lastIndexOf(" "));
                        h.append("<td><a href=\"/itres/find?findtext=");
                        h.append( firstname + "\">" + firstname + "</a> ");
                        h.append("<a href=\"/itres/find?findtext=");
                        h.append( surname + "\">" + surname + "</a></td></tr>");
                    } else {
                        h.append("<td>" + results.getString("user") + "</td></tr>");
                    }
                }
                if ( titleDone ) {
                    h.append("</table>");
                }
                con.close();
            } catch (SQLException sqlex) {
                h.append("<body><p>Something broke...<br>");
                h.append("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
            }
            con = null;
            out.println(h);
        } // end sync
    }
}



