/* 
 * @(#) RegistrationBean.java    0.1 2006/10/16
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * Stores a visitors identity.
 *
 * @version       0.1    16 October 2006
 * @author        Steven Lilley
 */
public class RegistrationBean implements Serializable { 

    /** SQL query to add a new user. */
    private final String sqlInsertNewUser = 
        "INSERT INTO register (given, surname, email, team, site, uname, pw) "
        + "VALUES (?, ?, ?, ?, ?, ?, PASSWORD(?))";
    /** SQL query to check to see if a username exists in the database. */
    private final String sqlCheckForExistingUser = 
        "SELECT count(uname) AS qty FROM register WHERE uname = ?";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private String userName = "";
    private String userGiven = "";
    private String userSurname = "";
    private String userEmail = "";
    private String userTeam = "";
    private String userOffice = "";
    private String newPassword = "";
    private String confirmPassword = "";
    private String team_onlyPref = "";
    private String currentPage = "";
    private String errors = "";
    private boolean dataValid = false;
    private String process;
    
    public RegistrationBean() {
        process = "start";
System.out.println("RegistrationBean instantiated.");
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            // FIXME log error
        }
    }
    
    public void setUserName(String newUser) {
        userName = newUser;
    }
    
    public void setGiven(String newGiven) {
        userGiven = newGiven;
    }
    
    public void setSurname(String newSurname) {
        userSurname = newSurname;
    }
    
    public void setEmail(String newEmail) {
        userEmail = newEmail;
    }
    
    public void setTeam(String newTeam) {
        userTeam = newTeam;
    }
    
    public void setOffice(String newOffice) {
        userOffice = newOffice;
    }
    
    public void setNewPassword(String pw) {
        newPassword = pw;
    }
    
    public void setConfirmPassword(String pw) {
        confirmPassword = pw;
    }
    
    public void setCurrentPage(String thisPage) {
        currentPage = thisPage;
    }
    
    public void setClearErrorList() {
        errors = "";
    }
    
    public String getUserName() {
        return userName;
    }
    
    public String getGiven() {
        return userGiven;
    }
    
    public String getSurname() {
        return userSurname;
    }
    
    public String getEmail() {
        return userEmail;
    }
    
    public int getUserLevel() {
        return 0;
    }
    
    public String getTeam() {
        return userTeam;
    }
    
    public String getOffice() {
        return userOffice;
    }
    
    public String getCurrentPage() {
        return currentPage;
    }
    
    public String getRegistrationProcess() {
        if (process.equals("start")) {
            process = "running";
System.out.println("RegBean, process set to running");
            return getRegistrationForm();
        }
        if (validateData()) {
System.out.println("RegBean data valid");
            if (writeToDatabase()) {
System.out.println("RegBean data written");
                process = "complete";
                return getRegistrationSuccessPage();
            } else {
                return "<p>error saving</p>";
            }
        } else {
            return getRegistrationForm();
        }
    }
    
    private String getRegistrationForm() {
        StringBuffer h = new StringBuffer();
        DropDownBean teams = new DropDownBean("teams", "team");
        DropDownBean office = new DropDownBean("sites", "office");
        h.append("<p>Please complete this form to register.</p>");
        h.append("<form method=\"post\" action=\"register.jsp\" name=\"registration\">");
        // given name
        h.append("<p>Given name: <input type=\"text\" name=\"given\" size=\"30\" maxlength=\"30\" value=\"");
        h.append(userGiven);
        h.append("\"");
        if (errors.indexOf("given") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // surname 
        h.append("<p>Surname: <input type=\"text\" name=\"surname\" size=\"30\" maxlength=\"40\" value=\"");
        h.append(userSurname);
        h.append("\"");
        if (errors.indexOf("surname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // email
        h.append("<p>Email address: <input type=\"text\" name=\"email\" size=\"40\" maxlength=\"120\" value=\"");
        h.append(userEmail);
        h.append("\"");
        if (errors.indexOf("email") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // team
        teams.setSelectedValue(userTeam);
        h.append("<p>Team: ");
        h.append(teams.getSelectControl());
        h.append("</p>");
        // site
        office.setSelectedValue(userOffice);
        h.append("<p>Office: ");
        h.append(office.getSelectControl());
        h.append("</p>");
        // user name  
        h.append("<p>Username: <input type=\"text\" name=\"userName\" size=\"16\" maxlength=\"16\" value=\"");
        h.append(userName);
        h.append("\"");
        if (errors.indexOf("uname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // new password
        h.append("<p>New password: <input type=\"password\" name=\"newPassword\" size=\"16\" maxlength=\"16\"");
        if (errors.indexOf("newpw") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // confirm password 
        h.append("<p>Confirm password: <input type=\"password\" name=\"confirmPassword\" size=\"16\" maxlength=\"16\"");
        if (errors.indexOf("confirmpw") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>");
        // buttons 
        h.append("<p><input type=\"submit\" value=\"Submit\"> ");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("</form>");
        // errors
        if (errors.length() > 0) {
            h.append("<p><strong>Please correct these errors.</strong></p>");
            if (errors.indexOf("inuse") > -1) {
                // UNAME is valid but already in use 
                h.append("<p>The user name is valid but has ");
                h.append("been taken.  Please try something different.</p>");
            }
            if (errors.indexOf("invpw") > -1) {
                // password was invalid for account 
                h.append("<p>Invalid password for ");
                h.append("this account.</p>");
            }
            if (errors.indexOf("newPassword,confirmPassword") > -1) {
                // passwords differ 
                h.append("<p>The passwords entered do not ");
                h.append("match.</p>");
            }
            if (errors.indexOf("pwlen") > -1) {
                // password is too short 
                h.append("<p class=\"nb\">Passwords need to be longer than ");
                h.append("five characters.</p>");
            }
        }
        return h.toString();
    }
    
    private boolean writeToDatabase() {
        Connection con;
        PreparedStatement saveUserDetails;
        boolean userSaved = false;
        try {
            con = dataSrc.getConnection();
            saveUserDetails = con.prepareStatement(sqlInsertNewUser);
            saveUserDetails.clearParameters();
            saveUserDetails.setString(1, userGiven);
            saveUserDetails.setString(2, userSurname);
            saveUserDetails.setString(3, userEmail);
            saveUserDetails.setString(4, userTeam);
            saveUserDetails.setString(5, userOffice);
            saveUserDetails.setString(6, userName);
            saveUserDetails.setString(7, newPassword);
            if (saveUserDetails.executeUpdate() == 1) {
                userSaved = true;
            }
            saveUserDetails.close();
            con.close();
        } catch (SQLException sqlex) {
            // FIXME  catch this somehow
        }
        con = null;
        return userSaved;
    }
    
    private String getRegistrationSuccessPage() {
        return "<p>Registered</p>";
    }
    
    /** 
     * Validates the information entered into the form.
     * Errors are added to a String and returned.
     * The string can be used by the caller or by the doUserForm method. 
     */ 
    private boolean validateData() {
        Connection con;
        PreparedStatement checkForUsername;
        ResultSet results;
        StringBuffer errorList = new StringBuffer();
        if (!textOnly(userGiven) || userGiven.length() < 2) { errorList.append("given,"); }
        if (!textOnly(userSurname)) { errorList.append("surname,"); }
        if (!emailAddrOkay(userEmail)) { errorList.append("email"); }
        if (!textOnly(userTeam)) { errorList.append("team,"); }
        if (!textOnly(userOffice)) { errorList.append("office,"); }
        if (!textOnly(userName) || userName.length() < 4) { errorList.append("uname,"); }
        if (newPassword.compareTo(confirmPassword) != 0) { 
            errorList.append("newPassword,confirmPassword,"); 
        }
        if (newPassword.length() > 0) {
            if (newPassword.length() < 5) { errorList.append("pwlen,"); }
        }
        try {
            con = dataSrc.getConnection();
            checkForUsername = con.prepareStatement(sqlCheckForExistingUser);
            checkForUsername.setString(1, userName);
            results = checkForUsername.executeQuery();
            if (results.next()) {
                if(results.getInt("qty") > 0) { 
                    errorList.append("inuse,");
                }
            }
            results.close();
            checkForUsername.close();
            con.close();
        } catch (SQLException sqlex) {
            System.out.println("IdentityBean, validateData:" + sqlex);
        }
        con = null;
        errors = errorList.toString();
        if (errors.length() == 0) {
            dataValid = true;
        }
        return dataValid;
    }

    /** 
     * Check that the String is only made up of numbers and/or letters.
     * @param testMe the String to check
     * @return true is the String is only letters and/or/numbers
     */ 
    private boolean textOnly(String testMe) {
        for (int c = 0; c < testMe.length(); c++) {
            if ( !Character.isLetterOrDigit(testMe.charAt(c))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Check that the email address is reasonably formed. 
     */ 
    private boolean emailAddrOkay(String emailAddr) {
        boolean result = true;
        if (emailAddr.length() < 6) {
            result = false;
        }
        if (emailAddr.indexOf("@") < 1) {
            result = false;
        }
        int startAt = 0;
        int dotCount = 0;
        while (emailAddr.indexOf(".", startAt) > -1) {
            startAt = emailAddr.indexOf(".", startAt) + 1;
            dotCount++;
        }
        if (dotCount < 2) {
            result = false;
        }
        return result;
    }
}

