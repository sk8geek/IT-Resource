/* 
 * @(#) OutPersonDetails.java    1.1 2006/10/04
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
/**
 * Outputs an HTML table contain details of a person.
 *
 * @version       1.1 4 October 2006
 * @author        Steven Lilley
 */
public class OutPersonDetails extends HttpServlet {

    /** 
     * Query to get a resource of the specified type.
     * The table can hold multiple instances of each service.  
     * Each instance of a service should have a unique <tt>property</tt> 
     * value.  This allows alternative service providers to be selected.
     */
    private final String sqlGetResourceOfType = 
        "SELECT * FROM resources WHERE service = ? AND priority > ? "
        + "ORDER BY priority LIMIT 1";
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private String LDAPBaseDomain;
    private Resource LDAPServer;
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutPersonDetails, init: Unable to get logger from context.");
        } else {
            log.finer("OutPersonDetails servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutPersonDetails", "init", nex);
        }
        // LDAP settings
        LDAPServer = getResource("LDAP");
        LDAPBaseDomain = LDAPServer.getMoreInfo();
    }

    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            String distinguishedName = new String();
            if (rqst.getParameter("dn") != null) {
                distinguishedName = (rqst.getParameter("dn"));
                h.append("<h3>Person Details</h3>");
                out.println(h);
                out.println(findFromLDAP(distinguishedName));
            }
        }
    }

    /**
     * Search LDAP for an object with the specified distinguished name.
     * @param objectName the distinguished name of the object we seek
     */
    private String findFromLDAP(String objectName) {
        StringBuffer h = new StringBuffer();
        LDAPConnection LDAPCon = new LDAPConnection();
        LDAPAttributeSet attrSet;
        LDAPAttribute attrib;
        try {
            LDAPCon.connect(LDAPServer.getHostName(), LDAPServer.getPortNumber());
            LDAPCon.bind(LDAPConnection.LDAP_V3,"","");
            attrSet = LDAPCon.read(objectName, HouseKeeper.LDAPFields).getAttributeSet();
            h.append("<!-- person details -->");
            h.append("<table class=\"boxed\">");
            for (int c = 0; c < HouseKeeper.LDAPFields.length; c++) {
                attrib = attrSet.getAttribute(HouseKeeper.LDAPFields[c]);
                if (attrib != null) {
                    h.append("<tr><td class=\"grey\">");
                    h.append(HouseKeeper.LDAPFieldNames[c] + ":</td><td>");
                    if (HouseKeeper.LDAPFields[c].equalsIgnoreCase("mail")) {
                        h.append("<a href=\"mailto:");
                        h.append(attrib.getStringValue().toLowerCase());
                        h.append("\">");
                        h.append(attrib.getStringValue());
                        h.append("</a></td></tr>");
                    } else {
                        h.append(attrib.getStringValue() + "</td></tr>");
                    }
                } 
            }
            h.append("</table>");
            LDAPCon.disconnect();
        } catch (LDAPException ldapex) {
            context.log("OutPersonDetails, findFromLDAP:", ldapex);
            h.setLength(0);
            h.append("<p>Unable to connect to the directory server.</p>");
        }
        return h.toString();
    }

    /**
     * Get a resource of the specified type.
     * @param type the type of service (e.g. SMTP, LDAP) that we want
     * @return a Resource object of the requested type if possible
     */
    private Resource getResource(String type) {
        Resource resource;
        Connection con;
        PreparedStatement getResource;
        ResultSet result;
        try {
            con = dataSrc.getConnection();;
            getResource = con.prepareStatement(sqlGetResourceOfType);
            getResource.setString(1, type);
            getResource.setInt(2, 0);
            result = getResource.executeQuery();
            if (result.next()) {
                resource = new Resource(
                    result.getString("service"), 
                    result.getString("host"), 
                    result.getInt("port"), 
                    result.getString("user"), 
                    result.getString("pw"), 
                    result.getString("info1"));
            } else {
                resource = null;
            }
            con.close();
        } catch (SQLException sqlex) {
            // can't use log here as this method is static
            System.out.print(itemDateTime.format(new Date()));
            System.err.println(" OutPersonDetails, getResource:" + sqlex);
            resource = null;
        }
        con = null;
        return resource;
    }
}

