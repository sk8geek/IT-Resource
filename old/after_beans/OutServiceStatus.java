/* 
 * @(#) OutServiceStatus.java    0.4 2005/10/29
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Produces a tabular list of the monitored services.
 *
 * @version       0.4 29 October 2005
 * @author        Steven Lilley
 */
public class OutServiceStatus extends HttpServlet {

    private final String sql_getServices = "SELECT * FROM services ORDER BY hostname";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutServiceStatus, init: Unable to get logger from context.");
        } else {
            log.finer("OutServiceStatus servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutServiceStatus", "init", nex);
        }
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getServices;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            HttpSession session = rqst.getSession(false);
            StringBuffer h = new StringBuffer();
            Date issueExpires = new Date();
            String lastCheck = "";
            String status = "";
            String issue = "";
            long issexpire = 0;
            long msRemaining = 0;
            int userLevel = 0;
            if (session != null) {
                if (session.getAttribute("ulevel") != null) {
                    userLevel = ((Integer)session.getAttribute("ulevel")).intValue();
                }
            }
            try {
                con = dataSrc.getConnection();
                getServices = con.prepareStatement(sql_getServices);
                results = getServices.executeQuery();
                h.append("<table class=\"boxed\">");
                 if (userLevel == 9) {
                    h.append("<form method=\"post\" name=\"servicenotes\"");
                    h.append(" action=\"servicestatus\">\n");
                }
                h.append("<tr><th>Service</th>");
                h.append("<th>Last checked</th>");
                h.append("<th class=\"center\">Status</th></tr>\n");
                while (results.next()) {
                    status = "";
                    if (results.getString("status") != null) {
                        status = results.getString("status");
                    }
                    issue = "";
                    if (results.getString("issue") != null) {
                        issue = results.getString("issue");
                    }
                    if (results.getTimestamp("last_check") != null) {
                        lastCheck = HouseKeeper.itemDateTime.format(results.getTimestamp("last_check"));
                    } else {
                        lastCheck = "Never";
                    }
                    h.append("<tr><td class=\"");
                    if (status.indexOf("OK") == 0) {
                        if (issue.length() == 0) {
                            h.append("serviceok");
                        } else {
                            h.append("servicebad");
                        }
                    } else 
                    if (status.indexOf("DOWN") == 0 ) {
                        h.append("servicedead");
                    } else {
                        h.append("serviceunknown");
                    }
                    h.append("\">");
                    h.append(results.getString("cname"));
                    h.append("</td>");
                    h.append("<td>" + lastCheck + "</td>");
                    h.append("<td class=\"servstat\">");
                    if (status.indexOf("OK") == 0) {
                        if (issue.length() == 0) {
                            h.append("<img src=\"images/up.png\" alt=\"Okay\" title=\"Okay\" class=\"servstat\">");
                        } else {
                            h.append("<img src=\"images/warn.png\" alt=\"Bad\" title=\"Bad\" class=\"servstat\">");
                        }
                    } else 
                    if (status.indexOf("DOWN") == 0 ) {
                        h.append("<img src=\"images/down.png\" alt=\"DEAD\" title=\"DEAD\" class=\"servstat\">");
                    } else {
                        h.append("<img src=\"images/ukn.png\" alt=\"Unknown\" title=\"Unknown\" class=\"servstat\">");
                    }
                    h.append("</td></tr>\n");
                    if (userLevel == 9) {
                        h.append("<tr class=\"grey\"><td colspan=\"2\">");                        
                        h.append("Note: <input type=\"text\" name=\"s");
                        h.append(results.getInt("sid") + "\" ");
                        h.append("maxlength=\"200\" size=\"60\" ");
                        if (results.getString("issue") != null) {
                            h.append("value=\"");
                            h.append(results.getString("issue"));
                            h.append("\"> Clear ");
                            h.append("<input type=\"checkbox\" name=\"c");
                            h.append(results.getInt("sid") + "\" ");
                        }
                        h.append("> ");
                        try {
                            issueExpires = results.getTimestamp("issexpire");
                        } catch (SQLException sqlex) {
                            if (sqlex.toString().indexOf("can not be represented") == -1) {
                                throw sqlex;
                            }
                        }
                        issexpire = issueExpires.getTime();
                        msRemaining = (issexpire - System.currentTimeMillis());
                        // convert to hours remaining, rounding up
                        msRemaining = (msRemaining + 1800000) / 3600000;
                        int hours = (int)msRemaining;
                        if (hours < 0) {
                            hours = 0;
                        }
                        h.append("Life(hrs): <select name=\"l");
                        h.append(results.getInt("sid") + "\">");
                        for (int i=1; i < 11; i++ ) {
                            h.append("<option value=\"" + i + "\" ");
                            if (i == hours) {
                                h.append("selected ");
                            }
                            h.append("/>");
                            h.append(i + "</option>");
                        }
                        h.append("</select> ");
                        h.append("</td></tr>\n");
                    } else {
                        if (results.getString("issue") != null) {
                            h.append("<tr><td colspan=\"2\" ");
                            h.append("class=\"greytext\">");
                            h.append(results.getString("issue"));
                            h.append("</td></tr>\n");
                        }
                    }
                }
                if (userLevel == 9) {
                    h.append("<tr><td colspan=\"2\" class=\"right\">");
                    h.append("<input type=\"submit\" value=\"Save\">");
                    h.append("</tr>\n</form>\n");
                }
                h.append("</table>\n");
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutServiceStatus, doGet:", sqlex);
            }
            con = null;
            out.println(h);
        } // end sync
    }
}

