/* 
 * @(#) SocketTester.java	0.4 2006/10/04
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Page to show the status of monitored services.
 *
 * @version		0.4 4 October 2006
 * @author		Steven Lilley
 */
public class SocketTester extends HttpServlet {
    
    private final String sqlAddIssue = "UPDATE services SET issue = ?, "
        + "issexpire = ADDDATE(NOW(), INTERVAL ? HOUR) WHERE "
        + "sid = ? LIMIT 1";
    private final String sqlClearIssue = "UPDATE services SET issue = null, "
        + "issexpire = NOW() WHERE sid = ? LIMIT 1";
    /** SQL query to get a text fragment. */
    protected static final String sqlGetFragment = 
        "SELECT fragment FROM fragments WHERE fragid = ?";
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:SocketTester, init: Unable to get logger from context.");
        } else {
            // FIXME log.finer("SocketTester servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("SocketTester", "init", nex);
        }
    }

    /** 
     * Display a list of monitored services and their current status.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        resp.setContentType("text/html");
        rqst.setAttribute("page", "services");
        RequestDispatcher handler;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("Service Monitor", 
                "Service Monitor", "index", HouseKeeper.normalDelay));
            out.println(getFragment("serviceIntro"));
            handler = context.getNamedDispatcher("OutServiceStatus");
            handler.include(rqst, resp);
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }
    
    /**
     * Accepts admin note/s against services and writes them to the 
     * database.
     * Enumerates the posted parameters.  For any that it finds it writes 
     * the change to the database.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
	public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {
        resp.setContentType("text/html");
        rqst.setAttribute("page", "services");
        RequestDispatcher handler;
        Connection con;
        PreparedStatement setServiceIssue;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            String service = "";
    		try {
                con = dataSrc.getConnection();
                // enumerate posted parameters
                for (Enumeration e = rqst.getParameterNames(); e.hasMoreElements();) {
                    String issue = "";
                    int lifespan = 0;
                    boolean clearIssue = false;
                    int serviceId = 0;
                    // look for s items
                    service = (String)e.nextElement();
                    if (service.startsWith("s") && rqst.getParameter(service).length() > 0) {
                        // we have an s item and it's not empty, get other parameters
                        serviceId = Integer.parseInt(service.substring(1));
                        issue = rqst.getParameter(service);
                        if (rqst.getParameter("l" + Integer.toString(serviceId)) != null) {
                            lifespan = Integer.parseInt(rqst.getParameter("l" + Integer.toString(serviceId)));
                        }
                        if (rqst.getParameter("c" + Integer.toString(serviceId)) != null) {
                            clearIssue = true;
                        }
                        if (clearIssue) {
                            setServiceIssue = con.prepareStatement(sqlClearIssue);
                            setServiceIssue.clearParameters();
                            setServiceIssue.setInt(1, serviceId);
                            setServiceIssue.executeUpdate();
                        } else {
                            setServiceIssue = con.prepareStatement(sqlAddIssue);
                            setServiceIssue.clearParameters();
                            setServiceIssue.setString(1, issue);
                            setServiceIssue.setInt(2, lifespan);
                            setServiceIssue.setInt(3, serviceId);
                            setServiceIssue.executeUpdate();
                        }
                    }
                }
                con.close();
    		} catch (SQLException sqlex) {
    			context.log("SocketTester, doPost:", sqlex);
    		}
            con = null;
            out.println(HouseKeeper.doHead("Service Monitor", 
                "Service Monitor", "index", HouseKeeper.normalDelay));
            out.println(getFragment("serviceIntro"));
            handler = context.getNamedDispatcher("OutServiceStatus");
            handler.include(rqst, resp);
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
	}
    
    /**
     * Get a piece of text from the database.
     * @param fragid the fragment identifier to get
     * @return the requested text
     */
    private String getFragment(String fragid) {
        String fragment = new String();
        Connection con;
        PreparedStatement getFragment;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getFragment = con.prepareStatement(sqlGetFragment);
            getFragment.setString(1, fragid);
            result = getFragment.executeQuery();
            if (result.next()) {
                fragment = result.getString("fragment");
            }
            con.close();
        } catch (SQLException sqlex) {
            // can't use log here as this method is static
            System.out.print(itemDateTime.format(new Date()));
            System.out.println("[itres] HouseKeeper,getFragment:" + sqlex);
        }
        con = null;
        return fragment;
    }

}

