/* 
 * @(#) OutMachinesBySite.java	0.2 2005/11/01
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Outputs a tablular list of machines by site.
 *
 * @version       0.2 1 November 2005
 * @author        Steven Lilley
 */
public class OutMachinesBySite extends HttpServlet {

    private SimpleDateFormat coverDate = new SimpleDateFormat("MMM yyyy");
    private final String sql_createTempTable = new String(
        "CREATE TEMPORARY TABLE cover (badge int, start_date date)");
    private final String sql_getLatestCover = new String(
        "INSERT INTO cover SELECT cover_history.badge, "
        + "MAX(start_date) FROM cover_history "
        + "LEFT JOIN hw_badged USING (badge) "
        + "WHERE location = ? GROUP BY badge ORDER BY badge");
    private final String sql_listHardwareBySite = new String(
        "SELECT hw_badged.badge, live, type, descrip, location, team, "
        + "user, ccentre, cover.start_date, provider, charge "
        + "FROM (hw_badged LEFT JOIN cover USING (badge)) "
        + "LEFT JOIN cover_history USING (badge, start_date) "
        + "WHERE location LIKE ? AND type like ? "
        + "ORDER BY location, hw_badged.badge");
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:SocketTester, init: Unable to get logger from context.");
        } else {
            log.finer("SocketTester servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("SocketTester", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement createTempTable;
        PreparedStatement getLatestCover;    
        PreparedStatement listHardware;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer("<!-- List by Site -->\n");
            String siteName = "";
            String filterType = "";
            String title = "";
            if (rqst.getParameter("filterhwbytype") != null) {
                if (rqst.getParameter("hwtype") != null) {
                    filterType = rqst.getParameter("hwtype");
                }
            } else {
                filterType = "%";
            }
            if (rqst.getParameter("sitename") != null) {
                siteName = rqst.getParameter("sitename");
            }
            try {
                con = dataSrc.getConnection();
                createTempTable = con.prepareStatement(sql_createTempTable);
                getLatestCover = con.prepareStatement(sql_getLatestCover);
                getLatestCover.clearParameters();
                getLatestCover.setString(1, siteName);
                listHardware = con.prepareStatement(sql_listHardwareBySite);
                listHardware.setString(1, siteName);
                listHardware.setString(2, filterType);
                createTempTable.executeUpdate();
                getLatestCover.executeUpdate();
                results = listHardware.executeQuery();
                title = "<h3>Hardware by Location: " + siteName + "</h3>\n";
                h.append(HouseKeeper.doMachineSummary(title, results, true));
                out.println(h.toString());
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutMachinesBySite, doPost:", sqlex);
            }
            con = null;
        } // end sync
    }
}

