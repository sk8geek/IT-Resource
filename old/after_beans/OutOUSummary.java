/* 
 * @(#) OutOUSummary.java    1.0 2005/08/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import java.text.DateFormat;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
/**
 * Lists the users in an Organisational Unit (Novell).
 *
 * @version        1.0 6 August 2005
 * @author        Steven Lilley
 */
public class OutOUSummary extends HttpServlet {

    // LDAP related
    private final String[] userAttribs = new String[] {"givenname", 
        "surname", "title", "l", "LID", "mail", "telephoneNumber", 
        "facsimileTelephoneNumber"};
    private final String[] userAttribsFriendly = new String[] {
        "Name", "Surname", "Title", "Location", "LID", "E-mail", 
        "Telephone", "Facsimile"};
    private final String[] ouAttribs = new String[] {"telephoneNumber", 
        "facsimileTelephoneNumber", "l"};
    /** 
     * Query to get a resource of the specified type.
     * The table can hold multiple instances of each service.  
     * Each instance of a service should have a unique <tt>property</tt> 
     * value.  This allows alternative service providers to be selected.
     */
    private final String sql_getResourceOfType = new String(
        "SELECT * FROM resources WHERE service = ? AND priority > ? "
        + "ORDER BY priority LIMIT 1");
    private String LDAPBaseDomain;
    private Resource LDAPServer;
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutOUSummary, init: Unable to get logger from context.");
        } else {
            log.finer("OutOUSummary servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutOUSummary", "init", nex);
        }
        // LDAP settings
        LDAPServer = getResource("LDAP");
        LDAPBaseDomain = LDAPServer.getMoreInfo();
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            String organisationalUnit = new String();
            if (rqst.getAttribute("find") != null) {
                organisationalUnit = (String)(rqst.getAttribute("find"));
                h.append("<h3>Person Details</h3>");
                out.println(h);
                out.println(findFromLDAP(organisationalUnit));
            }
        }
    }

    /**
     * Search LDAP for an object with the specified distinguished name.
     * @param ou the Organisation Unit to iterate
     */
    private String findFromLDAP(String ou) {
        StringBuffer h = new StringBuffer();
        LDAPConnection LDAPCon = new LDAPConnection();
        try {
            LDAPCon.connect(LDAPServer.getHostName(), 
                LDAPServer.getPortNumber());
            LDAPCon.bind(LDAPConnection.LDAP_V3,"","");
            String orgUnit = new String(ou + "," +  LDAPBaseDomain);
            String filter = new String("surname=*");
            LDAPSearchResults results = LDAPCon.search(orgUnit, 
                LDAPConnection.SCOPE_SUB, filter, userAttribs, false);
            boolean titleDone = false;
            while (results.hasMore()) {
                if (titleDone == false) {
                    h.append("<h3>People in OU</h3>");
                    h.append("<table cellspacing=\"0\" ");
                    h.append("cellpadding=\"4\" class=\"boxed\">");
                    h.append("<tr><th>Given</th><th>Surname</th>");
                    h.append("<th>Title</th><th>Location</th>");
                    h.append("<th>LID</th></tr>");
                    titleDone = true;
                }
                LDAPAttributeSet attrSet = results.next().getAttributeSet();
                h.append("<tr>");
                for (int c = 0; c < 5; c++) {
                    LDAPAttribute attr = attrSet.getAttribute(userAttribs[c]);
                    h.append("<td>");
                    if (attr != null) {
                        h.append(attr.getStringValue());
                    }
                    h.append("</td>");
                }
                h.append("</tr>");
            }
            if (titleDone) {
                h.append("</table>");
            }
            LDAPCon.disconnect();
        } catch (LDAPException ldapex) {
            System.out.println("OutOUSummary,doPost:" + ldapex);
        }
        return h.toString();
    }

    /** 
     * Get a resource of the specified type.
     * @param type the type of service (e.g. SMTP, LDAP) that we want
     * @return a Resource object of the requested type if possible
     */
    private Resource getResource(String type) {
        Resource resource;
        Connection con;
        PreparedStatement getResource;
        ResultSet results;
        try {
            con = dataSrc.getConnection();
            getResource = con.prepareStatement(sql_getResourceOfType);
            getResource.setString(1, type);
            getResource.setInt(2, 0);
            results = getResource.executeQuery();
            if (results.next()) {
                resource = new Resource(
                    results.getString("service"), 
                    results.getString("host"), 
                    results.getInt("port"), 
                    results.getString("user"), 
                    results.getString("pw"), 
                    results.getString("info1"));
            } else {
                resource = null;
            }
            con.close();
        } catch (SQLException sqlex) {
            System.err.println("OutOUSummary, getResource:" + sqlex);
            resource = null;
        }
        con = null;
        return resource;
    }

}

