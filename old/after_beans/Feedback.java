/* 
 * @(#) Feedback.java    0.2 2005/10/23
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Collects user feedback from the selfhelp system.
 *
 * @version       0.2 23 October 2005
 * @author        Steven Lilley
 */
public class Feedback extends HttpServlet { 

    private final String sql_saveFeedback = new String(
        "INSERT INTO feedback (src, rating, sitepage, comments) "
        + "VALUES (?, ?, ?, ?)");
    /** The context in which this servlet is running. */
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:Feedback, init: Unable to get logger from context.");
        } else {
            log.finer("Feedback servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("Feedback", "init", nex);
        }
    }
    
    /**
     * Handle POST requests
     * This is the normal method used for this servlet.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) { 
            rqst.setAttribute("page","selfhelp");
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter();
            RequestDispatcher handler;
            Connection con;
            PreparedStatement saveFeedback;
            int modifiedRows = 0;
            StringBuffer h = new StringBuffer();
            String rating = new String();
            String sitepage = new String();
            String comments = new String();
            if (rqst.getParameter("rating") != null) {
                rating = rqst.getParameter("rating");
            }
            if (rqst.getParameter("sitepage") != null) {
                sitepage = rqst.getParameter("sitepage");
            }
            if (rqst.getParameter("comments") != null) {
                comments = rqst.getParameter("comments");
            }
            try {
                con = dataSrc.getConnection();
                saveFeedback = con.prepareStatement(sql_saveFeedback);
                saveFeedback.clearParameters();
                saveFeedback.setString(1, rqst.getRemoteHost());
                saveFeedback.setString(2, rating);
                saveFeedback.setString(3, sitepage);
                saveFeedback.setString(4, comments);
                modifiedRows = saveFeedback.executeUpdate();
                if (modifiedRows == 1) {
                    h.append("<p>Your feedback has been accepted.</p>");
                    h.append("<p>Thank you.</p></div>");
                } else {
                    h.append("<p>Sorry, your feedback could not be ");
                    h.append("submitted!</p>");
                }
                con.close();
                out.println(HouseKeeper.doHead("Feedback", 
                    "Feedback", "index", HouseKeeper.normalDelay));
                out.println(h.toString());
                out.println("</div>");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
            } catch (SQLException sqlex) {
                context.log("Feedback, doPost", sqlex);
            }
            con = null;
        } // end sync
    }
}

