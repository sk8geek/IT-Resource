/* 
 * @(#) OutChoreList.java    0.2 2005/03/08
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Outputs a table listing the housekeeping chores.
 *
 * @version       0.2 8 March 2006
 * @author        Steven Lilley
 */
public class OutChoreList extends HttpServlet {

    private final String sql_getChoreList = "SELECT * FROM chores ORDER BY choreid";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutChoreList, init: Unable to get logger from context.");
        } else {
            log.finer("OutChoreList servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutChoreList", "init", nex);
        }
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getChoreList;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            String choreID = "";
            Calendar nextDue = Calendar.getInstance();
            Date lastRun = new Date();
            StringBuffer h = new StringBuffer("<!-- chore list starts -->\n");
            try {
                con = dataSrc.getConnection();
                getChoreList= con.prepareStatement(sql_getChoreList);
                results = getChoreList.executeQuery();
                h.append("<form method=\"post\" action=\"admin\">");
                h.append("<table class=\"boxed\"><thead><tr><th>Chore Name</th>");
                h.append("<th>Last run</th><th>Latency</th><th>Next Due</th>");
                h.append("<th>Actions</th><tr></thead>");
                while (results.next()) {
                    h.append("<tr ");
                    if (results.getInt("state") == 0) {
                        h.append("class=\"stopped\"><td>");
                    } else {
                        h.append("class=\"running\"><td>");
                    }
                    h.append(results.getString("chore_name") + "</td>");
                    if (results.getTimestamp("lastrun") != null) {
                        lastRun = results.getTimestamp("lastrun");
                    } else {
                        lastRun.setTime(0);
                    }
                    if (lastRun.getTime() > 0) {
                        h.append("<td>" + HouseKeeper.itemDateTime.format(results.getTimestamp("lastrun")));
                    } else {
                        h.append("<td class=\"centre\">never");
                    }
                    h.append("</td><td class=\"right\">");
                    h.append(Integer.toString(results.getInt("latency")));
                    h.append("</td>");
                    if (results.getInt("state") != 0) {
                        nextDue.setTime(lastRun);
                        nextDue.add(Calendar.SECOND, results.getInt("latency"));
                        if ((nextDue.getTimeInMillis() + results.getInt("latency") * 1000) < System.currentTimeMillis()) {
                            h.append("<td class=\"centre\">next poll");
                        } else {
                            h.append("<td>" + HouseKeeper.itemDateTime.format(nextDue.getTime()));
                        }
                    } else {
                        h.append("<td class=\"centre\">stopped");
                    }
                    h.append("</td><td>");
                    choreID = Integer.toString(results.getInt("choreid"));
                    if (results.getInt("state") == 0) {
                        h.append("<input type=\"checkbox\" name=\"");
                        h.append(choreID + "_on\">start ");
                    } else {
                        h.append("<input type=\"checkbox\" name=\"");
                        h.append(choreID + "_off\">stop ");
                    }
                    h.append("<input type=\"checkbox\" name=\"");
                    h.append(choreID + "_run\">run ");
                    h.append("</td>");
                    h.append("</tr>");
                }
                h.append("<tr><td colspan=\"5\" class=\"right\">");
                h.append("<input type=\"submit\" ");
                h.append("value=\"Execute\">");
                h.append("<input type=\"hidden\" name=\"action\" ");
                h.append("value=\"setchorestate\"></td></tr>");
                h.append("</table></form>");
                results.close();
                con.close();
                h.append("<!-- chore list ends -->\n");
                out.println(h.toString());
            } catch (SQLException sqlex) {
                context.log("OutChoreList:" + sqlex);
            }
            con = null;
        } // end sync
    }
}
