/* 
 * @(#) Admin.java    0.4 2006/09/10
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * An administration servlet.
 * This servlet gives the system admin a number of tools.  
 * Services can be stopped or run immediately, test emails sent and 
 * user status levels changed.
 *
 * @version       0.4 10 September 2006
 * @author        Steven Lilley
 */
 public class Admin extends HttpServlet { 

    private Properties mailService = new Properties();
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Connection con;
    private final String sql_setChoreState = "UPDATE chores SET state = ? WHERE choreid = ? LIMIT 1";
    private final String sql_setUserLevel = "UPDATE register SET ulevel = ? WHERE uname = ? LIMIT 1";
    private final String[] logLevels = {"OFF", "SEVERE", "WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST", "ALL"};
    private Logger log;

    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:Admin, init: Unable to get logger from context.");
        } else {
            log.finer("Admin servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("Admin", "init", nex);
        }
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) { 
            resp.setContentType("text/html");
            resp.setDateHeader("Last-Modified", System.currentTimeMillis());
            rqst.setAttribute("page", "admin");
            PrintWriter out = resp.getWriter();
            RequestDispatcher handler;
            // check that we have a valid session 
            HttpSession session = rqst.getSession(false);
            if (session == null) {
                // no session so kick the user out
                out.println(HouseKeeper.doHead("System Administration", 
                    "Not Logged In", "index", HouseKeeper.errorDelay));
                out.println("<p>Your session has expired, or you are ");
                out.println("not logged in.</p>");
            } else {
                // we have a valid session, get user name and status 
                String uname = (String)session.getAttribute("uname");
                out.println(HouseKeeper.doHead("System Administration", 
                    "Administration", "index", HouseKeeper.secureDelay));
                handler = context.getNamedDispatcher("OutUserList");
                handler.include(rqst, resp);
                handler = context.getNamedDispatcher("OutChoreList");
                handler.include(rqst, resp);
                out.println(setLogLevelControl());
                out.println(doMailTestButton());
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
        } // end sync
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) { 
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            String failureReason = new String();
            String action = new String();
            rqst.setAttribute("page", "admin");
            resp.setContentType("text/html");
            resp.setDateHeader("Last-Modified", System.currentTimeMillis());
            // check that we have a valid session 
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                // we have a valid session, get user name and status 
                String uname = (String)session.getAttribute("uname");
                if (rqst.getParameter("action") != null) {
                    action = rqst.getParameter("action");
                }
                if (action.equalsIgnoreCase("admin")) {
                    doGet(rqst, resp);
                } else
                if (action.equalsIgnoreCase("setuserlevel")) {
                    setUserLevel(rqst, resp);
                } else
                if (action.equalsIgnoreCase("setchorestate")) {
                    setChoreState(rqst, resp);
                } else
                if (action.equalsIgnoreCase("testemail")) {
                    testEmail(rqst, resp);
                } else 
                if (action.equalsIgnoreCase("setLogLevel")) {
                    setLogLevel(rqst, resp);
                } else {
                    failureReason = "The action requested was invalid.";
                }
            } else {
                failureReason = "You do not appear to be logged in, or your"
                    + "session has expired.";
            }
            out.println(HouseKeeper.doHead("System Administration", 
                "Administration", "index", HouseKeeper.errorDelay));
            if (failureReason.length() > 0) {
                out.println("<p>" + failureReason + "</p>");
            } else {
                handler = context.getNamedDispatcher("OutUserList");
                handler.include(rqst, resp);
                handler = context.getNamedDispatcher("OutChoreList");
                handler.include(rqst, resp);
                out.println(setLogLevelControl());
                out.println(doMailTestButton());
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } //end sync
    }
    
    /**
     * Control to allow the log level to be changed.
     */
    private String setLogLevelControl() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"admin\" name=\"setLogLevel\" >\n");
        h.append("<p>");
        h.append("<select name=\"logLevel\">");
        for (int i = 0; i < logLevels.length; i++) {
            h.append("<option ");
            if (logLevels[i].equalsIgnoreCase(log.getLevel().toString())) {
                h.append("selected");
            }
            h.append(">" + logLevels[i] + "</option>");
        }
        h.append("</select>");
        // action 
        h.append("<input type=\"hidden\" name=\"action\" value=\"setLogLevel\">");
        h.append("<input type=\"submit\" value=\"Set Log Level\">");
        h.append("</p></form>\n");
        return h.toString();
    }
    
    /**
     * Set the log level.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void setLogLevel(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        String newLogLevel = log.getLevel().toString();
        if (rqst.getParameter("logLevel") != null) {
            newLogLevel = rqst.getParameter("logLevel");
        }
        try {
            log.setLevel(Level.parse(newLogLevel));
            log.finer("Log level set to " + log.getLevel().toString());
        } catch (NullPointerException npex) {
            log.throwing("Admin", "setLogLevel", npex);
        }
    }
    
    /**
     * Change the authority level of a user.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void setUserLevel(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement setUserLevel;
        String setUser = "";
        int newLevel = 0;
        if (rqst.getParameter("setUser") != null) {
            setUser = rqst.getParameter("setUser");
        }
        if (rqst.getParameter("newLevel") != null) {
            try {
                newLevel = Integer.parseInt(rqst.getParameter("newLevel"));
            } catch (NumberFormatException nfex) {
                context.log("Admin, doPost: Invalid newLevel parameter.");
            }
        }
        try {
            con = dataSrc.getConnection();
            setUserLevel = con.prepareStatement(sql_setUserLevel);
            setUserLevel.clearParameters();
            setUserLevel.setInt(1, newLevel);
            setUserLevel.setString(2, setUser);
            if (setUserLevel.executeUpdate() == 1) {
                rqst.setAttribute("message", "user level set");
                log.finer("User " + setUser + " set to level " + newLevel);
            } else {
                rqst.setAttribute("message", "user user FAILED");
                log.fine("FAILED to set user " + setUser + " (to level " + newLevel + ")");
            }
            con.close();
        } catch (SQLException sqlex) {
            log.throwing("Admin", "setUserLevel", sqlex);
        }
        con = null;
        return;
    }
    
    /**
     * Start, stop or immediately run a housekeeping chore.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void setChoreState(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        Connection con;
        PreparedStatement setChoreState;
        RequestDispatcher handler;
        String param = new String();
        int choreID = 0;
        try {
            con = dataSrc.getConnection();
            setChoreState = con.prepareStatement(sql_setChoreState);
            for (Enumeration e = rqst.getParameterNames(); 
                e.hasMoreElements() ;) {
                param = (String)e.nextElement();
                // test for service start 
                if (param.indexOf("_on") > -1) {
                    choreID = Integer.parseInt(
                        param.substring(0, param.indexOf("_")));
                    setChoreState.clearParameters();
                    setChoreState.setInt(1, -1);
                    setChoreState.setInt(2, choreID);
                    if (setChoreState.executeUpdate() == 1) {
                        log.finer("Chore " + choreID + " switched on.");
                    }
                }
                if (param.indexOf("_off") > -1) {
                    choreID = Integer.parseInt(
                        param.substring(0, param.indexOf("_")));
                    setChoreState.clearParameters();
                    setChoreState.setInt(1, 0);
                    setChoreState.setInt(2, choreID);
                    if (setChoreState.executeUpdate() == 1) {
                        log.finer("Chore " + choreID + " switched OFF.");
                    }
                }
                if (param.indexOf("_run") > -1) {
                    choreID = Integer.parseInt(param.substring(0, param.indexOf("_")));
                    log.finer("Chore " + choreID + " forced to run.");
                    handler = context.getNamedDispatcher("HouseKeeper");
                    rqst.setAttribute("runChore", new Integer(choreID));
                    handler.include(rqst, resp);
                }
            }
            con.close();
        } catch (SQLException sqlex) {
            log.throwing("Admin", "setChoreState", sqlex);
        } catch (NumberFormatException nfex) {
            log.throwing("Admin", "setChoreState", nfex);
        }
        con = null;
        return;
    }
    
    /**
     * Sends a test message to the given address.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void testEmail(HttpServletRequest rqst, HttpServletResponse resp) {
        String message;
        String mailTo;
        try {
            mailTo = rqst.getParameter("mailto");
            String testMessage = 
                "This is a test message sent from the IT Resource suite.";
            if (HouseKeeper.sendMail(mailTo, "Test Message", testMessage)) {
                rqst.setAttribute("message", "email sent");
            } else {
                rqst.setAttribute("message", "email FAILED");
            }
        } catch (NullPointerException npex) {
            context.log("midgley: Admin, testEmail", npex);
            message = "FAIL Test email failed (null pointer exception).";
        }
    }
    
    /** 
     * Produce an email test button.
     * @return HTML for the email test address box and button
     */
    private String doMailTestButton() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"admin\" ");
        h.append("name=\"emailTest\" >\n");
        h.append("<p>");
        h.append("Mail to: <input type=\"text\" name=\"mailto\" ");
        h.append("maxlength=\"60\" size=\"30\">");
        h.append(" <input type=\"submit\" value=\"Test Email\">");
        h.append("<input type=\"hidden\" name=\"action\" ");
        h.append("value=\"testEmail\">");
        h.append("</p></form>\n");
        return h.toString();
    }
}

