/* 
 * @(#) UserAdmin.java    0.5 2006/10/03
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * User self administration servlet.
 * Handles initial user registration and user preferences changes.
 * User operations, such as login/out and checkin/out have been moved to 
 * the UserOps class.
 *
 * @version       0.5 3 October 2006
 * @author        Steven Lilley
 */
public class UserAdmin extends HttpServlet { 
    
    /** SQL query to add a new user. */
    private final String sqlInsertNewUser = 
        "INSERT INTO register (uname, pw, given, surname, email, team, site) "
        + "VALUES (?, PASSWORD(?), ?, ?, ?, ?, ?)";
    /** SQL query to get user data. */
    private final String sqlGetUserData = 
        "SELECT given, surname, email, team, site FROM register WHERE uname=?";
    /** SQL query to update all of a user's data. */
    private final String sqlUpdatePrefsIncludingPassword = 
        "UPDATE register SET given=?, surname=?, email=?, team=?, site=?, "
        + "pw=PASSWORD(?) WHERE uname=? LIMIT 1";
    /** SQl query to update user data, exluding the password. */
    private final String sqlUpdatePrefsExcludingPassword = 
        "UPDATE register SET given=?, surname=?, email=?, team=?, site=? "
        + "WHERE uname=? LIMIT 1";
    /** SQL query to check to see if a username exists in the database. */
    private final String sqlCheckForExistingUser = 
        "SELECT count(uname) AS qty FROM register WHERE uname = ?";
    /** SQL query to check a password. */
    private final String sqlCheckPassword = 
        "SELECT count(uname) AS qty FROM register WHERE uname = ? "
        + "AND pw = PASSWORD(?)";
    /** The context in which this servlet is running. */
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:UserAdmin, init: Unable to get logger from context.");
        } else {
            log.finer("UserAdmin servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("UserAdmin", "init", nex);
        }
    }

    /** 
     * This should only be called by an unregistered user.
     * Present a registration form. 
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","ereg");
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Administration", 
                "Registration", "index", HouseKeeper.normalDelay));
            out.println(userForm("", "", "", "", "", "", false, false, true, 
                "register", "", "User Registration"));
            out.println("</div>");
            RequestDispatcher handler = 
                context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called for many different user operations.
     * This class handles user registration and preferences changes.
     * The <code>userop</code> parameter determines which method is called.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "register");
            RequestDispatcher handler;
            String action = new String();
            if (rqst.getParameter("userop") != null) {
                action = rqst.getParameter("userop");        
            }
            if (action.equalsIgnoreCase("register")) {
                userRegistration(rqst, resp);
            } else 
            if (action.equalsIgnoreCase("editprefs")) {
                editPrefs(rqst, resp);
            } else
            if (action.equalsIgnoreCase("saveprefs")) {
                savePrefs(rqst, resp);
            } else {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Administration", 
                "Request Error", "index", HouseKeeper.errorDelay));
            out.println("<p>Invalid request.</p>");
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
            }
        } // end sync
    }
    
    /**
     * Accept the user registration form.
     * This method may not directly produce any output.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userRegistration(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out;
        RequestDispatcher handler;
        // database variables
        Connection con;
        PreparedStatement insertNewUser;
        synchronized (this) {
            int modified = 0;
            String errors = "";
            String failureReason = "";
            // parameter variables
            String uname = "";
            String given = "";
            String surname = "";
            String email = "";
            String team = "";
            String site = "";
            String pw1 = "";
            String pw2 = "";
            if (rqst.getParameter("uname") != null) {
                uname = rqst.getParameter("uname");
            }
            if (rqst.getParameter("given") != null) {
                given = rqst.getParameter("given");        
            }
            if (rqst.getParameter("surname") != null) {
                surname = rqst.getParameter("surname");
            }
            if (rqst.getParameter("email") != null) {
                email = rqst.getParameter("email");
            }
            if (rqst.getParameter("team") != null) {
                team = rqst.getParameter("team");
            }
            if (rqst.getParameter("site") != null) {
                site = rqst.getParameter("site");
            }
            if (rqst.getParameter("pw1") != null) {
                pw1 = rqst.getParameter("pw1");
            }
            if (rqst.getParameter("pw2") != null) {
                pw2 = rqst.getParameter("pw2");
            }
            // validate form 
            errors = validateData(given, surname, email, team, site, uname, 
                "", pw1, pw2, false);
            if (errors.length() > 0) {
                // error occured, show registration form again
                out = resp.getWriter();
                out.println(HouseKeeper.doHead("User Administration", 
                    "Registration", "index", HouseKeeper.secureDelay));
                out.println(userForm(given, surname, email, team, site, 
                    uname, false, false, true, "register", errors, 
                    "User Registration"));
                out.println("</div>");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
            } else {
                // data is valid 
                try {
                    con = dataSrc.getConnection();
                    insertNewUser = con.prepareStatement(sqlInsertNewUser);
                    insertNewUser.clearParameters();
                    insertNewUser.setString(1, uname);
                    insertNewUser.setString(2, pw1);
                    insertNewUser.setString(3, given);
                    insertNewUser.setString(4, surname);
                    insertNewUser.setString(5, email);
                    insertNewUser.setString(6, team);
                    insertNewUser.setString(7, site);
                    if (insertNewUser.executeUpdate() == 1) {
                        // stored user okay, set cookies
                        Cookie eregCookie = new Cookie("eregid", uname);
                        eregCookie.setMaxAge(5184000);
                        Cookie eregCookie2 = new Cookie("eregsite", site);
                        eregCookie2.setMaxAge(5184000);
                        resp.addCookie(eregCookie);
                        resp.addCookie(eregCookie2);
                        handler = rqst.getRequestDispatcher("/index");
                        rqst.setAttribute("userop", ""); // reset userop
                        handler.forward(rqst, resp);
                    } else {
                        failureReason = 
                            "Unable to create new account.";
                    }
                    con.close();
                } catch (SQLException sqlex) {
                    context.log("UserAdmin, userRegistration:", sqlex);
                    failureReason = "Unable to access database.";
                }
                con = null;
                if (failureReason.length() > 0) {
                    out = resp.getWriter();
                    out.println(HouseKeeper.doHead("User Administration", 
                        "Registration Error", "index", HouseKeeper.errorDelay));
                    out.println("<p>" + failureReason + "</p>");
                    out.println("</div>");
                    handler = context.getNamedDispatcher("MenuFragment");
                    handler.include(rqst, resp);
                    out.close();
                }
            }
        } // end sync
    }
    
    /**
     * Allows the user to edit their preferences.
     * This method <em>always</em> produces output.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void editPrefs(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        RequestDispatcher handler;
        // database variables
        Connection con;
        PreparedStatement getUserData;
        ResultSet results;
        synchronized (this) {
            String failureReason = "";
            String uname = "";
            String given = "";
            String surname = "";
            String email = "";
            String team = "";
            String site = "";
            // session check 
            HttpSession session = rqst.getSession(false);
            if (session == null) {
                // no valid session
                out.println(HouseKeeper.doHead("User Administration", 
                    "Not Logged In", "index", HouseKeeper.errorDelay));
                out.println("<p>You are not logged in.</p>");
                out.println("<p>In order to change your settings you must be");
                out.println("logged in.</p>");
            } else {
                // valid session 
                try {
                    uname = (String)session.getAttribute("uname");
                    con = dataSrc.getConnection();
                    getUserData = con.prepareStatement(sqlGetUserData);
                    getUserData.clearParameters();
                    getUserData.setString(1, uname);
                    results = getUserData.executeQuery();
                    if (results.next()) {
                        given = results.getString("given");
                        surname = results.getString("surname");
                        email = results.getString("email");
                        team = results.getString("team");
                        site = results.getString("site");
                        out.println(HouseKeeper.doHead("User Preferences", 
                            "User Administration", "index", 
                            HouseKeeper.secureDelay));
                        out.println(userForm(given, surname, email, team, 
                            site, uname, true, true, true, "saveprefs", 
                            "", "Preferences"));
                    } else {
                        context.log("UserAdmin failed to find account");
                        failureReason = "Unable to find account.";
                    }
                    con.close();
                } catch (SQLException sqlex) {
                    context.log("UserAdmin, editPrefs:", sqlex);
                    failureReason = "Unable to access database.";
                } catch (NullPointerException npex) {
                    failureReason = "Session expired.";
                }
                con = null;
            }
            if (failureReason.length() > 0) {
                out = resp.getWriter();
                out.println(HouseKeeper.doHead("User Preferences", 
                    "Error", "index", HouseKeeper.errorDelay));
                out.println("<p>" + failureReason + "</p>");
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
    
    /**
     * Accept the user registration form.
     * This method may not directly produce any output.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void savePrefs(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out;
        RequestDispatcher handler;
        String errors = "";
        String failureReason = "";
        // database variables
        Connection con;
        PreparedStatement updatePrefs;
        // parameter variables
        String uname = "";
        String given = "";
        String surname = "";
        String email = "";
        String team = "";
        String site = "";
        String oldpw = "";
        String pw1 = "";
        String pw2 = "";
        if (rqst.getParameter("uname") != null) {
            uname = rqst.getParameter("uname");
        }
        if (rqst.getParameter("given") != null) {
            given = rqst.getParameter("given");        
        }
        if (rqst.getParameter("surname") != null) {
            surname = rqst.getParameter("surname");
        }
        if (rqst.getParameter("email") != null) {
            email = rqst.getParameter("email");
        }
        if (rqst.getParameter("team") != null) {
            team = rqst.getParameter("team");
        }
        if (rqst.getParameter("site") != null) {
            site = rqst.getParameter("site");
        }
        if (rqst.getParameter("oldpw") != null) {
            oldpw = rqst.getParameter("oldpw");
        }
        if (rqst.getParameter("pw1") != null) {
            pw1 = rqst.getParameter("pw1");
        }
        if (rqst.getParameter("pw2") != null) {
            pw2 = rqst.getParameter("pw2");
        }
        // session check 
        HttpSession session = rqst.getSession(false);
        if (session == null) {
            // no valid session
            failureReason = "Not logged on, or session expired.";
        } else {
            // valid session
            errors = validateData(given, surname, email, team, site, uname, 
                oldpw, pw1, pw2, true);
            if (errors.length() > 0) {
                // error occured, show registration form again
                out = resp.getWriter();
                out.println(HouseKeeper.doHead("User Administration", 
                    "Preferences", "index", HouseKeeper.secureDelay));
                out.println(userForm(given, surname, email, team, 
                    site, uname, true, true, true, "saveprefs", 
                    "", "Preferences"));
                out.println("</div>");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
            } else {
                // data is valid 
                try {
                    uname = (String)session.getAttribute("uname");
                    con = dataSrc.getConnection();
                    if (oldpw.length() > 4) {
                        updatePrefs = con.prepareStatement(
                            sqlUpdatePrefsIncludingPassword);
                        updatePrefs.clearParameters();
                        updatePrefs.setString(6, pw1);
                        updatePrefs.setString(7, uname);
                    } else {
                        updatePrefs = con.prepareStatement(
                            sqlUpdatePrefsExcludingPassword);
                        updatePrefs.clearParameters();
                        updatePrefs.setString(6, uname);
                    }
                    updatePrefs.setString(1, given);
                    updatePrefs.setString(2, surname);
                    updatePrefs.setString(3, email);
                    updatePrefs.setString(4, team);
                    updatePrefs.setString(5, site);
                    if (updatePrefs.executeUpdate() == 1) {
                        Cookie eregCookie = new Cookie("eregid", uname);
                        eregCookie.setMaxAge(5184000);
                        Cookie eregCookie2 = new Cookie("eregsite", site);
                        eregCookie2.setMaxAge(5184000);
                        resp.addCookie(eregCookie);
                        resp.addCookie(eregCookie2);
                        handler = rqst.getRequestDispatcher("/index");
                        rqst.setAttribute("userop", ""); // reset userop
                        handler.forward(rqst, resp);
                    } else {
                        failureReason = "Unable to save preferences.";
                    }
                    con.close();
                } catch (SQLException sqlex) {
                    context.log("UserAdmin, savePrefs:", sqlex);
                    failureReason = "Unable to access database.";
                } catch (NullPointerException npex) {
                    System.out.print(
                        HouseKeeper.itemDateTime.format(new Date()));
                    failureReason = "Session expired.";
                }
            }
        }
        if (failureReason.length() > 0) {
            out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Administration", 
                "Registration Error", "index", HouseKeeper.errorDelay));
            out.println("<p>" + failureReason + "</p>");
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }
    
    /** 
     * Validates the information entered into the form.
     * Errors are added to a String and returned.
     * The string can be used by the caller or by the doUserForm method. 
     * @param given the users given name
     * @param surname the users surname
     * @param team the team in which the user works
     * @param site the site (office) in which the user works
     * @param uname the internal user ID
     * @param oldpw the previous password, if the password is being changed
     * @param pw1 the new password, for password changes
     * @param pw2 confirmation of the new password, for password changes
     * @param shouldExist indicates if we expect the user to exist or not
     * @return a list of the errors encountered during validation
     */ 
    private String validateData(String given, String surname, String email, String team, 
        String site, String uname, String oldpw, String pw1, String pw2, 
        boolean shouldExist) {
        Connection con;
        PreparedStatement checkForUsername;
        PreparedStatement checkPassword;
        ResultSet results;
        StringBuffer errors = new StringBuffer();
        if (!textOnly(given)) { errors.append("given,"); }
        if (!textOnly(surname)) { errors.append("surname,"); }
        if (!emailAddrOkay(email)) { errors.append("email"); }
        if (!textOnly(team)) { errors.append("team,"); }
        if (!textOnly(site)) { errors.append("site,"); }
        if (!textOnly(uname)) { errors.append("uname,"); }
        if (pw1.compareTo(pw2) != 0) { errors.append("pw1,pw2,"); }
        if (pw1.length() > 0) {
            if (pw1.length() < 5) { errors.append("pwlen,"); }
        }
        try {
            con = dataSrc.getConnection();
            checkForUsername = 
                con.prepareStatement(sqlCheckForExistingUser);
            checkForUsername.setString(1, uname);
            results = checkForUsername.executeQuery();
            if (results.next()) {
                if(results.getInt("qty") > 0) { 
                    if (!shouldExist) {
                        errors.append("inuse,");
                    }
                } else {
                    if (shouldExist) {
                        errors.append("nofind");
                    }
                }
            }
            if (pw1.length() != 0 && shouldExist) {
                checkPassword = con.prepareStatement(sqlCheckPassword);
                checkPassword.clearParameters();
                checkPassword.setString(1, uname);
                checkPassword.setString(2, oldpw);
                results = checkPassword.executeQuery();
                if (results.next()) {
                    if(results.getInt("qty") != 1) { 
                        errors.append("invpw,");
                    }
                }
            }
            con.close();
        } catch (SQLException sqlex) {
            context.log("UserAdmin, validateData:", sqlex);
        }
        con = null;
        return errors.toString();
    }

    /** 
     * Check that the String is only made up of numbers and/or letters.
     * @param testMe the String to check
     * @return true is the String is only letters and/or/numbers
     */ 
    private boolean textOnly(String testMe) {
        for (int c = 0; c < testMe.length(); c++) {
            if ( !Character.isLetterOrDigit(testMe.charAt(c))) {
                return false;
            }
        }
        return true;
    }

    /**
     * A method that returns a user registration/preferences form.
     * @param given the users first name, if known
     * @param surname the users surname, if known
     * @param team the team that the user is in, if known
     * @param site the site that the user works at, if known
     * @param uname the users system username
     * @param existing true if this a a preferences update, false for 
     * new user registration
     * @param oldpw true if the existing password box should be shown
     * @param pwpair true if the new/confirm password boxes should be shown
     * @param userop the value for the <code>userop</code> that will be 
     * submitted with the form
     * @param errors a list of validation errors
     * @param title the form title
     * @return an HTML string containing a table and form.
     */
    private String userForm(String given, String surname, String email, String team, 
        String site, String uname, boolean existing, boolean oldpw, 
        boolean pwpair, String userop, String errors, String title) {
        StringBuffer h = new StringBuffer();
        ArrayList teams = (ArrayList)context.getAttribute("teams");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        h.append("<h3>" + title + "</h3>\n");
        if (!existing) {
            h.append("<p>Please complete all boxes.</p>\n");
        }
        h.append("<form method=\"post\" action=\"useradmin\" ");
        h.append("name=\"userdetails\" ");
        h.append("onMouseOver=\"userFormFocus();\">\n");
        h.append("<table class=\"borderless\">\n");
        // given name 
        h.append("<tr><td>first name:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"30\" ");
        h.append("name=\"given\" size=\"30\"");
        if (given.length() > 0) {
            h.append(" value=\"" + given + "\"");
        }
        if (errors.indexOf("given") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>");
        // surname 
        h.append("<tr><td>surname:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"40\" ");
        h.append("name=\"surname\" size=\"30\"");
        if (surname.length() > 0) {
            h.append(" value=\"" + surname + "\"");
        }
        if (errors.indexOf("surname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // email
        h.append("<tr><td>email:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"120\" ");
        h.append("name=\"email\" size=\"40\"");
        if (surname.length() > 0) {
            h.append(" value=\"" + email + "\"");
        }
        if (errors.indexOf("email") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // team
        if (teams != null) {
            h.append("<tr><td>team:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<select name=\"team\">");
            for (int c = 0; c < teams.size(); c++) {
                DropDownItem d = (DropDownItem)teams.get(c);
                h.append("<option value=\"" + d.code + "\"");
                if (d.code.equalsIgnoreCase(team)) {
                    h.append(" selected");
                }
                h.append(">" + d.desc + "</option>");
            }
            h.append("</select></td></tr>");
        }
        // site
        if (sites != null) {
            h.append("<tr><td>office:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<select name=\"site\">");
            for (int c = 0; c < sites.size(); c++) {
                DropDownItem d = (DropDownItem)sites.get(c);
                h.append("<option value=\"" + d.code + "\"");
                if ( d.code.equalsIgnoreCase(site) ) {
                    h.append(" selected");
                }
                h.append(">" + d.desc + "</option>");
            }
            h.append("</select></td></tr>");
        }
        // user name  
        h.append("<tr><td>user name:</td>");
        if (!existing) {
            h.append("<td>");
            h.append("<input type=\"text\" maxlength=\"16\" ");
            h.append("name=\"uname\" size=\"16\"");
            if (uname.length() > 0) {
                h.append(" value=\"" + uname + "\"");
            }
            if (errors.indexOf("uname") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td>");
            h.append("<td><span class=\"note\">");
            h.append("Enter a user name up to sixteen ");
            h.append("characters long.</span>");
        } else {
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"hidden\" name=\"uname\" value=\"");
            h.append(uname + "\"><b>" + uname + "</b>");
        }
        h.append("</td></tr>\n");
        // old password 
        if (oldpw) {
            h.append("<tr><td>");
            if (pwpair) { h.append("old ");    }
            h.append("password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"oldpw\" size=\"16\"");
            if (errors.indexOf("oldpw") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
        }
        if (pwpair) {
            // password  
            h.append("<tr><td>password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"pw1\" size=\"16\"");
            if (errors.indexOf("pw1") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
            // confirm password 
            h.append("<tr><td>confirm password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"pw2\" size=\"16\"");
            if (errors.indexOf("pw2") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
        }
        // buttons 
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"submit\" value=\"Submit\">");
        h.append("<input type=\"reset\" value=\"Reset\">");
        // action 
        h.append("<input type=\"hidden\" name=\"userop\" ");
        h.append("value=\"" + userop + "\">");
        // close table 
        h.append("</td></tr></table>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("inuse") > -1) {
            // UNAME is valid but already in use 
            h.append("<p class=\"nb\">The user name is valid but has ");
            h.append("been taken.  Please try something different.</p>");
        }
        if (errors.indexOf("invpw") > -1) {
            // password was invalid for account 
            h.append("<p class=\"nb\">Invalid password for ");
            h.append("this account.</p>");
        }
        if (errors.indexOf("pw1,pw2") > -1) {
            // passwords differ 
            h.append("<p class=\"nb\">The passwords entered do not ");
            h.append("match.</p>");
        }
        if (errors.indexOf("pwlen") > -1) {
            // password is too short 
            h.append("<p class=\"nb\">Passwords need to be longer than ");
            h.append("five characters.</p>");
        }
        return h.toString();
    }

    /**
     * Check that the email address is reasonably formed. 
     */ 
    private boolean emailAddrOkay(String emailAddr) {
        boolean result = true;
        if (emailAddr.length() < 6) {
            result = false;
        }
        if (emailAddr.indexOf("@") < 1) {
            result = false;
        }
        int startAt = 0;
        int dotCount = 0;
        while (emailAddr.indexOf(".", startAt) > -1) {
            startAt = emailAddr.indexOf(".", startAt) + 1;
            dotCount++;
        }
        if (dotCount < 2) {
            result = false;
        }
        return result;
    }
    
    private String failureReason(String reason) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>System Failure</h3>\n");
        h.append("<p>Unfortunately I have not been able to complete ");
        h.append("your request.</p>\n");
        if (reason.length() > 0) { 
            h.append("<p>" + reason + "</p>");
        } else {
            h.append("<p>Something is broken, but I don't know what.</p>");
        }
        h.append("<p>This fault has been logged. ");
        h.append("Please try again later.</p>");
        return h.toString();
    }
}
