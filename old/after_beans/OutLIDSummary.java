/* 
 * @(#) OutLIDSummary.java    0.4 2006/10/04
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import java.text.DateFormat;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
/**
 * Output a tabular list of person details from an LDAP source.
 *
 * @version       0.4 4 October 2006
 * @author        Steven Lilley
 */
public class OutLIDSummary extends HttpServlet {

    /** 
     * Query to get a resource of the specified type.
     * The table can hold multiple instances of each service.  
     * Each instance of a service should have a unique <tt>property</tt> 
     * value.  This allows alternative service providers to be selected.
     */
    private final String sqlGetResourceOfType = 
        "SELECT * FROM resources WHERE service = ? AND priority > ? "
        + "ORDER BY priority LIMIT 1";
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private StringBuffer filter = new StringBuffer();
    private String LDAPBaseDomain;
    private Resource LDAPServer;
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutLIDSummary, init: Unable to get logger from context.");
        } else {
            log.finer("OutLIDSummary servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutLIDSummary", "init", nex);
        }
        // LDAP settings
        LDAPServer = getResource("LDAP");
        LDAPBaseDomain = LDAPServer.getMoreInfo();
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            boolean titleDone = false;
            LDAPConnection LDAPCon = new LDAPConnection();
            LDAPSearchResults searchResults;
            LDAPAttributeSet attribSet;
            LDAPAttribute attrib;
            try {
                LDAPCon.connect(LDAPServer.getHostName(), LDAPServer.getPortNumber());
                LDAPCon.bind(LDAPConnection.LDAP_V3,"","");
                String findPart = (String)(rqst.getAttribute("find"));
                StringBuffer filter = new StringBuffer("(|(LID=");
                filter.append(findPart);
                filter.append(")(cn=");
                filter.append(findPart);
                filter.append("))");
                searchResults = LDAPCon.search(LDAPBaseDomain, LDAPConnection.SCOPE_SUB, 
                    filter.toString(), HouseKeeper.LDAPFields, false);
                while (searchResults.hasMore()) {
                    if (titleDone == false) {
                        h.append("<h3>People</h3>");
                        h.append("<table class=\"boxed\">");
                        h.append("<tr><th colspan=\"2\">People</th></tr>");
                        titleDone = true;
                    }
                    attribSet = searchResults.next().getAttributeSet();
                    for (int c = 0; c < HouseKeeper.LDAPFields.length; c++) {
                        attrib = attribSet.getAttribute(HouseKeeper.LDAPFields[c]);
                        if (attrib != null) {
                            h.append("<tr><td class=\"grey\">");
                            h.append(HouseKeeper.LDAPFieldNames[c] + ":</td><td>");
                            if (HouseKeeper.LDAPFields[c].equalsIgnoreCase("mail")) {
                                h.append("<a href=\"mailto:");
                                h.append(attrib.getStringValue().toLowerCase());
                                h.append("\">");
                                h.append(attrib.getStringValue());
                                h.append("</a></td></tr>");
                            } else {
                                h.append(attrib.getStringValue());
                                h.append("</td></tr>");
                            }
                        }
                    }
                }
                if (titleDone) {
                    h.append("</table>");
                }
                LDAPCon.disconnect();
                // now search USERS for a matching LID 
                out.println(h);
            }catch (LDAPException ldapex) {
                System.out.println("OutLIDDetails,doPost:" + ldapex);
            }
        }    // end sync
    }

    /**
     * Get a resource of the specified type.
     * @param type the type of service (e.g. SMTP, LDAP) that we want
     * @return a Resource object of the requested type if possible
     */
    private Resource getResource(String type) {
        Resource resource;
        Connection con;
        PreparedStatement getResource;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getResource = con.prepareStatement(sqlGetResourceOfType);
            getResource.setString(1, type);
            getResource.setInt(2, 0);
            result = getResource.executeQuery();
            if (result.next()) {
                resource = new Resource(
                    result.getString("service"), 
                    result.getString("host"), 
                    result.getInt("port"), 
                    result.getString("user"), 
                    result.getString("pw"), 
                    result.getString("info1"));
            } else {
                resource = null;
            }
            con.close();
        } catch (SQLException sqlex) {
            // can't use log here as this method is static
            System.out.print(itemDateTime.format(new Date()));
            System.err.println(" OutLIDSummary, getResource:" + sqlex);
            resource = null;
        }
        con = null;
        return resource;
    }
}

