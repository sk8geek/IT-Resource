/* 
 * @(#) Directory.java    0.2 2006/10/06
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Directory servlet.
 * Handles manual directory operations (user table).
 *
 * @version       0.2    6 October 2006
 * @author        Steven Lilley
 */
public class Directory extends HttpServlet { 

    private final String sqlBorgUser = "UPDATE users SET admin = ? WHERE dn = ? LIMIT 1";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:Directory, init: Unable to get logger from context.");
        } else {
            log.finer("Directory servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("Directory", "init", nex);
        }
    }

    /** 
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "directory");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("Directory", "Directory", "index", HouseKeeper.secureDelay));
            if (rqst.getParameter("dn") != null) {
                handler = context.getNamedDispatcher("OutLIDDetails");
                log.finest("Distinguished Name=" + rqst.getParameter("dn"));
            } else {
                handler = context.getNamedDispatcher("OutLIDGrid");
                log.finest("No Distinguished Name found.");
            }
            handler.include(rqst, resp);
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "directory");
            RequestDispatcher handler;
            String action = "";
            String editAction = "";
            if (rqst.getParameter("editAction") != null) {
                editAction = rqst.getParameter("editAction");        
            }
            if (editAction.equalsIgnoreCase("borg")) {
                borgUser(rqst, resp);
            } else 
            if (editAction.equalsIgnoreCase("update")) {
                // updateUser(rqst, resp);
            } else
            if (editAction.equalsIgnoreCase("expire")) {
                // userCheckout(rqst, resp);
            } else {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("Directory", "Request Error", "index", HouseKeeper.errorDelay));
            out.println("<p>Invalid request.</p>");
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
            }
        } // end sync
    }
    
    /**
     * Set the users admin to the current admin
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void borgUser(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        Connection con;
        PreparedStatement borgUser;
        synchronized (this) {
            int modified = 0;
            String failureReason = "";
            String dn = "";
            String admin = "";
            if (rqst.getParameter("dn") != null) {
                dn = rqst.getParameter("dn");
            }
            if (rqst.getParameter("admin") != null) {
                admin = rqst.getParameter("admin");
            }
            try {
                con = dataSrc.getConnection();
                borgUser = con.prepareStatement(sqlBorgUser);
                borgUser.setString(1, admin);
                borgUser.setString(2, dn);
                if (borgUser.executeUpdate() == 1) {
                    handler = rqst.getRequestDispatcher("index");
                    handler.forward(rqst, resp);
                } else {
                    failureReason = "Unable to borg user.";
                }
                con.close();
            } catch (SQLException sqlex) {
                log.throwing("Directory", "borgUser", sqlex);
                failureReason = "[Directory] SQL Exception.";
            }
            if (failureReason.length() > 0) {
                PrintWriter out = resp.getWriter();
                out.println(HouseKeeper.doHead("Directory", 
                    "Borg Error", "index", HouseKeeper.secureDelay));
                out.println("<p>" + failureReason + "</p>");
                out.println("</div>");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
            }
        } //end sync
        con = null;
    }
}

