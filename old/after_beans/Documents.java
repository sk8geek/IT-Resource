/* 
 * @(#) Documents.java    0.5 2006/09/10
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * A servlet to list static content.
 *
 * @version       0.5 10 September 2006
 * @author        Steven Lilley
 */
public class Documents extends HttpServlet { 

    private String staticURL = "";
    private String staticPath = "";
    private String documentPath = "";
    private String itDocumentPath = "";
    private String itMinutesPath = "";
    private ServletContext context;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("Documents, init: Unable to get logger from context.");
        } else {
            log.finer("Documents servlet has context wide logger.");
        }
        staticURL = context.getInitParameter("staticURL");
        staticPath = context.getInitParameter("staticPath");
        documentPath = context.getInitParameter("documents");
        itDocumentPath = context.getInitParameter("itDocuments");
        itMinutesPath = context.getInitParameter("itMinutes");
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        class docFileFilter implements FilenameFilter {
            public boolean accept(File dir, String name) {
                if (name.endsWith(".html") || name.endsWith(".pdf")) {
                    return true;
                }
                return false;
            }
        }
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            File documentDir = new File(staticPath + documentPath);
            File itDocumentDir = new File(staticPath + itDocumentPath);
            File itMinutesDir = new File(staticPath + itMinutesPath);
            String[] fileList = documentDir.list(new docFileFilter());
            String[] itFileList = itDocumentDir.list(new docFileFilter());
            String[] itMinutesFileList = itMinutesDir.list(new docFileFilter());
            DocObject[] documentList = new DocObject[fileList.length];
            DocObject[] itDocumentList = new DocObject[itFileList.length];
            DocObject[] itMinutesDocList = new DocObject[itMinutesFileList.length];
            rqst.setAttribute("page", "documents");
            resp.setContentType("text/html");
            StringBuffer h = new StringBuffer();
            int c = 0;
            for (c = 0; c < documentList.length; c++) {
                documentList[c] = new DocObject(fileList[c]);
            }
            for (c = 0; c < itDocumentList.length; c++) {
                itDocumentList[c] = new DocObject(itFileList[c]);
            }
            Arrays.sort(itDocumentList);
            for (c = 0; c < itMinutesDocList.length; c++) {
                itMinutesDocList[c] = new DocObject(itMinutesFileList[c]);
            }
            Arrays.sort(itMinutesDocList);
            out.println(HouseKeeper.doHead("Documents", 
                "Documents", "documents", HouseKeeper.normalDelay));
            h.append("<h3>General</h3><p>");
            for (c = (documentList.length-1); c >= 0 ; c--) {
                h.append("<a href=\"" + staticURL + documentPath);
                h.append(documentList[c].fileName + "\">");
                h.append(documentList[c].docTitle + "</a>");
                // identify file type 
//                if ((documentList[c].fileName).endsWith(".html")) {
//                    h.append(" <span class=\"htmlfile\">html</span>");
//                }
                if ((documentList[c].fileName).endsWith(".pdf")) {
                    h.append(" <span class=\"pdffile\">pdf</span>");
                }
                if (c > 0) {
                    h.append("<br />\n");
                }
            }
            h.append("</p>\n");
            // Technical documents
            h.append("<h3>IT/Technical</h3><p>");
            for (c = (itDocumentList.length-1); c >= 0 ; c--) {
                h.append("<a href=\"" + staticURL + itDocumentPath);
                h.append(itDocumentList[c].fileName + "\">");
                h.append(itDocumentList[c].docTitle + "</a> ");
                h.append("<span class=\"date\"> ");
                h.append(HouseKeeper.itemDate.format(
                    itDocumentList[c].docModified));
                h.append("</span>");
                if (c > 0) {
                    h.append("<br />\n");
                }
            }
            h.append("</p>\n");
            
            // CITSTEG dropdown 
            h.append("<h3><abbr title=\"Community Services IT Steering ");
            h.append("Group\">CITSTEG</abbr></h3>");
            h.append("<p>Our IT Steering Group meets every six weeks to ");
            h.append("coordinate IT issues.<br />");
            h.append("<form method=\"get\" name=\"itminutes\" ");
            h.append("onSubmit=\"return getMinutes();\">");
            h.append("Minutes: <select name=\"filename\">");
            for (c = (itMinutesDocList.length-1); c >= 0 ; c--) {
                h.append("<option value=\"");
                h.append(staticURL + itMinutesPath);
                h.append(itMinutesDocList[c].fileName);
                h.append("\">");
                h.append(HouseKeeper.itemDate.format(
                    itMinutesDocList[c].docModified));
                h.append("</option>\n");
            }
            h.append("</select> \n");
            h.append("<input type=\"submit\" value=\"Go\" />\n</form></p>\n");
            h.append("</div>\n");
            out.println(h);
            RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
}

