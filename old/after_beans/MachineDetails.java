/* 
 * @(#) MachineDetails.java	0.2 2006/08/27
 * 
 * Copyright (C) 2003-2008 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Show machine details.
 *
 * @version       0.2 27 August 2006
 * @author        Steven Lilley
 */
public class MachineDetails extends HttpServlet { 

    private ServletContext context;
    private Logger log;
    
    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:MachineDetails, init: Unable to get logger from context.");
        } else {
            log.finer("MachineDetails servlet has context wide logger.");
        }
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        resp.setContentType("text/html");
        rqst.setAttribute("page", "inventory");
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            String b = (String)(rqst.getParameter("badgeno"));
            out.println(HouseKeeper.doHead("Inventory", "Machine Details", 
                "machinedetails", HouseKeeper.normalDelay));
            try {
                handler = context.getNamedDispatcher("OutMachineDetails");
                rqst.setAttribute("find", b);
                handler.include(rqst, resp);
            } catch (NumberFormatException nfex) {
                out.println("<p>Invalid badge number.</p>");
            }
            out.println("</div>\n");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
}

