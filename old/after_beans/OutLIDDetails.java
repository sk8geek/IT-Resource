/* 
 * @(#) OutLIDDetails.java    0.4 2006/10/04
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import java.text.DateFormat;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
/**
 * Gets details of a person from an LDAP source and displays them in a table.
 *
 * @version       0.4 4 October 2006
 * @author        Steven Lilley
 */
public class OutLIDDetails extends HttpServlet {

    /** 
     * Query to get a resource of the specified type.
     * The table can hold multiple instances of each service.  
     * Each instance of a service should have a unique <tt>property</tt> 
     * value.  This allows alternative service providers to be selected.
     */
    private final String sqlGetResourceOfType = 
        "SELECT * FROM resources WHERE service = ? AND priority > ? "
        + "ORDER BY priority LIMIT 1";
    private final DateFormat itemDateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    private StringBuffer filter = new StringBuffer();
    private String LDAPBaseDomain;
    private Resource LDAPServer;
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutLIDDetails, init: Unable to get logger from context.");
        } else {
            log.finer("OutLIDDetails servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutLIDDetails", "init", nex);
        }
        // LDAP settings
        LDAPServer = getResource("LDAP");
        LDAPBaseDomain = LDAPServer.getMoreInfo();
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        LDAPSearchResults searchResults;
        LDAPAttributeSet attribSet;
        LDAPAttribute attrib;
        LDAPEntry personEntry;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            // get a connection to the LDAP server
            LDAPConnection LDAPCon = new LDAPConnection();
            String find = "";
            String dn = "";
            StringBuffer filter = new StringBuffer();
            StringBuffer h = new StringBuffer();
            try {
                LDAPCon.connect(LDAPServer.getHostName(), LDAPServer.getPortNumber());
                LDAPCon.bind(LDAPConnection.LDAP_V3, "", "");
                if (rqst.getParameter("findtext") != null) {
                    find = (rqst.getParameter("findtext"));
                }
                if (rqst.getParameter("dn") != null) {
                    find = (rqst.getParameter("dn"));
                }
                filter.append("(|(LID=");
                filter.append(find);
                filter.append(")(cn=");
                filter.append(find);
                filter.append("))");
                searchResults = LDAPCon.search(LDAPBaseDomain, LDAPConnection.SCOPE_SUB, 
                    filter.toString(), HouseKeeper.LDAPFields, false);
                boolean titleDone = false;
                while (searchResults.hasMore()) {
                    personEntry = searchResults.next();
                    dn = personEntry.getDN();
                    if (titleDone == false) {
                        h.append("<h3>People</h3>");
                        h.append("<table class=\"boxed\">");
                        h.append("<tr><th colspan=\"2\">Details</th></tr>");
                        titleDone = true;
                    }
                    attribSet = personEntry.getAttributeSet();
                    for (int c = 0; c < HouseKeeper.LDAPFields.length; c++) {
                        attrib = attribSet.getAttribute(HouseKeeper.LDAPFields[c]);
                        if (attrib != null) {
                            h.append("<tr><td class=\"grey\">");
                            h.append(HouseKeeper.LDAPFieldNames[c] + ":</td><td>");
                            if (HouseKeeper.LDAPFieldNames[c].equalsIgnoreCase("mail")) {
                                h.append("<a href=\"mailto:");
                                h.append(attrib.getStringValue().toLowerCase());
                                h.append("\">");
                                h.append(attrib.getStringValue());
                                h.append("</a></td></tr>");
                            } else {
                                h.append(attrib.getStringValue());
                                h.append("</td></tr>");
                            }
                        }
                    }
                }
                if (titleDone) {
                    h.append("</table>");
                }
                LDAPCon.disconnect();
            } catch (LDAPException ldapex) {
                context.log("OutLIDDetails, doPost:", ldapex);
                h.append("<p class=\"resultinfo\">A directory error occured.</p>");
            }
            out.println(h);
        } //end sync
    }
    
    /** 
     * Get a resource of the specified type.
     * @param type the type of service (e.g. SMTP, LDAP) that we want
     * @return a Resource object of the requested type if possible
     */
    private Resource getResource(String type) {
        Resource resource;
        Connection con;
        PreparedStatement getResource;
        ResultSet result;
        try {
            con = dataSrc.getConnection();
            getResource = con.prepareStatement(sqlGetResourceOfType);
            getResource.setString(1, type);
            getResource.setInt(2, 0);
            result = getResource.executeQuery();
            if (result.next()) {
                resource = new Resource(
                    result.getString("service"), 
                    result.getString("host"), 
                    result.getInt("port"), 
                    result.getString("user"), 
                    result.getString("pw"), 
                    result.getString("info1"));
            } else {
                resource = null;
            }
            con.close();
        } catch (SQLException sqlex) {
            // can't use log here as this method is static
            System.out.print(itemDateTime.format(new Date()));
            System.err.println(" OutPersonDetails, getResource:" + sqlex);
            resource = null;
        }
        con = null;
        return resource;
    }
}

