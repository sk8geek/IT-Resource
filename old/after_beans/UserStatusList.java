/* 
 * @(#) UserStatusList.java    0.4 2006/09/24
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Lists the current status (unknown/seen/in/out) of workers.
 * This class no longer handles POST requests.  All user operations are 
 * now handled by the UserOps class.  User registration and preferences 
 * changes are handled by the UserAdmin class.
 *
 * @version       0.4 24 September 2006
 * @author        Steven Lilley
 */
public class UserStatusList extends HttpServlet { 

    private final DateFormat itemDateTime = 
        DateFormat.getDateTimeInstance(DateFormat.MEDIUM, 
        DateFormat.MEDIUM);
    private final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    private final String sql_getUserStatus = "SELECT * from ereg WHERE uname = ?";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    private long responseTime;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:UserStatusList, init: Unable to get logger from context.");
        } else {
            log.finer("UserStatusList servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("UserStatusList", "init", nex);
        }
    }

    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doGet(rqst, resp);
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","userlist");
            PrintWriter out = resp.getWriter();
            RequestDispatcher handler;
            StringBuffer h = new StringBuffer(HouseKeeper.doHead(
                "e-Register", "e-Register", "userlist", 
                HouseKeeper.normalDelay));
            String siteCookieValue = "";
            String teamCookieValue = "";
            String workerCookieValue = "";
            ArrayList sites = (ArrayList)context.getAttribute("sites");
            int i = 0;
            // look for site and team cookies
            Cookie[] cks = rqst.getCookies();
            if (cks != null) {
                for (i = 0; i < cks.length; i++) {
                    if ((cks[i].getName()).equalsIgnoreCase("eregsite")) {
                        siteCookieValue = cks[i].getValue();
                    }
                    if ((cks[i].getName()).equalsIgnoreCase("eregteam")) {
                        teamCookieValue = cks[i].getValue();
                    }
                    if ((cks[i].getName()).equalsIgnoreCase("eregid")) {
                        workerCookieValue = cks[i].getValue();
                    }
                }
            }
            if (siteCookieValue.length() > 0) {
                rqst.setAttribute("office", siteCookieValue);
            }
            if (teamCookieValue.length() > 0) {
                rqst.setAttribute("team", teamCookieValue);
            }
            if (workerCookieValue.length() > 0) {
                rqst.setAttribute("eregid", teamCookieValue);
            }
            // generate page
            h.append("<h3>Who's in?</h3>");
            out.println(h);
            h.setLength(0);
            handler = context.getNamedDispatcher("OutUserList");
            handler.include(rqst, resp);
            if (sites != null) {
                h.append("<form method=\"get\" ");
                h.append("action=\"userlist\">");
                h.append("See users at: <select name=\"diffoffice\" ");
                h.append("onChange=\"submit();\">");
                for (i = 0; i < sites.size(); i++) {
                    DropDownItem d = (DropDownItem)sites.get(i);
                    h.append("<option value=\"" + d.code + "\"");
                    if (siteCookieValue.length() > 0) {
                        if (d.code.equalsIgnoreCase(siteCookieValue) ) {
                            h.append(" selected");
                        }
                    }
                    h.append(">" + d.desc + "</option>\n");
                }
                h.append("<option value=\"\">All Offices</option>\n");
                h.append("</select>");
                h.append("<input type=\"submit\" value=\"Change\">");
                h.append("</form>");
            } else {
                System.out.print(
                    HouseKeeper.itemDateTime.format(new Date()) + " ");
                System.out.println("available attributes are:");
                for(Enumeration e = context.getAttributeNames(); 
                    e.hasMoreElements(); ) {
                    System.out.println("\t" + e.nextElement());
                }
            }
            h.append("<p class=\"date\">page generated at ");
            h.append(itemTime.format(new Date()));
            h.append("</p>");
            // no registered user cookie? offer registration
            if (workerCookieValue.length() == 0) {
                h.append("<p><a href=\"useradmin\">");
                h.append("Register on e-register</a>.</p>");
            }
            h.append("</div>\n");
            out.println(h);
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } //end sync
    }
}

