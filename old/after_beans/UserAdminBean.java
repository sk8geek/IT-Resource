/* 
 * @(#) UserAdminBean.java    0.1 2006/10/15
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * A bean to provide the status of monitored systems.
 *
 * @version     0.1    15 October 2006
 * @author      Steven Lilley
 */
public class UserAdminBean implements Serializable {
    
    private final String sqlInsertNewUser = 
        "INSERT INTO register (uname, pw, given, surname, email, team, site) "
        + "VALUES (?, PASSWORD(?), ?, ?, ?, ?, ?)";
    /** SQL query to get user data. */
    private final String sqlGetUserData = 
        "SELECT given, surname, email, team, site FROM register WHERE uname=?";
    /** SQL query to update all of a user's data. */
    private final String sqlUpdatePrefsIncludingPassword = 
        "UPDATE register SET given=?, surname=?, email=?, team=?, site=?, "
        + "pw=PASSWORD(?) WHERE uname=? LIMIT 1";
    /** SQl query to update user data, exluding the password. */
    private final String sqlUpdatePrefsExcludingPassword = 
        "UPDATE register SET given=?, surname=?, email=?, team=?, site=? "
        + "WHERE uname=? LIMIT 1";
    /** SQL query to check to see if a username exists in the database. */
    private final String sqlCheckForExistingUser = 
        "SELECT count(uname) AS qty FROM register WHERE uname = ?";
    private InitialContext initCnxt;
    private Context cnxt;
    private DataSource dataSrc;
    private String userName = "";
    private String givenName = "";
    private String surname = "";
    private String oldPassword = "";
    private String newPassword = "";
    private String confirmPassword = "";
    private String team = "";
    private String office = "";
    private StringBuffer errorList = new StringBuffer();
    
    public UserAdminBean() {
        try {
            initCnxt = new InitialContext();
            cnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)cnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            System.out.println("UserAdminBean: constructor: " + nex);
        }
    }
    
    public void setUserName(String user) {
        userName = user;
    }

    public void setGivenName(String given) {
        givenName = given;
    }

    public void setLastName(String lastName) {
        surname = lastName;
    }

    public void setCurrentPassword(String existingPassword) {
        oldPassword = existingPassword;
    }

    public void setNewPassword(String password) {
        newPassword = password;
    }

    public void SetConfirmPassword(String confirm) {
        confirmPassword = confirm;
    }

    public void setTeam(String newTeam) {
        team = newTeam;
    }

    public void setOffice(String newOffice) {
        office = newOffice;
    }

}

