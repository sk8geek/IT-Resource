/* 
 * @(#) Inventory.java    0.2 2005/10/15
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Hardware and Software Inventory Servlet.
 * Allows users to query the database to summarise hardware and software by 
 * information.  e.g. List hardware by location or cost centre.
 * Users can then add comments against items to request changes.
 *
 * @version       0.2 15 October 2005
 * @author        Steven Lilley
 */
public class Inventory extends HttpServlet { 
    
    /** The context in which this servlet is running. */
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:Inventory, init: Unable to get logger from context.");
        } else {
            log.finer("Inventory servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("Inventory", "init", nex);
        }
    }

    /** 
     * Presents the user with a query form.
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            RequestDispatcher handler;
            resp.setContentType("text/html");
            rqst.setAttribute("page","inventory");
            PrintWriter out = resp.getWriter();
            boolean hasParameters = false;
            String paramName = new String();
            Enumeration params;
            out.println(HouseKeeper.doHead("Inventory", 
                "Hardware Inventory", "inventory", HouseKeeper.normalDelay));
            for (params = rqst.getParameterNames(); 
                params.hasMoreElements(); ) {
                if (!hasParameters) {
                    hasParameters = true;
                }
                paramName = (String)params.nextElement();
                if (paramName.equalsIgnoreCase("badge")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineDetails");
                    handler.include(rqst, resp);
                } else
                if (paramName.equalsIgnoreCase("sitename")) {
                    handler = context.getNamedDispatcher(
                        "OutMachinesBySite");
                    handler.include(rqst, resp);
                } else
                if (paramName.equalsIgnoreCase("ccentre")) {
                    handler = context.getNamedDispatcher(
                        "OutMachinesByCC");
                    handler.include(rqst, resp);
                }
            }
            if (!hasParameters) {
                out.println("<h3>List by Cost Centre</h3>");
                out.println(queryForm("", false, "", ""));
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called for many different user operations.
     * This class handles user registration and preferences changes.
     * The <code>userop</code> parameter determines which method is called.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "inventory");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            String action = new String();
            if (rqst.getParameter("querytype") != null) {
                action = rqst.getParameter("querytype");        
                out.println(HouseKeeper.doHead("Inventory Results", 
                    "Hardware Inventory", "index", HouseKeeper.normalDelay));
                if (action.equalsIgnoreCase("list")) {
                    handler = context.getNamedDispatcher(
                        "OutMachinesByCC");
                    handler.include(rqst, resp);
                } else 
                if (action.equalsIgnoreCase("detail")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineDetails");
                    handler.include(rqst, resp);
                } else
                if (action.equalsIgnoreCase("summary")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineSummary");
                    handler.include(rqst, resp);
                } else 
                if (action.equalsIgnoreCase("updatehw")) {
                    handler = context.getNamedDispatcher(
                        "UpdateHardware");
                    handler.include(rqst, resp);
                    handler = context.getNamedDispatcher("OutMachinesByCC");
                    handler.include(rqst, resp);
                } else {
                    out.println(queryForm("", false, "", ""));
                }
                out.println("</div>");
            } else {
                out.println(HouseKeeper.doHead("Inventory", 
                    "Hardware Inventory", "index", HouseKeeper.normalDelay));
                out.println(queryForm("", false, "", ""));
                out.println("</div>");
            }
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
    
    /**
     * A method that returns a query form.
     * @param ccentre cost centre
     * @param filterhwtype true if hardware type is filtered
     * @param hwtype the hardware type
     * @param errors list of errors
     * @return an HTML string containing a table and form.
     */
    private String queryForm(String ccentre, boolean filterhwtype, 
        String hwtype, String errors) {
        StringBuffer h = new StringBuffer();
        ArrayList teams = (ArrayList)context.getAttribute("teams");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        ArrayList hwtypes = (ArrayList)context.getAttribute("hwtypes");
        h.append("<form method=\"post\" action=\"inventory\" ");
        h.append("name=\"queryform\">\n");
        h.append("<p>Please enter a service pair (two digits) or ");
        h.append("cost centre (four digits).<br>\n");
        // cost centre
        h.append("Service pair/Cost centre: ");
        h.append("<input type=\"text\" maxlength=\"4\" ");
        h.append("name=\"ccentre\" size=\"4\"");
        if (ccentre.length() > 0) {
            h.append(" value=\"" + ccentre + "\"");
        }
        if (errors.indexOf("ccentre") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></p>\n");
        h.append("<p>Only tick this box if you want to limit your ");
        h.append("results to the selected equipment type.</p>\n");
        // hwtype
        if (hwtype != null) {
            h.append("<p>Hardware Type: ");
            h.append("<input type=\"checkbox\" ");
            h.append("name=\"filterhwbytype\" value=\"filterhwtype\"");
            if (filterhwtype) {
                h.append(" checked");
            }
            h.append("> ");
            h.append("<select name=\"hwtype\">\n");
            for (int c = 0; c < hwtypes.size(); c++) {
                DropDownItem d = (DropDownItem)hwtypes.get(c);
                h.append("<option value=\"" + d.code + "\"");
                if (d.code.equalsIgnoreCase(hwtype)) {
                    h.append(" selected");
                }
                h.append(">" + d.desc + "</option>\n");
            }
            h.append("</select></p>\n");
        }
        // buttons 
        h.append("<p>\n");
        h.append("<input type=\"hidden\" name=\"querytype\" ");
        h.append("value=\"list\">\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("</p>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("inuse") > -1) {
            // UNAME is valid but already in use 
            h.append("<p class=\"nb\">The user name is valid but has ");
            h.append("been taken.  Please try something different.</p>");
        }
        return h.toString();
    }
}
