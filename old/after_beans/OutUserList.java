/* 
 * @(#) OutUserList.java    0.3 2005/11/02
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Displays a table of workers and their current status.
 *
 * @version       0.3 2 November 2005
 * @author        Steven Lilley
 */
public class OutUserList extends HttpServlet {

    private final String sql_listAllWorkers = new String(
        "SELECT * FROM register ORDER BY surname");
    private final String sql_listWorkerStatus = new String(
        "SELECT * FROM register WHERE site=? ORDER BY surname");
    private ServletContext context;
    private String sqlDB;
    private String sqlUser;
    private String sqlPassword;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutUserList, init: Unable to get logger from context.");
        } else {
            log.finer("OutUserList servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutUserList", "init", nex);
        }
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            Connection con;
            PreparedStatement getWorkerList;
            ResultSet results;
            String office = new String();
            int coworkerCount = 0;
            boolean allowCoworkerCheckOut = false;
            boolean thatsMeThatIs = false;
            long now = System.currentTimeMillis();
            Date lastMod;
            String eregid = new String();
            // retrieve cookie if one exists
            Cookie[] cks = rqst.getCookies();
            if (cks != null) {
                for (int i = 0; i < cks.length; i++) {
                    if ((cks[i].getName()).equalsIgnoreCase("eregid")) {
                        eregid = cks[i].getValue();
                    }
                }
            }
            try {
                if (eregid != null) {
                    allowCoworkerCheckOut = true;
                }
                if (rqst.getAttribute("office") != null) {
                    office = rqst.getAttribute("office").toString();
                }
                if (rqst.getParameter("diffoffice") != null) {
                    office = rqst.getParameter("diffoffice");
                }
                con = dataSrc.getConnection();
                if (office.length() > 0) {
                    getWorkerList = con.prepareStatement(sql_listWorkerStatus);
                    getWorkerList.setString(1, office);
                } else {
                    getWorkerList = con.prepareStatement(sql_listAllWorkers);
                }
                results = getWorkerList.executeQuery();
                h.append("<table class=\"boxed\">\n");
                if (allowCoworkerCheckOut) {
                    h.append("<form method=\"post\" ");
                    h.append("action=\"userops\">\n");
                }
                h.append("<tr><th>Person</th><th>Status</th>");
                h.append("<th>Where</th><th>Changed</th></tr>\n");
                while (results.next()) {
                    if ((results.getString("uname"))
                        .equalsIgnoreCase(eregid)) {
                        thatsMeThatIs = true;
                    } else {
                        thatsMeThatIs = false;
                    }
                    h.append("<tr><td>" + results.getString("given") + " ");
                    h.append(results.getString("surname") + "</td>\n");
                    switch(results.getInt("presence")) {
                        case -2 :
                            if (results.getString("returning") != null) {
                                h.append("<td>Back ");
                                h.append(results.getString("returning")
                                    .toLowerCase());
                            } else {
                                h.append("<td>Back later");
                            }
                            break;
                        case -1 :
                            if (results.getString("returning") != null) {
                                h.append("<td>Back ");
                                h.append(results.getString("returning")
                                    .toLowerCase());
                            } else {
                                h.append("<td>Back later");
                            }
                            break;
                        case 0 :
                            h.append("<td>Unknown");
                            break;
                        case 1 :
                            h.append("<td>Seen");
                            break;
                        case 2 :
                            h.append("<td>In");
                    }
                    if (results.getString("whereto") != null) {
                        h.append("</td>\n<td>");
                        h.append(results.getString("whereto"));
                    } else {
                        h.append("</td>\n<td>&nbsp;");
                    }
                    lastMod = new Date(results.getTimestamp("lastmod")
                        .getTime());
                    if (now - lastMod.getTime() > 43200000) {
                        // yesterday
                        h.append("</td>\n<td>");
                        h.append(HouseKeeper.itemDate.format(lastMod));
                    } else {
                        // today
                        h.append("</td>\n<td>");
                        h.append(HouseKeeper.itemTime.format(lastMod));
                    }
                    if (allowCoworkerCheckOut && 
                        !thatsMeThatIs && results.getInt("presence") > -1) {
                        h.append("</td>\n<td class=\"grey\">");
                        h.append("<input type=\"radio\" ");
                        h.append("name=\"coworker\" ");
                        h.append("value=\"" + results.getString("uname"));
                        h.append("\">");
                        coworkerCount ++;
                    } else {
                        h.append("</td>\n<td class=\"grey\">&nbsp;");
                    }
                    h.append("</td></tr>\n");
                }
                if (allowCoworkerCheckOut) {
                    if (coworkerCount>0) {
                        h.append("<tr><td colspan=\"4\">&nbsp;</td>\n");
                        h.append("<td align=\"right\" class=\"grey\">");
                        h.append("<input type=\"submit\" ");
                        h.append("value=\"Check out\"></td></tr>\n");
                    }
                    h.append("<input type=\"hidden\" name=\"userop\" ");
                    h.append("value=\"coworker\" />\n</form>\n");
                }
                h.append("</table>\n");
                results.close();
                con.close();
                if (allowCoworkerCheckOut) {
                    h.append("<p>To check out a co-worker select the ");
                    h.append("dot beside their name and click the ");
                    h.append("&quot;Check out&quot; button.</p>\n");
                }
            } catch (SQLException sqlex) {
                System.out.println("OutUserList, doPost:" + sqlex);
                h.append("OutUserList, doPost:" + sqlex);
            }
            con = null;
            out.println(h);
        } //end sync
    }
}



