/* 
 * @(#) OutPersonSummary.java        0.4 2006/03/14
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Produces a table of matching user records from the MySQL database.
 *
 * @version       0.5 14 March 2006
 * @author        Steven Lilley
 */
public class OutPersonSummary extends HttpServlet {

    private final String sqlUserSearch = 
        "SELECT * FROM users WHERE fullname LIKE ? OR location LIKE ? ORDER BY surname";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutPersonSummary, init: Unable to get logger from context.");
        } else {
            log.finer("OutPersonSummary servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutPersonSummary", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement userSearch;
        ResultSet results;
        synchronized (this) {
            StringBuffer h = new StringBuffer("<!-- Person Summary -->\n");
            String s = "";
            boolean showAdminControls = false;
            String adminName = "";
            String borgMaster = "";
            HttpSession session = rqst.getSession(false);
            if (session != null) {
                if (session.getAttribute("ulevel") != null) {
                    if (((Integer)session.getAttribute("ulevel")).intValue() == 9) {
                        showAdminControls = true;
                        adminName = (String)session.getAttribute("uname");
                    }
                }
            }
            PrintWriter out = resp.getWriter();
            if (rqst.getAttribute("find") != null) {
                s = ("%" + rqst.getAttribute("find") + "%");
            } else {
                s = "badcall";
            }
            int nullCount = 0;
            boolean someNullValues = false;
            boolean titleDone = false;
            try {
                con = dataSrc.getConnection();
                userSearch = con.prepareStatement(sqlUserSearch);
                userSearch.setString(1, s);
                userSearch.setString(2, s);
                results = userSearch.executeQuery();
                while (results.next()) {
                    nullCount = 0;
                    if (titleDone == false) {
                        h.append("<h3>People Summary</h3>");
                        h.append("<table class=\"boxed\"><thead><tr>");
                        h.append("<th>Name</th>");
                        h.append("<th>LID</th>");
                        h.append("<th>Job Title</th>");
                        h.append("<th>Phone</th>");
                        h.append("<th>Location</th>");
                        h.append("<th class=\"controlHeading\">Controls</th>");
                        h.append("</tr></thead><tbody>");
                        titleDone = true;
                    }
                    if (results.getString("admin") != null) {
                        borgMaster = results.getString("admin");
                    } else {
                        borgMaster = "";
                    }
                    h.append("<form method=\"post\" action=\"directory\" ");
                    h.append("name=\"update:" + results.getString("dn") + "\"><tr>\n");
                    h.append("<td><a href=\"find?dn=");
                    h.append(results.getString("dn") + "\">");
                    h.append(results.getString("fullname"));
                    h.append("</a></td>");
                    // LID
                    h.append("<td>");
                    if (results.getString("lid") != null) {
                        h.append(results.getString("lid"));
                    } else {
                        nullCount++;
                        h.append("<input type=\"text\" name=\"lid\" size=\"4\" maxlength=\"4\" value=\"\">");
                    }
                    h.append("</td>");
                    // title
                    h.append("<td>");
                    if (results.getString("title") != null) {
                        h.append(results.getString("title"));
                    } else {
                        nullCount++;
                        h.append("<input type=\"text\" name=\"title\" size=\"20\" maxlength=\"80\" value=\"\">");
                    }
                    h.append("</td>");
                    // phone
                    h.append("<td>");
                    if (results.getString("phone") != null) {
                        h.append(results.getString("phone"));
                    } else {
                        nullCount++;
                        h.append("<input type=\"text\" name=\"phone\" size=\"8\" maxlength=\"20\" value=\"\">");
                    }
                    h.append("</td>");
                    // location
                    h.append("<td>");
                    if (results.getString("location") != null) {
                        h.append("<a href=\"find?location=");
                        h.append(results.getString("location"));
                        h.append("\">");
                        h.append(results.getString("location"));
                        h.append("</a>");
                    } else {
                        nullCount++;
                        h.append("<input type=\"text\" name=\"location\" size=\"20\" maxlength=\"40\" value=\"\">");
                    }
                    h.append("</td>");
                    // controls
                    h.append("<td>");
//                    if (nullCount > 0) {
//                        someNullValues = true;
//                        h.append("<input type=\"radio\" name=\"editAction\" value=\"update\" checked> Update ");
//                    }
//                    h.append("<input type=\"radio\" name=\"editAction\" value=\"expire\"> Expire ");
                    if (showAdminControls) {
                        if (!borgMaster.equalsIgnoreCase(adminName)) {
                            h.append("<input type=\"radio\" name=\"editAction\" value=\"borg\"> Borg ");
                            h.append("<input type=\"hidden\" name=\"admin\" value=\"" + adminName + "\">");
//                    }
                    h.append("<input type=\"submit\" value=\"Submit\">\n");
                    h.append("<input type=\"reset\" value=\"Reset\">\n");
                    h.append("<input type=\"hidden\" name=\"dn\" value=\"");
                    h.append(results.getString("dn") + "\"></td>");
                    h.append("</td>");
                        }
                    }
                    // end of row
                    h.append("</tr></form>");
                }
                if (titleDone) {
                    h.append("</tbody></table>");
                }
//                if (someNullValues) {
//                    h.append("<p>Some rows are missing data.  Please enter values if you know them.</p>");
//                    h.append("<p>(You can only update a row at a time.)</p>");
//                }
                out.println(h);
                con.close();
            }
            catch (SQLException sqlex) {
                System.out.println("OutPersonSummary, doPost:" + sqlex);
            }
            con = null;
        } // end sync
    }
}



