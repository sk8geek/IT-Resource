/* 
 * @(#) UserOps.java    0.4 2006/10/03
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * User operations servlet.
 * Handles user operations such as logging in and out and checking in and 
 * out.  Also handles the checkout of a co-worker.
 * User registation and preferences setting are now handled by the 
 * UserAdmin class.
 *
 * @version       0.4  3 October 2006
 * @author        Steven Lilley
 */
public class UserOps extends HttpServlet { 

    private final String sqlCheckPassword = 
        "SELECT uname, ulevel, site FROM register "
        + "WHERE uname = ? AND pw = password(?)";
    private final String sqlGetUserData = 
        "SELECT given, surname, team, site FROM register WHERE uname = ?";
    private final String sqlSetStatus = 
        "UPDATE register SET presence = ? WHERE uname = ? LIMIT 1";
    /** SQL query to check a user in. */
    private final String sqlUserCheckin = 
        "UPDATE register SET presence = 2, whereto = NULL, returning = NULL "
        + "WHERE uname = ? LIMIT 1";
    /** SQL query to check a user out. */
    private final String sqlUserCheckout = 
        "UPDATE register SET presence = ?, whereto = ?, returning = ? "
        + "WHERE uname = ? LIMIT 1";
    /** Query to check is a given user name exists in the table. */
    private final String sqlCheckForUser = 
        "SELECT email FROM register WHERE uname=?";
    /** Update to reset a user password to a new random value. */
    private final String sqlResetPassword = 
        "UPDATE register SET pw = password(?) WHERE uname=? LIMIT 1";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:UserOps, init: Unable to get logger from context.");
        } else {
            log.finer("UserOps servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("UserOps", "init", nex);
        }
    }

    /** 
     * This should only be called by an unregistered user.
     * Present a registration form. 
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","userops");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Operations", 
                "Login", "index", HouseKeeper.secureDelay));
            out.println(accountLogin("uname", "oldpw"));
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called for many different user operations.
     * The <code>userop</code> parameter determines which method is called.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "register");
            RequestDispatcher handler;
            String action = new String();
            if (rqst.getParameter("userop") != null) {
                action = rqst.getParameter("userop");        
            }
            if (action.equalsIgnoreCase("checkin")) {
                userCheckin(rqst, resp);
            } else 
            if (action.equalsIgnoreCase("checkoutform")) {
                userCheckoutForm(rqst, resp);
            } else
            if (action.equalsIgnoreCase("checkout")) {
                userCheckout(rqst, resp);
            } else
            if (action.equalsIgnoreCase("login")) {
                userLogin(rqst, resp);
            } else
            if (action.equalsIgnoreCase("logout")) {
                userLogout(rqst, resp);
            } else
            if (action.equalsIgnoreCase("coworker")) {
                coworkerCheckout(rqst, resp);
            } else {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Operations", 
                "Request Error", "index", HouseKeeper.errorDelay));
            out.println("<p>Invalid request.</p>");
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
            }
        } // end sync
    }
    
    /**
     * Check the user in to the office.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userCheckin(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        String failureReason = new String();
        // database variables
        Connection con;
        PreparedStatement userCheckin;
        int modified = 0;
        // parameter variables
        String uname = new String();
        if (rqst.getParameter("uname") != null) {
            uname = rqst.getParameter("uname");
        }
        try {
            con = dataSrc.getConnection();
            userCheckin = con.prepareStatement(sqlUserCheckin);
            userCheckin.setString(1, uname);
            if (userCheckin.executeUpdate() == 1) {
                handler = rqst.getRequestDispatcher("index");
                rqst.setAttribute("userop", ""); // reset userop
                handler.forward(rqst, resp);
            } else {
                failureReason = "Unable to check user in.";
            }
        } catch (SQLException sqlex) {
            System.out.print(HouseKeeper.itemDateTime.format(new Date()));
            System.out.println(
                "itres: UserOps, userCheckin:" + sqlex);
            failureReason = "Unable to access database.";
        }
        if (failureReason.length() > 0) {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Operations", 
                "Checkin Error", "index", HouseKeeper.secureDelay));
            out.println("<p>" + failureReason + "</p>");
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Present a form to allow the user to check out.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userCheckoutForm(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        String failureReason = "";
        StringBuffer h = new StringBuffer();
        String uname = "";
        // retrieve cookie if one exists
        Cookie[] cks = rqst.getCookies();
        if (cks != null) {
            for (int i = 0; i < cks.length; i++) {
                if ((cks[i].getName()).equalsIgnoreCase("eregid")) {
                    uname = cks[i].getValue();
                }
            }
        }
        // parameter variables
        if (rqst.getParameter("uname") != null) {
            uname = rqst.getParameter("uname");
        }
        h.append("<p>Please choose when you expect to return. ");
        h.append("Optionally put where you are going.</p>\n");
        h.append("<p>Unless you specify otherwise, you will be checked ");
        h.append("back in over night.</p>\n");
        h.append("<p>\n");
        h.append("<form method=\"post\" ");
        h.append("action=\"userops\">\n");
        if (context.getAttribute("backat") != null) {
            h.append("When will you be back? \n");
            h.append("<select name=\"backat\">\n");
            ArrayList backat = (ArrayList)context.getAttribute("backat");
            for (int i = 0; i < backat.size(); i++) {
                DropDownItem d = (DropDownItem)backat.get(i);
                h.append("<option value=\"" + d.desc + "\">");
                h.append(d.desc + "</option>\n");
            }
            h.append("</select><br>\n");
        } else {
            h.append("<input type=\"hidden\" name=\"backat\" ");
            h.append("value=\"soon\" />\n");
        }
        h.append("<span id=\"smaller\">Prevent automatic check-in ");
        h.append("<input type=\"checkbox\" name=\"noautotidy\"></span><br>\n");
        h.append("Where are you going? \n");
        h.append("<input type=\"text\" name=\"site\" ");
        h.append("maxlength=\"40\" size=\"30\" /><br>\n");
        h.append("<input type=\"submit\" value=\"Save\">\n");
        h.append("<input type=\"hidden\" name=\"userid\" value=\"");
        h.append(uname + "\" />\n");
        h.append("<input type=\"hidden\" name=\"userop\" ");
        h.append("value=\"checkout\" />\n");
        h.append("</form>\n</p>\n");
        PrintWriter out = resp.getWriter();
        out.println(HouseKeeper.doHead("User Operations", 
            "User Checkout", "index", HouseKeeper.secureDelay));
        out.println(h.toString());
        out.println("</div>\n");
        handler = context.getNamedDispatcher("MenuFragment");
        handler.include(rqst, resp);
        out.close();
    }
    
    /**
     * Check the user out of the office.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userCheckout(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        String failureReason = new String();
        // database variables
        Connection con;
        PreparedStatement userCheckout;
        int modified = 0;
        // parameter variables
        String userid = "";
        String site = "";
        String backat = "";
        int checkoutLevel = -1;
        if (rqst.getParameter("userid") != null) {
            userid = rqst.getParameter("userid");
        }
        if (rqst.getParameter("site") != null) {
            site = rqst.getParameter("site");
        }
        if (rqst.getParameter("backat") != null) {
            backat = rqst.getParameter("backat");
        }
        if (rqst.getParameter("noautotidy") != null) {
            checkoutLevel = -2;
        }
        try {
            HttpSession session = rqst.getSession(false);
            con = dataSrc.getConnection();
            userCheckout = con.prepareStatement(sqlUserCheckout);
            userCheckout.clearParameters();
            userCheckout.setInt(1, checkoutLevel);
            userCheckout.setString(2, site);
            userCheckout.setString(3, backat);
            userCheckout.setString(4, userid);
            if (userCheckout.executeUpdate() == 1) {
                handler = rqst.getRequestDispatcher("index");
                rqst.setAttribute("userop", ""); // reset userop
                handler.forward(rqst, resp);
            } else {
                failureReason = "Unable to check user out.";
            }
            con.close();
        } catch (SQLException sqlex) {
            System.out.print(HouseKeeper.itemDateTime.format(new Date()));
            System.out.println(
                "itres: UserOps, userCheckout:" + sqlex);
            failureReason = "Unable to access database.";
        }
        con = null;
        if (failureReason.length() > 0) {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Operations", 
                "Checkout Error", "index", HouseKeeper.secureDelay));
            out.println("<p>" + failureReason + "</p>\n");
            out.println("</div>\n");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }
    
    /**
     * User login.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userLogin(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        HttpSession session;
        Connection con;
        PreparedStatement checkPassword;
        PreparedStatement checkForUser;
        PreparedStatement resetPassword;
        PreparedStatement userCheckin;
        ResultSet results;
        synchronized (this) {
            String failureReason = "";
            int modified = 0;
            String uname = "";
            String oldpw = "";
            String site = "";
            String emailAddress;
            String newPassword;
            boolean forgotPassword = false;
            boolean guestPC = false;
            if (rqst.getParameter("uname") != null) {
                uname = rqst.getParameter("uname");
            }
            if (rqst.getParameter("oldpw") != null) {
                oldpw = rqst.getParameter("oldpw");
            }
            if (rqst.getParameter("guestpc") != null) {
                guestPC = true;
            }
            if (rqst.getParameter("forgotpw") != null) {
                forgotPassword = true;
            }
            try {
                con = dataSrc.getConnection();
                if (uname.length() > 0 && oldpw.length() > 0) {
                    checkPassword = con.prepareStatement(sqlCheckPassword);
                    checkPassword.clearParameters();
                    checkPassword.setString(1, uname);
                    checkPassword.setString(2, oldpw);
                    results = checkPassword.executeQuery();
                    if (results.next()) {
                        // user name and password match found 
                        uname = results.getString("uname");
                        site = results.getString("site");
                        session = rqst.getSession(true);
                        session.setAttribute("uname", uname);
                        session.setAttribute("ulevel", 
                            new Integer(results.getInt("ulevel")));
                            session.setMaxInactiveInterval(580);
                        // attempt user check-in
                        userCheckin = con.prepareStatement(sqlUserCheckin);
                        userCheckin.setString(1, uname);
                        userCheckin.executeUpdate();
                        if (!guestPC) {
                            // set cookie if not on a guest PC
                            Cookie eregCookie = new Cookie("eregid", uname);
                            eregCookie.setMaxAge(5184000);
                            Cookie eregCookie2 = new Cookie("eregsite", site);
                            eregCookie2.setMaxAge(5184000);
                            resp.addCookie(eregCookie);
                            resp.addCookie(eregCookie2);
                            handler = 
                                rqst.getRequestDispatcher("index");
                            // reset userop
                            rqst.setAttribute("userop", "");
                            handler.forward(rqst, resp);
                        }
                    }
                } else 
                if (uname.length() > 0 && forgotPassword) {
                    checkForUser = con.prepareStatement(sqlCheckForUser);
                    checkForUser.clearParameters();
                    checkForUser.setString(1, uname);
                    results = checkForUser.executeQuery();
                    if (results.next()) {
                        emailAddress = results.getString("email");
                        newPassword = generatePasscode();
                        // user exists, reset password
                        resetPassword = con.prepareStatement(sqlResetPassword);
                        resetPassword.clearParameters();
                        resetPassword.setString(1, newPassword);
                        resetPassword.setString(2, uname);
                        if (resetPassword.executeUpdate() == 1) {
                            String message = "The password for user " + uname + " on LS-DB has "
                                + "been reset to " + newPassword;
                            HouseKeeper.sendMail(emailAddress, "New password for LS-DB", message);
                        }
                    }
                }
                // login failed due to user
                PrintWriter out = resp.getWriter();
                out.println(HouseKeeper.doHead("User Operations", 
                    "Login", "index", HouseKeeper.secureDelay));
                out.println(accountLogin(uname, "oldpw"));
                out.println("</div>\n");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
                con.close();
            } catch (SQLException sqlex) {
                System.out.print(HouseKeeper.itemDateTime.format(new Date()));
                System.out.println(
                    "itres: UserOps, userCheckout:" + sqlex);
                failureReason = "Unable to access database.";
            }
            con = null;
            if (failureReason.length() > 0) {
                PrintWriter out = resp.getWriter();
                out.println(HouseKeeper.doHead("User Operations", 
                    "Login Error", "index", HouseKeeper.secureDelay));
                out.println("<p>" + failureReason + "</p>\n");
                out.println("</div>\n");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                out.close();
            }
        } // end sync
    }

    /**
     * User logout.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void userLogout(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        HttpSession session = rqst.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        handler = rqst.getRequestDispatcher("index");
        // reset userop
        rqst.setAttribute("userop", "");
        handler.forward(rqst, resp);
    }
    
    /**
     * Check a co-worker out of the office.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    private void coworkerCheckout(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        String failureReason = new String();
        // database variables
        Connection con;
        PreparedStatement userCheckout;
        int modified = 0;
        // parameter variables
        String coworker = new String();
        if (rqst.getParameter("coworker") != null) {
            coworker = rqst.getParameter("coworker");
        }
        try {
            con = dataSrc.getConnection();
            userCheckout = con.prepareStatement(sqlUserCheckout);
            userCheckout.clearParameters();
            userCheckout.setInt(1, -2);
            userCheckout.setString(2, "Checked out by colleague");
            userCheckout.setString(3, "Later");
            userCheckout.setString(4, coworker);
            if (userCheckout.executeUpdate() == 1) {
                handler = rqst.getRequestDispatcher("userlist");
                rqst.setAttribute("userop", ""); // reset userop
                handler.forward(rqst, resp);
            } else {
                failureReason = "Unable to check user out.";
            }
            con.close();
        } catch (SQLException sqlex) {
            System.out.print(HouseKeeper.itemDateTime.format(new Date()));
            System.out.println(
                "itres: UserOps, userCheckout:" + sqlex);
            failureReason = "Unable to access database.";
        }
        con = null;
        if (failureReason.length() > 0) {
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("User Operations", 
                "Checkout Error", "index", HouseKeeper.secureDelay));
            out.println("<p>" + failureReason + "</p>\n");
            out.println("</div>\n");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }
    
    /** 
     * Returns the HTML user login form.
     * @param uname the user ID
     * @param errors comma separated list of errors on the form
     * @return the HTML form
     */
    private String accountLogin(String uname, String errors) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>Account Login</h3>\n");
        h.append("<p>Please enter your user name and password.</p>\n");
        h.append("<p>If you've forgotten your password then tick the ");
        h.append("&quot;Forgotten Password&quot; box.  A new password ");
        h.append("will be sent to your email address.</p>\n");
        h.append("<form method=\"post\" action=\"userops\" ");
        h.append("name=\"accountlogin\">\n");
        h.append("<table class=\"borderless\">\n");
        // user name  
        h.append("<tr>\n<td>user name:</td>\n");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"16\" ");
        h.append("name=\"uname\" size=\"16\"");
        if (uname.length() > 0) {
            h.append(" value=\"" + uname + "\"");
        }
        if (errors.indexOf("uname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td>\n</tr>\n");
        // old password 
        h.append("<tr>\n<td>password:</td>\n");
        h.append("<td><input type=\"password\" maxlength=\"16\" ");
        h.append("name=\"oldpw\" size=\"16\"");
        if (errors.indexOf("oldpw") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td>\n");
        h.append("<td>forgotten: <input type=\"checkbox\" ");
        h.append("name=\"forgotpw\" value=\"forgotpw\"></td>\n");
        h.append("</tr>\n");
        // guest computer (don't set a cookie)
        h.append("<tr>\n<td colspan=\"3\">");
        h.append("<input type=\"checkbox\" name=\"guestpc\" ");
        h.append("value=\"guestpc\"> tick here if this is NOT your ");
        h.append("normal computer.</td>\n");
        h.append("</tr>\n");
        // buttons 
        h.append("<tr>\n<td colspan=\"3\">");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        // action 
        h.append("<input type=\"hidden\" name=\"userop\" ");
        h.append("value=\"login\">\n");
        // close table 
        h.append("</td>\n</tr>\n</table>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("invpw") > -1) {
            // password was invalid for account 
            h.append("<p class=\"nb\">Invalid password for ");
            h.append("this account.</p>\n");
        }
        return h.toString();
    }
    
    /**
     * Generates a random String, eight characters in length.
     * Vowels are never used in order to prevent the potential creation 
     * of recognisable English words.
     * @return String of eight random characters
     */ 
    private String generatePasscode() {
        StringBuffer code = new StringBuffer();
        for (int loop = 0; loop < 8; loop++) {
            int ch = (int)(98 + Math.random() * 24);
            if (ch == 101 | ch == 105 | ch == 111 | ch== 117) {
                ch++;
            }
            code.append(Character.valueOf((char)ch));
        }
        return code.toString();
    }


}

