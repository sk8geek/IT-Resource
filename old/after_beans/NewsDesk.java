/* 
 * @(#) NewsDesk.java    1.2 2006/03/01
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * A servlet that provides news editing facilities.
 *
 * @version     1.2 2 March 2006
 * @author      Steven Lilley
 */
 public class NewsDesk extends HttpServlet { 

    /** SQL query to save an updated story */
    private final String sql_saveUpdate = 
        "UPDATE itnews SET headline = ?, story = ?, site = ?, poster = ?, "
        + "src = ?, expires = ? WHERE storynum = ? LIMIT 1";
    /** SQL query to save a new story */
    private final String sql_saveNewStory = 
        "INSERT INTO itnews (headline, story, site, poster, src, expires) "
        + "VALUES (?, ?, ?, ?, ?, ?)";
    /** SQL query to increment the user post count by one */
    private final String sql_incPostCount = 
        "UPDATE register SET posts = (posts + 1) WHERE uname = ?";
    /** SQL query to delete a story */
    private final String sql_deleteStory = 
        "DELETE FROM itnews WHERE storynum = ? AND poster = ? LIMIT 1";
    /** SQL query to get a particular story */
    private final String sql_getStory = 
        "SELECT * FROM itnews WHERE storynum = ?";
    /** The servlet context */
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:NewsDesk, init: Unable to get logger from context.");
        } else {
            log.finer("NewsDesk servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("NewsDesk", "init", nex);
        }
    }

    /**
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        resp.setContentType("text/html");
        rqst.setAttribute("page", "newsdesk");
        PrintWriter out = resp.getWriter();
        RequestDispatcher handler;
        out.println(HouseKeeper.doHead("News Desk", "Not Logged In", "index", HouseKeeper.errorDelay));
        out.println(simplePage("Invalid User Name", "You do not seem to be logged in."));
        out.println("</div>\n");
        handler = context.getNamedDispatcher("MenuFragment");
        handler.include(rqst, resp);
        out.close();
    }

    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        RequestDispatcher handler;
        PreparedStatement deleteStory;
        PreparedStatement getStory;
        PreparedStatement saveNewStory;
        PreparedStatement incPostCount;
        PreparedStatement saveUpdate;
        ResultSet result;
        synchronized (this) { 
            resp.setContentType("text/html");
            rqst.setAttribute("page", "newsdesk");
            PrintWriter out = resp.getWriter();
            String uname = "";
            String action = "";
            String editaction = "";
            String headline = "";
            String site = "";
            String sitescope = "";
            String srcHost = "";
            String story = "";
            int ageLimitDays = 7;
            int ageLimitHours = 0;
            int storynum = 0;
            // check that we have a valid session 
            HttpSession session = rqst.getSession(false);
            if (session == null) {
                // no session so kick the user out 
                out.println(HouseKeeper.doHead("News Desk", "Not Logged In", "index", HouseKeeper.errorDelay));
                out.println(simplePage("Session Expired", "You do not seem to be logged in."));
            } else {
                // we have a valid session, check for a user name too
                
                if (session.getAttribute("uname") == null) {
                    // no valid user name, kick the user out 
                    out.println(HouseKeeper.doHead("News Desk", "Not Logged In", "index", HouseKeeper.errorDelay));
                    out.println(simplePage("Invalid User Name", "You do not seem to be logged in."));
                } else {
                    uname = (String)session.getAttribute("uname");
                    if (rqst.getParameter("action") != null) {
                        action = rqst.getParameter("action");
                    } else {
                        if (rqst.getAttribute("action") != null) {
                            action = (String)rqst.getAttribute("action");
                        }
                    }
                    // set site from cookie
                    Cookie[] cks = rqst.getCookies();
                    if (cks != null) {
                        for (int i = 0; i < cks.length; i++) {
                            if ((cks[i].getName()).equalsIgnoreCase(
                                "eregsite")) {
                                site = cks[i].getValue();
                            }
                        }
                    }
                    if (rqst.getParameter("editaction") != null) {
                        editaction = rqst.getParameter("editaction");
                    }
                    if (rqst.getParameter("headline") != null) {
                        headline = rqst.getParameter("headline");
                    }
                    if (rqst.getParameter("sitescope") != null) {
                        sitescope = rqst.getParameter("sitescope");
                        if (sitescope.equalsIgnoreCase("single")) {
                            if (rqst.getParameter("site") != null) {
                                site = rqst.getParameter("site");
                            }
                        } else {
                            site = "all";
                        }
                    } else {
                        if (rqst.getParameter("site") != null) {
                            site = rqst.getParameter("site");
                        }
                    }
                    if (rqst.getParameter("srcHost") != null) {
                        srcHost = rqst.getRemoteHost();
                    }
                    if (rqst.getParameter("story") != null) {
                        story = rqst.getParameter("story");
                    }
                    if (rqst.getParameter("livedays") != null) {
                        ageLimitDays = Integer.parseInt(
                            rqst.getParameter("livedays"));
                    }
                    if (rqst.getParameter("livehours") != null) {
                        ageLimitHours = Integer.parseInt(
                            rqst.getParameter("livehours"));
                    }
                    if (rqst.getParameter("storynum") != null) {
                        storynum = Integer.parseInt(
                            rqst.getParameter("storynum"));
                    }
                    try {
                        con = dataSrc.getConnection();
                        if (action == null) {
                            // ACTION is invalid 
                            action = "void";
                        } else 
                        if (action.equalsIgnoreCase("editnews")) {
                            // ACTION == EDITNEWS 
                            // list own stories and a new story form
                            out.println(HouseKeeper.doHead("News Desk", "News Desk", "index", HouseKeeper.secureDelay));
                            out.println(simplePage("Existing Items", "These are your existing stories/items."));
                            rqst.setAttribute("uname", uname);
                            handler = context.getNamedDispatcher("OutNewsFeed");
                            handler.include(rqst, resp);
                            out.println(storyEditor(0, "", "", site, 7, 0, uname));
                        } else
                        if (action.equalsIgnoreCase("editstory")) {
                            // ACTION = EDITSTORY 
                            // called for both edit existing and delete
                            // existing determine action first 
                            if (editaction.equalsIgnoreCase("delete")) {
                                // DELETE the story
                                deleteStory = con.prepareStatement(sql_deleteStory);
                                deleteStory.clearParameters();
                                deleteStory.setInt(1, storynum);
                                deleteStory.setString(2, uname);
                                if (deleteStory.executeUpdate() == 1) {
                                    out.println(HouseKeeper.doHead("News Desk", "News Desk", "index", HouseKeeper.errorDelay));
                                    out.println(simplePage("Item Deleted", "The item has been deleted"));
                                } else {
                                    out.println(HouseKeeper.doHead("News Desk", "Delete Desk", "index", HouseKeeper.errorDelay));
                                    out.println(simplePage("Delete Failed", "The selected item could NOT be deleted."));
                                }
                            } else {
                                // EDIT the story
                                getStory = con.prepareStatement(sql_getStory);
                                getStory.clearParameters();
                                getStory.setInt(1, storynum);
                                result = getStory.executeQuery();
                                if (result.next()) {
                                    headline = result.getString("headline");
                                    story = result.getString("story");
                                    site = result.getString("site");
                                    if (site == null) {
                                        site = "all";
                                    }
                                    uname = result.getString("poster");
                                    int remainingLife = (int)((result.getTimestamp("expires").getTime() - System.currentTimeMillis()) / 3600000l);
                                    ageLimitDays = remainingLife / 24;
                                    ageLimitHours = remainingLife - (24 * ageLimitDays);
                                    if (ageLimitDays <= 0) { 
                                        ageLimitDays = 0; 
                                    }
                                    if (ageLimitHours <= 0) {
                                        if (ageLimitDays == 0) {
                                            ageLimitHours = 1;
                                        } else { 
                                            ageLimitHours = 0;
                                        }
                                    }
                                }
                                out.println(HouseKeeper.doHead("News Desk", "News Desk", "index", HouseKeeper.secureDelay));
                                out.println(storyEditor(storynum, headline, story, site, ageLimitHours, ageLimitDays, uname));
                            }
                        } else
                        if (action.equalsIgnoreCase("savenew")) {
                            // ACTION = SAVENEW 
                            saveNewStory = con.prepareStatement(sql_saveNewStory);
                            saveNewStory.clearParameters();
                            saveNewStory.setString(1, launderText(headline));
                            saveNewStory.setString(2, launderText(story));
                            if (site.equalsIgnoreCase("all")) {
                                saveNewStory.setNull(3, java.sql.Types.CHAR);
                            } else {
                                saveNewStory.setString(3, site);
                            }
                            saveNewStory.setString(4, uname);
                            saveNewStory.setString(5, srcHost);
                            Calendar tempCal = Calendar.getInstance();
                            tempCal.add(Calendar.DATE, ageLimitDays);
                            tempCal.add(Calendar.HOUR, ageLimitHours);
                            saveNewStory.setTimestamp(6, 
                                new Timestamp(tempCal.getTimeInMillis()));
                            if (saveNewStory.executeUpdate() == 1) {
                                // story saved, increment post counter 
                                incPostCount = con.prepareStatement(sql_incPostCount);
                                incPostCount.clearParameters();
                                incPostCount.setString(1, uname);
                                incPostCount.executeUpdate();
                                out.println(HouseKeeper.doHead("News Desk", 
                "News Desk", "index", HouseKeeper.errorDelay));
                                out.println(simplePage("Item Posted", 
                                    "Your item has been posted."));
                            } else {
                                // story was NOT saved 
                                out.println(HouseKeeper.doHead("News Desk", 
                "News Desk", "index", HouseKeeper.errorDelay));
                                out.println(simplePage("Posting Failed", 
                                    "Your item could NOT be posted."));
                            }
                        } else 
                        if (action.equalsIgnoreCase("saveupdate")) {
                            // ACTION = SAVEUPDATE 
                            saveUpdate = con.prepareStatement(sql_saveUpdate);
                            saveUpdate.clearParameters();
                            saveUpdate.setString(1, launderText(headline));
                            saveUpdate.setString(2, launderText(story));
                            if (site.equalsIgnoreCase("all")) {
                                saveUpdate.setNull(3, java.sql.Types.CHAR);
                            } else {
                                saveUpdate.setString(3, site);
                            }
                            saveUpdate.setString(4, uname);
                            saveUpdate.setString(5, srcHost);
                            Calendar tempCal = Calendar.getInstance();
                            tempCal.add(Calendar.DATE, ageLimitDays);
                            tempCal.add(Calendar.HOUR, ageLimitHours);
                            saveUpdate.setTimestamp(6, 
                                new Timestamp(tempCal.getTimeInMillis()));
                            saveUpdate.setInt(7, storynum);
                            if (saveUpdate.executeUpdate() == 1) {
                                // story saved 
                                out.println(HouseKeeper.doHead("News Desk", 
                "News Desk", "index", HouseKeeper.errorDelay));
                                out.println(simplePage("Item Posted", 
                                    "Your item has been posted."));
                            } else {
                                // story was NOT saved 
                                out.println(HouseKeeper.doHead("News Desk", 
                "News Desk", "index", HouseKeeper.errorDelay));
                                out.println(simplePage("Post Failed", 
                                    "Your story/item was NOT posted."));
                            }
                        } else {
                            // catch all 
                            out.println(HouseKeeper.doHead("News Desk", 
                "News Desk", "index", HouseKeeper.errorDelay));
                            out.println("<p>Invalid request.</p>\n");
                        }
                        con.close();
                    } catch (SQLException sqlex) {
                        System.out.println("NewsDesk, doPost:" + sqlex);
                        out.println(sqlex);
                    }
                    con = null;
                    // close the page 
                    out.println("</div>\n");
                    handler = context.getNamedDispatcher("MenuFragment");
                    handler.include(rqst, resp);
                    out.close();
                }
            }
        }
    }

    /** 
     * Remove unwanted HTML tags from a string. 
     * Square bracket tags are also converted to HTML tags.
     * Allowed tags are: b, i, em, strong, a
     * @param dirtyText the string to be cleaned
     * @return the laundered text
     */
    private String launderText(String dirtyText) {
        String[] allowedTags = { "b", "i", "em", "strong", "a" };
        String openingTag = "";
        String closingTag = "";
        StringBuffer laundry = new StringBuffer(dirtyText);
        int pointer = 0;
        int i = 0;
        // remove HTML tags
        while (laundry.indexOf("<") > -1) {
            if (laundry.indexOf(">", laundry.indexOf("<")) > -1) {
                laundry.delete(laundry.indexOf("<"), laundry.indexOf(">") + 1);
            } else {
                laundry.deleteCharAt(laundry.indexOf("<"));
            }
        }
        for (i = 0; i < allowedTags.length; i++) {
            pointer = 0;
            openingTag = "[" + allowedTags[i];
            closingTag = "[/" + allowedTags[i] + "]";
            while ((pointer = laundry.indexOf(openingTag)) > -1) {
                laundry.replace(pointer, pointer + 1, "<");
                pointer = laundry.indexOf("]", pointer);
                laundry.replace(pointer, pointer + 1, ">");
                pointer = laundry.indexOf(closingTag, pointer);
                laundry.replace(pointer, (pointer + closingTag.length()), ("</" + allowedTags[i] + ">"));
            }
        }
        return laundry.toString();
    }

    /** 
     * Returns a (very) simple page.
     * @param heading The page heading to display.
     * @param message The message to display.
     */
    private String simplePage(String heading, String message) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>" + heading + "</h3>");
        h.append("<p>" + message + "</p>");
        return h.toString();
    }

    /** 
     * Presents a form for editing/posting stories 
     * @param storynum story number or 0 for a new story
     * @param headline existing headline, if any
     * @param story existing story, if any
     * @param site site scope, if any
     * @param ageLimitHours story lifetime, hours coponent
     * @param ageLimitDays story lifetime, days component
     * @param uname current user ID
     */
    private String storyEditor(int storynum, String headline, String story, 
        String site, int ageLimitHours, int ageLimitDays, String uname) {
        StringBuffer h = new StringBuffer();
        if (headline.length() == 0) {
            h.append("<h3>New Story</h3>\n");
        } else {
            h.append("<h3>Edit Story</h3>");
        }
        h.append("<p>Use this form to post a new/edited item.</p>");
        h.append("<form method=\"post\" action=\"/itres/newsdesk\" ");
        h.append("name=\"storyeditor\" ");
        h.append("onSubmit=\"return validateNews();\">\n");
        h.append("<table class=\"borderless\">\n");
        // headline/story title 
        h.append("<tr><td>headline/title:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"120\" ");
        h.append("name=\"headline\" size=\"60\"");
        if (headline.length() > 0) {
            h.append(" value=\"" + headline + "\"");
        }
//        if (errors.indexOf("headline") > -1) {
//            h.append(" class=\"reqd\"");
//        }
        h.append("></td></tr>\n");
        // story  
        h.append("<tr><td valign=\"top\">story:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<textarea cols=\"60\" rows=\"4\" ");
        h.append("name=\"story\" wrap=\"physical\">");
        if (uname.length() > 0) {
            h.append(story);
        }
        h.append("</textarea></td>");
        h.append("</tr>\n");
        // site
        h.append("<tr><td>office:</td><td colspan=\"2\">");
        h.append("<input type=\"radio\"");
        if (!site.equalsIgnoreCase("all")) {
            h.append(" checked");
        }
        h.append(" name=\"sitescope\" value=\"single\"> ");
        h.append("<select name=\"site\">");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        for (int c = 0; c < sites.size(); c++) {
            DropDownItem d = (DropDownItem)sites.get(c);
            h.append("<option value=\"" + d.code + "\"");
            if ( d.code.equalsIgnoreCase(site) ) {
                h.append(" selected");
            }
            h.append(">" + d.desc + "</option>");
        }
        h.append("</select> ");
        h.append("<input type=\"radio\"");
        if (site.equalsIgnoreCase("all")) {
            h.append(" checked");
        }
        h.append(" name=\"sitescope\" value=\"all\"> ");
        h.append("all offices</td></tr>");
        // age limit in hours and days 
        h.append("<tr><td>display period:</td>");
        h.append("<td>");
        h.append("<select name=\"livehours\">");
        for (int c = 0; c < 29; c++ ) {
            h.append("<option value=\"" + c + "\" ");
            if (c == ageLimitHours) {
                h.append("selected ");
            }
            h.append("/>");
            h.append(c + "</option>");
        }
        h.append("</select> hours</td>");
        h.append("<td>");
        h.append("<select name=\"livedays\">");
        for (int c = 0; c < 23; c++ ) {
            h.append("<option value=\"" + c + "\" ");
            if (c == ageLimitDays) {
                h.append("selected ");
            }
            h.append("/>");
            h.append(c + "</option>");
        }
        h.append("</select> days</td></tr>");
        // buttons 
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"submit\" value=\"Post\">");
        h.append("<input type=\"reset\" value=\"Reset\">");
        // action 
        h.append("<input type=\"hidden\" name=\"action\" ");
        if (storynum > 0) {
            h.append("value=\"saveupdate\">");
            h.append("<input type=\"hidden\" name=\"storynum\" value=\"");
            h.append(storynum + "\">");
        } else {
            h.append("value=\"savenew\">");
        }
        // close table 
        h.append("</td></tr></table>\n");
        h.append("</form>\n");
        return h.toString();
    }

}


