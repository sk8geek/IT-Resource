/* 
 * @(#) WebFaults.java    0.1 2006/06/29
 * 
 * Copyright (C) 2006 Calderdale MBC
 * parts Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Simple form to allow capture of web access issues.
 *
 * @version       0.1 29 June 2006
 * @author        Steven Lilley
 */
public class WebFaults extends HttpServlet { 

    private final String sqlRecordFault = "INSERT INTO webfaultlog VALUES (?, ?, ?, ?)";
    private ServletContext context;
    private String sqlDB;
    private String sqlUser;
    private String sqlPassword;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() throws ServletException {
        context = getServletContext();
        sqlDB = context.getInitParameter("sqlDB");
        sqlUser = context.getInitParameter("sqlUser");
        sqlPassword = context.getInitParameter("sqlPassword");
    }

    /** 
     * Present a fault report form. 
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","webfault");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            out.println(HouseKeeper.doHead("Web Fault", "Web Faults", "index", HouseKeeper.normalDelay));
            out.println("<h3>Record a Fault</h3>");
            out.println(getFaultReportForm(rqst.getRemoteHost()));
            out.println("<h3>Fault Statistics</h3>");
            handler = context.getNamedDispatcher("OutWebFaultStats");
            handler.include(rqst, resp);
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called to store the fault report.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement recordFault;
        String host = "";
        String fault = "";
        String otherFault = "";
        RequestDispatcher handler;
        synchronized (this) {
            if (rqst.getParameter("host") != null) {
                host = rqst.getParameter("host");        
            }
            if (rqst.getParameter("fault") != null) {
                fault = rqst.getParameter("fault");        
            }
            if (rqst.getParameter("other") != null) {
                otherFault = rqst.getParameter("other");        
            }
            try {
                con = DriverManager.getConnection(sqlDB, sqlUser, sqlPassword);
                recordFault = con.prepareStatement(sqlRecordFault);
                recordFault.clearParameters();
                recordFault.setString(1, host);
                recordFault.setString(2, fault);
                recordFault.setString(3, otherFault);
                recordFault.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                if (recordFault.executeUpdate() != 1) {
                    System.out.println("WebFaults, doPost: insert count mismatch.");
                }
                con.close();
                handler = rqst.getRequestDispatcher("index");
                handler.forward(rqst, resp);
            } catch (SQLException sqlex) {
                System.out.print(HouseKeeper.itemDateTime.format(new Date()));
                System.out.println("itres: WebFaults, doPost: " + sqlex);
            }
        } // end sync
    }
    
    /**
     * Present a fault report form.
     * @param host the remote host name
     * @return the HTML form
     */
    private String getFaultReportForm(String host) {
        StringBuffer h = new StringBuffer();
        h.append("<p>Please select the fault type or enter a description if ");
        h.append("the fault type doesn't already exist.");
        h.append("<table cellspacing=\"0\" cellpadding=\"4\">\n");
        h.append("<form method=\"post\" ");
        h.append("action=\"webfault\">\n");
        if (context.getAttribute("webfaults") != null) {
            h.append("<tr><td>Fault Type:</td>\n");
            h.append("<td><select name=\"fault\">\n");
            ArrayList fault = (ArrayList)context.getAttribute("webfaults");
            for (int i = 0; i < fault.size(); i++) {
                DropDownItem d = (DropDownItem)fault.get(i);
                h.append("<option value=\"" + d.code + "\">");
                h.append(d.desc + "</option>\n");
            }
            h.append("</select></td></tr>\n");
        } else {
            h.append("<input type=\"hidden\" name=\"fault\" ");
            h.append("value=\"broken\" />\n");
        }
        h.append("<tr><td valign=\"top\">Other Fault:</td>\n");
        h.append("<td><textarea name=\"other\" cols=\"40\" rows=\"4\" wrap=\"virtual\">");
        h.append("</textarea></td></tr>\n");
        h.append("<tr><td colspan=\"2\" align=\"right\">");
        h.append("<input type=\"submit\" value=\"Save\"></td></tr>\n");
        h.append("<input type=\"hidden\" name=\"host\" value=\"");
        h.append(host + "\">\n");
        h.append("</form>\n</table>\n");
        return h.toString();
    }
}

