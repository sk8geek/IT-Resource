/* 
 * @(#) OutNewsFeed.java    1.3 2006/09/10
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * A named dispatcher that displays stories from 
 * the <code>itnews</code> table.
 *
 * @version       1.3 10 September 2006
 * @author        Steven Lilley
 */
public class OutNewsFeed extends HttpServlet {

    private final String sqlGetSiteNews = "SELECT * FROM itnews WHERE "
        + "site IS NULL OR site=? ORDER BY issued DESC";
    private final String sqlGetNonSiteSpecificNews = "SELECT * FROM itnews "
        + "WHERE site IS NULL ORDER BY issued DESC";
    private final String sqlGetOwnedNews = "SELECT * FROM itnews WHERE "
        + "poster=? ORDER BY issued DESC";
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutNewsFeed: init: Unable to get logger from context.");
        } else {
            log.finer("OutNewsFeed servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutNewsFeed", "init", nex);
        }
    }

    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }

    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement getNews;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            String site = "";
            long now = System.currentTimeMillis();
            boolean editMode = false;
            try {
                con = dataSrc.getConnection();
                // retrieve site cookie
                Cookie[] cks = rqst.getCookies();
                if (cks != null) {
                    for (int i = 0; i < cks.length; i++) {
                        if ((cks[i].getName()).equalsIgnoreCase(
                            "eregsite")) {
                            site = cks[i].getValue();
                        }
                    }
                }
                if (rqst.getAttribute("uname") == null) {
                    if (site.length() > 0) {
                        getNews = con.prepareStatement(sqlGetSiteNews);
                        getNews.setString(1, site);
                    } else {
                        getNews = con.prepareStatement(sqlGetNonSiteSpecificNews);
                    }
                } else {
                    // poster attribute exists, so get owned news
                    getNews = con.prepareStatement(sqlGetOwnedNews);
                    getNews.setString(1, rqst.getAttribute("uname").toString());
                    editMode = true;
                }
                results = getNews.executeQuery();
                h.append("<dl class=\"news\">");
                while (results.next()) {
                    if (results.getTimestamp("expires").getTime() > now){
                        h.append("<dt class=\"headline\">");
                        h.append(results.getString("headline"));
                        h.append("</dt>");
                        if (!editMode) {
                            long age = System.currentTimeMillis() 
                                - results.getTimestamp("issued").getTime();
                            if (age < 28800000) {
                                h.append("<dd class=\"latest\">");
                            } else {
                                h.append("<dd class=\"news\">");
                            }
                        }
                        h.append(EncodedTextField
                            .getEncodedText(results.getString("story")));
                        if (results.getDate("issued") != null) {
                            h.append("<br><span class=\"date\">");
                            h.append(HouseKeeper.itemDateTime.format(
                                results.getTimestamp("issued")));
                            h.append("</span>");
                        } else {
                            h.append("<br><span class=\"date\">");
                            h.append("&nbsp;</span>");
                        }
                        if (editMode) {
                            h.append("<form method=\"post\" ");
                            h.append("action=\"/itres/newsdesk\">");
                            h.append("<input type=\"hidden\" ");
                            h.append("name=\"storynum\" value=\"");
                            h.append(results.getInt("storynum") + "\">");
                            h.append("<input type=\"radio\" ");
                            h.append("name=\"editaction\" ");
                            h.append("value=\"edit\" checked >Edit ");
                            h.append("<input type=\"radio\" ");
                            h.append("name=\"editaction\" ");
                            h.append("value=\"delete\">Delete ");
                            h.append("<input type=\"hidden\" ");
                            h.append("name=\"action\" value=\"editstory\">");
                            h.append("<input type=\"submit\" ");
                            h.append("value=\"Go\"></form>");
                        }
                    h.append("</dd>");
                    }
                }
                h.append("</dl>");
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                log.throwing("OutNewsFeed", "doPost", sqlex);
                h.append("OutNewsFeed, doPost:" + sqlex);
            }
            con = null;
            out.println(h);
        } // end sync
    }
}

