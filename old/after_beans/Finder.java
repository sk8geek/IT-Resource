/* 
 * @(#) Finder.java        0.6 2005/11/16
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Searches lots of sources trying to match the users search criteria.
 *
 * @version       0.6 16 November 2005
 * @author        Steven Lilley
 */
public class Finder extends HttpServlet { 

    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;

    public void init() {
        context = getServletContext();
    }

    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            RequestDispatcher handler;
            resp.setContentType("text/html");
            rqst.setAttribute("page","finder");
            PrintWriter out = resp.getWriter();
            boolean hasParameters = false;
            String paramName = new String();
            Enumeration params;
            out.println(HouseKeeper.doHead("Find", 
                "Find", "index", HouseKeeper.normalDelay));
            for (params = rqst.getParameterNames(); 
                params.hasMoreElements(); ) {
                if (!hasParameters) {
                    hasParameters = true;
                }
                paramName = (String)params.nextElement();
                if (paramName.equalsIgnoreCase("dn")) {
                    handler = context.getNamedDispatcher("OutPersonDetails");
                    handler.include(rqst, resp);
                } else
                if (paramName.equalsIgnoreCase("location")) {
                    handler = context.getNamedDispatcher("OutMachinesBySite");
                    handler.include(rqst, resp);
                }
            }
            if (!hasParameters) {
                out.println("<h3>Find Stuff</h3>");
                out.println("Still working on this page, please use the ");
                out.println("find box at the bottom of the page. (Add query form)");
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Search various sources for data.
     * The given search string must be at least three characters long.
     * The method first tries to determine what the user is looking for.
     * This then determines where the look.  Known patterns are as follows:
     * <dl><dt>.nnn</dt><dd>last digit in an IP address (base 10)</dd>
     * <dt>nnnn[n]</dt><dd>hardware badge number/dd>
     * <dt>aaaa</dt><dd>person by LID</dd>
     * <dt>aaa*[a]</dt><dd>people or machines by user</dd>
     * <dt>%ou=%</dt><dd>people in the given organisational unit</dd>
     * <dt>cn=%</dt><dd>person with the given common name</dd></dl>
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            RequestDispatcher handler;
            rqst.setAttribute("page", "finder");
            resp.setContentType("text/html");
            out.println(HouseKeeper.doHead("Finder", 
                "Find Anything", "index", HouseKeeper.normalDelay));
            String find = (String)rqst.getParameter("findtext");
            if (find.length() >= 3) {
                // Test for an IP address
                if (find.charAt(0) == '.' && find.length() < 5) {
                    try {
                        int ipa = Integer.parseInt(find.substring(1));
                        if (ipa >= 0 && ipa <= 255) {
                            handler = context.getNamedDispatcher("OutMachineSummary");
                            rqst.setAttribute("find", find);
                            rqst.setAttribute("findby", "ipa");
                            handler.include(rqst, resp);
                        }
                    } catch (NumberFormatException nfex) {
                        // not an IP address, this search can be ignored.
                    }
                }
                // Test for badge number 
                try {
                    int x = Integer.parseInt(find);
                    handler = context.getNamedDispatcher("OutMachineDetails");
                    rqst.setAttribute("badge", x);
                    handler.include(rqst, resp);
                } catch (NumberFormatException nfex) {
                    // Not a badge number, this search can be ignored. 
                }
                // Test for distinguished name
                if (find.indexOf("ou=") == 0) {
                    handler = context.getNamedDispatcher("OutOUSummary");
                    rqst.setAttribute("find", find);
                    handler.include(rqst, resp);
                } else if (find.indexOf("cn=") > -1) {
                    handler = context.getNamedDispatcher("OutPersonDetails");
                    rqst.setAttribute("find", find);
                    handler.include(rqst, resp);
                } else {
                    // We have a string so look for names and LIDs
                    // Machines by location, team or user
                    handler = context.getNamedDispatcher("OutMachineSummary");
                    rqst.setAttribute("find", find);
                    handler.include(rqst, resp);
                    // People by name
                    handler = context.getNamedDispatcher("OutPersonSummary");
                    rqst.setAttribute("find", find);
                    handler.include(rqst, resp);
                    // LIDs (but only if the length is 4 characters)
                    if (Pattern.matches("[a-zA-Z]\\w\\w\\w", find)) {
                        handler = context.getNamedDispatcher("OutLIDDetails");
                        rqst.setAttribute("find", find);
                        handler.include(rqst, resp);
                    }
                }
            } else {
                out.println("<p>The search string entered was too short. ");
                out.println("Please enter at least three characters.</p>");
            }
            out.print("<p class=\"date\">(Find results as at ");
            out.print(HouseKeeper.itemTime.format(new Date()));
            out.println(")</p></div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }

    /**
     * A method that returns a query form.
     * @param ccentre cost centre
     * @param filterhwtype true if hardware type is filtered
     * @param hwtype the hardware type
     * @param errors list of errors
     * @return an HTML string containing a table and form.
     */
    private String queryForm(String ccentre, boolean filterhwtype, 
        String hwtype, String errors) {
        StringBuffer h = new StringBuffer();
        ArrayList teams = (ArrayList)context.getAttribute("teams");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        ArrayList hwtypes = (ArrayList)context.getAttribute("hwtypes");
        h.append("<form method=\"post\" action=\"inventory\" ");
        h.append("name=\"queryform\">\n");
        h.append("<p>Please enter a service pair (two digits) or ");
        h.append("cost centre (four digits).<br>\n");
        // cost centre
        h.append("Service pair/Cost centre: ");
        h.append("<input type=\"text\" maxlength=\"4\" ");
        h.append("name=\"ccentre\" size=\"4\"");
        if (ccentre.length() > 0) {
            h.append(" value=\"" + ccentre + "\"");
        }
        if (errors.indexOf("ccentre") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("</p>\n");
        h.append("<p>Only tick this box if you want to limit your ");
        h.append("results to the selected equipment type.</p>\n");
        // hwtype
        if (hwtype != null) {
            h.append("<p>Hardware Type: ");
            h.append("<input type=\"checkbox\" ");
            h.append("name=\"filterhwbytype\" value=\"filterhwtype\"");
            if (filterhwtype) {
                h.append(" checked");
            }
            h.append("> ");
            h.append("<select name=\"hwtype\">\n");
            for (int c = 0; c < hwtypes.size(); c++) {
                DropDownItem d = (DropDownItem)hwtypes.get(c);
                h.append("<option value=\"" + d.code + "\"");
                if (d.code.equalsIgnoreCase(hwtype)) {
                    h.append(" selected");
                }
                h.append(">" + d.desc + "</option>\n");
            }
            h.append("</select></p>\n");
        }
        // buttons 
        h.append("<p>\n");
        h.append("<input type=\"hidden\" name=\"querytype\" ");
        h.append("value=\"list\">\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("</p>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("inuse") > -1) {
            // UNAME is valid but already in use 
            h.append("<p class=\"nb\">The user name is valid but has ");
            h.append("been taken.  Please try something different.</p>");
        }
        return h.toString();
    }
}

