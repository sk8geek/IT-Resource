/* 
 * @(#) UpdateHardware.java	0.1 2005/10/17
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Output the details of a piece of hardware.
 *
 * @version		0.1 17 Oct 2005
 * @author		Steven Lilley
 */
public class UpdateHardware extends HttpServlet {

    private final String sql_updateHardware = new String(
        "UPDATE hw_badged SET live=?, descrip=?, location=?, "
        + "team=?, user=?, ipa=?, sn=?, ccentre=? WHERE "
        + "badge=? LIMIT 1");
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:SocketTester, init: Unable to get logger from context.");
        } else {
            log.finer("SocketTester servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("SocketTester", "init", nex);
        }
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement updateHardware;
        synchronized (this) {
            HttpSession session = rqst.getSession(false);
            PrintWriter out = resp.getWriter();
            int badge = 0;
            String live = new String();
            String descrip = new String();
            String location = new String();
            String team = new String();
            String user = new String();
            String ipa = new String();
            String sn = new String();
            String ccentre = new String();
            if (rqst.getParameter("live") != null) {
                live = rqst.getParameter("live");
            }
            if (rqst.getParameter("descrip") != null) {
                descrip = rqst.getParameter("descrip");
            }
            if (rqst.getParameter("location") != null) {
                location = rqst.getParameter("location");
            }
            if (rqst.getParameter("team") != null) {
                team = rqst.getParameter("team");
            }
            if (rqst.getParameter("user") != null) {
                user = rqst.getParameter("user");
            }
            if (rqst.getParameter("ipa") != null) {
                ipa = rqst.getParameter("ipa");
            }
            if (rqst.getParameter("sn") != null) {
                sn = rqst.getParameter("sn");
            }
            if (rqst.getParameter("ccentre") != null) {
                ccentre = rqst.getParameter("ccentre");
            }
            try {
                if (rqst.getParameter("badge") != null) {
                    badge = Integer.parseInt(rqst.getParameter("badge"));
                }
                con = dataSrc.getConnection();
                updateHardware = con.prepareStatement(sql_updateHardware);
                updateHardware.clearParameters();
                updateHardware.setString(1, live);
                updateHardware.setString(2, descrip);
                updateHardware.setString(3, location);
                updateHardware.setString(4, team);
                updateHardware.setString(5, user);
                updateHardware.setString(6, ipa);
                updateHardware.setString(7, sn);
                updateHardware.setString(8, ccentre);
                updateHardware.setInt(9, badge);
                if (session != null) {
                    if (session.getAttribute("ulevel") != null) {
                        if (((Integer)session
                            .getAttribute("ulevel")).intValue() >= 5) {
                                if (updateHardware.executeUpdate() == 1) {
                                    out.println("<p>Record updated.</p>");
                                } else {
                                    out.println("<p>Update failed.</p>");
                                }
                        } else {
                            out.println("<p>Insufficient rights.</p>");
                        }
                    } else {
                        out.println("<p>No user level found.</p>");
                    }
                } else {
                    out.println("<p>No valid session.</p>");
                }
                con.close();
            } catch (SQLException sqlex) {
                context.log("UpdateHardware", sqlex);
                out.println("<p>SQL Exception.</p>");
            } catch (NumberFormatException nfex) {
                context.log("UpdateHardware", nfex);
                out.println("<p>Invalid badge number.</p>");
            }
            con = null;
        } // end sync
    }
}

