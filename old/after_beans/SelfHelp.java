/* 
 * @(#) SelfHelp.java    0.2 2005/10/22
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.Calendar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
/**
 * Presents FAQs by category
 *
 * @version       0.2 22 October 2005
 * @author        Steven Lilley
 */
public class SelfHelp extends HttpServlet { 

    private final String sql_getCategories = 
        "SELECT category, COUNT(docref) FROM selfhelp GROUP BY category";
    private final String sql_getDocuments = 
        "SELECT docref, symptoms, moddate FROM selfhelp WHERE category = ?"
        + "ORDER BY docref";
    private final String sql_getSolution = 
        "SELECT symptoms, fix FROM selfhelp WHERE docref = ?";
    private final String sql_getItem = 
        "SELECT category, symptoms, fix FROM selfhelp WHERE docref = ?";
    private final String sql_addItem = 
        "INSERT INTO selfhelp (category, symptoms, fix) VALUES (?, ?, ?)";
    private final String sql_updateItem = 
        "UPDATE selfhelp SET category = ?, symptoms = ?, fix = ? WHERE "
        + "docref = ? LIMIT 1";
    private final String sql_deleteItem = 
        "DELETE FROM selfhelp WHERE docref = ? LIMIT 1";
    /** The context in which this servlet is running. */
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;
    
    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:SelfHelp, init: Unable to get logger from context.");
        } else {
            log.finer("SelfHelp servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("SelfHelp", "init", nex);
        }
    }
    
    /**
     * Handle GET requests
     * This is the normal method used for this servlet.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        RequestDispatcher handler;
        Connection con;
        PreparedStatement getCategories;
        PreparedStatement getDocuments;
        PreparedStatement getSolution;
        ResultSet results;
        synchronized (this) { 
            rqst.setAttribute("page","selfhelp");
            resp.setContentType("text/html");
            HttpSession session = rqst.getSession(false);
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            int helpLevel = 0;
            int documentRef = 0;
            String category = "";
            try {
                if (rqst.getParameter("helplevel") != null ) {
                    helpLevel = Integer.parseInt(
                        rqst.getParameter("helplevel"));
                }
                if (rqst.getParameter("docref") != null ) {
                    documentRef = Integer.parseInt(
                        rqst.getParameter("docref"));
                }
            } catch (NumberFormatException nfex) {
                // helplevel and/or docref should be numeric but aren't
            }
            if (rqst.getParameter("category") != null) {
                category = rqst.getParameter("category");
            }
            try {
                con = dataSrc.getConnection();
                getCategories = con.prepareStatement(sql_getCategories);
                results = getCategories.executeQuery();
                if (helpLevel == 2) {
                    h.append("<div class=\"help\">\n");
                }
                if (helpLevel == 1 || helpLevel == 2) {
                    h.append("<div class=\"help0chosen\">\n");
                } else {
                    h.append("<div class=\"help0choose\">\n");
                }
                h.append("<h3>Categories</h3>\n");
                h.append("<ul>\n");
                while (results.next()) {
                    h.append("<li><a href=\"selfhelp?helplevel=1");
                    h.append("&category=");
                    h.append(results.getString("category"));
                    h.append("\">");
                    h.append(results.getString("category"));
                    h.append("</a></li>\n");
                }
                h.append("</ul>\n</div>\n");
                if (helpLevel > 0) {
                    Calendar ageLimitForNew = Calendar.getInstance();
                    ageLimitForNew.add(Calendar.DATE, -7);
                    getDocuments = con.prepareStatement(sql_getDocuments);
                    getDocuments.clearParameters();
                    getDocuments.setString(1, category);
                    results = getDocuments.executeQuery();
                    if (helpLevel == 1) {
                        h.append("<div class=\"help1choose\">\n");
                    } else {
                        h.append("<div class=\"help1chosen\">\n");
                    }
                    h.append("<h3>" + category + "</h3>\n");
                    h.append("<ul>\n");
                    while (results.next()) {
                        h.append("<li>");
                        if (results.getDate("moddate").getTime() > 
                            ageLimitForNew.getTimeInMillis()) {
                            h.append("<img src=\"images/newitem.png\" alt=\"new\" ");
                            h.append("style=\"height:9px;width:9px;float:left;");
                            h.append("margin:2px;\">");
                        }
                        h.append("<a href=\"selfhelp?");
                        h.append("helplevel=2&");
                        h.append("category=");
                        h.append(category);
                        h.append("&docref=");
                        h.append(results.getInt("docref"));
                        h.append("\">");
                        h.append(results.getString("symptoms"));
                        h.append("</a>");
                        h.append("</li>\n");
                    }
                    h.append("</ul>\n</div>\n");
                    if (helpLevel == 2) {
                        h.append("</div>\n");
                        getSolution = con.prepareStatement(sql_getSolution);
                        getSolution.clearParameters();
                        getSolution.setInt(1, documentRef);
                        results = getSolution.executeQuery();
                        if (results.next()) {
                            h.append("<h3>");
                            h.append(results.getString("symptoms"));
                            h.append("</h3>\n");
                            h.append("<div class=\"fix\">\n<p>");
                            h.append(results.getString("fix"));
                        } else {
                            h.append("<div class=\"fix\">\n<p>");
                            h.append("No solution found,.");
                        }
                        h.append("</p>\n</div>\n");
                        results.close();
                        con.close();
                        h.append("<form method=\"post\" ");
                        h.append("action=\"feedback\">\n");
                        h.append("<p><b>Please rate this document: </b>");
                        h.append("&nbsp<select name=\"rating\">");
                        h.append("<option selected value=\"okay\">");
                        h.append("Fine, solved my problem</option>\n");
                        h.append("<option value=\"tootech\">");
                        h.append("Solved problem but too technical/unclear");
                        h.append("</option>\n");
                        h.append("<option value=\"nosolve\">");
                        h.append("Didn't solve problem</option>\n");
                        h.append("</select>\n");
                        h.append("<input type=\"hidden\" name=\"sitepage\" ");
                        h.append("value=\"selfhelp" + documentRef + "\">");
                        h.append("<input type=\"submit\" value=\"Submit\">");
                        h.append("</p></form>\n");
                        if (session != null) {
                            if (session.getAttribute("ulevel") != null) {
                                if (((Integer)session
                                    .getAttribute("ulevel"))
                                    .intValue() == 9) {
                                    h.append(doAdminOptions(category, 
                                        documentRef));
                                }
                            }
                        }

                    }
                }
                out.println(HouseKeeper.doHead("Self Help", 
                    "Help!", "selfhelp", HouseKeeper.normalDelay));
                out.println(h.toString());
                out.println("</div>");
                handler = context.getNamedDispatcher("MenuFragment");
                handler.include(rqst, resp);
                con.close();
            } catch (SQLException sqlex) {
                context.log("SelfHelp, doGet:", sqlex);
            }
            con = null;
        } // end sync
    }
    
    /**
     * Handle POST requests.
     * This method is only used by the system administrator.
     * It allows records to be added, edited and deleted.
     * @param rqst the servlet request
     * @param resp the servlet response
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        HttpSession session = rqst.getSession(false);
        RequestDispatcher handler;
        String failureReason = new String();
        if (session.getAttribute("ulevel") != null) {
            if (((Integer)session.
                getAttribute("ulevel")).intValue() == 9) {
                processAction(rqst, resp);
            } else {
                failureReason = "You are not an administrator.";
            }
        } else {
            failureReason = "Your session has expired or is invalid.";
        }
        rqst.setAttribute("page","selfhelp");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println(HouseKeeper.doHead("Self Help", 
            "Help!", "selfhelp", HouseKeeper.errorDelay));
        out.println("<p>" + failureReason + "</p>");
        out.println("</div>");
        handler = context.getNamedDispatcher("MenuFragment");
        handler.include(rqst, resp);
    }
    
    private void processAction(HttpServletRequest rqst, 
        HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher handler;
        Connection con;
        PreparedStatement getItem;
        PreparedStatement deleteItem;
        PreparedStatement saveItem;
        ResultSet result;
        synchronized (this) {
            rqst.setAttribute("page","selfhelp");
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter();
            int updatedRecords = 0;
            StringBuffer h = new StringBuffer();
            int documentRef = 0;
            String category = new String();
            String symptoms = new String();
            String fix = new String();
            String action = new String();
            if (rqst.getParameter("action") != null) {
                action = rqst.getParameter("action");
            }
            if (rqst.getParameter("category") != null) {
                category = rqst.getParameter("category");
            }
            if (rqst.getParameter("symptoms") != null) {
                symptoms = rqst.getParameter("symptoms");
            }
            if (rqst.getParameter("fix") != null) {
                fix = rqst.getParameter("fix");
            }
            try {
                if (rqst.getParameter("docref") != null ) {
                    documentRef = Integer.parseInt(
                        rqst.getParameter("docref"));
                }
                con = dataSrc.getConnection();
                if (action.equalsIgnoreCase("add")) {
                    h.append(doForm(0, category, "", ""));
                } else
                if (action.equalsIgnoreCase("edit")) {
                    getItem = con.prepareStatement(sql_getItem);
                    getItem.clearParameters();
                    getItem.setInt(1, documentRef);
                    result = getItem.executeQuery();
                    if (result.next()) {
                        symptoms = result.getString("symptoms");
                        fix = result.getString("fix");
                    }
                    h.append(doForm(documentRef, category, symptoms, fix));
                } else
                if (action.equalsIgnoreCase("delete")) {
                    deleteItem = con.prepareStatement(sql_deleteItem);
                    deleteItem.clearParameters();
                    deleteItem.setInt(1, documentRef);
                    deleteItem.executeUpdate();
                } else
                if (action.equalsIgnoreCase("save")) {
                    if (documentRef == 0) {
                        saveItem = con.prepareStatement(sql_addItem);
                        saveItem.clearParameters();
                    } else {
                        saveItem = con.prepareStatement(sql_updateItem);
                        saveItem.clearParameters();
                        saveItem.setInt(4, documentRef);
                    }
                    saveItem.setString(1, category);
                    saveItem.setString(2, symptoms);
                    saveItem.setString(3, fix);
                    updatedRecords = saveItem.executeUpdate();
                    if (updatedRecords == 1) {
                        h.append("<p>Item saved.</p>");
                    } else {
                        h.append("<p>Something didn't work.</p>");
                    }
                }
                con.close();
            } catch (NumberFormatException nfex) {
                // docref should be numeric but aren't
            } catch (SQLException sqlex) {
                context.log("SelfHelp, doPost:", sqlex);
            }
            con = null;
            out.println(HouseKeeper.doHead("Self Help", 
                "Help!", "selfhelp", HouseKeeper.secureDelay));
            out.println(h.toString());
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
        } //end sync
    }
    
    private String doAdminOptions(String category, int docref) {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"selfhelp\">\n");
        h.append("<p><input type=\"radio\" name=\"action\" ");
        h.append("value=\"edit\" checked>Edit &nbsp;\n");
        h.append("<input type=\"radio\" name=\"action\" value=\"add\">");
        h.append("Add &nbsp;\n");
        h.append("<input type=\"radio\" name=\"action\" value=\"delete\">");
        h.append("Delete &nbsp;\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"hidden\" name=\"category\" value=\"");
        h.append(category + "\">\n");
        h.append("<input type=\"hidden\" name=\"docref\" value=\"");
        h.append(docref + "\">\n</p></form>\n");
        return h.toString();
    }
    
    private String doForm(int docref, String category, String symptoms, 
        String fix) {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"selfhelp\" ");
        h.append("name=\"itemform\">\n");
        h.append("<table class=\"borderless\">\n");
        h.append("<tr><td class=\"grey\">Category:</td>");
        h.append("<td><input type=\"text\" size=\"30\" maxlength=\"30\" ");
        h.append("name=\"category\" value=\"" + category + "\"></td>");
        h.append("</tr>\n");
        h.append("<tr><td class=\"grey\">Symptoms:</td>");
        h.append("<td><input type=\"text\" size=\"30\" maxlength=\"60\" ");
        h.append("name=\"symptoms\" value=\"" + symptoms + "\"></td>");
        h.append("</tr>\n");
        h.append("<tr><td class=\"grey\" colspan=\"2\">");
        h.append("Solution:</td></tr>\n");
        h.append("<tr><td colspan=\"2\"><textarea cols=\"80\" ");
        h.append("rows=\"15\" name=\"fix\" wrap=\"virtual\">");
        h.append(fix);
        h.append("</textarea></td></tr>\n");
        h.append("<tr><td colspan=\"2\" class=\"right\">\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("<input type=\"hidden\" name=\"docref\" ");
        h.append("value=\"" + docref + "\">\n");
        h.append("<input type=\"hidden\" name=\"action\" ");
        h.append("value=\"save\">\n</td></tr>");
        h.append("</table>\n</form>\n");
        return h.toString();
    }
}

