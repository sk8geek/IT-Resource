/* 
 * @(#) OutMachineSummary.java	0.1 2005/10/15
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Outputs a tabular list of machines ordered by badge number.
 * This class performs a wildcard search for a text string in the 
 * location, team and user fields of the hw_badged table.
 *
 * @version       0.1 15 October 2005
 * @author        Steven Lilley
 */
public class OutMachineSummary extends HttpServlet {

    private SimpleDateFormat coverDate = new SimpleDateFormat("MMM yyyy");
    private final String sql_hardwareSummary = new String(
        "SELECT badge, live, type, descrip, location, team, user "
        + "FROM hw_badged WHERE location LIKE ? OR team like ? OR "
        + "user LIKE ? ORDER BY badge");
    private ServletContext context;
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    private Logger log;

    public void init() {
        context = getServletContext();
        log = (Logger)context.getAttribute("log");
        if (log != null) {
            System.out.println("itres:OutMachineSummary, init: Unable to get logger from context.");
        } else {
            log.finer("OutMachineSummary servlet has context wide logger.");
        }
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.throwing("OutMachineSummary", "init", nex);
        }
    }
    
    /**
     * Handle a GET request.  (Call the <code>doPost</code> method.)
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        doPost(rqst, resp);
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        Connection con;
        PreparedStatement hardwareSummary;
        ResultSet results;
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer("<!-- Hardware Summary -->\n");
            String searchFor = "";
            String title = "";
            if (rqst.getAttribute("find") != null) {
                searchFor = "%" + rqst.getAttribute("find") + "%";
            }
            try {
                con = dataSrc.getConnection();
                hardwareSummary = con.prepareStatement(sql_hardwareSummary);
                hardwareSummary.setString(1, searchFor);
                hardwareSummary.setString(2, searchFor);
                hardwareSummary.setString(3, searchFor);
                results = hardwareSummary.executeQuery();
                title = "<h3>Hardware Summary: (" + searchFor + ")</h3>\n";
                h.append(HouseKeeper.doMachineSummary(title, results, true));
                out.println(h.toString());
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutMachineSummary, doPost:", sqlex);
            }
            con = null;
        } // end sync
    }
}

