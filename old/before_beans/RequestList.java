/* 
 * @(#) RequestList.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import java.text.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * List requests.
 *
 * @version		0.1 6 Feb 2004
 * @author		Steven Lilley
 */
public class RequestList extends HttpServlet { 

  private static final DateFormat ITEMDATE = DateFormat.getDateInstance(DateFormat.MEDIUM);
  private StringBuffer httpOut = new StringBuffer();
  private PrintWriter out;

  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    resp.setContentType("text/html");
    out = resp.getWriter();
    httpOut.delete(0,httpOut.length());
    httpOut.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"");
    httpOut.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
    httpOut.append("<html><!-- Copyright 2003 Steven Lilley --><head>");
    httpOut.append("<title>ls-db</title>");
    httpOut.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />");
    httpOut.append("<meta name=\"robots\" content=\"noindex,nofollow\" />");
    httpOut.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" /></head>");
    httpOut.append("<body><h1>Leisure Services Database Server</h1>");
    httpOut.append("<h2>Request List for " + rqst.getParameter("badgeno") + "</h2>");
    out.println(httpOut);
    
    rqst.setAttribute("allRequests",new Boolean(true));
    ServletContext ctxt = getServletContext();
    RequestDispatcher handler = ctxt.getNamedDispatcher("OutRequestList");
    handler.include(rqst, resp);

    httpOut.delete(0,httpOut.length());
    httpOut.append("<br clear=\"all\"><p class=\"cr\"><a href=\"/servlets/index\">HOME</a></p>");
    httpOut.append("</body></html>");
    out.println(httpOut);
    out.close();
  }
}


