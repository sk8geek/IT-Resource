/* 
 * @(#) AcceptAdmin.java	0.1 2004/02/05
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class AcceptAdmin extends HttpServlet { 

  private static final DateFormat ITEMDATE = DateFormat.getDateInstance(DateFormat.MEDIUM);
  private java.sql.PreparedStatement insertRequest;
  private StringBuffer insertSQL = new StringBuffer();
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private java.util.Date targetDate;
  private ServletContext context;


  public void init() throws ServletException {
    context = getServletContext();
    insertSQL.append("INSERT INTO requests (badge,contact,phone,site,");
    insertSQL.append("request,tasktype,priority,submitted,src,worker,");
    insertSQL.append("target,workdone,closed,duration,techlevel) ");
    insertSQL.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
  try { 
      Class.forName("com.mysql.jdbc.Driver");
    }
    catch (ClassNotFoundException cnfex) {
    }
  }



  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>Accept Request (Admin)</h2>");
    h.delete(0,h.length());

    synchronized (this) {  
      String pw = new String("");
      if ( rqst.getSession(false) != null ) {
        HttpSession adminSession = rqst.getSession(false);
        pw = (String)adminSession.getAttribute("password");
      }
      try {
        java.sql.Connection con = 
          DriverManager.getConnection(DBCON,"itadmin",pw);
        java.sql.PreparedStatement getTaskProfile = con.prepareStatement(
          "SELECT typdur,typlead FROM tasktypes WHERE taskcode=?");
        getTaskProfile.setString(1, rqst.getParameter("tasktype"));
        java.sql.ResultSet res = getTaskProfile.executeQuery();
        
        if (res.next()) {
          String leadTime = res.getString("typlead");
          String leadHours = leadTime.substring(0,leadTime.indexOf(":"));
          String leadMins = leadTime.substring(leadTime.indexOf(":")+1,leadTime.lastIndexOf(":"));
          String leadSecs = leadTime.substring(leadTime.lastIndexOf(":")+1,leadTime.length());
          Calendar cal = Calendar.getInstance();
          cal.add(Calendar.HOUR, Integer.parseInt(leadHours));
          cal.add(Calendar.MINUTE, Integer.parseInt(leadMins));
          cal.add(Calendar.SECOND, Integer.parseInt(leadSecs));
          targetDate = cal.getTime();
        }

        insertRequest = con.prepareStatement(insertSQL.toString());
        insertRequest.clearParameters();
        int modifiedRowCount = 0;
  
        h.append("<h3>Badge#" + rqst.getParameter("badgeno") + ")</h3>");
        
        insertRequest.setInt(1, Integer.parseInt(rqst.getParameter("badgeno")));
        insertRequest.setString(2, rqst.getParameter("contact"));
        insertRequest.setString(3, rqst.getParameter("phoneno"));
        insertRequest.setString(4, rqst.getParameter("site"));
        insertRequest.setString(5, rqst.getParameter("faultdesc"));
        insertRequest.setString(6, rqst.getParameter("tasktype"));
        insertRequest.setString(7, rqst.getParameter("priority"));

        long now = System.currentTimeMillis();
        insertRequest.setDate(8, new java.sql.Date(now));
        insertRequest.setString(9, rqst.getRemoteHost());
        insertRequest.setString(10, rqst.getParameter("worker"));
        insertRequest.setDate(11, new java.sql.Date(targetDate.getTime()));
        insertRequest.setString(12, rqst.getParameter("workdone"));
        if ( rqst.getParameter("closenow") != null ) {
          insertRequest.setDate(13, new java.sql.Date(now));
        } else {
          insertRequest.setDate(13, null);
        }
        insertRequest.setString(14, rqst.getParameter("duration"));
        insertRequest.setString(15, rqst.getParameter("techlevel"));

        modifiedRowCount = insertRequest.executeUpdate();
        con.close();
             
        if ( modifiedRowCount == 1 ) {
          h.append("<p>Your request has been successfully submitted.</p>");
        } else {
          h.append("<p>Your request could not be submitted!</p>");
        }

        // the new request has (hopefully) been inserted into the database
        // now we do another query so that the user can see a list of outstanding 
        // faults for the badge number that they have just submitted for.

        h.append("<h3>Open Requests</h3>");
        out.println(h);
        h.delete(0,h.length());
    
        rqst.setAttribute("allRequests",new Boolean(false));
        RequestDispatcher handler = context.getNamedDispatcher("OutRequestList");
        handler.include(rqst, resp);

        h.append("</div>\n");
        out.println(h);
        handler = context.getNamedDispatcher("MenuFragment");
        rqst.setAttribute("page","index");
        handler.include(rqst, resp);
        out.close();
      }
      catch (SQLException sqlex) {
        System.err.println("AcceptAdmin,goPost:" + sqlex);
      }
    } // end sync
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


