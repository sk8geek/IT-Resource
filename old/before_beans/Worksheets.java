/* 
 * @(#) Worksheets.java	0.1 2004/02/12
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Worksheets for use when setting up PCs.
 *
 * @version		0.1 12 Feb 2004
 * @author		Steven Lilley
 */
public class Worksheets extends HttpServlet { 

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private StringBuffer SQLQuery = new StringBuffer();

  public void init() throws ServletException {
  }

  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    synchronized (this) {  
      String pw = new String("");
      if ( rqst.getSession(false) != null ) {
        HttpSession adminSession = rqst.getSession(false);
        pw = (String)adminSession.getAttribute("password");
      }
      try {
        java.sql.Connection con = DriverManager.getConnection(DBCON,"itadmin",pw);

        // CREATE THE WORKSHEET HEADER

        SQLQuery.delete(0,SQLQuery.length());
        SQLQuery.append("INSERT INTO wshead (pcid, descrip, os, ");
        SQLQuery.append("processor, memory, hdd, display, estcost, ");
        SQLQuery.append("flcode, site, user, notes, type, badge, live) ");
        SQLQuery.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,'LIVE')");

        java.sql.PreparedStatement createWorksheet = 
          con.prepareStatement(SQLQuery.toString());
        createWorksheet.clearParameters();
        int modifiedRowCount = 0;
        
        createWorksheet.setString(1, rqst.getParameter("pcid"));
        createWorksheet.setString(2, rqst.getParameter("descrip"));
        createWorksheet.setString(3, rqst.getParameter("os"));
        createWorksheet.setString(4, rqst.getParameter("processor"));
        try {
          createWorksheet.setInt(5, Integer.parseInt(rqst.getParameter("memory")));
        }
        catch (NumberFormatException nfex) {
          createWorksheet.setInt(5, 0);
        }
        createWorksheet.setString(6, rqst.getParameter("hdd"));
        createWorksheet.setString(7, rqst.getParameter("display"));
        try {
          createWorksheet.setInt(8, Integer.parseInt(rqst.getParameter("estcost")));
        }
        catch (NumberFormatException nfex) {
          createWorksheet.setInt(8, 500);
        }
        createWorksheet.setString(9, rqst.getParameter("flcode"));
        createWorksheet.setString(10, rqst.getParameter("site"));
        createWorksheet.setString(11, rqst.getParameter("user"));
        createWorksheet.setString(12, rqst.getParameter("notes"));
        createWorksheet.setString(13, rqst.getParameter("type"));
        createWorksheet.setString(14, rqst.getParameter("badge"));
        modifiedRowCount = createWorksheet.executeUpdate();

        // CREATE THE WORKSHEET ITEMS

        SQLQuery.delete(0,SQLQuery.length());
        SQLQuery.append("INSERT INTO worksheets (pcid, wsitem) ");
        SQLQuery.append("SELECT wshead.pcid, wsitems.seq FROM ");
        SQLQuery.append("wshead, wsitems WHERE wshead.pcid=? AND ");
        SQLQuery.append("wsitems.os=wshead.os");
        java.sql.PreparedStatement createChecklist = 
          con.prepareStatement(SQLQuery.toString());
        createChecklist.clearParameters();
        createChecklist.setString(1, rqst.getParameter("pcid"));
        modifiedRowCount = createChecklist.executeUpdate();
        
        // ADD ANY OPTIONAL ITEMS

        con.close();
      }
      catch (SQLException sqlex) {
        System.err.println("Worksheets,doPost:" + sqlex);
      }
      doGet(rqst, resp);
    } // end sync
  }



  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();
    
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>Worksheets (Admin)</h2>");
    out.println(h);
    
    RequestDispatcher handler = context.getNamedDispatcher("OutWorksheetList");
    handler.include(rqst, resp);

    h.delete(0,h.length()); 
    h.append("<form method=\"post\" action=\"/servlets/worksheetcreate\">");
    h.append("PC ID: <input type=\"text\" name=\"pcid\" maxlength=\"30\" size=\"20\" />");
    h.append("<input type=\"submit\" value=\"New Sheet\">");
    h.append("</form></div>");
    out.println(h);

    handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


