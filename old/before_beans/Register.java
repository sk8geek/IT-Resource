/* 
 * @(#) Register.java    1.0 2005/08/07
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * User registration servlet.
 * Developed from uk.co.channele.midgley.Register class.
 *
 * @version       1.0 7 August 2005
 * @author        Steven Lilley
 */
public class Register extends HttpServlet { 

    private final int infoDelay = 2;
    private final int errorDelay = 4;
    private final int secureDelay = 300;
    private final int normalDelay = 600;
    private final DateFormat itemDateTime = 
        DateFormat.getDateTimeInstance(DateFormat.MEDIUM, 
        DateFormat.MEDIUM);
    private final String sql_insertNewUser = 
        "INSERT INTO ereg (uname, pw, given, surname, team, site) "
        + "VALUES (?, PASSWORD(?), ?, ?, ?, ?)";
    private final String sql_checkPassword = 
        "SELECT uname, ulevel, site FROM ereg "
        + "WHERE uname=? AND pw=password(?)";
    private final String sql_getUserData = 
        "SELECT given, surname, team, site FROM ereg WHERE uname=?";
    private final String sql_updatePrefsIncludingPassword = 
        "UPDATE ereg SET given=?, surname=?, team=?, site=?, "
        + "pw=PASSWORD(?) WHERE uname=? LIMIT 1";
    private final String sql_updatePrefsExcludingPassword = 
        "UPDATE ereg SET given=?, surname=?, team=?, site=? "
        + "WHERE uname=? LIMIT 1";
    private final String sql_setStatus =
        "UPDATE ereg SET status=? WHERE uname=? LIMIT 1";
    private final String sql_getPasswordByEmailAddress = 
        "SELECT pw FROM listeners WHERE email_pri=?";
    private ServletContext context;
    private String sqlDB;
    private String sqlUser;
    private String sqlPassword;

    public void init() throws ServletException {
        context = getServletContext();
        sqlDB = context.getInitParameter("sqlDB");
        sqlUser = context.getInitParameter("sqlUser");
        sqlPassword = context.getInitParameter("sqlPassword");
    }

    /** 
     * This should only be called by an unregistered user.
     * Present a registration form. 
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","ereg");
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            out.println(doHead(secureDelay, "", "get"));
            out.println(userForm("", "", "", "", "", false, false, true, 
                "reguser", "", "User Registration"));
            h.append("</div>\n");
            out.println(h);
            RequestDispatcher handler = 
                context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /** 
     * This is called for many different user administration tasks.
     * The <code>regop</code> parameter determines behaviour. 
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page","ereg");
            PrintWriter out = resp.getWriter();
            String failureReason = new String();
            // accepted parameters
            String regop = new String();
            String given = new String();
            String surname = new String();
            String team = new String();
            String site = new String();
            String uname = new String();
            String oldpw = new String();
            String pw1 = new String();
            String pw2 = new String();
            boolean forgotpw = false;
            boolean guestPC = false;
            // get parameter values 
            if (rqst.getParameter("regop") != null) {
                regop = rqst.getParameter("regop");        
            }
            if (rqst.getParameter("given") != null) {
                given = rqst.getParameter("given");        
            }
            if (rqst.getParameter("surname") != null) {
                surname = rqst.getParameter("surname");
            }
            if (rqst.getParameter("team") != null) {
                team = rqst.getParameter("team");
            }
            if (rqst.getParameter("site") != null) {
                site = rqst.getParameter("site");
            }
            if (rqst.getParameter("uname") != null) {
                uname = rqst.getParameter("uname");
            }
            if (rqst.getParameter("oldpw") != null) {
                oldpw = rqst.getParameter("oldpw");
            }
            if (rqst.getParameter("pw1") != null) {
                pw1 = rqst.getParameter("pw1");
            }
            if (rqst.getParameter("pw2") != null) {
                pw2 = rqst.getParameter("pw2");
            }
            if (rqst.getParameter("forgotpw") != null) {
                forgotpw = true;
            }
            if (rqst.getParameter("guestpc") != null) {
                guestPC = true;
            }
            // Show the registration form
            if (regop.equalsIgnoreCase("regform")) {
                out.println(doHead(secureDelay, "", "get"));
                out.println(userForm("", "", "", "", "", false, false, true, 
                    "reguser", "", "User Registration"));
            } else
            // Register the user.
            if (regop.equalsIgnoreCase("reguser")) {
                // validate form 
                String errors = new String(
                    validateData(given, surname, team, site, uname, oldpw, 
                        pw1, pw2));
                if (errors.length() > 0) {
                    // error occured, represent registration form 
                    out.println(doHead(secureDelay, "", 
                        "reguser, error"));
                    out.println(userForm(given, surname, team, site, 
                        uname, false, false, true, "reguser", errors, 
                        "User Registration"));
                } else {
                    // data is valid 
                    try {
                        Connection con = DriverManager.getConnection(
                            sqlDB, sqlUser, sqlPassword);
                        PreparedStatement insertNewUser = 
                            con.prepareStatement(sql_insertNewUser);
                        insertNewUser.clearParameters();
                        insertNewUser.setString(1, uname);
                        insertNewUser.setString(2, pw1);
                        insertNewUser.setString(3, given);
                        insertNewUser.setString(4, surname);
                        insertNewUser.setString(5, team);
                        insertNewUser.setString(6, site);
                        if (insertNewUser.executeUpdate() == 1) {
                            // stored user okay, inform user
                            out.println(doHead(infoDelay, "", 
                                "reguser, user registered"));
                            out.println(simplePage(
                                "Registration Successful", 
                                "Welcome to e-register version 2."));
                        } else {
                            failureReason = 
                                "Unable to create new account.";
                        }
                        con.close();
                    } catch (SQLException sqlex) {
                        System.out.print(itemDateTime.format(new Date()));
                        System.out.println(
                            "[itres]EReg,doPost[reguser]:" + sqlex);
                        failureReason = "Unable to access database.";
                    }
                    if (failureReason.length() > 0) { //FIXME 
                        out.println(doHead(errorDelay, "", 
                            "reguser, final error"));
                            out.println(failureReason(failureReason));
                    }
                }
            } else
            // User login.
            if (regop.equalsIgnoreCase("login")) {
                if (uname.length() > 0 && oldpw.length() > 0) {
                    try {
                        Connection con = DriverManager.getConnection(
                            sqlDB, sqlUser, sqlPassword);
                        PreparedStatement checkPassword = 
                            con.prepareStatement(sql_checkPassword);
                            checkPassword.clearParameters();
                            checkPassword.setString(1, uname);
                            checkPassword.setString(2, oldpw);
                        ResultSet result = checkPassword.executeQuery();
                        if (result.next()) {
                            // user name and password match found 
                            uname = result.getString("uname");
                            site = result.getString("site");
                            HttpSession sess = rqst.getSession(true);
                            sess.setAttribute("uname", uname);
                            sess.setAttribute("ulevel", 
                                new Integer(result.getInt("ulevel")));
                                sess.setMaxInactiveInterval(580);
                            if (!guestPC) {
                                // set cookie if not on a guest PC
                                Cookie eregCookie = 
                                    new Cookie("eregid", uname);
                                eregCookie.setMaxAge(5184000);
                                Cookie eregCookie2 = 
                                    new Cookie("eregsite", site);
                                    eregCookie2.setMaxAge(5184000);
                                resp.addCookie(eregCookie);
                                resp.addCookie(eregCookie2);
                                out.println(doHead(infoDelay, "", 
                                    "successful login"));
                                out.println(simplePage("Login Successful", 
                                    "You are logged in. Enjoy!"));
                            }
                        } else {
                            // username/password mismatch 
                            out.println(doHead(errorDelay, "", 
                                "login, username/password mismatch"));
                            out.println(simplePage("Login Failed", 
                                "Your username or password are incorrect. "
                                + "Please try again."));
                        }
                    } catch (SQLException sqlex) {
                        System.out.println(
                            "[itres]EReg,doPost[login]:" + sqlex);
                        failureReason = "Unable to access database.";
                    }
                } else 
                if (uname.length() == 0) {
                    // cookie not found so user logging clicked button.
                    // present a login form.
                    out.println(doHead(secureDelay, "", 
                        "login (no cookie, no username), present form"));
                    out.println(accountLogin("", ""));
                } else {
                    // username entered but password wasn't 
                    out.println(doHead(errorDelay, "", 
                        "login, password left empty"));
                    out.println(simplePage("Login Failed", 
                        "You need to enter your password."));
                }
                if (failureReason.length() > 0) { //FIXME 
                    out.println(doHead(errorDelay, "", 
                        "login, final error"));
                    out.println(failureReason(failureReason));
                }
            } else
            // User logout.
            if (regop.equalsIgnoreCase("logout")) {
                // session check 
                HttpSession session = rqst.getSession(false);
                out.println(doHead(infoDelay, "", 
                    "logout, logged out okay"));
                if (session != null) {
                    session.invalidate();
                }
                out.println(simplePage("Logged Out", 
                    "You are logged out.  Thanks"));
            } else
            // User changing settings.
            if (regop.equalsIgnoreCase("useradmin")) {
            // session check 
                HttpSession session = rqst.getSession(false);
                if (session == null) {
                    // no valid session, show them the door 
                    out.println(doHead(errorDelay, "", 
                    "logout, expired session"));
                    out.println(simplePage("Session Expired", 
                        "Your log in session has expired.  Please log in "
                        + "again to continue."));
                } else {
                // valid session 
                    try {
                        uname = (String)session.getAttribute("uname");
                        Connection con = DriverManager.getConnection(
                            sqlDB, sqlUser, sqlPassword);
                        PreparedStatement getUserData = 
                            con.prepareStatement(sql_getUserData);
                        getUserData.clearParameters();
                        getUserData.setString(1, uname);
                        ResultSet result = getUserData.executeQuery();
                        if (result.next()) {
                            given = result.getString("given");
                            surname = result.getString("surname");
                            team = result.getString("team");
                            site = result.getString("site");
                            out.println(doHead(secureDelay, "", 
                                "useradmin, logged in, present form"));
                            out.println(userForm(given, surname, team, 
                                site, uname, true, true, true, "modPrefs", 
                                "", "Preferences"));
                        } else {
                            out.println(doHead(errorDelay, "", 
                                "useradmin, can't find account"));
                            out.println(simplePage("Account Not Found", 
                                "Unable to find your account. "
                                + "Please check your details and try "
                                + "again.  If they are correct then "
                                + "please inform your system admin.\n"
                                + "(This event has been logged.)"));
                            System.out.println("User Admin failed to find "
                                + "account: " + uname);
                        }
                        con.close();
                    } catch (SQLException sqlex) {
                        System.out.println(
                            "[itres]EReg,doPost[useradmin]:" + sqlex);
                        failureReason = "Unable to access database.";
                    } catch (NullPointerException npex) {
                        out.println(doHead(errorDelay, "", 
                            "useradmin, no session getting username"));
                        out.println(simplePage("Session Expired", 
                            "Your session has expired.  Please log in "
                            + "again if you wish to continue."));
                    }
                }
            } else 
            // Save user settings.
            if (regop.equalsIgnoreCase("modPrefs")) {
            // session check 
                HttpSession session = rqst.getSession(false);
                if (session == null) {
                    // no valid session, show them the door 
                    out.println(doHead(errorDelay, "", 
                        "useradmin, session expired saving preferences"));
                        out.println(simplePage("Session Expired",  
                        "Your session expired.  Please log in again if "
                        + "you wish to continue making changes."));
                } else {
                    // valid session 
                    try {
                        uname = (String)session.getAttribute("uname");
                        Connection con = DriverManager.getConnection(
                            sqlDB, sqlUser, sqlPassword);
                        PreparedStatement updatePrefs;
                        if (oldpw.length() > 4) {
                            updatePrefs = con.prepareStatement(
                                sql_updatePrefsIncludingPassword);
                            updatePrefs.clearParameters();
                            updatePrefs.setString(5, pw1);
                            updatePrefs.setString(6, uname);
                        } else {
                            updatePrefs = con.prepareStatement(
                                sql_updatePrefsExcludingPassword);
                            updatePrefs.clearParameters();
                            updatePrefs.setString(5, uname);
                        }
                        updatePrefs.setString(1, given);
                        updatePrefs.setString(2, surname);
                        updatePrefs.setString(3, team);
                        updatePrefs.setString(4, site);
                        int modified = updatePrefs.executeUpdate();
                        if (modified == 1) {
                            Cookie eregCookie = 
                                new Cookie("eregid", uname);
                            eregCookie.setMaxAge(5184000);
                            Cookie eregCookie2 = 
                                new Cookie("eregsite", site);
                            eregCookie2.setMaxAge(5184000);
                            resp.addCookie(eregCookie);
                            resp.addCookie(eregCookie2);
                            out.println(doHead(infoDelay, "", 
                                "modPrefs, prefs updated okay"));
                            out.println(simplePage("Settings Changed", 
                                "Your preferences have been saved."));
                        } else {
                            out.println(doHead(errorDelay, "", 
                                "modPrefs, update prefs error"));
                            out.println(simplePage(
                                "Unable to Save Settings", 
                                "Your preferences have NOT been changed."));
                            System.out.println("Unable to save user prefs "
                                + "for user " + uname);
                        }
                        con.close();
                    } catch (SQLException sqlex) {
                        System.out.println(
                            "[itres]EReg,doPost[savePrefs]:" + sqlex);
                        failureReason = "Unable to access database.";
                    } catch (NullPointerException npex) {
                        out.println(doHead(errorDelay, "", 
                            "modPrefs, no session getting username"));
                        out.println(simplePage("Session Expired", 
                        "Your session has expired.  Please log in again "
                            + "if you wish to continue."));
                    }
                }
            } else 
            // User checking in.
            if (regop.equalsIgnoreCase("checkin")) {
                try {
                    Connection con = DriverManager.getConnection(
                        sqlDB, sqlUser, sqlPassword);
                    PreparedStatement setStatus = 
                    con.prepareStatement(sql_setStatus);
                    setStatus.clearParameters();
                    if (regop.equalsIgnoreCase("checkin")) {
                        setStatus.setInt(1, 2);
                    } else {
                        setStatus.setInt(1, -1);
                    }
                    setStatus.setString(2, uname);
                    if (setStatus.executeUpdate() == 1) {
                        out.println(doHead(infoDelay, "", 
                            "checkin okay"));
                        if (regop.equalsIgnoreCase("checkin")) {
                            out.println(simplePage("Checked In", 
                                "Thank you, have a good day."));
                        } else {
                            out.println(simplePage("Checked Out", 
                                "Thank you, see you soon."));
                        }
                    } else {
                        out.println(doHead(errorDelay, "", 
                            "check in/out problem"));
                        out.println(simplePage("Check In/Out Failed", 
                            "Sorry, unable to check you in/out."));
                    }
                } catch (SQLException sqlex) {
                    System.out.println(
                        "[itres]EReg,doPost[savePrefs]:" + sqlex);
                    failureReason = "Unable to access database.";
                }
            } else 
            if (regop.equalsIgnoreCase("checkout")) {

                
                
            } else {
                // catch all 
                out.println(doHead(errorDelay, "", "invalid action"));
                out.println(simplePage("Invalid Request", 
                    "Sorry, don\'t know you want."));
            }
            // close the page 
            out.println("</div>\n");
            RequestDispatcher handler = 
                context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
    
    private void mailPasswordToUser(String emailAddr) {
    // get password 
        String password = new String();
        try {
            Connection con = DriverManager.getConnection(
                sqlDB, sqlUser, sqlPassword);
            PreparedStatement checkPassword = con.prepareStatement(
                sql_getPasswordByEmailAddress);
            checkPassword.setString(1, emailAddr);
            ResultSet result = checkPassword.executeQuery();
            if (result.next()) {
                password = result.getString("pw");
            }
            con.close();
        } catch (SQLException sqlex) {
            System.out.println(sqlex); // XXX 
        }
        StringBuffer msgText = new StringBuffer(
            "Password reminder from Event Log at ls-db.\n\n");
        msgText.append("Your password is: " + password);
        msgText.append("\n\nThis is an automated message. "
            + "Please do not reply to this address.");
        HouseKeeper.sendMail(emailAddr, "[LSDB] Password Reminder", 
            msgText.toString());
    }

/** Validate the data presented.  
Errors are added to a String and returned. 
The string can be used by the caller or by the doUserForm method. */ 
    private String validateData(String given, String surname, String team, 
        String site, String uname, String oldpw, String pw1, String pw2) {
        StringBuffer errors = new StringBuffer();
        if (!textOnly(given)) { errors.append("given,"); }
        if (!textOnly(surname)) { errors.append("surname,"); }
        if (!textOnly(team)) { errors.append("team,"); }
        if (!textOnly(site)) { errors.append("site,"); }
        if (!textOnly(uname)) { errors.append("uname,"); }
        if (pw1.compareTo(pw2) != 0) { errors.append("pw1,pw2,"); }
        if (pw1.length() > 0) {
            if (pw1.length() < 5) { errors.append("pwlen,"); }
        }
        try {
            Connection con = DriverManager.getConnection(
                sqlDB, sqlUser, sqlPassword);
            PreparedStatement checkForUsername = con.prepareStatement(
                "SELECT count(uname) AS qty FROM ereg WHERE uname=?");
            checkForUsername.setString(1, uname);
            ResultSet result = checkForUsername.executeQuery();
            if (result.next()) {
                if(result.getInt("qty") > 0) { errors.append("inuse,"); }
            }
            // check if PW1 length is zero as user may have entered a null 
            // OLDPW even though one was requested. 
            if (pw1.length() == 0) {
                PreparedStatement checkPassword = con.prepareStatement(
                    "SELECT count(uname) AS qty FROM ereg "
                    + "WHERE uname=? AND pw=PASSWORD(?)");
                checkPassword.clearParameters();
                checkPassword.setString(1, uname);
                checkPassword.setString(2, oldpw);
                result = checkPassword.executeQuery();
                if (result.next()) {
                    if(result.getInt("qty") != 1) { 
                        errors.append("invpw,");
                    }
                }
            }
            con.close();
        } catch (SQLException sqlex) {
            System.out.println("[itres]EReg,validateData:" + sqlex);
        }
        return errors.toString();
    }

/** Check that the String is only made up of numbers and/or letters. */ 
    private boolean textOnly(String testMe) {
        for (int c = 0; c < testMe.length(); c++) {
            if ( !Character.isLetterOrDigit(testMe.charAt(c))) {
                return false;
            }
        }
        return true;
    }

/** Retrieve the requested PAGE FRAGMENT from the database and 
return it as a String */
    private String getPageFragment(String frag_id) {
        String fragment = new String();
        try {
            Connection con = DriverManager.getConnection(
                sqlDB, sqlUser, sqlPassword);
            PreparedStatement getPageFragment = con.prepareStatement(
                "SELECT fragment FROM pagefrags WHERE frag_id=?");
            getPageFragment.setString(1, frag_id);
            ResultSet result = getPageFragment.executeQuery();
            if (result.next()) {
                fragment = result.getString("fragment");
            }
            con.close();
        } catch (SQLException sqlex) {
            System.out.println("[itres]EReg,getPageFragment:" + sqlex);
        }
        return fragment;
    }

/** Return the standard page HEAD section as a String.  The page refresh 
timeout interval and target page must be supplied. */ 
    private String doHead(int timeout, String target, String provider) {
        StringBuffer h = new StringBuffer();
        h.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n");
        h.append("\"http://www.w3.org/TR/html4/strict.dtd\">\n");
        h.append("<html><!-- Copyright (c) ");
        h.append("2003-2005 Steven Lilley -->\n<head>");
        h.append("<title>Community Services</title>");
        h.append("<meta http-equiv=\"content-type\" content=\"text/html; ");
        h.append("charset=UTF-8\">\n");
        h.append("<meta name=\"robots\" content=\"noindex,nofollow\">\n");
        h.append("<meta http-equiv=\"refresh\" content=\"");
        h.append(timeout);
        h.append(";URL=");
        if (target.length() == 0) {
            h.append("/servlets/index\">\n");
        } else {
            h.append(target + "\">\n");
        }
        h.append("<link rel=\"stylesheet\" ");
        h.append("type=\"text/css\" href=\"/lsdb.css\">\n</head>");
        h.append("<body><h1>Community Services</h1>");
        h.append("<h2>e-Register</h2>");
        h.append("<div class=\"content\">");
        return h.toString();
    }
    
/** Login form */
    private String accountLogin(String uname, String errors) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>Account Login</h3>\n");
        h.append("<p>Please enter your user name and password.</p>");
        h.append("<p>If you've forgotten your password then tick the ");
        h.append("&quot;Forgotten Password&quot; box.  A new password ");
        h.append("will be sent to your email address.</p>\n");
        h.append("<form method=\"post\" action=\"/servlets/register\" ");
        h.append("name=\"accountlogin\">\n");
        h.append("<table class=\"borderless\">\n");
        // user name  
        h.append("<tr><td>user name:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"16\" ");
        h.append("name=\"uname\" size=\"16\"");
        if (uname.length() > 0) {
            h.append(" value=\"" + uname + "\"");
        }
        if (errors.indexOf("uname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // old password 
        h.append("<tr><td>password:</td>");
        h.append("<td>");
        h.append("<input type=\"password\" maxlength=\"16\" ");
        h.append("name=\"oldpw\" size=\"16\"");
        if (errors.indexOf("oldpw") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td>");
        h.append("<td>forgotten: <input type=\"checkbox\" ");
        h.append("name=\"forgotpw\" ");
        h.append("value=\"forgotpw\"></td>");
        h.append("</tr>\n");
        // guest computer (don't set a cookie)
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"checkbox\" name=\"guestpc\" ");
        h.append("value=\"guestpc\"> tick here if this is NOT your ");
        h.append("normal computer.</td>");
        h.append("</tr>\n");
        // buttons 
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"submit\" value=\"Submit\">");
        h.append("<input type=\"reset\" value=\"Reset\">");
        // action 
        h.append("<input type=\"hidden\" name=\"regop\" ");
        h.append("value=\"login\">");
        // close table 
        h.append("</td></tr></table>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("invpw") > -1) {
            // password was invalid for account 
            h.append("<p class=\"nb\">Invalid password for ");
            h.append("this account.</p>");
        }
        return h.toString();
    }

    private String userForm(String given, String surname, String team, 
        String site, String uname, boolean existing, boolean oldpw, 
        boolean pwpair, String regop, String errors, String title) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>" + title + "</h3>\n");
        if (!existing) {
            h.append("<p>Please complete all boxes.</p>\n");
        }
        h.append("<form method=\"post\" action=\"/servlets/register\" ");
        h.append("name=\"userdetails\" ");
        h.append("onMouseOver=\"userFormFocus();\">\n");
        h.append("<table class=\"borderless\">\n");
        // given name 
        h.append("<tr><td>first name:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"30\" ");
        h.append("name=\"given\" size=\"30\"");
        if (given.length() > 0) {
            h.append(" value=\"" + given + "\"");
        }
        if (errors.indexOf("given") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>");
        // surname 
        h.append("<tr><td>surname:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"40\" ");
        h.append("name=\"surname\" size=\"30\"");
        if (surname.length() > 0) {
            h.append(" value=\"" + surname + "\"");
        }
        if (errors.indexOf("surname") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>\n");
        // team
        h.append("<tr><td>team:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<select name=\"team\">");
        ArrayList teams = (ArrayList)context.getAttribute("teams");
        for (int c = 0; c < teams.size(); c++) {
            DropDownItem d = (DropDownItem)teams.get(c);
            h.append("<option value=\"" + d.code + "\"");
            if (d.code.equalsIgnoreCase(team)) {
                h.append("selected ");
            }
            h.append(">" + d.desc + "</option>");
        }
        h.append("</select></td></tr>");
        // site
        h.append("<tr><td>office:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<select name=\"site\">");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        for (int c = 0; c < sites.size(); c++) {
            DropDownItem d = (DropDownItem)sites.get(c);
            h.append("<option value=\"" + d.code + "\"");
            if ( d.code.equalsIgnoreCase(site) ) {
                h.append("selected ");
            }
            h.append(">" + d.desc + "</option>");
        }
        h.append("</select></td></tr>");
        // user name  
        h.append("<tr><td>user name:</td>");
        if (!existing) {
            h.append("<td>");
            h.append("<input type=\"text\" maxlength=\"16\" ");
            h.append("name=\"uname\" size=\"16\"");
            if (uname.length() > 0) {
                h.append(" value=\"" + uname + "\"");
            }
            if (errors.indexOf("uname") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td>");
            h.append("<td><span class=\"note\">");
            h.append("Enter a user name up to sixteen ");
            h.append("characters long.</span>");
        } else {
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"hidden\" name=\"uname\" value=\"");
            h.append(uname + "\"><b>" + uname + "</b>");
        }
        h.append("</td></tr>\n");
        // old password 
        if (oldpw) {
            h.append("<tr><td>");
            if (pwpair) { h.append("old ");    }
            h.append("password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"oldpw\" size=\"16\"");
            if (errors.indexOf("oldpw") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
        }
        if (pwpair) {
            // password  
            h.append("<tr><td>password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"pw1\" size=\"16\"");
            if (errors.indexOf("pw1") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
            // confirm password 
            h.append("<tr><td>confirm password:</td>");
            h.append("<td colspan=\"2\">");
            h.append("<input type=\"password\" maxlength=\"16\" ");
            h.append("name=\"pw2\" size=\"16\"");
            if (errors.indexOf("pw2") > -1) {
                h.append(" class=\"reqd\"");
            }
            h.append("></td></tr>\n");
        }
        // buttons 
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"submit\" value=\"Submit\">");
        h.append("<input type=\"reset\" value=\"Reset\">");
        // action 
        h.append("<input type=\"hidden\" name=\"regop\" ");
        h.append("value=\"" + regop + "\">");
        // close table 
        h.append("</td></tr></table>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("inuse") > -1) {
            // UNAME is valid but already in use 
            h.append("<p class=\"nb\">The user name is valid but has ");
            h.append("been taken.  Please try something different.</p>");
        }
        if (errors.indexOf("invpw") > -1) {
            // password was invalid for account 
            h.append("<p class=\"nb\">Invalid password for ");
            h.append("this account.</p>");
        }
        if (errors.indexOf("pw1,pw2") > -1) {
            // passwords differ 
            h.append("<p class=\"nb\">The passwords entered do not ");
            h.append("match.</p>");
        }
        if (errors.indexOf("pwlen") > -1) {
            // password is too short 
            h.append("<p class=\"nb\">Passwords need to be longer than ");
            h.append("five characters.</p>");
        }
        return h.toString();
    }

    private String failureReason(String reason) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>System Failure</h3>\n");
        h.append("<p>Unfortunately I have not been able to complete ");
        h.append("your request.</p>\n");
        if (reason.length() > 0) { 
            h.append("<p>" + reason + "</p>");
        } else {
            h.append("<p>Something is broken, but I don't know what.</p>");
        }
        h.append("<p>This fault has been logged. ");
        h.append("Please try again later.</p>");
        return h.toString();
    }

    /** 
     * Display a simple message page. 
     */
    private String simplePage(String heading, String message) {
        StringBuffer h = new StringBuffer();
        h.append("<h3>" + heading + "</h3>");
        if (message.length() > 0) {
            h.append("<p>" + message + "</p>");
        }
        return h.toString();
    }
}