/* 
 * @(#) Admin.java	0.2 2004/07/11
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import com.mysql.jdbc.Driver;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
/**
 * Part of my IT Resource suite of servlets.
 *
 * @version		0.2 11 July 2004
 * @author		Steven Lilley
 */
public class Admin extends HttpServlet { 

	private static final DateFormat ITEMDATE = 
		DateFormat.getDateInstance(DateFormat.MEDIUM);
	private static final SimpleDateFormat SHORTDATE = 
		new SimpleDateFormat("dd/MM");
	private static final String DBCON = 
		new String("jdbc:mysql://ls-db:3306/itres");
	private ServletContext context;

	public void init() throws ServletException {
		context = getServletContext();
		try { 
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException cnfex) {
			System.out.println("Admin, init:" + cnfex);
		}
	}

	public void doGet(HttpServletRequest rqst, HttpServletResponse resp)
		throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		if (rqst.getSession(false) != null) {
			out.println(doHead(60, "/servlets/index"));
			out.println("<!-- Admin line 60 -->\n");
			out.println(doBody(rqst));
		} else {
			out.println(doHead(60, "/servlets/index"));
			out.println("<!-- Admin line 64 -->\n");
			out.println(doPasswordPrompt());
		}
		out.println("</div>");
		RequestDispatcher handler = 
			context.getNamedDispatcher("MenuFragment");
		rqst.setAttribute("page","admin");
		handler.include(rqst, resp);
	}

	public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		StringBuffer h = new StringBuffer();
		String a = (String)rqst.getParameter("action");
	// if no action is set then set it to login 
		if (a == null) {
			a = "login";
		}
	// action is login 
		if (a.equalsIgnoreCase("login")) {
			synchronized (this) {
			// test for password password?
				String newPassword = (String)rqst.getParameter("password");
				if (newPassword == null) {
					newPassword = "";
				}
				if (tryAuth(newPassword) ) {
				// authenticated, save password
					HttpSession sess = rqst.getSession(true);
					sess.setAttribute("password", newPassword);
					sess.setMaxInactiveInterval(1200);
				} else {
					out.println(doHead(10, "/servlets/index"));
					h.delete(0,h.length());
					h.append("<p>Invalid password. ");
					h.append("<a href=\"/servlets/admin\">");
					h.append("Try again?</a></p></div>");
				}
			}
		} else
	// action is invalid password 
		if (a.equalsIgnoreCase("invalid")) {
			out.println(doHead(60, "/servlets/index"));
			out.println(doPasswordPrompt());
		} else
	// action is logout 
		if (a.equalsIgnoreCase("logout")) {
			out.println(doHead(2, "/servlets/index"));
			h.delete(0,h.length());
		/* we should have a session, but check anyway */
			synchronized (this) {
				HttpSession s = rqst.getSession(false);
				if (s != null) {
					s.invalidate();
					a = "login";
					h.append("<p>Session closed.</p></div>");
				} else {
					h.append("<p>No valid session found. ");
					h.append("<a href=\"/servlets/admin\">");
					h.append("Login?</a></p></div>");
				}
			}
		} else {
		/* catch all */
			out.println(doHead(10, "/servlets/index"));
			h.delete(0,h.length());
			h.append("<p>Invalid request.</p></div>");
		}
		if (rqst.getSession(false) != null) {
			out.println(doHead(600, "/servlets/admin"));
 			out.println("<!-- Admin line 135 -->\n");
			out.println(doBody(rqst));
		}
		out.println("</div>");
		out.println(h);
		RequestDispatcher handler = 
			context.getNamedDispatcher("MenuFragment");
		rqst.setAttribute("page","admin");
		handler.include(rqst, resp);
		out.close();
	}

	private boolean tryAuth(String passwordToTest) {
		try {
			Connection con = DriverManager.getConnection(
				DBCON,"itadmin",passwordToTest);
			con.close();
			return true;
		} catch (SQLException sqlex) {
			// ignore this exception. If it occurs then admin can't log in. 
			return false;
		}
	}

	private String doPasswordPrompt() {
		StringBuffer h = new StringBuffer();
		h.append("<form method=\"post\" action=\"/servlets/admin\" ");
		h.append("name=\"pwprompt\" />");
		h.append("Password: <input type=\"password\" ");
		h.append("name=\"password\" size=\"8\" maxlength=\"14\" />");
		h.append("<input type=\"hidden\" name=\"action\" ");
		h.append("value=\"login\" />");
		h.append("<input type=\"submit\" value=\"Log in\" />");
		h.append("</form></div>");
		return h.toString();
	}

	private String doBody(HttpServletRequest rqst) {
		StringBuffer h = new StringBuffer();
		try {
			h.append("<h3>Requests</h3>\n<table class=\"admin\" ");
			h.append("cellspacing=\"0\" cellpadding=\"4\">");
			synchronized (this) {
				java.util.Date now = new java.util.Date();
				HttpSession sess = rqst.getSession(false);
				if (sess != null) {
					Connection con = DriverManager.getConnection(
						DBCON, "itadmin", 
						(String)sess.getAttribute("password"));
					PreparedStatement getRequests = con.prepareStatement(
						"SELECT * FROM requests WHERE closed IS NULL "
						+ "ORDER BY submitted");
					ResultSet results = getRequests.executeQuery();
					while (results.next()) {
					// set row colour to show age 
						if (results.getDate("target") != null) {
							long d = now.getTime() 
								- results.getDate("target").getTime();
							d = d / 86400000;
							if (d <= 0) {
								h.append("<tr class=\"targ0\">");
							} else 
							if (d > 7) {
								h.append("<tr class=\"targ2\">");
							} else
							if (d > 2) {
								h.append("<tr class=\"targ1\">");
							}
						} else {
							h.append("<tr>");
						}
						h.append("<td valign=\"top\">");
						h.append("<a href=\"/servlets/requestdetails?");
						h.append("reqnum=");
						h.append(results.getString("reqnum") + "\">");
						h.append(results.getString("reqnum") + "</a></td>");
						h.append("<td valign=\"top\">");
						h.append(results.getString("contact") + "</td>");
						h.append("<td valign=\"top\">");
						h.append(results.getString("tasktype") + "</td>");
						if (results.getString("request") != null) {
							int dispLen = results.getString("request")
								.length();
							if (dispLen > 59) {
								dispLen = 59; 
							}
							h.append("<td valign=\"top\">");
							h.append(results.getString("request")
								.substring(0,dispLen) + "</td>");
						} else {
							if (results.getString("workdone") != null) {
								int dispLen = results.getString("workdone")
									.length();
								if (dispLen > 59) { 
									dispLen = 59;
								}
								h.append("<td valign=\"top\">");
								h.append(results.getString("workdone")
									.substring(0,dispLen) + "</td>");
							} else {
								h.append("<td valign=\"top\">NULL</td>");
							}
						}
						h.append("<td valign=\"top\">");
						if (results.getDate("submitted") == null) {
							h.append("NULL");
						} else {
							h.append(SHORTDATE.format(results
								.getDate("submitted")));
						}
						h.append("</td><td valign=\"top\">");
						if (results.getDate("target") == null) {
							h.append("NULL");
						} else {
							h.append(SHORTDATE.format(results
								.getDate("target")));
						}
						h.append("</td></tr>");
					} 
					h.append("</table></div>");
					con.close();
				}
			}
		} catch (SQLException sqlex) {
			h.delete(0, h.length());
			if (sqlex.toString().indexOf("Invalid auth") > -1) {
				h.append("<p>The password entered was invalid.</p></div>");
			} else {
				System.err.println("Admin, doBody:" + sqlex);
			}
		}
		return h.toString();
	}

	private String doHead(int timeout, String target) {
		StringBuffer h = new StringBuffer();
		h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
		h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
		h.append("<html><!-- Copyright 2003,2004 Steven Lilley -->\n<head>");
		h.append("<title>Community Services</title>");
		h.append("<meta http-equiv=\"content-type\" ");
		h.append("content=\"text/html; charset=UTF-8\" />\n");
		h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
		h.append("<meta http-equiv=\"refresh\" content=\"");
		h.append(timeout + ";URL=" + target + "\" />\n");
		h.append("<link rel=\"stylesheet\" type=\"text/css\" ");
		h.append("href=\"/lsdb.css\" />\n</head>");
		h.append("<body onLoad=\"document.pwprompt.password.focus();\" />");
		h.append("<h1>Community Services</h1>\n");
		h.append("<h2>Admin</h2>\n<div class=\"content\">\n");
		return h.toString();
	}
}
