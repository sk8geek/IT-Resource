/* 
 * @(#) OutSnippetSummary.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */

package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.novell.ldap.*;

/**
 * Output the titles of the information snippets.
 *
 * @version		0.1 6 Feb 2004
 * @author		Steven Lilley
 */
public class OutSnippetSummary extends HttpServlet {

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");



  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    doPost(rqst, resp);
  }



  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();
    java.sql.PreparedStatement getSnippets;
    java.sql.Connection con;
    java.sql.ResultSet res;

    PrintWriter out = resp.getWriter();
    StringBuffer h = new StringBuffer();

    try {
      synchronized (this) {
        con = DriverManager.getConnection(DBCON,"dbq","quizmaster");
        getSnippets = con.prepareStatement("SELECT sid,title FROM snippets WHERE (title LIKE ?) or (snippet LIKE ?) ORDER BY sid");
        String findBit = new String("%" + (String)(rqst.getAttribute("find")) + "%");
        getSnippets.setString(1, findBit);
        getSnippets.setString(2, findBit);
        res = getSnippets.executeQuery();

        boolean titleDone = false;
        while (res.next()) {
          if ( titleDone == false ) {
            h.append("<h3>Snippet Summary</h3>");
            h.append("<table cellspacing=\"0\" cellpadding=\"4\" class=\"boxed\">");
            h.append("<tr><th>Title</th></tr>");
            titleDone = true;
          }

          h.append("<tr><td><a href=\"/servlets/snippet?sid=");
          h.append(res.getString("sid") + "\">" + res.getString("title") + "</a></td></tr>");
        }
        if ( titleDone ) {
          h.append("</table>");
        }
        con.close();
      } 
      out.println(h);
    } // end sync
    catch (SQLException sqlex) {
      h.append("<body><p>Something broke...<br>");
      h.append("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
    }
  }
}



