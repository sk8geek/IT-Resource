/* 
 * @(#) WorksheetEditor.java	0.1 2004/02/28
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Editor for PC worksheet.
 *
 * @version		0.1 28 Feb 2004
 * @author		Steven Lilley
 */
public class WorksheetEditor extends HttpServlet { 

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private static DateFormat TIMEDISPLAY = 
    DateFormat.getTimeInstance(DateFormat.SHORT);
  private RequestDispatcher handler;
  ServletContext context;


  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>Worksheet Editor (Admin)</h2>");

    if ( rqst.getParameter("pcid") != null ) {
      h.append("<h3>" + rqst.getParameter("pcid") + "</h3>");
      
      // RETRIEVE HEADER AND WORKSHEET
      
      synchronized (this) {
        String pw = new String("");
        if ( rqst.getSession(false) != null ) {
          HttpSession adminSession = rqst.getSession(false);
          pw = (String)adminSession.getAttribute("password");
        }
        try {
          StringBuffer SQLQuery = 
            new StringBuffer("SELECT * FROM wshead WHERE pcid=?");
          java.sql.Connection con = DriverManager.getConnection(DBCON,"itadmin",pw);
          java.sql.PreparedStatement getHeader = 
            con.prepareStatement(SQLQuery.toString());
          getHeader.clearParameters();
          getHeader.setString(1, rqst.getParameter("pcid"));
          java.sql.ResultSet res = getHeader.executeQuery();
          if ( res.next() ) {
            h.append("<table cellspacing=\"0\" cellpadding=\"4\">");
            h.append("<form method=\"post\" action=\"/servlets/worksheetsave\">");
  
            h.append("<tr><td class=\"grey\">PC Type:</td>");
            h.append("<td><b>" + res.getString("descrip") + "</b></td>");
  
            h.append("<td class=\"grey\">OS:</td>");
            h.append("<td><b>" + res.getString("os") + "</b></td></tr>");
  
            h.append("<tr><td class=\"grey\">Display:</td>");
            h.append("<td><input type=\"text\" name=\"display\" size=\"30\" maxlength=\"30\" ");
            h.append("value=\"" + res.getString("display") + "\" /></td>");
  
            h.append("<td class=\"grey\">Processor:</td>");
            h.append("<td><input type=\"text\" name=\"processor\" size=\"15\" maxlength=\"15\" ");
            h.append("value=\"" + res.getString("processor") + "\" /></td></tr>");
  
            h.append("<tr><td class=\"grey\">Memory:</td>");
            h.append("<td><input type=\"text\" name=\"memory\" size=\"6\" maxlength=\"6\" ");
            h.append("value=\"" + res.getString("memory") + "\" /></td>");
  
            h.append("<td class=\"grey\">Hard disk:</td>");
            h.append("<td><input type=\"text\" name=\"hdd\" size=\"10\" maxlength=\"10\" ");
            h.append("value=\"" + res.getString("hdd") + "\" /></td></tr>");

            h.append("<tr><td class=\"grey\">IP Addr:</td>");
            h.append("<td colspan=\"3\"><input type=\"text\" name=\"ipa\" ");
            h.append("size=\"15\" maxlength=\"15\" ");
            h.append("value=\"" + res.getString("ipa") + "\" /></td></tr>");
  

  
            h.append("<tr><td class=\"grey\">Site:</td>");
            h.append("<td><input type=\"text\" name=\"site\" size=\"30\" maxlength=\"30\" ");
            h.append("value=\"" + res.getString("site") + "\" /></td>");
  
            h.append("<td class=\"grey\">User:</td>");
            h.append("<td><input type=\"text\" name=\"user\" size=\"20\" maxlength=\"20\" ");
            h.append("value=\"" + res.getString("user") + "\" /></td></tr>");
  
            h.append("<tr><td class=\"grey\">FL Code:</td>");
            h.append("<td><input type=\"text\" name=\"flcode\" size=\"8\" maxlength=\"8\" ");
            h.append("value=\"" + res.getString("flcode") + "\" /></td>");
  
            h.append("<td class=\"grey\">Est. cost:</td>");
            h.append("<td><input type=\"text\" name=\"estcost\" size=\"6\" maxlength=\"6\" ");
            h.append("value=\"" + res.getString("estcost") + "\" /></td></tr>");
  
            h.append("<tr><td class=\"grey\">Order#:</td>");
            h.append("<td><input type=\"text\" name=\"ordnum\" size=\"10\" maxlength=\"10\" ");
            h.append("value=\"" + res.getString("ordnum") + "\" /></td>");
  
            h.append("<td class=\"grey\">Order date:</td>");
            h.append("<td><input type=\"text\" name=\"orddate\" size=\"10\" maxlength=\"10\" ");
            h.append("value=\"" + res.getString("orddate") + "\" /></td></tr>");
  
            h.append("<tr><td class=\"grey\">Delivered:</td>");
            h.append("<td><input type=\"text\" name=\"delivered\" size=\"10\" maxlength=\"10\" ");
            h.append("value=\"" + res.getString("delivered") + "\" /></td>");
  
            h.append("<td class=\"grey\">Badge:</td>");
            h.append("<td colspan=\"3\"><b><input type=\"text\" name=\"badge\" size=\"6\" maxlength=\"6\" ");
            h.append("value=\"" + res.getString("badge") + "\" /></b></td></tr>");
  
            h.append("<tr><td class=\"grey\">Serial#:</td>");
            h.append("<td><input type=\"text\" name=\"sn\" size=\"20\" maxlength=\"20\" ");
            h.append("value=\"" + res.getString("sn") + "\" /></td>");
  
            h.append("<td class=\"grey\">Display SN:</td>");
            h.append("<td><input type=\"text\" name=\"displaysn\" size=\"20\" maxlength=\"20\" ");
            h.append("value=\"" + res.getString("displaysn") + "\" /></td></tr>");
  
            h.append("<tr><td  class=\"grey\" valign=\"top\">Notes:</td><td colspan=\"3\">");
            h.append("<textarea name=\"addnotes\" rows=\"5\" cols=\"42\">");
            h.append(res.getString("notes"));
            h.append("</textarea></td></tr>");
  
            h.append("<tr><td colspan=\"4\">&nbsp;</td></tr>");
            h.append("<input type=\"hidden\" name=\"os\" value=\"");
            h.append(res.getString("os") + "\" />");
          }
  
          SQLQuery.delete(0,SQLQuery.length());
          SQLQuery.append("SELECT wsitem,done,shortdesc FROM worksheets ");
          SQLQuery.append("LEFT JOIN wsitems ON wsitem=seq WHERE pcid=");
          SQLQuery.append("? ORDER BY wsitem");
          java.sql.PreparedStatement getWorksheetItems = 
            con.prepareStatement(SQLQuery.toString());
          getWorksheetItems.clearParameters();
          getWorksheetItems.setString(1, rqst.getParameter("pcid"));
          res = getWorksheetItems.executeQuery();
          while ( res.next() ) {
            h.append("<tr><td colspan=\"3\" class=\"grey\">");
            h.append(res.getString("shortdesc") + "</td>");
            if ( res.getInt("done") == -1 ) {
              h.append("<td class=\"grey\">");
              h.append("<input type=\"checkbox\" name=\"");
              h.append(res.getString("wsitem") + "\" checked />");
            } else {
              h.append("<td>");
              h.append("<input type=\"checkbox\" name=\"");
              h.append(res.getString("wsitem") + "\" />");
            }
            h.append("</td></tr>");
          }
  
          h.append("<tr><td colspan=\"4\"><input type=\"radio\" ");
          h.append("name=\"action\" value=\"save\" checked> Save ");
          h.append("<input type=\"radio\" name=\"action\" ");
          h.append("value=\"complete\" /> Complete ");
          h.append("<input type=\"radio\" name=\"action\" ");
          h.append("value=\"delete\" /> Delete ");
          h.append("<input type=\"submit\" value=\"Execute\">");
          h.append("<input type=\"reset\" value=\"Reset\"></tr></td>");
          h.append("<input type=\"hidden\" name=\"pcid\" value=\"");
          h.append(rqst.getParameter("pcid") + "\" />");
          h.append("</form></table>");
        }
        catch (SQLException sqlex) {
          System.err.println("WorksheetEditor,doGet:" + sqlex);
        }
      } // END SYNC
    } else {
      h.append("<p>The PC ID can not be null. ");
      h.append("<a href=\"/servlets/worksheets\" />Try again?</a></p>");
    }

    h.append("</div>\n");

    out.println(h);
    RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2004 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
//    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


