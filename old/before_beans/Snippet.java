/* 
 * @(#) Snippet.java	0.1 2003/10/18
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Snippets of information, in a database and searchable.
 *
 * @version		0.1 18 Oct 2003
 * @author		Steven Lilley
 */
public class Snippet extends HttpServlet { 

  private StringBuffer h = new StringBuffer();
  private StringBuffer SQLQuery = new StringBuffer();
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private java.sql.PreparedStatement getSnippet;
  private java.sql.Connection con;
  private java.sql.ResultSet res;
  private RequestDispatcher handler;

  public void init() throws ServletException {
  }


  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    doPost(rqst, resp);
  }


  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();
    
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    synchronized (this) {
      out.println(doHead());
      StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
      h.append("<div class=\"content\"><h2>Information</h2>");
      try {
        synchronized (this) {
          con = DriverManager.getConnection(DBCON,"dbq","quizmaster");
          getSnippet = con.prepareStatement("SELECT * FROM snippets WHERE sid=?");
          int sid = Integer.parseInt((String)(rqst.getParameter("sid")));
          getSnippet.setInt(1, sid);
          res = getSnippet.executeQuery();
  
          if (res.next()) {
            h.append("<h3>" + res.getString("title") + "</h3>");
            h.append("<p>" + res.getString("snippet") + "</p>");
          } else {
            h.append("<p>No matching document can be found.</p>");
          }
          con.close();
        } 
      }
      catch (NumberFormatException nfex) {
        System.err.println("Snippet,doPost:" + nfex);
        h.append("<p>Invalid snippet reference.</p>");
      }
      catch (SQLException sqlex) {
        System.err.println("Snippet,doPost:" + sqlex);
      }

      h.append("</div>\n");
      out.println(h);
      handler = context.getNamedDispatcher("MenuFragment");
      rqst.setAttribute("page","index");
      handler.include(rqst, resp);
    } // end sync
  }

  
  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }  
}


