/* 
 * @(#) HWbyUser.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;


public class HWbyUser extends HttpServlet { 

  private java.sql.Connection con;
  private java.sql.ResultSet res;
  private StringBuffer SQLQuery = new StringBuffer();
  private StringBuffer httpOut = new StringBuffer();
  private java.sql.PreparedStatement findHW;

  public void init() throws ServletException {
  }


  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    httpOut.delete(0,httpOut.length());
    httpOut.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"");
    httpOut.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
    httpOut.append("<html><!-- Copyright 2003 Steven Lilley --><head>");
    httpOut.append("<title>ls-db</title>");
    httpOut.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />");
    httpOut.append("<meta name=\"robots\" content=\"noindex,nofollow\" />");
    httpOut.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" /></head>");
    httpOut.append("<body><h1>Leisure Services Database Server</h1>");
      
    try {
      con = DriverManager.getConnection("jdbc:mysql://ls-db:3306/itres","dbq","quizmaster");
      findHW = con.prepareStatement("SELECT * FROM hw_badged WHERE user LIKE ?");
      findHW.setString(1, "%" + rqst.getParameter("namebit") + "%");
      res = findHW.executeQuery();

      synchronized (this) {
        httpOut.append("<h2>Hardware matching search for '%" + rqst.getParameter("namebit") + "%'</h2>");
        httpOut.append("<table cellspacing=\"0\" cellpadding=\"4\" /><tr>");
        httpOut.append("<tr><th>Badge</th><th>Type</th><th>Description</th><th>Location</th>");
        httpOut.append("<th>User</th><th>OS</th><th>IP Addr</th></tr>");

        while (res.next()) {
        
          httpOut.append("<tr><td><a href=\"/servlets/machinedetails?badgeno=");
          httpOut.append(res.getString("badge") + "\">" + res.getString("badge") + "</a></td>");
          httpOut.append("<td>" + res.getString("type") + "</td>");
          httpOut.append("<td>" + res.getString("descrip") + "</td>");
          httpOut.append("<td>" + res.getString("location") + "</td>");
          httpOut.append("<td>" + res.getString("user") + "</td>");
          httpOut.append("<td>" + res.getString("os") + "</td>");
          httpOut.append("<td>" + res.getString("ipa") + "</td></tr>");
        }
        httpOut.append("</tr><tr><td colspan=\"7\" class=\"centre\">-- END --</td></tr>");
        httpOut.append("</table>");

        httpOut.append("<br clear=\"all\"><p class=\"cr\"><a href=\"/servlets/index\">HOME</a>");
        if ( rqst.getSession(false) != null ) {
          httpOut.append(" | <a href=\"/servlets/admin\">ADMIN</a>&nbsp;");
        }
        httpOut.append("</p></body></html>");
        out.println(httpOut);
        out.close();
        con.close();
      } // end sync
    }
    catch (SQLException sqlex) {
      out.println("<body><p>Something broke...<br>");
      out.println("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
    }
  }  
}


