/* 
 * @(#) WorksheetSave.java	0.1 2004/02/28
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Save a worksheet.
 *
 * @version		0.1 28 Feb 2004
 * @author		Steven Lilley
 */
public class WorksheetSave extends HttpServlet { 

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private StringBuffer SQLQuery = new StringBuffer();


  public void init() throws ServletException {
  try { 
      Class.forName("com.mysql.jdbc.Driver");
    }
    catch (ClassNotFoundException cnfex) {
    }
  }



  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();
    
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>Worksheet Save (Admin)</h2>");
    out.println(h);
    h.delete(0,h.length());

    synchronized (this) {  
      String pw = new String("");
      if ( rqst.getSession(false) != null ) {
        HttpSession adminSession = rqst.getSession(false);
        pw = (String)adminSession.getAttribute("password");
      }
      try {
        java.sql.Connection con = DriverManager.getConnection(DBCON,"itadmin",pw);

  /* THE SAVE OPTION IS RUN IN ALL CASES.  THIS MAKES IT EASIER TO COPY THE 
     DATA TO HW_BADGED IS THE "COMPLETE" OPTION IS CHOSEN */

        // CREATE THE WORKSHEET HEADER

        SQLQuery.delete(0,SQLQuery.length());
        SQLQuery.append("UPDATE wshead SET processor=?,memory=?,hdd=?,");
        SQLQuery.append("display=?,estcost=?,flcode=?,site=?,user=?,");
        SQLQuery.append("ordnum=?,orddate=?,delivered=?,sn=?,displaysn=?,");
        SQLQuery.append("badge=?,notes=?,ipa=? WHERE pcid=?");

        java.sql.PreparedStatement createWorksheet = 
          con.prepareStatement(SQLQuery.toString());
        createWorksheet.clearParameters();
        int modifiedRowCount = 0;
        
        createWorksheet.setString(1, rqst.getParameter("processor"));
        try {
          createWorksheet.setInt(2, Integer.parseInt(rqst.getParameter("memory")));
        }
        catch (NumberFormatException nfex) {
          createWorksheet.setInt(2, 0);
        }
        createWorksheet.setString(3, rqst.getParameter("hdd"));
        createWorksheet.setString(4, rqst.getParameter("display"));
        try {
          createWorksheet.setInt(5, Integer.parseInt(rqst.getParameter("estcost")));
        }
        catch (NumberFormatException nfex) {
          createWorksheet.setInt(5, 500);
        }
        createWorksheet.setString(6, rqst.getParameter("flcode"));
        createWorksheet.setString(7, rqst.getParameter("site"));
        createWorksheet.setString(8, rqst.getParameter("user"));
        createWorksheet.setString(9, rqst.getParameter("ordnum"));
        try {
          createWorksheet.setDate(10, java.sql.Date.valueOf(rqst.getParameter("orddate")));
        }
        catch (IllegalArgumentException iaex) {
          createWorksheet.setDate(10, new java.sql.Date((new java.util.Date()).getTime()));
        }
        try {
          createWorksheet.setDate(11, java.sql.Date.valueOf(rqst.getParameter("delivered")));
        }
        catch (IllegalArgumentException iaex) {
          createWorksheet.setDate(11, null);
        }
        createWorksheet.setString(12, rqst.getParameter("sn"));
        createWorksheet.setString(13, rqst.getParameter("displaysn"));
        try {
          createWorksheet.setInt(14, Integer.parseInt(rqst.getParameter("badge")));
        }
        catch (NumberFormatException nfex) {
          createWorksheet.setInt(14, 0 );
        }
        
        createWorksheet.setString(15, rqst.getParameter("notes"));
        createWorksheet.setString(16, rqst.getParameter("ipa"));

        createWorksheet.setString(17, rqst.getParameter("pcid"));
        modifiedRowCount = createWorksheet.executeUpdate();
        if ( modifiedRowCount != 1 ) {
          h.append("<p class=\"alert\">WSHEAD save failed!!</p>");
        }
        // UPDATE THE WORKSHEET ITEMS
        /* Start by retrieveing the item list from WORKSHEETS then loop 
           through and see if we get get the parameter that matches the 
           sequence number.  If we can then it's done, if we can't then 
           it isn't done. */

        SQLQuery.delete(0,SQLQuery.length());
        SQLQuery.append("SELECT wsitem FROM worksheets WHERE pcid=?");
        java.sql.PreparedStatement getChecklistItems = 
          con.prepareStatement(SQLQuery.toString());
        java.sql.PreparedStatement updateChecklistItem = 
          con.prepareStatement("UPDATE worksheets SET done=? WHERE pcid=? AND wsitem=?");
        getChecklistItems.clearParameters();
        getChecklistItems.setString(1, rqst.getParameter("pcid"));
        updateChecklistItem.setString(2, rqst.getParameter("pcid"));
        java.sql.ResultSet res = getChecklistItems.executeQuery();
        
        while (res.next()) {
          updateChecklistItem.setInt(3,res.getInt("wsitem"));
          if ( rqst.getParameter(res.getString("wsitem")) != null ) {
            // the parameter exists, therefore the task is done
            updateChecklistItem.setInt(1, -1);
          } else {
            updateChecklistItem.setInt(1,0);
          }
          modifiedRowCount = updateChecklistItem.executeUpdate();
        }
        
        // ADD ANY OPTIONAL ITEMS

        if ( rqst.getParameter("action").equalsIgnoreCase("complete")) {
          try {
            int badge = Integer.parseInt(rqst.getParameter("badge"));
            if ( badge > 0 ) {
              /* ADD THIS DATA TO THE HW_BADGED TABLE
                 THE SHEET MAY HAVE BEEN USED FOR AN EXISTING PC */
              SQLQuery.delete(0,SQLQuery.length());
              java.sql.PreparedStatement checkExisting = 
                con.prepareStatement("SELECT badge FROM hw_badged WHERE badge=?");
              checkExisting.setInt(1,badge);
              res = checkExisting.executeQuery();
              java.sql.PreparedStatement saveData;
              if ( res.next() ) {
                // GOT A MATCH
                SQLQuery.append("UPDATE hw_badged SET os=?,location=?,");
                SQLQuery.append("user=? WHERE badge=?");
                saveData = con.prepareStatement(SQLQuery.toString());
                saveData.clearParameters();
                saveData.setString(1, rqst.getParameter("os"));
                saveData.setString(2, rqst.getParameter("site"));
                saveData.setString(3, rqst.getParameter("user"));
                saveData.setInt(4, badge);
              } else {
                SQLQuery.append("INSERT INTO hw_badged (descrip,live,type,os,processor,");
                SQLQuery.append("memory,harddisk,ipa,cost,ccentre,location,user,");
                SQLQuery.append("ordnum,orddate,delivdate,sn,badge,notes) ");
                SQLQuery.append("SELECT descrip,live,type,os,processor,memory,hdd,ipa,estcost,");
                SQLQuery.append("flcode,site,user,ordnum,orddate,delivered,sn,badge,");
                SQLQuery.append("notes FROM wshead WHERE pcid=?");
                saveData = con.prepareStatement(SQLQuery.toString());
                saveData.setString(1, rqst.getParameter("pcid"));
              }
              modifiedRowCount = saveData.executeUpdate();
              if ( modifiedRowCount != 1 ) {
                h.append("<p class=\"alert\">WSHEAD save failed!!</p>");
              } else {
                // SAVED OKAY, SO NOW DELETE THE WORKSHEET
                SQLQuery.delete(0,SQLQuery.length());
                SQLQuery.append("DELETE wshead,worksheets FROM wshead,");
                SQLQuery.append("worksheets WHERE worksheets.pcid=wshead.pcid ");
                SQLQuery.append("AND wshead.pcid=?");
                java.sql.PreparedStatement deleteWorksheet = 
                  con.prepareStatement(SQLQuery.toString());
                deleteWorksheet.setString(1, rqst.getParameter("pcid"));
                modifiedRowCount = deleteWorksheet.executeUpdate();
                if ( modifiedRowCount == 0 ) {
                  h.append("<p class=\"alert\">Deletion of worksheet failed!!</p>");
                }
              }
            } else {
              h.append("<p class=\"alert\">Badge number can not be zero.</p>");
            }
          }
          catch (NumberFormatException nfex) {
            h.append("<p class=\"alert\">Badge number must be an integer.</p>");
          }
        } 
        if ( rqst.getParameter("action").equalsIgnoreCase("delete")) {
          SQLQuery.delete(0,SQLQuery.length());
          SQLQuery.append("DELETE wshead,worksheets FROM wshead,");
          SQLQuery.append("worksheets WHERE worksheets.pcid=wshead.pcid ");
          SQLQuery.append("AND wshead.pcid=?");
          java.sql.PreparedStatement deleteWorksheet = 
            con.prepareStatement(SQLQuery.toString());
          deleteWorksheet.setString(1, rqst.getParameter("pcid"));
          modifiedRowCount = deleteWorksheet.executeUpdate();
          if ( modifiedRowCount == 0 ) {
            h.append("<p class=\"alert\">Deletion of worksheet failed!!</p>");
          }
        }

        con.close();
      }
      catch (SQLException sqlex) {
        System.err.println("WorksheetSave,doPost:" + sqlex);
      }
    } // end sync
    
    RequestDispatcher handler = context.getNamedDispatcher("OutWorksheetList");
    handler.include(rqst, resp);

    h.append("</div>");
    out.println(h);

    handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


