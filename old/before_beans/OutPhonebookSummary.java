/* 
 * @(#) OutPhonebookSummary.java		0.3 2004/06/24
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */

package uk.co.channele.itres;

import uk.co.channele.itres.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Part of my IT Resource suite of servlets.
 *
 * This version includes a call to the official intranet 'phone book.
 * The aim is to search any source of data that might be useful, making 
 * this the search tool of choice.
 *
 * @version		0.3 24 June 2004
 * @author		Steven Lilley
 */

public class OutPhonebookSummary extends HttpServlet {

	private static final DateFormat ITEMDATE = 
		DateFormat.getDateInstance(DateFormat.MEDIUM);
	private String userSearch = new String();
	private ServletContext context;
	
	public void init() {
		context = getServletContext();
	}
	
	public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {

		PrintWriter out = resp.getWriter();
		StringBuffer h = new StringBuffer();

		synchronized (this) {
			String searchFor = (String)rqst.getAttribute("find");
			out.println(h);
		}	// end sync
	}
}



