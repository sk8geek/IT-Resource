/* 
 * @(#) RequestList.java	0.1 2003/12/08
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Request system.
 *
 * @version		0.1 8 Dec 2003
 * @author		Steven Lilley
 */
/**
 * Request system.
 *
 * @version		0.1 6 Dec 2003
 * @author		Steven Lilley
 */
public class Requests extends HttpServlet { 

  private static final String DBCON = new String("jdbc:mysql://ls-db:3306/itres");
  private static final DateFormat ITEMDATE = DateFormat.getDateInstance(DateFormat.MEDIUM);
  private final String myFilePath = new String("/home/httpd/html/documents/");
  private final String litstegFilePath = new String("/home/httpd/html/litsteg/");

  private java.sql.Connection con;
  private java.sql.ResultSet res;
  private java.sql.Statement stmt;
  private StringBuffer SQLQuery = new StringBuffer();
  private StringBuffer httpOut = new StringBuffer();

  private File flag;
  private ServletContext context;
  private Thread statsEngine; 



  public void init() {
  }



  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    
    doHead();
    out.println(httpOut);

    httpOut.delete(0,httpOut.length());

    synchronized (this) {
      httpOut.append("<h2>Requests</h2><div class=\"half\">");

      httpOut.append("<form method=\"post\" action=\"/servlets/user\">\n");
      httpOut.append("<b>Submit and track requests/faults: </b><br />\n");
      httpOut.append("Badge#: <input type=\"text\" name=\"badgeno\" size=\"6\" maxlength=\"6\" />\n");
      httpOut.append("&nbsp;<select name=\"action\">\n");
      httpOut.append("<option selected value=\"track\">List requests</option>\n");
      httpOut.append("<option value=\"fault\">Submit request</option>\n");
      httpOut.append("<option value=\"details\">Machine details</option>\n</select> \n");
      httpOut.append("<input type=\"submit\" value=\"Go\" /></form>");

      httpOut.append("<form method=\"post\" action=\"/servlets/bycostcentre\">");
      httpOut.append("<b>Hardware by cost centre: </b>");
      httpOut.append("<input type=\"text\" name=\"ccentre\" size=\"4\" maxlength=\"4\" />");
      httpOut.append("<input type=\"submit\" value=\"Go\" /></form>");
      httpOut.append("</div>");
      httpOut.append("<div class=\"half\">");
      
      try {
        java.sql.Connection con = DriverManager.getConnection(
          DBCON,"dbq","quizmaster");

        SQLQuery.delete(0,SQLQuery.length());
        SQLQuery.append("SELECT COUNT(reqnum),SUM(duration) ");
        SQLQuery.append("FROM requests WHERE closed IS NULL");
        java.sql.Statement stmt = con.createStatement();
        java.sql.ResultSet res = stmt.executeQuery(SQLQuery.toString());
        res.next();

        httpOut.append("<h3>Open Requests</h3>");
        httpOut.append("<table cellspacing=\"0\" cellpadding=\"4\" />");
        httpOut.append("<tr><td>Number of requests:</td><td class=\"right\"><b>");
        httpOut.append(res.getString("count(reqnum)") + "</b></td></tr>");
        httpOut.append("<tr><td>Estimated total (hrs:mins):</td><td class=\"right\"><b>");
        httpOut.append(getHoursOnly(res.getString("sum(duration)")) + "</b></td></tr>");

        SQLQuery.append(" AND target<now()");
        stmt = con.createStatement();
        res = stmt.executeQuery(SQLQuery.toString());
        res.next();

        httpOut.append("<tr><td>Requests past target date:</td><td class=\"right\"><b>");
        httpOut.append(res.getString("count(reqnum)") + "</b></td></tr>");
        httpOut.append("<tr><td>Estimated total (hrs:mins):</td><td class=\"right\"><b>");
        httpOut.append(getHoursOnly(res.getString("sum(duration)")) + "</b></td></tr></table></div>");

        
        httpOut.append("</div>");
        out.println(httpOut);
        res.close();
        stmt.close();
        con.close();
      }
      catch (NumberFormatException nfex) {
        httpOut.append("<p>Invalid request number given.</p></body></html>");
        out.println(httpOut);
      }
      catch (SQLException sqlex) {
        httpOut.append("<p>Something broke...<br>");
        httpOut.append("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
        out.println(httpOut);
      }
    } // end sync


    httpOut.delete(0,httpOut.length());
    httpOut.append("<br clear=\"all\"><p class=\"cr\"><a href=\"/servlets/index\">HOME</a>");
    if ( rqst.getSession(false) != null ) {
      httpOut.append(" | <a href=\"/servlets/admin\">ADMIN</a>&nbsp;");
    }
    httpOut.append("</p></body></html>");
    out.println(httpOut);
  }
  
  private String getHoursOnly(String duration) {
    StringBuffer sb = new StringBuffer();
    if ( duration == null ) {
      return "0:00";
    }
    if ( duration.equalsIgnoreCase("0") ) {
      return "0:00";
    }
    if ( duration.length() >= 4 ) {
      sb.append(duration.substring(0,duration.length()-2));
      sb.insert(sb.length()-2,":");
    }
    if ( sb.length() == 3 ) {
      sb.insert(0,"0");
    }
    return sb.toString();
  }



  private void doHead() {
    httpOut.delete(0,httpOut.length());
    httpOut.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    httpOut.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    httpOut.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    httpOut.append("<title>ls-db</title>");
    httpOut.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    httpOut.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    httpOut.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    httpOut.append("<body>\n<h1>Leisure Services Database Server</h1>");
  }

 
}


