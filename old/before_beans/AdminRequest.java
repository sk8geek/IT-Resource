/* 
 * @(#) AdminRequest.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;


public class AdminRequest extends HttpServlet { 



  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>Request Submission (Admin)</h2>");

    try {
      java.sql.ResultSet results = (java.sql.ResultSet) rqst.getAttribute("results");
      if (results.next()) {
        h.append("<table class=\"boxed\" cellspacing=\"0\" cellpadding=\"4\">");
        h.append("<tr><td class=\"grey\">Badge:</td><td>" + results.getString("badge") + "</td>");
        h.append("<td class=\"grey\">Live:</td><td>" + results.getString("live") + "</td></tr>");
        h.append("<tr><td class=\"grey\">Type:</td><td>" + results.getString("type") + "</td>");
        h.append("<td class=\"grey\">Description:</td><td>" + results.getString("descrip") + "</td></tr>");
        h.append("<tr><td class=\"grey\">Team:</td><td colspan=\"3\">" + results.getString("team") + "</td></tr>");
        if ( results.getString("parent") != null ) {
          h.append("<tr><td class=\"grey\">Parent:</td>");
          h.append("<td><a href=\"/servlets/machinedetails?badgeno=");
          h.append(results.getString("parent") + "\">" + results.getString("parent"));
          h.append("</a></td></tr>");
        }
        h.append("<tr><td class=\"grey\">OS:</td><td>" + results.getString("os") + "</td>");
        h.append("<td class=\"grey\">IP Addr:</td><td>" + results.getString("ipa") + "</td></tr>");
        h.append("<tr><td class=\"grey\">Cover:</td><td colspan=\"3\">" + results.getString("maintby") + "</td></tr>");
        h.append("<tr><td class=\"grey\">Start:</td><td>" + results.getString("maintstart") + "</td>");
        h.append("<td class=\"grey\">Finish:</td><td>" + results.getString("maintfin") + "</td></tr>");
        h.append("</table>");

        h.append("<table cellspacing=\"0\" cellpadding=\"4\">");
        h.append("<form method=\"post\" action=\"/servlets/acceptadmin\">");
        h.append("<tr><td class=\"grey\">Contact:</td>");
        h.append("<td><input type=\"text\" name=\"contact\" maxlength=\"30\" ");
        h.append("value=\"" + results.getString("user") + "\" /></td></tr>");
        h.append("<tr><td class=\"grey\">Phone:</td>");
        h.append("<td><input type=\"text\" name=\"phoneno\" maxlength=\"15\" /></td></tr>");
        h.append("<tr><td class=\"grey\">Site:</td><td><input type=\"text\" name=\"sitename\" maxlength=\"60\" ");
        h.append("value=\"" + results.getString("location") + "\" /></td></tr>");
        h.append("<tr><td class=\"grey\">Task type:</td><td><select name=\"tasktype\">");
        ServletContext sc = getServletContext();
          ArrayList a = (ArrayList)sc.getAttribute("tasktypes");
          int c;
          for (c=0; c < a.size(); c++) {
            DropDownItem d = (DropDownItem)a.get(c);
            h.append("<option value=\"" + d.code + "\">" + d.desc + "</option>");
          }
          h.append("</select></td></tr>");
          h.append("<tr><td  class=\"grey\" colspan=\"2\">Request:</td></tr><tr><td colspan=\"2\">");
          h.append("<textarea name=\"faultdesc\" rows=\"5\" cols=\"42\">");
          h.append("</textarea></td></tr>");
          h.append("<tr><td class=\"grey\">Priority (1-5):</td><td><input type=\"text\" ");
          h.append("name=\"priority\" maxlength=\"1\" size=\"1\" value=\"3\" /></td></tr>");
          h.append("<tr><td  class=\"grey\" colspan=\"2\">Work done:</td></tr><tr><td colspan=\"2\">");
          h.append("<textarea name=\"workdone\" rows=\"5\" cols=\"42\">");
          h.append("</textarea></td></tr>");
          h.append("<tr><td class=\"grey\">Done by (initials):</td><td><input type=\"text\" ");
          h.append("name=\"worker\" maxlength=\"3\" size=\"3\" /></td></tr>");
          h.append("<tr><td>Time (hhh:mm:ss):</td><td><input type=\"text\" ");
          h.append("name=\"duration\" maxlength=\"9\" size=\"9\" /></td></tr>");
          h.append("<tr><td class=\"grey\">Stupidy rating:</td><td><select name=\"techlevel\">");
          ArrayList b = (ArrayList)sc.getAttribute("techlevels");
          for (c=0; c < b.size(); c++) {
            DropDownItem d = (DropDownItem)b.get(c);
            h.append("<option value=\"" + d.code + "\">" + d.desc + "</option>");
          }
          h.append("</select></td></tr>");
          h.append("<tr><td class=\"grey\">Close request:</td><td><input type=\"checkbox\" name=\"closenow\"></td></tr>");
          h.append("<tr><td colspan=\"2\"><input type=\"submit\" value=\"Submit Request\">");
          h.append("<input type=\"reset\" value=\"Reset!\">");
          h.append("<input type=\"hidden\" name=\"badgeno\" value=\"");
          h.append(rqst.getParameter("badgeno") + "\" /></tr></td>");
          h.append("</form></table>");
          h.append("</div>");

        } else {
          h.append("<p>No equipment with badge number ");
          h.append(rqst.getParameter("badgeno") + " can be found.</p>");
        }
      }
      catch ( SQLException sqlex) {
        out.println("<body><p>Something broke...<br>");
        out.println("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
      }

    h.append("</div>\n");

    out.println(h);
    RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


