/* 
 * @(#) SocketTester.java	0.2 2004/07/10
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
/**
 * Part of my IT Resource suite of servlets.
 *
 * @version		0.2 10 July 2004
 * @author		Steven Lilley
 */
public class InitHandler extends HttpServlet { 

	private static final String DBCON = new String(
		"jdbc:mysql://ls-db:3306/itres");
	private java.sql.PreparedStatement getDetailsByBadge;
	private RequestDispatcher handler;
	private String currentAction;
	private ServletContext context;
		
	public void init() throws ServletException {
		context = getServletContext();
		try { 
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException cnfex) {
			System.out.println("InitHandler, init:" + cnfex);
		}
	}

	public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {
		String sql_getDetailsByBadge = new String(
			"SELECT DISTINCT * FROM hw_badged WHERE badge=?");
		try {
			synchronized (this) {
				int currentBadge = 
					Integer.parseInt(rqst.getParameter("badgeno"));
				currentAction = rqst.getParameter("action");
				java.sql.Connection con = 
					DriverManager.getConnection(DBCON,"dbq","quizmaster");
				if (currentAction.equalsIgnoreCase("details") 
					| currentAction.equalsIgnoreCase("fault") ) {
					java.sql.PreparedStatement getDetailsByBadge = 
						con.prepareStatement(sql_getDetailsByBadge);
					getDetailsByBadge.setInt(1, 
						Integer.parseInt(rqst.getParameter("badgeno")));
					java.sql.ResultSet results = 
						getDetailsByBadge.executeQuery();
					rqst.setAttribute("results",results);
					con.close();
					if (currentAction.equalsIgnoreCase("fault")) {
						if (rqst.getRemoteHost().equalsIgnoreCase(
							"cmbc_gateway_1") || 
							rqst.getRemoteHost().equalsIgnoreCase(
							"cmbc_wm_gateway")) {
							handler = rqst.getRequestDispatcher(
								"/disallowproxy");
							handler.forward(rqst, resp);
						} else { 
							if (rqst.getSession(false) == null) {
								handler = rqst.getRequestDispatcher(
									"/submitrequest");
								handler.forward(rqst, resp);
							} else {
								handler = rqst.getRequestDispatcher(
									"/adminrequest");
								handler.forward(rqst, resp);
							}
						}
					} else {
						handler = rqst.getRequestDispatcher(
							"/machinedetails");
						handler.forward(rqst, resp);
					}					
				} else {
					handler = rqst.getRequestDispatcher("/requestlist");
					handler.forward(rqst, resp);
				}
			}	// synchronized ends
		}
		catch (NumberFormatException nfex) {
			currentAction = rqst.getParameter("action");
			if (currentAction.equalsIgnoreCase("track")) {
				handler = rqst.getRequestDispatcher("/requestsummary");
				handler.forward(rqst, resp);
			}
			if (currentAction.equalsIgnoreCase("details")) {
				handler = rqst.getRequestDispatcher("/hardwaresummary");
				handler.forward(rqst, resp);
			}
			resp.setContentType("text/html");
			PrintWriter out = resp.getWriter();
			out.println(doHead());
			StringBuffer h = new StringBuffer();
			h.append(rqst.getHeader("REFERER") + "\">");
			h.append("<link rel=\"stylesheet\" type=\"text/css\" ");
			h.append("href=\"/lsdb.css\" />\n</head>");
			h.append("<body><h1>Community Services</h1>");
			h.append("<h2>Invalid Badge Number</h2>");
			h.append("<div class=\"content\">");
			h.append("<p>You entered an invalid badge number. ");
			h.append("Please <a href=\"");
			h.append(rqst.getHeader("REFERER") + "\">try again.</a>");
			h.append("</p></div>");
			out.println(h);
			handler = context.getNamedDispatcher("MenuFragment");
			rqst.setAttribute("page","inithandler");
			handler.include(rqst, resp);
			out.close();
		}
		catch (SQLException sqlex) {
		}
	}

	private String doHead() {
		StringBuffer h = new StringBuffer();
		h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
		h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
		h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
		h.append("<title>Community Services</title>");
		h.append("<meta http-equiv=\"content-type\" ");
		h.append("content=\"text/html; charset=iso-8859-1\" />\n");
		h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
		h.append("<meta http-equiv=\"refresh\" content=\"10;URL=");
		return h.toString();
	}

}


