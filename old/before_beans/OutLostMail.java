/* 
 * @(#) OutLostMail.java	0.1 2006/03/05
 * 
 * Copyright (C) 2006 Calderdale MBC
 * parts Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.text.DateFormat;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Outputs two tabular lists of lost email, sent and recieved items.
 *
 * @version       0.1 5 March 2006
 * @author        Steven Lilley
 */
public class OutLostMail extends HttpServlet {

    static final DateFormat itemDateTime = 
        DateFormat.getDateTimeInstance(DateFormat.MEDIUM, 
        DateFormat.MEDIUM);
    private final String sqlListSent = "SELECT lostmail_header.msgid, "
        + "procdate, recips, ident, recip, name FROM lostmail_header LEFT JOIN "
        + "lostmail_detail USING (msgid) WHERE sender=? "
        + "ORDER BY lostmail_header.msgid";
    private final String sqlListReceived = "SELECT lostmail_header.msgid, "
        + "sender, procdate, recips, ident FROM lostmail_detail LEFT JOIN "
        + "lostmail_header USING (msgid) where recip=? "
        + "ORDER BY lostmail_header.msgid";
    private String sqlDB;
    private String sqlUser;
    private String sqlPassword;
    private ServletContext context;

    public void init() {
        context = getServletContext();
        sqlDB = context.getInitParameter("sqlDB");
        sqlUser = context.getInitParameter("sqlUser");
        sqlPassword = context.getInitParameter("sqlPassword");
    }
    
    /**
     * Handle a POST request.
     * @param rqst The servlet request.
     * @param resp The servlet response.
     */
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            Connection con;
            PreparedStatement sentItems;
            PreparedStatement receivedItems;    
            ResultSet results;
            StringBuffer h = new StringBuffer("<!-- Sent Items -->\n");
            String gwid = "";
            int msgId = 0;
            if (rqst.getParameter("gwid") != null) {
                gwid = rqst.getParameter("gwid");
            }
            try {
                con = DriverManager.getConnection(sqlDB, sqlUser, sqlPassword);
                out.println("<p><em>This list is not accurate.</em></p>");
                out.print("<p>This utility will (hopefully) give you a list of ");
                out.println("the emails that you have lost recently.</p>");
                out.print("<p>The log file has proved quite challenging to ");
                out.print("process.  Better results would be achievable given ");
                out.print("time, but I wanted to make information available ");
                out.print("before it expires.</p>");
                out.println("<h3>Sent Items</h3>");
                out.println("<table class=\"boxed\">");
                out.print("<thead><tr><th>Date/Time</th><th>Recipients</th>");
                out.println("</tr></thead><tbody>");
                sentItems = con.prepareStatement(sqlListSent);
                sentItems.clearParameters();
                sentItems.setString(1, gwid);
                results = sentItems.executeQuery();
                while (results.next()) {
                    if (results.getInt("msgid") != msgId) {
                        out.print("<tr class=\"grey\"><td>");
                        out.print(itemDateTime.format(results.getTimestamp("procdate")));
                        out.print("</td><td>No. of recipients: ");
                        out.print(results.getInt("recips"));
                        out.println("</td></tr>");
                        msgId = results.getInt("msgid");
                    }
                    out.print("<tr>");
                    out.print("<td>&nbsp</td><td>");
                    out.print(results.getString("recip"));
                    if (results.getString("name") != null) {
                        out.print(" " + results.getString("name"));
                    }
                    out.println("</td></tr>");
                }
                out.println("</tbody></table>");
                out.println("<h3>Received Items</h3>");
                out.println("<table class=\"boxed\">");
                out.print("<thead><tr><th>Date/Time</th><th>Sender</th>");
                out.println("<th>Recipients</th></tr></thead><tbody>");
                receivedItems = con.prepareStatement(sqlListReceived);
                receivedItems.clearParameters();
                receivedItems.setString(1, gwid);
                results = receivedItems.executeQuery();
                while (results.next()) {
                    out.print("<tr><td>");
                    out.print(itemDateTime.format(results.getTimestamp("procdate")));
                    out.print("</td><td>");
                    out.print(results.getString("sender"));
                    out.print("</td><td>");
                    out.print(results.getInt("recips"));
                    out.println("</td></tr>");
                }
                out.println("</tbody></table>");
                results.close();
                con.close();
            } catch (SQLException sqlex) {
                context.log("OutLostMail, doPost:", sqlex);
            }
        } // end sync
    }
}

