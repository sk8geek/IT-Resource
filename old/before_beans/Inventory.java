/* 
 * @(#) Inventory.java    0.1 2005/09/25
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Hardware and Software Inventory Servlet.
 * Allows users to query the database to summarise hardware and software by 
 * information.  e.g. List hardware by location or cost centre.
 * Users can then add comments against items to request changes.
 *
 * @version       0.1 25 September 2005
 * @author        Steven Lilley
 */
public class Inventory extends HttpServlet { 
    
    /** The context in which this servlet is running. */
    private ServletContext context;
    /** The SQL database connection string. */
    private String sqlDB;
    /** The user name for the SQL database. */
    private String sqlUser;
    /** The password for the SQL database. */
    private String sqlPassword;

    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        sqlDB = context.getInitParameter("sqlDB");
        sqlUser = context.getInitParameter("sqlUser");
        sqlPassword = context.getInitParameter("sqlPassword");
    }

    /** 
     * Presents the user with a query form.
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            RequestDispatcher handler;
            resp.setContentType("text/html");
            rqst.setAttribute("page","inventory");
            PrintWriter out = resp.getWriter();
            boolean hasParameters = false;
            String paramName = new String();
            Enumeration params;
            out.println(HouseKeeper.doHead("Inventory", 
                "Hardware Inventory", "inventory", HouseKeeper.normalDelay));
            for (params = rqst.getParameterNames(); 
                params.hasMoreElements(); ) {
                if (!hasParameters) {
                    hasParameters = true;
                }
                paramName = (String)params.nextElement();
                if (paramName.equalsIgnoreCase("badge")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineDetails");
                    handler.include(rqst, resp);
                } else
                if (paramName.equalsIgnoreCase("sitename")) {
                    handler = context.getNamedDispatcher(
                        "OutHWBySite");
                    handler.include(rqst, resp);
                } else
                if (paramName.equalsIgnoreCase("ccentre")) {
                    handler = context.getNamedDispatcher(
                        "OutMachinesByCC");
                    handler.include(rqst, resp);
                }
            }
            if (!hasParameters) {
                out.println("<h3>List by Cost Centre</h3>");
                out.println(queryForm("", false, "", ""));
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called for many different user operations.
     * This class handles user registration and preferences changes.
     * The <code>userop</code> parameter determines which method is called.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "register");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            String action = new String();
            if (rqst.getParameter("querytype") != null) {
                action = rqst.getParameter("querytype");        
                out.println(HouseKeeper.doHead("Inventory", 
                    "Hardware Inventory", "index", HouseKeeper.normalDelay));
                if (action.equalsIgnoreCase("list")) {
                    handler = context.getNamedDispatcher(
                        "OutMachinesByCC");
                    handler.include(rqst, resp);
                } else 
                if (action.equalsIgnoreCase("detail")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineDetails");
                    handler.include(rqst, resp);
                } else
                if (action.equalsIgnoreCase("summary")) {
                    handler = context.getNamedDispatcher(
                        "OutMachineSummary");
                    handler.include(rqst, resp);
                } else {
                    out.println(queryForm("", false, "", ""));
                }
                out.println("</div>");
            } else {
                out.println(HouseKeeper.doHead("Inventory", 
                    "Hardware Inventory", "index", HouseKeeper.normalDelay));
                out.println(queryForm("", false, "", ""));
                out.println("</div>");
            }
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
    
    /**
     * A method that returns a query form.
     * @param ccentre cost centre
     * @param filterhwtype true if hardware type is filtered
     * @param hwtype the hardware type
     * @param errors list of errors
     * @return an HTML string containing a table and form.
     */
    private String queryForm(String ccentre, boolean filterhwtype, 
        String hwtype, String errors) {
        StringBuffer h = new StringBuffer();
        ArrayList teams = (ArrayList)context.getAttribute("teams");
        ArrayList sites = (ArrayList)context.getAttribute("sites");
        ArrayList hwtypes = (ArrayList)context.getAttribute("hwtypes");
        h.append("<form method=\"post\" action=\"inventory\" ");
        h.append("name=\"queryform\">\n");
        h.append("<table class=\"borderless\">\n");
        // cost centre
        h.append("<tr><td>cost centre:</td>");
        h.append("<td colspan=\"2\">");
        h.append("<input type=\"text\" maxlength=\"4\" ");
        h.append("name=\"ccentre\" size=\"4\"");
        if (ccentre.length() > 0) {
            h.append(" value=\"" + ccentre + "\"");
        }
        if (errors.indexOf("ccentre") > -1) {
            h.append(" class=\"reqd\"");
        }
        h.append("></td></tr>");
        h.append("<tr><td colspan=\"3\">FILTERS</td></tr>");
        // hwtype
        if (hwtype != null) {
            h.append("<tr><td>hardware type:</td>");
            h.append("<td><input type=\"checkbox\" ");
            h.append("name=\"filterhwbytype\" value=\"filterhwtype\"");
            if (filterhwtype) {
                h.append(" checked");
            }
            h.append("></td>\n");
            h.append("<td>");
            h.append("<select name=\"hwtype\">");
            for (int c = 0; c < hwtypes.size(); c++) {
                DropDownItem d = (DropDownItem)hwtypes.get(c);
                h.append("<option value=\"" + d.code + "\"");
                if (d.code.equalsIgnoreCase(hwtype)) {
                    h.append(" selected");
                }
                h.append(">" + d.desc + "</option>");
            }
            h.append("</select></td></tr>");
        }
        // buttons 
        h.append("<tr><td colspan=\"3\">");
        h.append("<input type=\"hidden\" name=\"querytype\" ");
        h.append("value=\"list\">");
        h.append("<input type=\"submit\" value=\"Submit\">");
        h.append("<input type=\"reset\" value=\"Reset\">");
        // close table 
        h.append("</td></tr></table>\n");
        h.append("</form>\n");
        // add explainations where necessary 
        if (errors.indexOf("inuse") > -1) {
            // UNAME is valid but already in use 
            h.append("<p class=\"nb\">The user name is valid but has ");
            h.append("been taken.  Please try something different.</p>");
        }
        return h.toString();
    }
}
