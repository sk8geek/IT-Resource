/* 
 * @(#) WorksheetCreate.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Create a new worksheet for a PC.
 *
 * @version		0.1 6 Feb 2004
 * @author		Steven Lilley
 */
public class WorksheetCreate extends HttpServlet { 

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private static DateFormat TIMEDISPLAY = 
    DateFormat.getTimeInstance(DateFormat.SHORT);
  private RequestDispatcher handler;
  ServletContext context;


  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    h.append("<h2>New Worksheet (Admin)</h2>");

    if ( rqst.getParameter("pcid") != null ) {
      h.append("<h3>" + rqst.getParameter("pcid") + "</h3>");

      h.append("<table class=\"boxed\" cellspacing=\"0\" cellpadding=\"4\">");
      h.append("<form method=\"post\" action=\"/servlets/worksheets\">");

      h.append("<tr><td class=\"grey\">Description:</td>");
      h.append("<td><input type=\"text\" name=\"descrip\" size=\"20\" maxlength=\"30\" ");
      h.append("value=\"Clone PC\" /></td>");

      h.append("<td class=\"grey\">OS:</td><td><select name=\"os\">");
      ArrayList a = (ArrayList)context.getAttribute("os");
      int c;
      for (c=0; c < a.size(); c++) {
        DropDownItem d = (DropDownItem)a.get(c);
        h.append("<option value=\"" + d.code + "\">" + d.desc + "</option>");
      }
      h.append("</select></td></tr>");

      h.append("<tr><td class=\"grey\">Type:</td>");
      h.append("<td><input type=\"radio\" name=\"type\" ");
      h.append("value=\"PC\" checked> PC ");
      h.append("<input type=\"radio\" name=\"type\" ");
      h.append("value=\"Laptop\"> Laptop</td>");

      h.append("<td class=\"grey\">Badge:</td>");
      h.append("<td><input type=\"text\" name=\"badge\" size=\"6\" maxlength=\"6\" ");
      h.append("value=\"\" /></td></tr>");

      h.append("<tr><td class=\"grey\">Display:</td>");
      h.append("<td><input type=\"text\" name=\"display\" size=\"30\" maxlength=\"30\" ");
      h.append("value=\"Iiyama \" /></td>");

      h.append("<td class=\"grey\">Processor:</td>");
      h.append("<td><input type=\"text\" name=\"processor\" size=\"15\" maxlength=\"15\" ");
      h.append("value=\"Celeron\" /></td></tr>");

      h.append("<tr><td class=\"grey\">Memory:</td>");
      h.append("<td><input type=\"text\" name=\"memory\" size=\"6\" maxlength=\"6\" ");
      h.append("value=\"256\" /></td>");

      h.append("<td class=\"grey\">Hard disk:</td>");
      h.append("<td><input type=\"text\" name=\"hdd\" size=\"10\" maxlength=\"10\" ");
      h.append("value=\"Gb\" /></td></tr>");

      h.append("<tr><td class=\"grey\">Site:</td>");
      h.append("<td><input type=\"text\" name=\"site\" size=\"30\" maxlength=\"30\" ");
      h.append("value=\"\" /></td>");

      h.append("<td class=\"grey\">User:</td>");
      h.append("<td><input type=\"text\" name=\"user\" size=\"20\" maxlength=\"20\" ");
      h.append("value=\"\" /></td></tr>");

      h.append("<tr><td class=\"grey\">FL Code:</td>");
      h.append("<td><input type=\"text\" name=\"flcode\" size=\"8\" maxlength=\"8\" ");
      h.append("value=\"\" /></td>");

      h.append("<td class=\"grey\">Est. cost:</td>");
      h.append("<td><input type=\"text\" name=\"estcost\" size=\"6\" maxlength=\"6\" ");
      h.append("value=\"\" /></td></tr>");

/*
      h.append("<tr><td class=\"grey\"><b>Software:</b></td>");
      h.append("<td colspan=\"3\">MS Office: <input type=\"checkbox\" name=\"msoffice\"> ");
      h.append("CorpFin: <input type=\"checkbox\" name=\"corpfin\"> ");
      h.append("Web Enabled: <input type=\"checkbox\" name=\"wecf\"></td></tr>");
*/

      h.append("<tr><td  class=\"grey\" valign=\"top\">Notes:</td><td colspan=\"3\">");
      h.append("<textarea name=\"notes\" rows=\"5\" cols=\"42\">");
      h.append("</textarea></td></tr>");

      h.append("<tr><td colspan=\"4\"><input type=\"submit\" value=\"Save\">");
      h.append("<input type=\"reset\" value=\"Reset\"></tr></td>");
      h.append("<input type=\"hidden\" name=\"pcid\" value=\"");
      h.append(rqst.getParameter("pcid") + "\" />");
      h.append("</form></table>");

    } else {

      h.append("<p>The PC ID can not be null. ");
      h.append("<a href=\"/servlets/worksheets\" />Try again?</a></p>");
    }

    h.append("</div>\n");

    out.println(h);
    RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


