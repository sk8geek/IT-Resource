/* 
 * @(#) SiteList.java	0.1 2003/04/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */

package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * List sites.
 *
 * @version		0.1 6 Apr 2003
 * @author		Steven Lilley
 */
public class SiteList extends HttpServlet { 

    public void doPost(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException {
      
        // this handles the initial request, prompt for a badge number

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"");
        out.println("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
        out.println("<html><!-- Copyright 2003 Steven Lilley --><head>");
        out.println("<title>ls-db, IT Resources</title>");
        out.println("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />");
        out.println("<meta name=\"robots\" content=\"noindex,nofollow\" />");
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" /></head>");
        
        /*  First run a query to see how many different sites we get 
            based on the users criteria.  If we get more than one then 
            we want to present a drop down list for them to choose from.
            
            If we only have one site then we need to list the equipment
            
            If we can't find a site we need a drop down of all sites for 
            the user to choose from.  */ 
            
      try {
      
         DriverManager.registerDriver(new com.mysql.jdbc.Driver());
         java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://ls-db:3306/itres","dbq","quizmaster");
         java.sql.Statement smnt = con.createStatement();

         StringBuffer SQLQuery = new StringBuffer("SELECT location, COUNT(location) AS items FROM hw_badged WHERE ");
         SQLQuery.append("location LIKE '%" + req.getParameter("sitename") + "%' GROUP BY location");
         java.sql.ResultSet res = smnt.executeQuery( SQLQuery.toString() );
         res.last();
         
         if ( res.getRow() > 1 ) {
         
             // we have more than one site that matches
             
             out.println("<body><h1>Hardware Inventory</h1><h2>Results</h2>");
             out.println("<p>More than one site matched your search criteria:</p>");
             
             out.println("<table cellspacing=\"4\" cellpadding=\"2\"><tr>");
             out.println("<th>Location</th><th>Items</th><th>&nbsp;</th></tr>");
             res.beforeFirst();

             while (res.next()) {
                 
                 out.println("<tr><td>" + res.getString("location") + "</td>");
                 out.println("<td>" + res.getString("items") + "</td></tr>");
             }
                 
             out.println("</table><p>Please select a row to refine your search.</p>");
             
         } else if ( res.getRow() == 0 ) {
         
             // no sites matched the criteria
             
             SQLQuery.delete(0, SQLQuery.length());
             SQLQuery.append("SELECT location FROM hw_badged WHERE (location IS NOT NULL AND ");
             SQLQuery.append("location<>'') GROUP BY location");
             res = smnt.executeQuery( SQLQuery.toString() );

             out.println("<body><h1>Hardware Inventory</h1><h2>Results</h2>");
             out.println("<p>No matches where found for your search criteria.</p>");
             out.println("<p>Please select a site from the drop-down list.");
             
             out.println("<form method=\"post\" action=\"/servlets/sitelist\">");
             out.println("Site: <select name=\"sitename\">");
             
             while (res.next()) {
                 
                 out.println("<option selected value=\"" + res.getString("location") + "\">");
                 out.println(res.getString("location") + "</option>");
             }
                 
             out.println("</select><input type=\"submit\" value=\"Go\" />");
             
         } else if (res.getRow() == 1 ) {
         
             // only one site matches, list equipment
             
             SQLQuery.delete(0, SQLQuery.length());
             SQLQuery.append("SELECT badge, type, descrip, user FROM hw_badged WHERE location='");
             SQLQuery.append(res.getString("location") + "'");
             res = smnt.executeQuery( SQLQuery.toString() );

             out.println("<body><h1>Hardware Inventory</h1><h2>Results</h2>");
             out.println("<table cellspacing=\"4\" cellpadding=\"2\"><tr>");
             out.println("<th>Badge</th><th>Type</th><th>Description</th><th>User</th></tr>");

             while (res.next()) {
              
                 out.println("<tr><td>" + res.getString("badge") + "</td>");
                 out.println("<td>" + res.getString("type") + "</td>");
                 out.println("<td>" + res.getString("description") + "</td>");
                 out.println("<td>" + res.getString("user") + "</td></tr>");
             }

             out.println("</table><p>[END]</p>");

         }
             
         out.println("</body></html>");
         
         res.close();
         smnt.close();
         con.close();
  
      }
      
      catch (SQLException sqlex) {
      
         String msg = new String(sqlex.getMessage());
         
         out.println("<body><p>Something broke...<br>");
         out.println("SQL Exception:" + msg + "</p></body></html>");
      
      }
   }
}


