/* 
 * @(#) HWbySite.java	0.2 2006/08/27
 * 
 * Copyright (C) 2003-2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Output a list of hardware in site order.
 *
 * @version       0.2 27 August 2006
 * @author        Steven Lilley
 */
public class HWbySite extends HttpServlet { 

    private Connection con;
    private ResultSet res;
    private StringBuffer SQLQuery = new StringBuffer();
    private PreparedStatement findHW;
    private String sqlFinHardware = "SELECT * FROM hw_badged WHERE location LIKE ? ORDER BY badge";
    private InitialContext initCnxt;
    private Context nameCnxt;
    private DataSource dataSrc;
    
    public void init() throws ServletException {
        try {
            initCnxt = new InitialContext();
            nameCnxt = (Context)initCnxt.lookup("java:comp/env");
            dataSrc = (DataSource)nameCnxt.lookup("jdbc/itres");
        } catch (NamingException nex) {
            log.severe("Naming exception, unable to find data source.");
            log.throwing("HouseKeeper", "init", nex);
        }
    }
    
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        StringBuffer h = new StringBuffer();
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println(HouseKeeper.doHead("Inventory", "Hardware by Site", "inventory", HouseKeeper.normalDelay));
        listHW(rqst.getParameter("sitename"));
        h.append("<br clear=\"all\"><p class=\"cr\"><a href=\"/servlets/index\">HOME</a>");
        if ( rqst.getSession(false) != null ) {
            h.append(" | <a href=\"/servlets/admin\">ADMIN</a>");
        }
        h.append("</p></body></html>");
        out.println(h);
        out.close();
    }
    
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        resp.setContentType("text/html");
        synchronized (this) {
            PrintWriter out = resp.getWriter();
            StringBuffer h = new StringBuffer();
            StringBuffer s = new StringBuffer();
            out.println(HouseKeeper.doHead("Inventory", "Hardware by Site", 
                "inventory", HouseKeeper.normalDelay));
            s.append(rqst.getParameter("sitebit"));
            if (s.length() > 0) {
                s.insert(0,"%");
                s.append("%");
                listHW(s.toString());
            } else {
                listSiteSummary();
            }
            out.println(h);
            h.delete(0, h.length());
            h.append("<br clear=\"all\"><p class=\"cr\"><a href=\"/servlets/index\">HOME</a>");
            if (rqst.getSession(false) != null) {
                h.append(" | <a href=\"/servlets/admin\">ADMIN</a>&nbsp;");
            }
            h.append("</p></body></html>");
            out.println(h);
            out.close();
        } // end sync
    }
    
    private void listHW(String s) {
        StringBuffer h = new StringBuffer();
        try {      
            con = DriverManager.getConnection("jdbc:mysql://ls-db:3306/itres","dbq","quizmaster");
            findHW = con.prepareStatement();
            findHW.setString(1, s);
            res = findHW.executeQuery();
            synchronized (this) {
                h.append("<h2>Hardware matching search for "+ s + "</h2>");
                h.append("<table cellspacing=\"0\" cellpadding=\"4\" /><tr>");
                h.append("<tr><th>Badge</th><th>Type</th><th>Description</th><th>Location</th>");
                h.append("<th>User</th><th>OS</th><th>IP Addr</th></tr>");
                while (res.next()) {
                    h.append("<tr><td><a href=\"/servlets/machinedetails?badgeno=");
                    h.append(res.getString("badge") + "\">" + res.getString("badge") + "</a></td>");
                    h.append("<td>" + res.getString("type") + "</td>");
                    h.append("<td>" + res.getString("descrip") + "</td>");
                    h.append("<td><a href=\"/servlets/hwbysite?sitename=");
                    h.append(res.getString("location") + "\">" + res.getString("location") + "</a></td>");
                    h.append("<td>" + res.getString("user") + "</td>");
                    h.append("<td>" + res.getString("os") + "</td>");
                    h.append("<td>" + res.getString("ipa") + "</td></tr>");
                }
                h.append("</tr><tr><td colspan=\"7\" class=\"centre\">-- END --</td></tr>");
                h.append("</table>");
                con.close();
            } // end sync
        } catch (SQLException sqlex) {
            h.append("<p>Something broke...<br>");
            h.append("SQL Exception:" + sqlex.getMessage() + "</p>");
        }
    }
    
    private void listSiteSummary() {
        StringBuffer h = new StringBuffer();
        try {
            con = DriverManager.getConnection("jdbc:mysql://ls-db:3306/itres","dbq","quizmaster");
            java.sql.Statement smnt = con.createStatement();
            SQLQuery.delete(0,SQLQuery.length());
            SQLQuery.append("SELECT location,COUNT(location) AS items FROM hw_badged ");
            SQLQuery.append("WHERE live='LIVE' AND (location IS NOT NULL AND ");
            SQLQuery.append("location<>'') GROUP BY location");
            res = smnt.executeQuery(SQLQuery.toString());
            synchronized (this) {
                h.append("<h2>Count of Badged Hardware by Site</h2>");
                h.append("<table cellspacing=\"0\" cellpadding=\"4\" /><tr>");
                h.append("<tr><th>Location</th><th>Count</th></tr>");
                while (res.next()) {
                    h.append("<tr><td><a href=\"/servlets/hwbysite?sitename=");
                    h.append(res.getString("location") + "\">" + res.getString("location") + "</a></td>");
                    h.append("<td>" + res.getString("items") + "</td>");
                }
                h.append("</tr><tr><td colspan=\"2\" class=\"centre\">-- END --</td></tr>");
                h.append("</table>");
                con.close();
            } // end sync
        } catch (SQLException sqlex) {
            h.append("<p>Something broke...<br>");
            h.append("SQL Exception:" + sqlex.getMessage() + "</p>");
        }
    }
}

