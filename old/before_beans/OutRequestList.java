/* 
 * @(#) OutRequestList.java	0.1 2004/02/05
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.sql.*;
import java.text.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Output a list of requests..
 *
 * @version		0.1 2 Feb 2004
 * @author		Steven Lilley
 */
public class OutRequestList extends HttpServlet {

  private StringBuffer h = new StringBuffer();
  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");

  public void init() {
  }

  public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {

    PrintWriter out = resp.getWriter();

    try {
      synchronized (this) {
        java.sql.Connection con = DriverManager.getConnection(DBCON,"dbq","quizmaster");
        java.sql.PreparedStatement getRequestsByBadge;
         
        boolean all = new Boolean(rqst.getAttribute("allRequests").toString()).booleanValue();
        if (all) {
          getRequestsByBadge = con.prepareStatement(
            "SELECT * FROM requests WHERE badge=? ORDER BY reqnum DESC");
        } else {
          getRequestsByBadge = con.prepareStatement(
            "SELECT * FROM requests WHERE badge=? AND closed IS NULL ORDER BY reqnum DESC");
        }        
        getRequestsByBadge.setInt(1, Integer.parseInt(rqst.getParameter("badgeno")));
        java.sql.ResultSet res = getRequestsByBadge.executeQuery();

        h.delete(0,h.length());
        h.append("<table cellspacing=\"0\" cellpadding=\"4\" id=\"smaller\" >");
        h.append("<tr><th>Ref.</th><th>Submtd by</th><th>Request (Work done)</th><th>Submitted</th>");
        h.append("<th>Target</th><th>Closed</th></tr>");

        while (res.next()) {
          h.append("<tr><td><a href=\"/servlets/requestdetails?reqnum=");
          h.append(res.getString("reqnum") + "\">" + res.getString("reqnum") + "</a></td>");
          h.append("<td>" + res.getString("contact") + "</td>");
          if ( res.getString("request") != null ) {
            int dispLen =  res.getString("request").length();
            if ( dispLen > 59 ) { dispLen = 59; }
            h.append("<td>" + res.getString("request").substring(0,dispLen) + "</td>");
          } else {
            if ( res.getString("workdone") != null ) {
              int dispLen =  res.getString("workdone").length();
              if ( dispLen > 59 ) { dispLen = 59; }
              h.append("<td>" + res.getString("workdone").substring(0,dispLen) + "</td>");
            } else {
              h.append("<td>NULL</td>");
            }
          }
          h.append("<td>");
          if ( res.getDate("submitted") == null ) {
            h.append("NULL");
          } else {
            h.append(ITEMDATE.format(res.getDate("submitted")));
          }
          h.append("</td><td>");
          if ( res.getDate("target") == null ) {
            h.append("NULL");
          } else {
            h.append(ITEMDATE.format(res.getDate("target")));
          }
          h.append("</td><td>");
          if ( res.getDate("closed") == null ) {
            h.append("NULL");
          } else {
            h.append(ITEMDATE.format(res.getDate("closed")));
          }
          h.append("</td></tr>");
        } 
        h.append("<tr><td class=\"centre\" colspan=\"6\">-- END --</td></tr>");
        
        h.append("</table>");
        out.println(h);
        res.close();
        con.close();
      } // end sync
    }
    catch (SQLException sqlex) {
      System.out.println(sqlex);
    }
  }
}



