/* 
 * @(#) LostMail.java    0.1 2006/03/05
 * 
 * Copyright (C) 2006 Calderdale MBC
 * parts Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
 */
package uk.co.channele.itres;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet to allow users to get a clue about mail lost when server failed.
 *
 * @version       0.1 5 March 2006
 * @author        Steven Lilley
 */
public class LostMail extends HttpServlet { 
    
    /** The context in which this servlet is running. */
    private ServletContext context;
    /** The SQL database connection string. */
    private String sqlDB;
    /** The user name for the SQL database. */
    private String sqlUser;
    /** The password for the SQL database. */
    private String sqlPassword;

    /**
     * The init method.
     * Gets context parameters for the database.
     */
    public void init() throws ServletException {
        context = getServletContext();
        sqlDB = context.getInitParameter("sqlDB");
        sqlUser = context.getInitParameter("sqlUser");
        sqlPassword = context.getInitParameter("sqlPassword");
    }

    /** 
     * Presents the user with a query form.
     */ 
    public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            RequestDispatcher handler;
            resp.setContentType("text/html");
            rqst.setAttribute("page","lostmail");
            PrintWriter out = resp.getWriter();
            boolean hasParameters = false;
            String paramName = new String();
            Enumeration params;
            out.println(HouseKeeper.doHead("Lost Mail", 
                "Lost Mail", "lostmail", HouseKeeper.normalDelay));
            if (!hasParameters) {
                out.println("<h3>List Lost Mail</h3>");
                out.println("<p><em>This list is not accurate.</em></p>");
                out.println("<p>This utility will (hopefully) give you a list of ");
                out.println("the emails that you have lost recently.</p>");
                out.println("<p>The log file has proved quite challenging to ");
                out.println("process.  Better results would be achievable given ");
                out.println("time, but I wanted to make information available ");
                out.println("before it expires.</p>");
                out.println(queryForm());
            }
            out.println("</div>");
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        }
    }

    /**
     * Called for many different user operations.
     * This class handles user registration and preferences changes.
     * The <code>userop</code> parameter determines which method is called.
     */ 
    public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
        throws ServletException, IOException {
        synchronized (this) {
            resp.setContentType("text/html");
            rqst.setAttribute("page", "lostmail");
            RequestDispatcher handler;
            PrintWriter out = resp.getWriter();
            if (rqst.getParameter("gwid") != null) {
                out.println(HouseKeeper.doHead("Lost Mail Results", 
                    "Lost Mail Results", "index", HouseKeeper.normalDelay));
                handler = context.getNamedDispatcher("OutLostMail");
                handler.include(rqst, resp);
            }
            handler = context.getNamedDispatcher("MenuFragment");
            handler.include(rqst, resp);
            out.close();
        } // end sync
    }
    
    /**
     * A method that returns a query form.
     * @return an HTML string containing a table and form.
     */
    private String queryForm() {
        StringBuffer h = new StringBuffer();
        h.append("<form method=\"post\" action=\"lostmail\" ");
        h.append("name=\"queryform\">\n");
        h.append("<p>Please enter your GroupWise ID.  This is most likely ");
        h.append("the same as your Netware username.  It might be an abbrieviated ");
        h.append("form of your name or a LID.<br>\n");
        // GroupWise ID
        h.append("GroupWise ID: ");
        h.append("<input type=\"text\" maxlength=\"20\" ");
        h.append("name=\"gwid\" size=\"8\"");
        h.append("></p>\n");
        // buttons 
        h.append("<p>\n");
        h.append("<input type=\"submit\" value=\"Submit\">\n");
        h.append("<input type=\"reset\" value=\"Reset\">\n");
        h.append("</p>\n");
        h.append("</form>\n");
        return h.toString();
    }
}
