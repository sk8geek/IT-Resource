/* 
 * @(#) TestBean.java    0.1 2006/09/28
 * 
 * Copyright (C) 2006 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package uk.co.channele.itres;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.jsp.JspPage;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TestBean implements Serializable { 

    InitialContext initCntx;
    Context cntx;
    DataSource db;
    
    public TestBean() {
    }
    
    public String getOutput() {
        return listBackatValues();
        
    }
    
    private String listBackatValues() {
        StringBuffer h = new StringBuffer();
        Connection dbConn;
        PreparedStatement getBackatValues;
        ResultSet results;
        try {
            initCntx = new InitialContext();
            cntx = (Context)initCntx.lookup("java:comp/env");
            System.out.println("initCntx=" + initCntx.toString());
            db = (DataSource)cntx.lookup("jdbc/itres");
            dbConn = db.getConnection();
            getBackatValues = dbConn.prepareStatement("SELECT * FROM backat");
            results = getBackatValues.executeQuery();
            while (results.next()) {
                h.append("<li>");
                h.append(results.getString("retdesc"));
                h.append("</li>");
            }
            dbConn.close();
        } catch (SQLException sqlex) {
            System.out.println(sqlex);
        } catch (NamingException nex) {
            System.out.println(nex);
        }
        return h.toString();
    }

}

