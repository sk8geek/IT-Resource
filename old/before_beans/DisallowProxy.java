/* 
 * @(#) DisallowProxy.java	0.1 2004/07/04
 * 
 * Copyright (C) 2003,2004 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import uk.co.channele.itres.DisallowProxy;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
/**
 * Part of my IT Resource suite of servlets.
 *
 * @version		0.1 4 July 2004
 * @author		Steven Lilley
 */
 public class DisallowProxy extends HttpServlet { 

	public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {
		doPost(rqst, resp);
	}

	public void doPost(HttpServletRequest rqst, HttpServletResponse resp) 
		throws ServletException, IOException {

		ServletContext context = getServletContext();
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();

		out.println(doHead());
		StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
		h.append("<div class=\"content\">");
		h.append("<h2>Proxy Detected</h2>");

		h.append("<p>You seem to accessing this site via the ");
		h.append("proxy server.</p>");
		h.append("<p>This is not a problem, but some functions are ");
		h.append("unavailable.</p>");

		h.append("<p>If you have administrator rights to machine you ");
		h.append("can fix this yourself. ");
		h.append("To do so you need to add a line like this to your ");
		h.append("hosts file:<br /><br />");
		h.append("<code>172.75.20.200 &nbsp; lsdb &nbsp;lsdb</code>");
		h.append("<br /><br />");
		h.append("and set your browser not to use the proxy for ");
		h.append("that address.</p>");

		h.append("<p>If you don't have administrator rights or you ");
		h.append("don't understand ");
		h.append("these instructions then you might want to ");
		h.append("ask a system administrator for assistance.</p></div>");

		out.println(h);
		
		RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
		rqst.setAttribute("page","disallowproxy");
		handler.include(rqst, resp);
		out.close();
	}
	
	private String doHead() {
		StringBuffer h = new StringBuffer();
		h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
		h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
		h.append("<html><!-- Copyright 2003,2004 Steven Lilley -->\n<head>");
		h.append("<title>Community Services</title>");
		h.append("<meta http-equiv=\"content-type\" ");
		h.append("content=\"text/html; charset=UTF-8\" />\n");
		h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
		h.append("<link rel=\"stylesheet\" type=\"text/css\" ");
		h.append("href=\"/lsdb.css\" />\n</head>");
		return h.toString();
	}
}


