/* 
 * @(#) RequestDetails.java	0.1 2004/02/06
 * 
 * Copyright (C) 2003-2005 Steven J Lilley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 */
package uk.co.channele.itres;

import java.util.*;
import java.text.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class RequestDetails extends HttpServlet { 

  private static final DateFormat ITEMDATE = 
    DateFormat.getDateInstance(DateFormat.MEDIUM);
  private static final String DBCON = 
    new String("jdbc:mysql://ls-db:3306/itres");
  private StringBuffer SQLQuery = new StringBuffer();
  private java.sql.PreparedStatement getRequestByNumber;
  private java.sql.Connection con;
  private java.sql.ResultSet res;

/**
 * Present a form to users to enter a request.
 *
 * @version		0.1 6 Feb 2004
 * @author		Steven Lilley
 */
public void init() throws ServletException {
    try { 
      Class.forName("com.mysql.jdbc.Driver");
    }
    catch (ClassNotFoundException cnfex) {
    }
    SQLQuery = new StringBuffer("SELECT requests.*, tasktypes.taskdesc ");
    SQLQuery.append("FROM requests LEFT JOIN tasktypes ON ");
    SQLQuery.append("(tasktypes.taskcode=requests.tasktype) WHERE requests.reqnum=?");
  }



  public void doGet(HttpServletRequest rqst, HttpServletResponse resp) 
    throws ServletException, IOException {
    ServletContext context = getServletContext();

    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.println(doHead());
    StringBuffer h = new StringBuffer("<body><h1>Community Services</h1>");
    h.append("<div class=\"content\">");
    out.println(h);

    int reqnum = 0;
    try {
      reqnum = Integer.parseInt(rqst.getParameter("reqnum"));
    }
    catch ( NumberFormatException nfex) {
    }

    if ( rqst.getSession(false) != null ) {
      HttpSession adminSession = rqst.getSession(false);
      String pw = (String)adminSession.getAttribute("password");
      out.println(adminRequest(reqnum, pw));
    } else {
      out.println(userRequest(reqnum));
    }

    h.delete(0,h.length());
    h.append("</div>\n");
    out.println(h);
    RequestDispatcher handler = context.getNamedDispatcher("MenuFragment");
    rqst.setAttribute("page","index");
    handler.include(rqst, resp);
    out.close();
  }


  private String userRequest(int reqnum){
    StringBuffer h = new StringBuffer();
    synchronized (this) {
      try {
        con = DriverManager.getConnection(DBCON,"dbq","quizmaster");
        getRequestByNumber = con.prepareStatement(SQLQuery.toString());
        getRequestByNumber.setInt(1, reqnum);
        res = getRequestByNumber.executeQuery();

        if (res.next()) {
          h.append("<h2>Request Details</h2>");
          h.append("<h3>Request number: " + reqnum + "</h3>");
          h.append("<table cellspacing=\"0\" cellpadding=\"4\" class=\"boxed\" />");
          h.append("<tr><td valign=\"top\" class=\"grey\">Badge#: </td><td valign=\"top\">");
          h.append(res.getString("badge") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Contact: </td><td valign=\"top\">");
          h.append(res.getString("contact") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Phone: </td><td valign=\"top\">");
          h.append(res.getString("phone") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Site: </td><td colspan=\"3\" valign=\"top\">");
          h.append(res.getString("site") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Task type: </td><td colspan=\"3\" valign=\"top\">");
          h.append(res.getString("taskdesc") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Request: </td><td valign=\"top\">");
          h.append(res.getString("request") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Submitted: </td>");
          h.append("<td valign=\"top\">");
          if ( res.getDate("submitted") == null ) {
            h.append("null");
          } else {
            h.append(ITEMDATE.format(res.getDate("submitted")));
          }
          h.append("</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Target: </td>");
          h.append("<td valign=\"top\">");
          if ( res.getDate("target") == null ) {
            h.append("null");
          } else {
            h.append(ITEMDATE.format(res.getDate("target")));
          }
          h.append("</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Work done: </td><td valign=\"top\">");
          h.append(res.getString("workdone") + "</td></tr>");
          h.append("<tr><td valign=\"top\" class=\"grey\">Closed:</td>");
          h.append("<td valign=\"top\">");
          if ( res.getDate("closed") == null ) {
            h.append("null");
          } else {
            h.append(ITEMDATE.format(res.getDate("closed")));
          }
          h.append("</td></tr></table>");
        } else {
          h.append("<p>No request with this number can be found in the database.</p>");
        }

        res.close();
        con.close();
      }
      catch (SQLException sqlex) {
        h.append("<p>Something broke...<br>");
        h.append("SQL Exception:" + sqlex.getMessage() + "</p></body></html>");
      }
    } // end sync
    return h.toString();
  }
  
  
  
  private String adminRequest(int reqnum, String pw) {
    StringBuffer h = new StringBuffer();
    h.append("<h2>Request Editor</h2>");
    h.append("<applet code=\"RequestEditorApplet.class\" ");
    h.append("codebase=\"/classes\" width=\"740\" ");
    h.append("height=\"540\" archive=\"mysql-connector-java-2.0.14-bin.jar\" />");
    h.append("<param name=\"reqnum\" value=\"" + reqnum + "\">");
    h.append("<param name=\"pw\" value=\"" + pw + "\">");
    h.append("</applet>");
    return h.toString();
  }



  private String doHead() {
    StringBuffer h = new StringBuffer();
    h.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n");
    h.append("\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n");
    h.append("<html><!-- Copyright 2003 Steven Lilley -->\n<head>");
    h.append("<title>Community Services</title>");
    h.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />\n");
    h.append("<meta name=\"robots\" content=\"noindex,nofollow\" />\n");
    h.append("<meta http-equiv=\"refresh\" content=\"600;URL=/servlets/index\" />\n");
    h.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/lsdb.css\" />\n</head>");
    return h.toString();
  }
}


